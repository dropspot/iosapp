/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  Spot.h
//  Dropspot3
//
//  Created by Lukas Würzburger on 04.09.13.
//  Copyright (c) 2013 Dropspot. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

#import "History.h"
#import "User.h"

#define kSpotPhotoLoadingFinished @"kSpotPhotoLoadingFinished"

#define kDSSpotDidDetermineAddressNotificationName @"kDSSpotDidDetermineAddressNotificationName"

@class Collection, History;

@interface Spot : NSManagedObject

@property (nonatomic) double latitude;
@property (nonatomic) double longitude;
@property (nonatomic) float radius;
@property (nonatomic) BOOL active;
@property (nonatomic, retain) NSString * collectionSlugs;
@property (nonatomic, retain) NSString * text;
@property (nonatomic, retain) NSString * slug;
@property (nonatomic, retain) NSSet *collections;
@property (nonatomic, retain) User * user;
@property (nonatomic, retain) NSString * photoURL;
@property (nonatomic, retain) NSString * photoFile;
@property (nonatomic, retain) NSString * thumbnailURL;
@property (nonatomic, retain) NSString * thumbnailFile;
@property (nonatomic, retain) NSDate * modified;
@property (nonatomic, retain) NSDate * created;
@property (nonatomic, retain) NSString * url;
@property (nonatomic) BOOL subscribed;
@property (nonatomic) BOOL isOwnedByMe;
@property (nonatomic, retain) NSSet * history;
@property (nonatomic, retain) NSSet * links;
@property (nonatomic, retain) NSString * linksURL;
@property (nonatomic, retain) NSString * address;
@property (nonatomic) BOOL isSynced;
@property (nonatomic) BOOL isBeingSynced;
@property (nonatomic) BOOL isDeterminingAddress;
@property (nonatomic, retain) NSDate * expiresAt;

+ (Spot *)spotWithSlug:(NSString *)slug inManagedObjectContext:(NSManagedObjectContext *)context;

+ (Spot *)spotWithLatitude:(float)latitude longitude:(float)longitude inManagedObjectContext:(NSManagedObjectContext *)context;

- (History *)addHistoryWithType:(double)type inManagedObjectContext:(NSManagedObjectContext *)context;
- (int)visits;
- (BOOL)seen;

@end

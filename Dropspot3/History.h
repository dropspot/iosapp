/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  History.h
//  Dropspot3
//
//  Created by Lukas Würzburger on 1/8/14.
//  Copyright (c) 2014 Dropspot. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

#define kSpotHistoryEventTypeVisited        0
#define kSpotHistoryEventTypeSeen           1
#define kSpotHistoryEventTypeNotification   2

@class Spot;

@interface History : NSManagedObject

@property (nonatomic, retain) NSDate * date;
@property (nonatomic) double eventType;
@property (nonatomic, retain) Spot *spot;

+ (NSArray *)historyWithLimit:(int)limit inManagedObjectContext:(NSManagedObjectContext *)context;
+ (NSString *)timeStringFromDate:(NSDate *)date;

@end

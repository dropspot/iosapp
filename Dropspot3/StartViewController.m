/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  SignInViewController.m
//  Dropspot3
//
//  Created by Lukas Würzburger on 14.10.13.
//  Copyright (c) 2013 Dropspot. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import <CommonCrypto/CommonDigest.h>

#import "StartViewController.h"

#import "DSDropspot.h"
#import "DSFacebook.h"
#import "DSGlobal.h"
#import "DSDownload.h"
#import "UILabel+AutoHeight.h"

#import "SignUpViewController.h"
#import "SignInViewController.h"

#import "GAIHeader.h"

#define kAlertViewTagPleaseEnterLogin 1
#define kAlertViewTagLoginFailed 2
#define kAlertViewTagNoConnection 3

@interface StartViewController () <UITextFieldDelegate, UIAlertViewDelegate, SignUpViewControllerDelegate, SignInViewControllerDelegate> {
    IBOutlet UIImageView * backgroundImageView;
    
    IBOutlet UIButton * registerButton;
    IBOutlet UIButton * facebookLoginButton;
    IBOutlet UIButton * nativeLoginButton;
    
    IBOutlet UIButton * skipButton;
    
    IBOutlet UIView * progressOverlayView;
    
    IBOutlet UILabel * appNameLabel;
    
    NSArray * _demoImagesArray;
    int _currentDemoImageIndex;
    NSTimer * _timer;
    
    BOOL facebookButtonPressed;
}

@end

@implementation StartViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self localizeAndLayout];
    
    _demoImagesArray = [NSArray arrayWithArray:kDSOnboardingScreens];
    
    if (_demoImagesArray.count)
        backgroundImageView.image = [UIImage imageNamed:_demoImagesArray[0]];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(facebookSessionStateChanged) name:kDSFacebookSessionChangedNotificationName object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dropspotSessionStateChanged) name:kDSDropspotSessionChangedNotificationName object:nil];
    
    [[DSGlobal sharedGlobals] analyticsView:kDSScreenNameSignInView];
    
    [self startTimer];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationFade];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    progressOverlayView.hidden = YES;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kDSFacebookSessionChangedNotificationName object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kDSDropspotSessionChangedNotificationName object:nil];
    
    if (_timer) {
        [_timer invalidate];
        _timer = nil;
    }
}

- (BOOL)shouldAutorotate {
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

#pragma mark - Timer

- (void)startTimer {
    if (_timer) {
        [_timer invalidate];
        _timer = nil;
    }
    _currentDemoImageIndex = 0;
    
    _timer = [NSTimer scheduledTimerWithTimeInterval:4.0 target:self selector:@selector(handleTimerTick:) userInfo:nil repeats:YES];
}

- (void)handleTimerTick:(NSTimer *)timer {
    _currentDemoImageIndex++;
    if (_currentDemoImageIndex >= _demoImagesArray.count) {
        _currentDemoImageIndex = 0;
    }
    
    [UIView transitionWithView:backgroundImageView duration:0.6 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
        if (_demoImagesArray.count) {
            backgroundImageView.image = [UIImage imageNamed:_demoImagesArray[_currentDemoImageIndex]];
        }
    } completion:nil];
}

#pragma mark - Helper

- (void)localizeAndLayout {
    appNameLabel.text = kDSForceMainViewTitle;
    
    [registerButton setTitle:translate(@"SIGNIN_NATIVE_LOGIN_BUTTON_REGISTER") forState:UIControlStateNormal];
    [nativeLoginButton setTitle:translate(@"SIGNIN_NATIVE_LOGIN_BUTTON_EMAIL_LOGIN") forState:UIControlStateNormal];
    [facebookLoginButton setTitle:translate(@"SIGNIN_FACEBOOK_LOGIN_BUTTON_LOGIN") forState:UIControlStateNormal];
    [skipButton setTitle:translate(@"SIGNIN_SKIP_BUTTON_LABEL") forState:UIControlStateNormal];
}

#pragma mark - SignInViewController delegate

- (void)signInViewController:(SignInViewController *)signInViewController didLoginWithUser:(DSUser *)user {
    
    [signInViewController dismissViewControllerAnimated:YES completion:^{
        if ([self.delegate respondsToSelector:@selector(startViewController:didSignInWithUser:)]) {
            [self.delegate startViewController:self didSignInWithUser:user];
        }
    }];
}

- (void)signInViewControllerDidCancel:(SignInViewController *)signInViewController {
    
    [signInViewController dismissViewControllerAnimated:YES completion:nil];
}

- (void)signInViewControllerDidPressRegisterButton:(SignInViewController *)signInViewController {
    
    [signInViewController dismissViewControllerAnimated:YES completion:^{
        [self performSegueWithIdentifier:@"showSignUp" sender:nil];
    }];
}

#pragma mark - Sign up view controller delegate

- (void)signUpViewControllerDidCancel:(SignUpViewController *)signUpViewController {
    [signUpViewController dismissViewControllerAnimated:YES completion:nil];
}

- (void)signUpViewController:(SignUpViewController *)signUpViewController didRegisterAndLoginWithUser:(DSUser *)user {
    [[DSDropspot session] profileWithBlock:^(DSAPIProfileResponse *profile, NSError *error) {
        Mixpanel * mixPanel = [Mixpanel sharedInstance];
        [mixPanel identify:profile.username];
        [mixPanel.people set:@{
                               @"Sign Up Type" : @"Native",
                               @"Username" : profile.username
                               }];
        [mixPanel track:@"$signup" properties:@{
                                                @"Sign Up Type" : @"Native",
                                                @"Username" : profile.username
                                                }];
        [mixPanel flush];
    }];
    
    [signUpViewController dismissViewControllerAnimated:YES completion:^{
        if ([self.delegate respondsToSelector:@selector(startViewController:didSignInWithUser:)]) {
            [self.delegate startViewController:self didSignInWithUser:user];
        }
    }];
}

#pragma mark - Handle session

- (void)dropspotSessionStateChanged {
    if ([DSDropspot session].state == DSDropspotSessionStateFailed || [DSDropspot session].state == DSDropspotSessionStateLoginFailed) {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"login failed" message:@"dropspot login failed" delegate:nil cancelButtonTitle:translate(@"SIGNIN_NATIVE_LOGIN_FAILED_ALERT_DISMISS_BUTTON") otherButtonTitles:nil];
        [alert show];
        progressOverlayView.hidden = YES;
    } else if ([DSDropspot session].state == DSDropspotSessionStateOpen) {
        DSUser * user = [DSUser currentUser];
        if (user) {
            if ([self.delegate respondsToSelector:@selector(startViewController:didSignInWithUser:)]) {
                [self.delegate startViewController:self didSignInWithUser:user];
            }
        }
    }
}

- (void)facebookSessionStateChanged {
    if ([DSFacebook session].sessionState == FBSessionStateClosedLoginFailed) {
        
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Login failed" message:[NSString stringWithFormat:@"If you have denied the app to access your facebook account please go to Settings > Facebook > and switch %@ on.", kAppName] delegate:nil cancelButtonTitle:translate(@"SIGNIN_NATIVE_LOGIN_FAILED_ALERT_DISMISS_BUTTON") otherButtonTitles:nil];
        [alert show];
        progressOverlayView.hidden = YES;
    } else if ([DSFacebook session].sessionState == FBSessionStateOpen || [DSFacebook session].sessionState == FBSessionStateOpenTokenExtended) {

        if (facebookButtonPressed) {
            NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
            [defaults setObject:[NSDate date] forKey:kDSFacebookLoginButtonPressedDateKey];
            [defaults synchronize];
            NSLog(@"set defaults");

            facebookButtonPressed = NO;
        }
    }
}

#pragma mark - IB Actions

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showSignUp"]) {
        
        SignUpViewController * signUpViewController = segue.destinationViewController;
        if ([signUpViewController isKindOfClass:[SignUpViewController class]]) {
            signUpViewController.delegate = self;
        }
    } else if ([segue.identifier isEqualToString:@"showSignIn"]) {

        SignInViewController * signInViewController = segue.destinationViewController;
        if ([signInViewController isKindOfClass:[SignInViewController class]]) {
            signInViewController.delegate = self;
        }
    }
}

- (IBAction)facebookButtonPressed:(id)sender {
    progressOverlayView.hidden = NO;
    
    facebookButtonPressed = YES;
    
    [[DSFacebook session] checkLoginState];
}

- (IBAction)skipButtonPressed:(id)sender {
    if ([DSUser currentUser] && [DSUser currentUser].state == DSUserStateTemporary) {
        if ([self.delegate respondsToSelector:@selector(startViewControllerDidCancel:)]) {
            [self.delegate startViewControllerDidCancel:self];
        }
    } else {
        progressOverlayView.hidden = NO;
        
        NSString * randomString = [[NSUUID UUID] UUIDString];
        NSString * shortString = [randomString stringByReplacingOccurrencesOfString:@"-" withString:@""];
        shortString = [shortString substringWithRange:NSMakeRange(1, [shortString length]-2)];
        
        NSString * username = [NSString stringWithFormat:@"%@", shortString];
        NSString * email = [NSString stringWithFormat:@"%@@drpspt-bild.com", username];
        NSString * password = md5([NSString stringWithFormat:@"pass%@word", username]);
        
        [[DSDropspot session] registerNewUserWithEmail:email username:username password:password block:^(NSDictionary *result, NSError *error) {
            if (error) {
                NSLog(@"Handle register error: %@ result %@", error.localizedDescription, result);
                
                progressOverlayView.hidden = YES;
            } else {
                // register successfull
                
                [[DSDropspot session] openSessionWithUsername:username password:password block:^(DSUser *user, NSError *error) {
                    if (error) {
                        NSLog(@"Handle login error %@", error.localizedDescription);
                        
                        progressOverlayView.hidden = YES;
                    } else {
                        
                        [[DSDropspot session] profileWithBlock:^(DSAPIProfileResponse *profile, NSError *error) {
                            Mixpanel * mixPanel = [Mixpanel sharedInstance];
                            [mixPanel identify:profile.username];
                            [mixPanel.people set:@{
                                                   @"Sign Up Type" : @"Skip",
                                                   @"Username" : profile.username
                                                   }];
                            [mixPanel track:@"$signup" properties:@{
                                                                    @"Sign Up Type" : @"Skip",
                                                                    @"Username" : profile.username
                                                                    }];
                            [mixPanel flush];
                        }];
                        
                        [DSUser currentUser].state = DSUserStateTemporary;
                        if ([self.delegate respondsToSelector:@selector(startViewController:didSignInWithUser:)]) {
                            [self.delegate startViewController:self didSignInWithUser:user];
                        }
                    }
                }];
            }
        }];
    }
}

NSString * md5( NSString *str )
{
    const char *cStr = [str UTF8String];
    unsigned char result[16];
    CC_MD5( cStr, strlen(cStr), result );
    return [NSString
            stringWithFormat: @"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",
            result[0], result[1],
            result[2], result[3],
            result[4], result[5],
            result[6], result[7],
            result[8], result[9],
            result[10], result[11],
            result[12], result[13],
            result[14], result[15]
            ];
}

@end

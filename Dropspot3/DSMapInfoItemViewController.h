/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  DSMapInfoItemViewController.h
//  Dropspot3
//
//  Created by Lukas Würzburger on 07.10.13.
//  Copyright (c) 2013 Dropspot. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Spot.h"

@class DSMapInfoItemViewController;

@protocol DSMapInfoItemViewControllerDelegate <NSObject>

- (void)mapInfoItemDidPressSpot:(DSMapInfoItemViewController *)mapInfoItemViewController;
- (void)mapInfoItemDidPressCreateSpot:(Spot *)spot;
- (void)mapInfoItemDidPressCancelSpot:(Spot *)spot;

@end

@interface DSMapInfoItemViewController : UIViewController

@property (nonatomic) NSUInteger index;
@property (nonatomic, weak) Spot * spot;
@property (nonatomic) float viewHeight;

@property (nonatomic, assign) id <DSMapInfoItemViewControllerDelegate> delegate;

@end

/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  DSLoadingOverlay.m
//  Dropspot3
//
//  Created by Lukas Würzburger on 4/3/14.
//  Copyright (c) 2014 Dropspot GmbH. All rights reserved.
//

#import "DSLoadingOverlay.h"

#define kLoadingViewBoxSize CGSizeMake(180, 80)

@interface DSLoadingOverlay () {
    UIView * _boxView;
    UIActivityIndicatorView * _wheelView;
    UILabel * _label;
}

@end

@implementation DSLoadingOverlay

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
    }
    return self;
}

- (instancetype)initWithLoadingString:(NSString *)loadingString userInteractionEnabled:(BOOL)userInteractionEnabled {
    self = [super initWithFrame:CGRectMake(0, 0, 320, 480)];
    if (self) {
        [self initialize];

        self.userInteractionEnabled = userInteractionEnabled;
        self.loadingString = loadingString;
    }
    return self;
}

- (instancetype)init {
    self = [super initWithFrame:CGRectMake(0, 0, 320, 480)];
    if (self) {
        [self initialize];
    }
    return self;
}

- (void)initialize {
    self.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.95];
    
    _boxView = [[UIView alloc] initWithFrame:CGRectMake(self.frame.size.width / 2 - kLoadingViewBoxSize.width / 2,
                                                               self.frame.size.height / 2 - kLoadingViewBoxSize.height / 2,
                                                               kLoadingViewBoxSize.width,
                                                               kLoadingViewBoxSize.height)];
    _boxView.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.8];
    _boxView.layer.cornerRadius = 6;
    [self addSubview:_boxView];
    
    _wheelView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    _wheelView.center = CGPointMake(_boxView.frame.size.width / 2, _boxView.frame.size.height / 2 - 15);
    _wheelView.hidesWhenStopped = YES;
    [_boxView addSubview:_wheelView];
    
    _label = [[UILabel alloc] initWithFrame:CGRectMake(10, _boxView.frame.size.height / 2, _boxView.frame.size.width - 20, _boxView.frame.size.height / 2 - 10)];
    _label.backgroundColor = [UIColor clearColor];
    _label.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:17.0];
    _label.textAlignment = NSTextAlignmentCenter;
    _label.textColor = [UIColor whiteColor];
    _label.text = @"Loading...";
    _label.numberOfLines = 0;
    [_boxView addSubview:_label];
}

- (void)setLoadingString:(NSString *)loadingString {
    _loadingString = loadingString;
    
    _label.text = _loadingString;
}

- (void)showInView:(UIView *)view {
    [_wheelView startAnimating];
    
    if (self.superview != view) {
        self.frame = view.bounds;
        self.alpha = 0.0;
        [view addSubview:self];
        
        [UIView animateWithDuration:0.2 animations:^{
            self.alpha = 1.0;
        } completion:nil];
    }
}

- (void)hide {
    if (self.superview) {
        [_wheelView stopAnimating];
        
        [UIView animateWithDuration:0.2 animations:^{
            self.alpha = 0.0;
        } completion:^(BOOL finished) {
            [self removeFromSuperview];
        }];
    }
}

@end

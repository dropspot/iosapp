/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  DSAPISpotLinkResponse.m
//  Dropspot3
//
//  Created by Lukas Würzburger on 5/15/14.
//  Copyright (c) 2014 Dropspot GmbH. All rights reserved.
//

#import "DSAPISpotLinkResponse.h"

@implementation DSAPISpotLinkResponse

@end

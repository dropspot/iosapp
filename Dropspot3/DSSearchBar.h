/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  DSSearchBar.h
//  Dropspot3
//
//  Created by Lukas Würzburger on 1/30/14.
//  Copyright (c) 2014 Dropspot. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DSSearchBar : UIView

/**
 * The object that acts as the delgate of the receiving search bar.
 */
@property (nonatomic, assign) id <UISearchBarDelegate> delegate;

/**
 * The button which appears next to the search bar on the left.
 */
@property (nonatomic, strong) UIButton * leftButtonItem;

/**
 * The search string which appears in the input field.
 */
@property (nonatomic, strong) NSString * inputText;

/**
 * Information to be displayed underneath the search bar.
 */
@property (nonatomic, strong) NSString * infoText;

@end

/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  Collection.m
//  Dropspot3
//
//  Created by Lukas Würzburger on 04.09.13.
//  Copyright (c) 2013 Dropspot. All rights reserved.
//

#import "Collection.h"

@implementation Collection

@dynamic active;
@dynamic photoURL;
@dynamic photoFile;
@dynamic savedDate;
@dynamic slug;
@dynamic text;
@dynamic title;
@dynamic thumbnailFile;
@dynamic thumbnailURL;
@dynamic spots;
@dynamic spotsURL;
@dynamic modified;
@dynamic modifiedSpots;
@dynamic created;
@dynamic url;
@dynamic subscribed;
@dynamic isOwnedByMe;
@dynamic isSynced;
@dynamic isBeingSynced;
@dynamic user;

- (NSString *)description {
    return [NSString stringWithFormat:@"<Collection: %@>", self.slug];
}

+ (Collection *)collectionWithSlug:(NSString *)slug inManagedObjectContext:(NSManagedObjectContext *)context {
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Collection"];
    request.predicate = [NSPredicate predicateWithFormat:@"slug = %@", slug];
    
    NSError *error = nil;
    NSArray *matches = [context executeFetchRequest:request error:&error];
    
    if (matches && [matches count] == 1) {
        return matches[0];
    }
    
    return nil;
}

+ (Collection *)createEmptyCollectionInManagedObjectContext:(NSManagedObjectContext *)context {
    Collection * collection = [NSEntityDescription insertNewObjectForEntityForName:@"Collection"
                                                            inManagedObjectContext:context];
    
    collection.slug = [NSString stringWithFormat:@"createCollection{%.0f}", [NSDate date].timeIntervalSince1970];
    collection.title = @"";
    collection.isSynced = NO;
    collection.active = YES;
    collection.subscribed = YES;
    collection.isOwnedByMe = YES;
    
    return collection;
}

- (NSArray *)spotsSortedByDistanceFromLocation:(CLLocation *)location inManagedObjectContext:(NSManagedObjectContext *)context {
    NSFetchRequest * request = [[NSFetchRequest alloc] initWithEntityName:@"Spot"];
    request.predicate = [NSPredicate predicateWithFormat:@"ANY collections.slug = %@", self.slug];
    request.returnsObjectsAsFaults = NO;

    NSArray * spots = [context executeFetchRequest:request error:nil];
    NSMutableArray * array = [NSMutableArray array];
    for (Spot * spot in spots) {
        CLLocation * l = [[CLLocation alloc] initWithLatitude:spot.latitude longitude:spot.longitude];
        [array addObject:@{@"spot":spot,@"distance":[NSNumber numberWithFloat:[l distanceFromLocation:location]]}];
    }
    NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"distance" ascending:YES];
    NSMutableArray * sortedArray = [NSMutableArray array];
    for (NSDictionary * dict in [array sortedArrayUsingDescriptors:@[descriptor]]) {
        [sortedArray addObject:dict[@"spot"]];
    }
    return sortedArray;
}

@end

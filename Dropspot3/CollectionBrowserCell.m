/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  CollectionBrowserCell.m
//  Dropspot3
//
//  Created by Lukas Würzburger on 11/3/13.
//  Copyright (c) 2013 Dropspot. All rights reserved.
//

#import "CollectionBrowserCell.h"

#import "CollectionViewController.h"

@interface CollectionBrowserCell () <UIPageViewControllerDelegate, UIPageViewControllerDataSource> {
    IBOutlet UIView * containerView;
    IBOutlet UIPageControl * pageControl;
    
    UIPageViewController * pageViewController;
}

@end

@implementation CollectionBrowserCell

- (void)setCollections:(NSArray *)collections {
    if (_collections != collections) {
        _collections = collections;
    }
    
    [self setUp];
}

- (void)setUp {
    if (!pageViewController) {
        pageViewController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll
                                                             navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal
                                                                           options:@{UIPageViewControllerOptionInterPageSpacingKey:@0.0}];
        pageViewController.delegate = self;
        pageViewController.dataSource = self;
        
        pageViewController.view.layer.masksToBounds = YES;
        pageViewController.view.frame = CGRectMake(0, 0, containerView.frame.size.width, containerView.frame.size.height);
        [containerView addSubview:pageViewController.view];
    }
    
    if (self.collections && self.collections.count > 0) {
        [pageViewController setViewControllers:@[[self collectionViewControllerForIndex:0]]
                                     direction:UIPageViewControllerNavigationDirectionForward
                                      animated:NO
                                    completion:nil];
        pageControl.numberOfPages = self.collections.count;
        pageControl.currentPage = 0;
    } else {
        pageControl.numberOfPages = 0;
    }
}

#pragma mark - UIPageViewController

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(CollectionViewController *)viewController {
    
    return [self collectionViewControllerForIndex:viewController.index + 1];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(CollectionViewController *)viewController {
    
    return [self collectionViewControllerForIndex:viewController.index - 1];
}

- (CollectionViewController *)collectionViewControllerForIndex:(NSInteger)index {
    if (index < 0 || index >= self.collections.count) {
        return nil;
    }
    
    CollectionViewController * controller = [self.storyboard instantiateViewControllerWithIdentifier:@"feedCollection"];
    controller.collection = self.collections[index];
    controller.index = index;
    return controller;
}

- (void)pageViewController:(UIPageViewController *)thePageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray *)previousViewControllers transitionCompleted:(BOOL)completed {
    if (!completed) {
        return;
    }
    
    pageControl.currentPage = [(CollectionViewController *)thePageViewController.viewControllers.lastObject index];
}

#pragma mark - IBAction

- (IBAction)pageControlPressed:(UIPageControl *)sender {
    int currentPage = [(CollectionViewController *)pageViewController.viewControllers.lastObject index];
    
    UIPageViewControllerNavigationDirection direction;
    
    if (currentPage < sender.currentPage) {
        direction = UIPageViewControllerNavigationDirectionForward;
    } else {
        direction = UIPageViewControllerNavigationDirectionReverse;
    }
    
    [pageViewController setViewControllers:@[[self collectionViewControllerForIndex:sender.currentPage]] direction:direction animated:YES completion:nil];
}

@end

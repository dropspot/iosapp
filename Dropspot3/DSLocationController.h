/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  DSLocationManager.h
//  Dropspot3
//
//  Created by Lukas Würzburger on 18.09.13.
//  Copyright (c) 2013 Dropspot. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>

#import "Spot.h"

#define kDSLocationHitRadius 300

#define kDSLocationControllerAuthorizationStatusDidChangeNotificationName @"kDSLocationControllerAuthorizationStatusDidChangeNotificationName"

@class DSLocationController;

@protocol DSLocationControllerDelegate <NSObject>

- (void)locationController:(DSLocationController *)controller hitSpot:(Spot *)spot;
- (void)locationController:(DSLocationController *)controller didUpdateLocation:(CLLocation *)location;
- (void)locationController:(DSLocationController *)controller didReceiveSignificantLocationChange:(CLLocation *)location;

@end

@interface DSLocationController : NSObject <CLLocationManagerDelegate>

@property (nonatomic, retain) CLLocationManager * locationManager;
@property (nonatomic, assign) id <DSLocationControllerDelegate> delegate;

+ (DSLocationController *)controller;

+ (NSString *)distanceStringFromDistance:(float)distance;
+ (void)distancePropertiesFromDistance:(float)distance properties:(void (^)(double value, NSString * measurement))properties;
+ (NSString *)lowestMeasurement;

- (void)handleApplicationState:(UIApplicationState)state;
- (void)refreshTrackedSpots;
- (NSArray *)fetchSpotsAroundLocation:(CLLocation *)location inManagedObjectContext:(NSManagedObjectContext *)context;

- (void)stopTrackingSpot:(Spot *)spot;

@end

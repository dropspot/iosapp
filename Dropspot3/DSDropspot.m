/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  DSDropspot.m
//  Dropspot3
//
//  Created by Lukas Würzburger on 11.09.13.
//  Copyright (c) 2013 Dropspot. All rights reserved.
//

#import <LRResty/LRResty.h>
#import <RestKit/RestKit.h>

#import "DSDropspot.h"
#import "DSFacebook.h"

#import "DataHelper.h"
#import "DSGlobal.h"
#import "DSGlobalData.h"

@interface DSDropspot () <NSURLConnectionDelegate> {
    NSMutableDictionary * loadingStatus;
}

@end

@implementation DSDropspot

+ (DSDropspot *)session {
    static DSDropspot * instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[DSDropspot alloc] init];
    });
    return instance;
}

- (id)init {
    self = [super init];
    self.state = DSDropspotSessionStateClose;
    return self;
}

- (void)setState:(DSDropspotSessionState)state error:(NSError *)error {
    self.state = state;
    [self.delegate dropspotSessionChangedState:self state:state error:error];
}

- (NSDictionary *)user {
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    return [defaults objectForKey:kDSDropspotSessionDomainKeyName];
}

#pragma mark - Session

- (void)checkSession {
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    if ([defaults objectForKey:kDSDropspotSessionDomainKeyName]) {
        [self setState:DSDropspotSessionStateOpen error:nil];
    } else {
        NSError * error = [[NSError alloc] initWithDomain:kDSDropspotAPIErrorDomainName
                                                     code:kDSDropspotAPIErrorCodeNoSessionToken
                                                 userInfo:@{ NSLocalizedDescriptionKey : kDSDropspotAPIErrorDescriptionNoSessionToken }];
        [self setState:DSDropspotSessionStateClose error:error];
    }
}

- (void)openSessionWithUsername:(NSString *)username password:(NSString *)password block:(DSUserResultBlock)block {
    NSString * urlString = [self baseURLWithPath:@"token/.json"];
    
    NSDictionary * payload = @{ @"username" : username,
                                @"password" : password };
    
    LRRestyClient * client = [LRResty client];
    [client attachRequestModifier:^(LRRestyRequest *request) {
        [request addHeader:@"Authorization" value:NULL];
    }];
    [client setHandlesCookiesAutomatically:NO];
    [client post:urlString payload:payload headers:@{ @"Content-Type" : @"application/x-www-form-urlencoded" } withBlock:^(LRRestyResponse *response) {
        NSDictionary * dict = [DataHelper dictionaryFromJSONData:response.responseData];
        
        if (response.status == 200) {
            if (dict && dict[@"token"]) {
                
                DSUser * user = [DSUser currentUser];
                if (user && user.state == DSUserStateTemporary) {
                    user.state = DSUserStateTransitionToReal;
                    user.token = dict[@"token"];
                } else {
                    user = [DSUser newUserWithToken:dict[@"token"]];
                }
                user.loginType = DSUserLoginTypeNative;
                
                [DSGlobal sharedGlobals].lastSyncDate = nil;
                
                [self setState:DSDropspotSessionStateOpen error:nil];
                if (block) {
                    block(user, nil);
                }
            } else {
                NSError * error = [[NSError alloc] initWithDomain:kDSDropspotAPIErrorDomainName
                                                             code:kDSDropspotAPIErrorCodeNoValidResult
                                                         userInfo:@{ NSLocalizedDescriptionKey : kDSDropspotAPIErrorDescriptionNoValidResult, @"URL":urlString, @"Respone code:":[NSString stringWithFormat:@"%i", response.status] }];
                [self setState:DSDropspotSessionStateFailed error:error];
                if (block) {
                    block(nil, error);
                }
            }
        } else if (response.status == 400 || response.status == 401) {
            NSError * error;
            if (dict[@"non_field_errors"] && [dict[@"non_field_errors"] isKindOfClass:[NSArray class]] && [(NSArray *)dict[@"non_field_errors"] count] > 0) {
                error = [[NSError alloc] initWithDomain:kDSDropspotAPIErrorDomainName
                                                   code:kDSDropspotAPIErrorCodeNotAuthorized
                                               userInfo:@{ NSLocalizedDescriptionKey : dict[@"non_field_errors"][0] }];
            } else {
                error = [[NSError alloc] initWithDomain:kDSDropspotAPIErrorDomainName
                                                         code:kDSDropspotAPIErrorCodeNotAuthorized
                                                     userInfo:@{ NSLocalizedDescriptionKey : kDSDropspotAPIErrorDescriptionNotAuthorized }];
            }
            [self setState:DSDropspotSessionStateLoginFailed error:error];
            if (block) {
                block(nil, error);
            }
        } else {
            NSError * error = [[NSError alloc] initWithDomain:kDSDropspotAPIErrorDomainName
                                                         code:kDSDropspotAPIErrorCodeWrongResponse
                                                     userInfo:@{ NSLocalizedDescriptionKey : kDSDropspotAPIErrorDescriptionWrongResponse, @"URL":urlString, @"Respone code:":[NSString stringWithFormat:@"%i", response.status] }];
            [self setState:DSDropspotSessionStateFailed error:error];
            if (block) {
                block(nil, error);
            }
        }
    }];
}

- (void)openSessionWithFacebookToken:(NSString *)token block:(DSUserResultBlock)block {
    NSString * urlString = [self baseURLWithPath:@"token/.json"];
    NSString * payload = [NSString stringWithFormat:@"access_token=%@", token];
    
    NSString * stackKey = [NSString stringWithFormat:@"%@-%0.0f", urlString, [[NSDate dateWithTimeIntervalSinceNow:0] timeIntervalSince1970]];
    [[DSGlobal sharedGlobals] addLoadingStackObject:stackKey];

    LRRestyClient * client = [LRResty client];
    [client attachRequestModifier:^(LRRestyRequest *request) {
        [request addHeader:@"Authorization" value:NULL];
    }];
    [client setHandlesCookiesAutomatically:NO];
    [client post:urlString payload:payload headers:@{ @"Content-Type" : @"application/x-www-form-urlencoded" } withBlock:^(LRRestyResponse *response) {
        [[DSGlobal sharedGlobals] removeFromLoadingStack:stackKey];
        
        if (response.status == 200) {
            NSDictionary * dict = [DataHelper dictionaryFromJSONData:response.responseData];
            if (dict && dict[@"token"]) {
                
                DSUser * user = [DSUser currentUser];
                
                if (user && user.state == DSUserStateTemporary) {
                    user.state = DSUserStateTransitionToReal;
                    user.token = dict[@"token"];
                } else {
                    user = [DSUser newUserWithToken:dict[@"token"]];
                }

                user.loginType = DSUserLoginTypeFacebook;
                
                [DSGlobal sharedGlobals].lastSyncDate = nil;

                [self setState:DSDropspotSessionStateOpen error:nil];
                if (block) {
                    block(user, nil);
                }
            } else {
                NSError * error = [[NSError alloc] initWithDomain:kDSDropspotAPIErrorDomainName
                                                             code:kDSDropspotAPIErrorCodeNoValidResult
                                                         userInfo:@{ NSLocalizedDescriptionKey : kDSDropspotAPIErrorDescriptionNoValidResult, @"URL":urlString, @"payload":payload, @"Respone code:":[NSString stringWithFormat:@"%i", response.status] }];
                [self setState:DSDropspotSessionStateFailed error:error];
                if (block) {
                    block(nil, error);
                }
            }
        } else if (response.status == 400 || response.status == 401) {
            [[DSFacebook session] closeSession];
            NSError * error = [[NSError alloc] initWithDomain:kDSDropspotAPIErrorDomainName
                                                         code:kDSDropspotAPIErrorCodeNotAuthorized
                                                     userInfo:@{ NSLocalizedDescriptionKey : kDSDropspotAPIErrorDescriptionNotAuthorized }];
            [self setState:DSDropspotSessionStateLoginFailed error:error];
            if (block) {
                block(nil, error);
            }
        } else {
            NSError * error = [[NSError alloc] initWithDomain:kDSDropspotAPIErrorDomainName
                                                         code:kDSDropspotAPIErrorCodeWrongResponse
                                                     userInfo:@{ NSLocalizedDescriptionKey : kDSDropspotAPIErrorDescriptionWrongResponse, @"URL":urlString, @"Respone code:":[NSString stringWithFormat:@"%i", response.status] }];
            [self setState:DSDropspotSessionStateFailed error:error];
            if (block) {
                block(nil, error);
            }
        }
    }];
}

- (void)closeSession {
    [DSUser killCurrentUser];
    
    [[LRResty client] cancelAllRequests];
}

#pragma mark - Sign up

- (void)registerNewUserWithEmail:(NSString *)email username:(NSString *)username password:(NSString *)password block:(DSDictionaryResultBlock)block {
    NSString * urlString = [self baseURLWithPath:@"users/.json"];
    
    if (!email) {
        email = @"";
    }
    
    NSDictionary * payload = @{ DRPSPT_USER_CREATE_EMAIL : email,
                                DRPSPT_USER_CREATE_USERNAME : username,
                                DRPSPT_USER_CREATE_PASSWORD : password };
    
    NSLog(@"url %@ %@", urlString, payload);
    
    NSString * stackKey = [NSString stringWithFormat:@"%@-%0.0f", urlString, [[NSDate dateWithTimeIntervalSinceNow:0] timeIntervalSince1970]];
    [[DSGlobal sharedGlobals] addLoadingStackObject:stackKey];
    
    LRRestyClient * client = [LRResty client];
    [client attachRequestModifier:^(LRRestyRequest *request) {
        [request addHeader:@"Authorization" value:NULL];
    }];
    [client setHandlesCookiesAutomatically:NO];
    [client post:urlString payload:payload headers:@{ @"Content-Type" : @"application/x-www-form-urlencoded" } withBlock:^(LRRestyResponse *response) {
        [[DSGlobal sharedGlobals] removeFromLoadingStack:stackKey];
        
        NSDictionary * dict = [DataHelper dictionaryFromJSONData:response.responseData];
        
        if (response.status == 201) {
            block(dict, nil);
        } else if (response.status == 400) {
            NSError * error = [[NSError alloc] initWithDomain:kDSDropspotAPIErrorDomainName
                                                         code:kDSDropspotAPIErrorCodeRegistrationFailed
                                                     userInfo:@{ NSLocalizedDescriptionKey : kDSDropspotAPIErrorDescriptionRegistrationFailed, @"URL" : urlString }];
            block(dict, error);
        } else if (response.status == 401) {
            NSError * error = [[NSError alloc] initWithDomain:kDSDropspotAPIErrorDomainName
                                                         code:kDSDropspotAPIErrorCodeNotAuthorized
                                                     userInfo:@{ NSLocalizedDescriptionKey : kDSDropspotAPIErrorDescriptionNotAuthorized, @"URL" : urlString }];
            block(nil, error);
        } else if (response.status == 0) {
            NSError * error = [[NSError alloc] initWithDomain:kDSDropspotAPIErrorDomainName
                                                         code:kDSDropspotAPIErrorCodeNoResponse
                                                     userInfo:@{ NSLocalizedDescriptionKey : kDSDropspotAPIErrorDescriptionNoResponse, @"URL" : urlString }];
            block(nil, error);
        } else {
            NSLog(@"response code %i %@", response.status, response.asString);
            
            NSError * error = [[NSError alloc] initWithDomain:kDSDropspotAPIErrorDomainName
                                                         code:kDSDropspotAPIErrorCodeWrongResponse
                                                     userInfo:@{ NSLocalizedDescriptionKey : kDSDropspotAPIErrorDescriptionWrongResponse, @"URL" : urlString, @"Respone code:" : [NSString stringWithFormat:@"%i", response.status] }];
            block(dict, error);
        }
    }];
}

#pragma mark - Data

- (void)profileWithBlock:(DSProfileResultBlock)block {
    [self requestWithURLString:[self baseURLWithPath:@"profile/.json"] block:^(NSDictionary *result, NSError *error) {
        if (error) {
            if (block) {
                block(nil, error);
            }
        } else {
            DSAPIProfileResponse * response = [self profileResponseFromDictionary:result];
            
            if (block) {
                block(response, nil);
            }
        }
    }];
}

- (void)collectionsWithURL:(NSString *)urlString paginatedWithBlock:(DSCollectionResultBlock)block completion:(void (^)(NSError * error))completion {
    __block void (^PageBlock)(NSString *);
    
    PageBlock = ^(NSString * urlString) {
        [self requestWithURLString:urlString block:^(NSDictionary *result, NSError * error) {
            if (error) {
                if (completion) {
                    completion(error);
                }
            } else {
                dispatch_async(dispatch_queue_create("iterateCollections", NULL), ^{
                    for (NSDictionary * dictionary in result[@"results"]) {
                        
                        DSAPICollectionResponse * response = [self collectionResponseFromDictionary:dictionary];
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            if (block) {
                                block(response);
                            }
                        });
                    }
                    
                    if (result[@"next"] != [NSNull null]) {
                        dispatch_async(dispatch_get_main_queue(), ^{
#pragma ARC diagnostic ignore "-Wwarning-flag"
                            PageBlock(result[@"next"]);
                        });
                    } else {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            if (completion) {
                                completion(nil);
                            }
                        });
                    }
                });
            }
        }];
    };
    
    PageBlock([urlString stringByAppendingString:@"?page_size=100"]);
}

- (void)allCollectionsPaginatedWithBlock:(DSCollectionResultBlock)block completion:(void (^)(NSError * error))completion {
    __block void (^PageBlock)(NSString *);
    
    PageBlock = ^(NSString * urlString) {
        [self requestWithURLString:urlString block:^(NSDictionary *result, NSError * error) {
            if (error) {
                if (completion) {
                    completion(error);
                }
            } else {
                for (NSDictionary * dictionary in result[@"results"]) {
                    
                    DSAPICollectionResponse * response = [self collectionResponseFromDictionary:dictionary];
                    
                    if (block) {
                        block(response);
                    }
                }
                
                if (result[@"next"] != [NSNull null]) {
                    PageBlock(result[@"next"]);
                    NSLog(@"Another collection page...");
                } else {
                    if (completion) {
                        completion(nil);
                    }
                }
            }
        }];
    };
    
    PageBlock([self baseURLWithPath:@"collections/.json"]);
}

- (void)subscriptionsWithURL:(NSString *)urlString paginatedWithBlock:(DSCollectionResultBlock)block completion:(void (^)(NSError * error))completion {
    __block void (^PageBlock)(NSString *);
    
    PageBlock = ^(NSString * urlString) {
        [self requestWithURLString:urlString block:^(NSDictionary *result, NSError * error) {
            if (error) {
                if (completion) {
                    completion(error);
                }
            } else {
                dispatch_async(dispatch_queue_create("iterateSubscriptions", NULL), ^{
                    for (NSDictionary * subscriptions in result[@"results"]) {
                        NSDictionary * dictionary = subscriptions[@"content_object"];
                        
                        if (dictionary != [NSNull null]) {
                            
                            DSAPICollectionResponse * response = [self collectionResponseFromDictionary:dictionary];

                            dispatch_async(dispatch_get_main_queue(), ^{
                                if (block) {
                                    block(response);
                                }
                            });
                        }
                    }
                    
                    if (result[@"next"] != [NSNull null]) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            PageBlock(result[@"next"]);
                        });
                    } else {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            if (completion) {
                                completion(nil);
                            }
                        });
                    }
                });
            }
        }];
    };
    
    PageBlock([urlString stringByAppendingString:@"?page_size=100"]);
}

- (void)collectionWithURL:(NSString *)urlString success:(DSCollectionResultBlock)block completion:(void (^)(NSError * error))completion {
    [self requestWithURLString:urlString block:^(NSDictionary *result, NSError * error) {
        if (error) {
            if (completion) {
                completion(error);
            }
        } else {
            
            DSAPICollectionResponse * response = [self collectionResponseFromDictionary:result];
            
            if (block) {
                block(response);
            }
            
            if (completion) {
                completion(nil);
            }
        }
    }];
}

- (void)spotsWithURL:(NSString *)urlString paginatedWithBlock:(DSSpotResultBlock)block completion:(void (^)(NSError * error))completion {
    __block void (^PageBlock)(NSString *);
    
    PageBlock = ^(NSString * urlString) {
        [self requestWithURLString:urlString block:^(NSDictionary *result, NSError * error) {
            if (error) {
                if (completion) {
                    completion(error);
                }
            } else {
                dispatch_async(dispatch_queue_create("iterateSpots", NULL), ^{
                    for (NSDictionary * dictionary in result[@"results"]) {
                        
                        DSAPISpotResponse * response = [self spotResponseFromDictionary:dictionary];
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            if (block) {
                                block(response);
                            }
                        });
                    }
                    
                    if (result[@"next"] != [NSNull null]) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            PageBlock(result[@"next"]);
                        });
                    } else {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            if (completion) {
                                completion(nil);
                            }
                        });
                    }
                });
            }
        }];
    };
    
    PageBlock([urlString stringByAppendingString:@"?page_size=100"]);
}

- (void)linksWithURL:(NSString *)urlString paginatedWithBlock:(DSLinksResultBlock)block completion:(void (^)(NSError * error))completion {
    __block void (^PageBlock)(NSString *);
    
    PageBlock = ^(NSString * urlString) {
        [self requestWithURLString:urlString block:^(NSDictionary *result, NSError * error) {
            if (error) {
                if (completion) {
                    completion(error);
                }
            } else {
                dispatch_async(dispatch_queue_create("iterateSpots", NULL), ^{
                    for (NSDictionary * dictionary in result[@"results"]) {
                        if (dictionary) {
                            dispatch_async(dispatch_get_main_queue(), ^{

                                DSAPISpotLinkResponse * response = [self spotLinkResponseFromDictionary:dictionary];
                                
                                if (block) {
                                    block(response);
                                }
                            });
                        }
                    }
                    
                    if (result[@"next"] != [NSNull null]) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            PageBlock(result[@"next"]);
                        });
                    } else {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            if (completion) {
                                completion(nil);
                            }
                        });
                    }
                });
            }
        }];
    };
    
    PageBlock([urlString stringByAppendingString:@"?page_size=100"]);
}

- (void)collectionWithSlug:(NSString *)slug block:(DSDictionaryResultBlock)block {
    [self requestWithURLString:[NSString stringWithFormat:@"%@%@/.json", [self baseURLWithPath:@"collections/"], slug] block:block];
}

- (void)collectionsBySearch:(NSString *)search block:(DSDictionaryResultBlock)block {
    [self requestWithURLString:[NSString stringWithFormat:@"%@.json?q=%@&type=collection", [self baseURLWithPath:@"search/"], [search stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] block:block];
}

- (void)requestWithURLString:(NSString *)urlString block:(DSDictionaryResultBlock)block {
    //NSLog(@"requestWithURLString %@", urlString);
    
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    if ([defaults objectForKey:kDSDropspotSessionDomainKeyName] && [[defaults objectForKey:kDSDropspotSessionDomainKeyName] valueForKey:kDSDropspotSessionTokenKeyName]) {
        
        NSString * stackKey = [NSString stringWithFormat:@"%@-%0.0f", urlString, [[NSDate dateWithTimeIntervalSinceNow:0] timeIntervalSince1970]];
        [[DSGlobal sharedGlobals] addLoadingStackObject:stackKey];

        LRRestyClient * client = [LRResty client];
        [client setHandlesCookiesAutomatically:NO];
        [client attachRequestModifier:^(LRRestyRequest *request) {
            NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
            [request addHeader:@"Authorization" value:[NSString stringWithFormat:@"Token %@", [[defaults objectForKey:kDSDropspotSessionDomainKeyName] valueForKey:kDSDropspotSessionTokenKeyName]]];
        }];
        
        [client get:urlString withBlock:^(LRRestyResponse *response) {
            [[DSGlobal sharedGlobals] removeFromLoadingStack:stackKey];
            
            if (response.status == 200) {
                NSDictionary * dict = [DataHelper dictionaryFromJSONData:response.responseData];
                if (dict) {
                    
                    if (block) {
                        block(dict, nil);
                    }
                } else {
                    
                    if (block) {
                        NSError * error = [[NSError alloc] initWithDomain:kDSDropspotAPIErrorDomainName
                                                                     code:kDSDropspotAPIErrorCodeNoValidResult
                                                                 userInfo:@{ NSLocalizedDescriptionKey : kDSDropspotAPIErrorDescriptionNoValidResult, @"URL" : urlString }];
                        block(nil, error);
                    }
                }
            } else if (response.status == 400 || response.status == 401) {
                NSLog(@"response code %i %@", response.status, response.asString);
                
                NSError * error = [[NSError alloc] initWithDomain:kDSDropspotAPIErrorDomainName
                                                             code:kDSDropspotAPIErrorCodeNotAuthorized
                                                         userInfo:@{ NSLocalizedDescriptionKey : kDSDropspotAPIErrorDescriptionNotAuthorized, @"URL" : urlString }];
                block(nil, error);
            } else if (response.status == 0) {
                NSLog(@"response code %i %@", response.status, response.asString);
                
                NSError * error = [[NSError alloc] initWithDomain:kDSDropspotAPIErrorDomainName
                                                             code:kDSDropspotAPIErrorCodeNoResponse
                                                         userInfo:@{ NSLocalizedDescriptionKey : kDSDropspotAPIErrorDescriptionNoResponse, @"URL" : urlString, @"Respone code:" : [NSString stringWithFormat:@"%i", response.status] }];
                block(nil, error);
            } else {
                NSLog(@"response code %i %@", response.status, response.asString);
                
                NSError * error = [[NSError alloc] initWithDomain:kDSDropspotAPIErrorDomainName
                                                             code:kDSDropspotAPIErrorCodeWrongResponse
                                                         userInfo:@{ NSLocalizedDescriptionKey : kDSDropspotAPIErrorDescriptionWrongResponse, @"URL" : urlString, @"Respone code:" : [NSString stringWithFormat:@"%i", response.status] }];
                block(nil, error);
            }
        }];
    } else {
        NSError * error = [[NSError alloc] initWithDomain:kDSDropspotAPIErrorDomainName
                                                     code:kDSDropspotAPIErrorCodeNoSessionToken
                                                 userInfo:@{ NSLocalizedDescriptionKey : kDSDropspotAPIErrorDescriptionNoSessionToken }];
        block(nil, error);
    }
}

#pragma mark - Mapping

- (DSAPIProfileResponse *)profileResponseFromDictionary:(NSDictionary *)dictionary {
    
    DSAPIProfileResponse * profileResponse = [[DSAPIProfileResponse alloc] init];
    profileResponse.url = dictionary[@"url"];
    profileResponse.name = dictionary[@"name"];
    profileResponse.username = dictionary[@"username"];
    profileResponse.primaryAvatarURL = (dictionary[@"primary_avatar"] != [NSNull null])?dictionary[@"primary_avatar"]:@"";
    profileResponse.collectionsURL = dictionary[@"collections"];
    profileResponse.spotsURL = dictionary[@"spots"];
    profileResponse.subscriptionsURL = dictionary[@"subscriptions"];
    profileResponse.dateJoined = dictionary[@"date_joined"];
    
    return profileResponse;
}

- (DSAPISpotResponse *)spotResponseFromDictionary:(NSDictionary *)dictionary {
    
    DSAPISpotResponse * response = [[DSAPISpotResponse alloc] init];
    response.url = dictionary[DRPSPT_SPOT_URL];
    response.name = dictionary[DRPSPT_SPOT_TEXT];
    response.links = [dictionary valueForKeyPath:DRPSPT_SPOT_LINKS_KEYPATH];
    response.linksURL = [dictionary valueForKeyPath:DRPSPT_SPOT_LINKS_URL_KEYPATH];
    response.slug = dictionary[DRPSPT_SPOT_SLUG];
    response.address = dictionary[DRPSPT_SPOT_ADDRESS];
    if (dictionary[DRPSPT_SPOT_PHOTO_URL] != [NSNull null] &&
        [dictionary valueForKeyPath:DRPSPT_SPOT_PHOTO_SMALL_URL_KEYPATH] != [NSNull null]) {
        response.photoURL = [dictionary valueForKeyPath:DRPSPT_SPOT_PHOTO_SMALL_URL_KEYPATH];
    }
    if (dictionary[DRPSPT_SPOT_PHOTO_URL] != [NSNull null] &&
        [dictionary valueForKeyPath:DRPSPT_SPOT_PHOTO_TINY_URL_KEYPATH] != [NSNull null]) {
        response.thumbnailURL = [dictionary valueForKeyPath:DRPSPT_SPOT_PHOTO_TINY_URL_KEYPATH];
    }
    response.latitude = [dictionary[DRPSPT_SPOT_LATITUDE] doubleValue];
    response.longitude = [dictionary[DRPSPT_SPOT_LONGITUDE] doubleValue];
    response.user.username = [dictionary valueForKeyPath:DRPSPT_SPOT_CREATOR_USERNAME_KEYPATH];
    response.user.displayName = [dictionary valueForKeyPath:DRPSPT_SPOT_CREATOR_NAME_KEYPATH];
    if ([dictionary valueForKeyPath:DRPSPT_SPOT_CREATOR_IMAGE_KEYPATH] != [NSNull null]) {
        response.user.avatarImageURL = [dictionary valueForKeyPath:DRPSPT_SPOT_CREATOR_IMAGE_KEYPATH];
    }
    response.created = dictionary[DRPSPT_SPOT_CREATED];
    response.modified = dictionary[DRPSPT_SPOT_MODIFIED];
    
    if (dictionary[DRPSPT_SPOT_EXPIRES_AT] != [NSNull null]) {
        response.expiresAt = dictionary[DRPSPT_SPOT_EXPIRES_AT];
    }
    response.collectionSlugs = dictionary[DRPSPT_SPOT_COLLECTION_SLUGS];
    
    return response;
}

- (DSAPISpotLinkResponse *)spotLinkResponseFromDictionary:(NSDictionary *)dictionary {
    
    DSAPISpotLinkResponse * response = [[DSAPISpotLinkResponse alloc] init];
    response.identifier = dictionary[DRPSPT_SPOTLINK_ID];
    response.url = dictionary[DRPSPT_SPOTLINK_URL];
    response.link = dictionary[DRPSPT_SPOTLINK_LINK];
    response.modified = dictionary[DRPSPT_SPOTLINK_MODIFIED];
    if (dictionary[DRPSPT_SPOTLINK_TITLE] != [NSNull null]) {
        response.title = dictionary[DRPSPT_SPOTLINK_TITLE];
    }
    if (dictionary[DRPSPT_SPOTLINK_IMAGE_URL] != [NSNull null]) {
        response.photoURL = dictionary[DRPSPT_SPOTLINK_IMAGE_URL];
    }
    if (dictionary[DRPSPT_SPOTLINK_ICON_URL] != [NSNull null]) {
        response.iconURL = dictionary[DRPSPT_SPOTLINK_ICON_URL];
    }
    
    return response;
}

- (DSAPICollectionResponse *)collectionResponseFromDictionary:(NSDictionary *)dictionary {
    
    DSAPICollectionResponse * response = [[DSAPICollectionResponse alloc] init];
    response.url = dictionary[DRPSPT_COLLECTION_URL];
    response.title = dictionary[DRPSPT_COLLECTION_TITLE];
    response.text = dictionary[DRPSPT_COLLECTION_DESCRIPTION];
    response.slug = dictionary[DRPSPT_COLLECTION_SLUG];
    if (dictionary[DRPSPT_COLLECTION_PHOTO_URL] != [NSNull null]) {
        response.photoURL = [dictionary valueForKeyPath:DRPSPT_COLLECTION_PHOTO_URL_KEYPATH];
    }
    if (dictionary[DRPSPT_SPOT_PHOTO_URL] != [NSNull null] &&
        [dictionary valueForKeyPath:DRPSPT_SPOT_PHOTO_TINY_URL_KEYPATH] != [NSNull null]) {
        response.thumbnailURL = [dictionary valueForKeyPath:DRPSPT_SPOT_PHOTO_TINY_URL_KEYPATH];
    }
    response.subscribeURL = dictionary[DRPSPT_COLLECTION_SUBSCRIBE_URL];
    response.spotsURL = dictionary[DRPSPT_COLLECTION_SPOTS];
    response.user.username = [dictionary valueForKeyPath:DRPSPT_COLLECTION_CREATOR_USERNAME_KEYPATH];
    response.user.displayName = [dictionary valueForKeyPath:DRPSPT_COLLECTION_CREATOR_NAME_KEYPATH];
    if ([dictionary valueForKeyPath:DRPSPT_COLLECTION_CREATOR_IMAGE_KEYPATH] != [NSNull null]) {
        response.user.avatarImageURL = [dictionary valueForKeyPath:DRPSPT_COLLECTION_CREATOR_IMAGE_KEYPATH];
    }
    response.created = dictionary[DRPSPT_COLLECTION_CREATED];
    response.modified = dictionary[DRPSPT_COLLECTION_MODIFIED];
    if (dictionary[DRPSPT_COLLECTION_MODIFIED_SPOTS] != [NSNull null]) {
        response.modifiedSpots = dictionary[DRPSPT_COLLECTION_MODIFIED_SPOTS];
    }
    if (dictionary[DRPSPT_COLLECTION_PUBLISHED] == [NSNull null]) {
        response.published = NO;
    } else {
        response.published = [dictionary[DRPSPT_COLLECTION_PUBLISHED] boolValue];
    }
    
    return response;
}


#pragma mark - Subscriptions

- (void)subscribeToCollectionWithSlug:(NSString *)slug block:(DSDictionaryResultBlock)block {
    NSString * urlString = [NSString stringWithFormat:@"%@%@/subscribe/.json", [self baseURLWithPath:@"collections/"], slug];
    
    NSLog(@"url %@", urlString);
    
    NSString * stackKey = [NSString stringWithFormat:@"%@-%0.0f", urlString, [[NSDate dateWithTimeIntervalSinceNow:0] timeIntervalSince1970]];
    [[DSGlobal sharedGlobals] addLoadingStackObject:stackKey];
    
    LRRestyClient * client = [LRResty client];
    [client setHandlesCookiesAutomatically:NO];
    [client attachRequestModifier:^(LRRestyRequest *request) {
        NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
        [request addHeader:@"Authorization" value:[NSString stringWithFormat:@"Token %@", [[defaults objectForKey:kDSDropspotSessionDomainKeyName] valueForKey:kDSDropspotSessionTokenKeyName]]];
    }];
    
    [client put:urlString payload:nil withBlock:^(LRRestyResponse *response) {
        [[DSGlobal sharedGlobals] removeFromLoadingStack:stackKey];
        
        NSDictionary * dict = [DataHelper dictionaryFromJSONData:response.responseData];
        
        if (response.status == 200) {
            if (block) {
                block(dict, nil);
            }
        } else if (response.status == 401) {
            NSLog(@"%@", response.asString);
            NSError * error = [[NSError alloc] initWithDomain:kDSDropspotAPIErrorDomainName
                                                         code:kDSDropspotAPIErrorCodeNotAuthorized
                                                     userInfo:@{ NSLocalizedDescriptionKey : kDSDropspotAPIErrorDescriptionNotAuthorized, @"URL" : urlString }];
            if (block) {
                block(nil, error);
            }
        } else if (response.status == 0) {
            NSError * error = [[NSError alloc] initWithDomain:kDSDropspotAPIErrorDomainName
                                                         code:kDSDropspotAPIErrorCodeNoResponse
                                                     userInfo:@{ NSLocalizedDescriptionKey : kDSDropspotAPIErrorDescriptionNoResponse, @"URL" : urlString }];
            if (block) {
                block(nil, error);
            }
        } else {
            NSLog(@"response code %i %@", response.status, response.asString);
            
            NSError * error = [[NSError alloc] initWithDomain:kDSDropspotAPIErrorDomainName
                                                         code:kDSDropspotAPIErrorCodeWrongResponse
                                                     userInfo:@{ NSLocalizedDescriptionKey : kDSDropspotAPIErrorDescriptionWrongResponse, @"URL" : urlString, @"Respone code:" : [NSString stringWithFormat:@"%i", response.status] }];
            if (block) {
                block(dict, error);
            }
        }
    }];
}

- (void)unsubscribeFromCollectionWithSlug:(NSString *)slug block:(DSDictionaryResultBlock)block {
    NSString * urlString = [NSString stringWithFormat:@"%@%@/subscribe/.json", [self baseURLWithPath:@"collections/"], slug];
    
    NSLog(@"url %@", urlString);
    
    NSString * stackKey = [NSString stringWithFormat:@"%@-%0.0f", urlString, [[NSDate dateWithTimeIntervalSinceNow:0] timeIntervalSince1970]];
    [[DSGlobal sharedGlobals] addLoadingStackObject:stackKey];
    
    LRRestyClient * client = [LRResty client];
    [client setHandlesCookiesAutomatically:NO];
    [client attachRequestModifier:^(LRRestyRequest *request) {
        NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
        [request addHeader:@"Authorization" value:[NSString stringWithFormat:@"Token %@", [[defaults objectForKey:kDSDropspotSessionDomainKeyName] valueForKey:kDSDropspotSessionTokenKeyName]]];
    }];
    
    [client delete:urlString withBlock:^(LRRestyResponse *response) {
        [[DSGlobal sharedGlobals] removeFromLoadingStack:stackKey];
        
        NSDictionary * dict = [DataHelper dictionaryFromJSONData:response.responseData];
        
        if (response.status == 204) {
            block(dict, nil);
        } else if (response.status == 401) {
            NSLog(@"%@", response.asString);
            NSError * error = [[NSError alloc] initWithDomain:kDSDropspotAPIErrorDomainName
                                                         code:kDSDropspotAPIErrorCodeNotAuthorized
                                                     userInfo:@{ NSLocalizedDescriptionKey : kDSDropspotAPIErrorDescriptionNotAuthorized, @"URL" : urlString }];
            block(nil, error);
        } else if (response.status == 0) {
            NSError * error = [[NSError alloc] initWithDomain:kDSDropspotAPIErrorDomainName
                                                         code:kDSDropspotAPIErrorCodeNoResponse
                                                     userInfo:@{ NSLocalizedDescriptionKey : kDSDropspotAPIErrorDescriptionNoResponse, @"URL" : urlString }];
            block(nil, error);
        } else {
            NSLog(@"response code %i %@", response.status, response.asString);
            
            NSError * error = [[NSError alloc] initWithDomain:kDSDropspotAPIErrorDomainName
                                                         code:kDSDropspotAPIErrorCodeWrongResponse
                                                     userInfo:@{ NSLocalizedDescriptionKey : kDSDropspotAPIErrorDescriptionWrongResponse, @"URL" : urlString, @"Respone code:" : [NSString stringWithFormat:@"%i", response.status] }];
            block(dict, error);
        }
    }];
}

#pragma mark - Create content

- (void)createCollection:(Collection *)collection block:(DSCollectionResultAndErrorBlock)block {
    NSString * urlString = [self baseURLWithPath:@"collections/.json"];
    
    NSLog(@"Collection to create %@", collection);
    
    NSLog(@"url %@", urlString);
    
    NSString * stackKey = [NSString stringWithFormat:@"%@-%0.0f", urlString, [[NSDate dateWithTimeIntervalSinceNow:0] timeIntervalSince1970]];
    [[DSGlobal sharedGlobals] addLoadingStackObject:stackKey];
    
    RKObjectManager * manager = [[RKObjectManager alloc] initWithHTTPClient:[AFHTTPClient clientWithBaseURL:[NSURL URLWithString:[self baseURLWithPath:@""]]]];
    
    NSMutableURLRequest *request = [manager
                                    multipartFormRequestWithObject:nil
                                    method:RKRequestMethodPOST
                                    path:urlString
                                    parameters:[self mappedDictionaryFromCollection:collection]
                                    constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
                                        if (collection.photoFile) {
                                            [formData appendPartWithFileData:UIImageJPEGRepresentation([DataHelper imageWithFilePath:[DataHelper pathForCacheFile:collection.photoFile]], 0.1)
                                                                    name:@"photo"
                                                                fileName:@"photo.png"
                                                                mimeType:@"image/png"];
                                        }
                                    }];
    
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    [request addValue:[NSString stringWithFormat:@"Token %@", [[defaults objectForKey:kDSDropspotSessionDomainKeyName] valueForKey:kDSDropspotSessionTokenKeyName]] forHTTPHeaderField:@"Authorization"];
    
    RKObjectMapping * mapping = [[RKObjectMapping alloc] initWithClass:[DSAPICollectionResponse class]];
    [mapping addAttributeMappingsFromDictionary:[self responseMappingForCollection]];
    
    RKResponseDescriptor * responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:mapping method:RKRequestMethodPOST pathPattern:nil keyPath:nil statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    
    RKObjectRequestOperation * operation = [[RKObjectRequestOperation alloc] initWithRequest:request responseDescriptors:@[responseDescriptor]];
    
    [operation setCompletionBlockWithSuccess:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        [[DSGlobal sharedGlobals] removeFromLoadingStack:stackKey];
        if (block) {
            block(mappingResult.array[0], nil);
        }
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        NSLog(@"failure %@", error);
        [[DSGlobal sharedGlobals] removeFromLoadingStack:stackKey];
        if (block) {
            block(nil, error);
        }
    }];
    
    [manager enqueueObjectRequestOperation:operation];
}

- (void)createSpot:(Spot *)spot block:(DSSpotResultAndErrorBlock)block {
    NSString * urlString = [self baseURLWithPath:@"spots/.json"];
    
    NSLog(@"url %@", urlString);
    
    NSString * stackKey = [NSString stringWithFormat:@"%@-%0.0f", urlString, [[NSDate dateWithTimeIntervalSinceNow:0] timeIntervalSince1970]];
    [[DSGlobal sharedGlobals] addLoadingStackObject:stackKey];
    
    RKObjectManager * manager = [[RKObjectManager alloc] initWithHTTPClient:[AFHTTPClient clientWithBaseURL:[NSURL URLWithString:[self baseURLWithPath:@""]]]];
    
    NSMutableURLRequest *request = [manager
                                    multipartFormRequestWithObject:nil
                                    method:RKRequestMethodPOST
                                    path:urlString
                                    parameters:[self mappedDictionaryFromSpot:spot]
                                    constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
                                        
                                        UIImage * image = [DataHelper imageWithFilePath:[DataHelper pathForCacheFile:spot.photoFile]];
                                        if (image) {
                                            [formData appendPartWithFileData:UIImageJPEGRepresentation(image, 0.1)
                                                                        name:@"photo"
                                                                    fileName:@"photo.png"
                                                                    mimeType:@"image/png"];
                                        }
                                    }];
    
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    [request addValue:[NSString stringWithFormat:@"Token %@", [[defaults objectForKey:kDSDropspotSessionDomainKeyName] valueForKey:kDSDropspotSessionTokenKeyName]] forHTTPHeaderField:@"Authorization"];
    
    RKObjectMapping * mapping = [[RKObjectMapping alloc] initWithClass:[DSAPISpotResponse class]];
    [mapping addAttributeMappingsFromDictionary:[self responseMappingForSpot]];
    
    RKResponseDescriptor * responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:mapping method:RKRequestMethodPOST pathPattern:nil keyPath:nil statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    
    RKObjectRequestOperation * operation = [[RKObjectRequestOperation alloc] initWithRequest:request responseDescriptors:@[responseDescriptor]];
    
    [operation setCompletionBlockWithSuccess:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        [[DSGlobal sharedGlobals] removeFromLoadingStack:stackKey];
        if (block) {
            block(mappingResult.array[0], nil);
        }
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        NSLog(@"failure %@", error);
        [[DSGlobal sharedGlobals] removeFromLoadingStack:stackKey];
        if (block) {
            block(nil, error);
        }
    }];
    
    [manager enqueueObjectRequestOperation:operation];
}

- (void)editSpot:(Spot *)spot imageStatus:(DSPhotoStatus)status block:(DSSpotResultAndErrorBlock)block {
    NSString * urlString = spot.url;
    
    NSLog(@"url %@", urlString);
    
    NSString * stackKey = [NSString stringWithFormat:@"%@-%0.0f", urlString, [[NSDate dateWithTimeIntervalSinceNow:0] timeIntervalSince1970]];
    [[DSGlobal sharedGlobals] addLoadingStackObject:stackKey];
    
    RKObjectManager * manager = [[RKObjectManager alloc] initWithHTTPClient:[AFHTTPClient clientWithBaseURL:[NSURL URLWithString:[self baseURLWithPath:@""]]]];
    
    NSMutableDictionary * parameters = [[self mappedDictionaryFromSpot:spot] mutableCopy];
    if (status == DSPhotoStatusRemoveImage) {
        [parameters setValue:@"" forKey:@"photo"];
    }
    
    NSMutableURLRequest *request = [manager
                                    multipartFormRequestWithObject:nil
                                    method:RKRequestMethodPATCH
                                    path:urlString
                                    parameters:parameters
                                    constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
                                        
                                        UIImage * image = [DataHelper imageWithFilePath:[DataHelper pathForCacheFile:spot.photoFile]];
                                        if (status == DSPhotoStatusNewImage && image) {
                                            [formData appendPartWithFileData:UIImageJPEGRepresentation(image, 0.1)
                                                                        name:@"photo"
                                                                    fileName:@"photo.png"
                                                                    mimeType:@"image/png"];
                                        }
                                    }];
    
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    [request addValue:[NSString stringWithFormat:@"Token %@", [[defaults objectForKey:kDSDropspotSessionDomainKeyName] valueForKey:kDSDropspotSessionTokenKeyName]] forHTTPHeaderField:@"Authorization"];
    
    RKObjectMapping * mapping = [[RKObjectMapping alloc] initWithClass:[DSAPISpotResponse class]];
    
    [mapping addAttributeMappingsFromDictionary:[self responseMappingForSpot]];
    
    RKResponseDescriptor * responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:mapping method:RKRequestMethodPATCH pathPattern:nil keyPath:nil statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    
    RKObjectRequestOperation * operation = [[RKObjectRequestOperation alloc] initWithRequest:request responseDescriptors:@[responseDescriptor]];
    
    [operation setCompletionBlockWithSuccess:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        [[DSGlobal sharedGlobals] removeFromLoadingStack:stackKey];
        if (block) {
            block(mappingResult.array[0], nil);
        }
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        NSLog(@"failure %@", error);
        [[DSGlobal sharedGlobals] removeFromLoadingStack:stackKey];
        if (block) {
            block(nil, error);
        }
    }];
    
    [manager enqueueObjectRequestOperation:operation];
}

- (void)editCollection:(Collection *)collection imageStatus:(DSPhotoStatus)status block:(DSCollectionResultAndErrorBlock)block {
    NSString * urlString = collection.url;
    
    NSLog(@"Collection to edit %@", collection);
    
    NSLog(@"url %@", urlString);
    
    NSString * stackKey = [NSString stringWithFormat:@"%@-%0.0f", urlString, [[NSDate dateWithTimeIntervalSinceNow:0] timeIntervalSince1970]];
    [[DSGlobal sharedGlobals] addLoadingStackObject:stackKey];
    
    RKObjectManager * manager = [[RKObjectManager alloc] initWithHTTPClient:[AFHTTPClient clientWithBaseURL:[NSURL URLWithString:[self baseURLWithPath:@""]]]];
    
    NSMutableDictionary * parameters = [[self mappedDictionaryFromCollection:collection] mutableCopy];
    if (status == DSPhotoStatusRemoveImage) {
        [parameters setValue:@"" forKey:@"photo"];
    }
    
    NSMutableURLRequest *request = [manager
                                    multipartFormRequestWithObject:nil
                                    method:RKRequestMethodPATCH
                                    path:urlString
                                    parameters:parameters
                                    constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
                                        if (status == DSPhotoStatusNewImage && collection.photoFile) {
                                            [formData appendPartWithFileData:UIImageJPEGRepresentation([DataHelper imageWithFilePath:[DataHelper pathForCacheFile:collection.photoFile]], 0.1)
                                                                        name:@"photo"
                                                                    fileName:@"photo.png"
                                                                    mimeType:@"image/png"];
                                        }
                                    }];
    
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    [request addValue:[NSString stringWithFormat:@"Token %@", [[defaults objectForKey:kDSDropspotSessionDomainKeyName] valueForKey:kDSDropspotSessionTokenKeyName]] forHTTPHeaderField:@"Authorization"];
    
    RKObjectMapping * mapping = [[RKObjectMapping alloc] initWithClass:[DSAPICollectionResponse class]];
    
    [mapping addAttributeMappingsFromDictionary:[self responseMappingForCollection]];
    
    RKResponseDescriptor * responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:mapping method:RKRequestMethodPATCH pathPattern:nil keyPath:nil statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    
    RKObjectRequestOperation * operation = [[RKObjectRequestOperation alloc] initWithRequest:request responseDescriptors:@[responseDescriptor]];
    
    [operation setCompletionBlockWithSuccess:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        [[DSGlobal sharedGlobals] removeFromLoadingStack:stackKey];
        if (block) {
            block(mappingResult.array[0], nil);
        }
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        NSLog(@"failure %@", error);
        [[DSGlobal sharedGlobals] removeFromLoadingStack:stackKey];
        if (block) {
            block(nil, error);
        }
    }];
    
    [manager enqueueObjectRequestOperation:operation];
}

#pragma mark - Mapping

- (NSDictionary *)responseMappingForCollection {
    return @{
             DRPSPT_COLLECTION_TITLE : @"title",
             DRPSPT_COLLECTION_SLUG : @"slug",
             DRPSPT_COLLECTION_PHOTO_URL_KEYPATH : @"photoURL"
             };
}

- (NSDictionary *)mappedDictionaryFromSpotDictionary:(NSDictionary *)spotDictionary {
    NSMutableDictionary * dict = [NSMutableDictionary dictionary];
    if (spotDictionary[DRPSPT_CREATE_SPOT_TEXT]) {
        [dict setValue:spotDictionary[DRPSPT_CREATE_SPOT_TEXT] forKey:DRPSPT_SPOT_TEXT];
    }
    if (spotDictionary[DRPSPT_CREATE_SPOT_ADDRESS]) {
        [dict setValue:spotDictionary[DRPSPT_CREATE_SPOT_ADDRESS] forKey:DRPSPT_SPOT_ADDRESS];
    }
    if (spotDictionary[DRPSPT_CREATE_SPOT_LATITUDE]) {
        [dict setValue:spotDictionary[DRPSPT_CREATE_SPOT_LATITUDE] forKey:DRPSPT_SPOT_LATITUDE];
    }
    if (spotDictionary[DRPSPT_CREATE_SPOT_LONGITUDE]) {
        [dict setValue:spotDictionary[DRPSPT_CREATE_SPOT_LONGITUDE] forKey:DRPSPT_SPOT_LONGITUDE];
    }
/*    if (spotDictionary[DRPSPT_CREATE_SPOT_LINK]) {
        [dict setValue:spotDictionary[DRPSPT_CREATE_SPOT_LINK] forKey:DRPSPT_SPOT_LINK];
    }
*/    if (spotDictionary[DRPSPT_CREATE_SPOT_COLLECTION_SLUGS]) {
        [dict setValue:spotDictionary[DRPSPT_CREATE_SPOT_COLLECTION_SLUGS] forKey:DRPSPT_SPOT_COLLECTION_SLUGS];
    }
    return dict;
}

- (NSDictionary *)mappedDictionaryFromSpot:(Spot *)spot {
    NSMutableDictionary * dict = [@{
                                   DRPSPT_SPOT_TEXT : spot.text,
                                   DRPSPT_SPOT_LATITUDE : [NSString stringWithFormat:@"%f", spot.latitude],
                                   DRPSPT_SPOT_LONGITUDE : [NSString stringWithFormat:@"%f", spot.longitude],
                                   DRPSPT_SPOT_ADDRESS : spot.address
                                   } mutableCopy];
    NSMutableString * collection_slugs = [NSMutableString string];
    NSArray * collections = spot.collections.allObjects;
    for (int i = 0; i < collections.count; i++) {
        Collection * collection = collections[i];
        [collection_slugs appendString:collection.slug];
        if (i < collections.count - 1) {
            [collection_slugs appendString:@","];
        }
    }
    [dict setValue:collection_slugs forKey:DRPSPT_SPOT_COLLECTION_SLUGS];
/*
    if (spot.link) {
        [dict setValue:spot.link forKey:DRPSPT_SPOT_LINK];
    }
*/    return dict;
}

- (NSDictionary *)mappedDictionaryFromCollection:(Collection *)collection {
    return @{
             DRPSPT_COLLECTION_TITLE : collection.title,
             };
}


- (NSDictionary *)responseMappingForSpot {
    return @{
             DRPSPT_SPOT_TEXT : @"name",
             DRPSPT_SPOT_SLUG : @"slug",
             DRPSPT_SPOT_LATITUDE : @"latitude",
             DRPSPT_SPOT_LONGITUDE : @"longitude",
//             DRPSPT_SPOT_LINK : @"link",
             DRPSPT_SPOT_ADDRESS : @"address",
             DRPSPT_SPOT_URL : @"url"
             };
}

- (NSString *)baseURLWithPath:(NSString *)path {
    NSString * baseURL = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"DSBaseURL"];
    return [NSString stringWithFormat:@"%@%@", baseURL, path];
}

@end

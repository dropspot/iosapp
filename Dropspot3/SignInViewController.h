/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  NativeSignInViewController.h
//  SingleCollection
//
//  Created by Lukas Würzburger on 2/13/14.
//  Copyright (c) 2014 Lukas Würzburger. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SignInViewController, DSUser;

@protocol SignInViewControllerDelegate <NSObject>

- (void)signInViewControllerDidCancel:(SignInViewController *)signInViewController;
- (void)signInViewController:(SignInViewController *)signInViewController didLoginWithUser:(DSUser *)user;
- (void)signInViewControllerDidPressRegisterButton:(SignInViewController *)signInViewController;

@end

@interface SignInViewController : UIViewController

@property (nonatomic, weak) IBOutlet UIImage * backgroundImage;

@property (nonatomic, assign) id <SignInViewControllerDelegate> delegate;

@end

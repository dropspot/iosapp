/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  DSLinkPreview.m
//  Dropspot3
//
//  Created by Lukas Würzburger on 5/13/14.
//  Copyright (c) 2014 Dropspot GmbH. All rights reserved.
//

#import "DSLinkPreview.h"

#import "UILabel+AutoHeight.h"

#import "UIImage+Tint.h"

#define kTextMargin 10

@interface DSLinkPreview () {
    // UI
    UIImageView * _imageView;
    UIActivityIndicatorView * _loadingWheel;
    UIImageView * _arrowImageView;
    UILabel * _titleLabel;
    UIImageView * _sourceImageView;
    UILabel * _urlLabel;
    
    UIView * _loadingOverlay;
    UIActivityIndicatorView * _activityIndicatorView;
    
    // Gestures
    UITapGestureRecognizer * _tapGestureRecognizer;
}

@end

@implementation DSLinkPreview

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        
        [self initialize];
    }
    return self;
}

- (id)init {
    self = [super init];
    if (self) {
        
        [self initialize];
    }
    return self;
}

- (void)initialize {
    
    self.backgroundColor = [UIColor whiteColor];
    
    // Layer border
    self.layer.borderColor = [UIColor colorWithWhite:0.5 alpha:0.3].CGColor;
    self.layer.borderWidth = 0.5;

    // Layer shadow
    self.layer.shadowColor = [UIColor blackColor].CGColor;
    self.layer.shadowOffset = CGSizeMake(0, 0);
    self.layer.shadowOpacity = 0.2;
    self.layer.shadowRadius = 2;
    
    // Gestures
    _tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapGesture:)];
    [self addGestureRecognizer:_tapGestureRecognizer];
    
    // UI
    _imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 160)];
    _imageView.contentMode = UIViewContentModeScaleAspectFill;
    _imageView.backgroundColor = [UIColor colorWithWhite:0.97 alpha:1.0];
    _imageView.layer.masksToBounds = YES;
    [self addSubview:_imageView];
    
    UIView * view = [[UIView alloc] initWithFrame:CGRectMake(0, 0.5, self.bounds.size.width, 1)];
    view.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.3];
    [self addSubview:view];
    
    UIView * view2 = [[UIView alloc] initWithFrame:CGRectMake(0, _imageView.frame.origin.y + _imageView.frame.size.height - 1, self.bounds.size.width, 1)];
    view2.backgroundColor = [UIColor colorWithWhite:0.5 alpha:0.3];
    [self addSubview:view2];
    
    _arrowImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"preview_arrow_right.png"]];
    _arrowImageView.center = CGPointMake(self.frame.size.width - _arrowImageView.frame.size.width, _imageView.frame.size.height / 2);
    [self addSubview:_arrowImageView];
    
    _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(kTextMargin,
                                                            _imageView.frame.origin.y + _imageView.frame.size.height + kTextMargin,
                                                            self.frame.size.width - 2 * kTextMargin,
                                                            1)];
    _titleLabel.textColor = [UIColor colorWithWhite:0.05 alpha:1];
    _titleLabel.numberOfLines = 0;
    _titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:16];
    _titleLabel.backgroundColor = [UIColor clearColor];
    [self addSubview:_titleLabel];
    
    _sourceImageView = [[UIImageView alloc] initWithFrame:CGRectMake(kTextMargin, 0, 16, 16)];
    _sourceImageView.contentMode = UIViewContentModeScaleAspectFill;
    _sourceImageView.layer.masksToBounds = YES;
    [self addSubview:_sourceImageView];
    
    _urlLabel = [[UILabel alloc] initWithFrame:CGRectMake(kTextMargin,
                                                                  _imageView.frame.origin.y + _imageView.frame.size.height + kTextMargin,
                                                                  self.frame.size.width - 2 * kTextMargin,
                                                                  1)];
    _urlLabel.textColor = [UIColor colorWithWhite:0.5 alpha:1];
    _urlLabel.numberOfLines = 0;
    _urlLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:10];
    _urlLabel.backgroundColor = [UIColor clearColor];
    [self addSubview:_urlLabel];
    
}

- (void)setLayout {
    CGRect frame;
    float lastY = 0;
    
    lastY = _imageView.frame.origin.y + _imageView.frame.size.height;
    
    // Title label
    if (_titleLabel.text) {
        frame = [_titleLabel autoAdjustHeight];
        frame.origin.y = lastY + kTextMargin - 3;
        _titleLabel.frame = frame;

        lastY = _titleLabel.frame.origin.y + _titleLabel.frame.size.height;
    }
    
    // Source image view
    if (_sourceImageView.image) {
        frame = _sourceImageView.frame;
        frame.origin.y = lastY + kTextMargin - 3;
        _sourceImageView.frame = frame;

        lastY = _titleLabel.frame.origin.y + _titleLabel.frame.size.height;
    }
    
    
    // Url label
    if (_urlLabel.text) {
        frame = [_urlLabel autoAdjustHeight];
        if (_sourceImageView.image) {
            frame.origin.y = _sourceImageView.frame.origin.y + 2;
            frame.origin.x = _sourceImageView.frame.origin.x + _sourceImageView.frame.size.width + 8;
        } else {
            frame.origin.y = lastY + 4;
            frame.origin.x = _sourceImageView.frame.origin.x;
        }
        _urlLabel.frame = frame;

        lastY = _urlLabel.frame.origin.y + _urlLabel.frame.size.height;
    }
    
    // Self
    frame = self.frame;
    frame.size.height = lastY + kTextMargin;
    self.frame = frame;
}

#pragma mark - Setters & Getters

- (void)setImage:(UIImage *)image {
    
    if (_image != image) {
        
        _image = image;
        _imageView.image = _image;
        _imageView.contentMode = UIViewContentModeScaleAspectFill;
    }
}

- (void)setFavicon:(UIImage *)favicon {
    
    if (_favicon != favicon) {
        
        _favicon = favicon;
        
        _sourceImageView.image = _favicon;
        
        [self setLayout];
    }
}

- (void)setPlaceholderImage:(UIImage *)placeholderImage {

    if (_placeholderImage != placeholderImage) {

        _placeholderImage = placeholderImage;
        if (!_imageView.image) {
            
            _imageView.image = placeholderImage;
            _imageView.contentMode = UIViewContentModeCenter;
        }
    }
}

- (void)setTitle:(NSString *)title {

    if (_title != title) {
    
        _title = title;
        _titleLabel.text = _title;
        [self setLayout];
    }
}

- (void)setUrlHost:(NSString *)urlHost {
    
    if (_urlHost != urlHost) {
        
        _urlHost = urlHost;
        _urlLabel.text = _urlHost;
        [self setLayout];
    }
}

#pragma mark - Helpers

- (void)setHighlighted:(BOOL)highlighted {
    
    if (highlighted) {
        
        // Change color to light gray when touching the view.
        self.backgroundColor = [UIColor colorWithWhite:0.9 alpha:1.0];
    } else {

        // Change color to light gray when releasing the view.
        self.backgroundColor = [UIColor whiteColor];
    }
}

#pragma mark - Gestures

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    // Change highlighting
    [self setHighlighted:YES];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    
    CGPoint touchPoint = [[touches anyObject] locationInView:self];

    // Check wether the touch point is inside the own bounds
    if (CGRectContainsPoint(self.bounds, touchPoint)) {
        
        // Change highlighting
        [self setHighlighted:YES];
    } else {
        
        // Change highlighting
        [self setHighlighted:NO];
    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {

    // Change highlighting
    [self setHighlighted:NO];
    
    // Tell the delegate that the view was tapped.
    if ([self.delegate respondsToSelector:@selector(linkPreviewTapped:)]) {
        [self.delegate linkPreviewTapped:self];
    }
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {

    // Change highlighting
    [self setHighlighted:NO];
}

- (void)handleTapGesture:(UITapGestureRecognizer *)gestureRecognizer {

    if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
        
        // Change highlighting
        [self setHighlighted:YES];
    } else if (gestureRecognizer.state == UIGestureRecognizerStateRecognized) {
        
        // Tell the delegate that the view was tapped.
        if ([self.delegate respondsToSelector:@selector(linkPreviewTapped:)]) {
            [self.delegate linkPreviewTapped:self];
        }
    } else if (gestureRecognizer.state == UIGestureRecognizerStateCancelled ||
               gestureRecognizer.state == UIGestureRecognizerStateEnded) {
        
        // Change highlighting
        [self setHighlighted:NO];
    }
}

#pragma mark - Animations

- (void)startLoadingAnimation {
    
    // Create objects if it doesn't exist.
    if (!_loadingOverlay) {
        _loadingOverlay = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, _imageView.frame.size.height)];
        _loadingOverlay.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.98];
    }
    
    if (!_activityIndicatorView) {
        _activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        _activityIndicatorView.color = [UIColor lightGrayColor];
        _activityIndicatorView.center = CGPointMake(self.bounds.size.width / 2, _imageView.frame.origin.y + _imageView.frame.size.height / 2);
        [_loadingOverlay addSubview:_activityIndicatorView];
    }
    [self addSubview:_loadingOverlay];
    
    // Start animation.
    [_activityIndicatorView startAnimating];
}

- (void)stopLoadingAnimation {
    
    if (_activityIndicatorView) {
        
        // Stop animation.
        [_activityIndicatorView stopAnimating];
        
        // Destroy activity indicator view.
        if (_activityIndicatorView.superview) {
            
            [_activityIndicatorView removeFromSuperview];
        }
        _activityIndicatorView = nil;
    }
    
    if (_loadingOverlay) {
        
        // Destroy overlay.
        [_loadingOverlay removeFromSuperview];
        _loadingOverlay = nil;
    }
}


@end

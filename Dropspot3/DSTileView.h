/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  DSTileView.h
//  Dropspot3
//
//  Created by Lukas Würzburger on 2/8/14.
//  Copyright (c) 2014 Dropspot. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    DSTileViewStyleSmall,
    DSTileViewStyleBig
} DSTileViewStyle;

typedef enum {
    DSTileViewInfoStyleNormal,
    DSTileViewInfoStyleGreenBordered
} DSTileViewInfoStyle;

@interface DSTileView : UIView

@property (nonatomic, strong) NSString * time;
@property (nonatomic, strong) NSString * text;
@property (nonatomic, strong) NSString * distance;
@property (nonatomic, strong) NSString * info;
@property (nonatomic, strong) UIImage * image;
@property (nonatomic, strong) UIButton * inviteButton;

@property (nonatomic, assign) BOOL activityIndicatorHidden;

@property (nonatomic, assign) DSTileViewStyle style;
@property (nonatomic, assign) DSTileViewInfoStyle infoStyle;

- (void)setLayout;

- (void)addTarget:(id)target selector:(SEL)selector;

+ (float)heightWithStyle:(DSTileViewStyle)style width:(float)width text:(NSString *)text info:(NSString *)info time:(BOOL)time distance:(BOOL)distance image:(BOOL)image;

@end

/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  CollectionViewController.m
//  Dropspot3
//
//  Created by Lukas Würzburger on 11/3/13.
//  Copyright (c) 2013 Dropspot. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

#import "DSCollectionBrowserCell.h"
#import "UILabel+AutoHeight.h"
#import "DSDropspot.h"
#import "DSGlobal.h"
#import "DSGlobalData.h"
#import "DataHelper.h"

@interface DSCollectionBrowserCell () {
    UIImageView * _coverShadowImageView;
    UIImageView * _coverImageView;
    UIImageView * _shadowImageView;
    UILabel * _titleLabel;
    UIActivityIndicatorView * _activityIndicator;
    UIImageView * _sourceImageView;
    UILabel * _sourceLabel;
    UIView * _subscriptionOverlay;
}

@end

@implementation DSCollectionBrowserCell

- (id)initWithCollection:(Collection *)collection {
    self = [self init];
    if (self) {
        self.collection = collection;
        
        [self configureView];
    }
    return self;
}

- (id)init {
    self = [super init];
    if (self) {
        self.view.frame = CGRectMake(0, 0, 320.f, 320.f);

        // Cover image shadow
        _coverShadowImageView = [[UIImageView alloc] initWithFrame:self.view.bounds];
        _coverShadowImageView.contentMode = UIViewContentModeScaleAspectFill;
        _coverShadowImageView.backgroundColor = [UIColor clearColor];
        _coverShadowImageView.image = [UIImage imageNamed:@"CollectionShadow.png"];
        _coverShadowImageView.layer.masksToBounds = YES;
        [self.view addSubview:_coverShadowImageView];
        
        // Cover image
        _coverImageView = [[UIImageView alloc] initWithFrame:CGRectMake(10.f, 10.f, self.view.frame.size.width - 20.f, self.view.frame.size.height - 20)];
        _coverImageView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        _coverImageView.contentMode = UIViewContentModeScaleAspectFill;
        _coverImageView.layer.masksToBounds = YES;
        _coverImageView.backgroundColor = [UIColor blackColor];
        [self.view addSubview:_coverImageView];
        
        // Shadow
        _shadowImageView = [[UIImageView alloc] initWithFrame:_coverImageView.frame];
        _shadowImageView.contentMode = UIViewContentModeScaleToFill;
        _shadowImageView.backgroundColor = [UIColor clearColor];
        _shadowImageView.image = [UIImage imageNamed:@"coverShadow.png"];
        _shadowImageView.layer.masksToBounds = YES;
        [self.view addSubview:_shadowImageView];
        
        // Title label
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(28.f, self.view.frame.size.height - 36.f - 25.f, self.view.frame.size.width - 60.f, 36.f)];
        _titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:25.f];
        _titleLabel.textColor = [UIColor whiteColor];
        _titleLabel.numberOfLines = 0;
        _titleLabel.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
        _titleLabel.backgroundColor = [UIColor clearColor];
        [self.view addSubview:_titleLabel];
        
        // Activity indicator
        _activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        _activityIndicator.center = CGPointMake(self.view.frame.size.width / 2.f, self.view.frame.size.height / 2.f);
        _activityIndicator.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin;
        _activityIndicator.hidesWhenStopped = YES;
        [self.view addSubview:_activityIndicator];
        
        // Source image view
        _sourceImageView = [[UIImageView alloc] initWithFrame:CGRectMake(28.f, 28.f, 40.f, 40.f)];
        _sourceImageView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleBottomMargin;
        _sourceImageView.layer.cornerRadius = 3.f;
        _sourceImageView.layer.masksToBounds = YES;
        [self.view addSubview:_sourceImageView];
        
        // Source label
        _sourceLabel = [[UILabel alloc] initWithFrame:CGRectMake(20.f, 28.f, 300.f, 21.f)];
        _sourceLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:17.f];
        _sourceLabel.textColor = [UIColor whiteColor];
        [self.view addSubview:_sourceLabel];
        
        UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(collectionPressed:)];
        [self.view addGestureRecognizer:tap];
    }
    return self;
}

- (void)setCollection:(Collection *)collection {
    if (_collection != collection) {
        _collection = collection;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];

    [self configureView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updatePhoto) name:kCollectionPhotoLoadingFinished object:nil];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kCollectionPhotoLoadingFinished object:nil];
}

- (void)updatePhoto {
    
    UIImage * image = [DataHelper imageWithFilePath:[DataHelper pathForCacheFile:self.collection.photoFile]];
    
    if (image) {
        
        _coverImageView.image = image;
    } else {
        
        image = [DataHelper imageWithFilePath:[DataHelper pathForCacheFile:self.collection.thumbnailFile]];
                 
        if (image) {
            
            _coverImageView.image = image;
        }
    }
    
    [self setLayout];
}

- (void)configureView {
    _titleLabel.text = self.collection.title;
    _sourceLabel.text = self.collection.user.displayName;
    _subscriptionOverlay.hidden = YES;
    _sourceImageView.image = [DataHelper imageWithFilePath:[DataHelper pathForCacheFile:self.collection.user.photoFile]];

    [self setLayout];

    [self updatePhoto];
}

- (void)setLayout {
    CGRect frame;
    
    if (_sourceImageView.image) {
        frame = _sourceLabel.frame;
        frame.origin.x = _sourceImageView.frame.origin.x + _sourceImageView.frame.size.width + 10;
        frame.origin.y = (_sourceImageView.frame.size.height / 2) - (frame.size.height / 2) + _sourceImageView.frame.origin.y;
        _sourceLabel.frame = frame;
    }
    
    frame = [_titleLabel autoAdjustHeight];
    frame.origin.y = self.view.frame.size.height - frame.size.height - 26;
    _titleLabel.frame = frame;
    
    if (!_coverImageView.image) {
        if (!_activityIndicator.isAnimating) {
            [_activityIndicator startAnimating];
        }
    } else {
        if (_activityIndicator.isAnimating) {
            [_activityIndicator stopAnimating];
        }
    }
}

#pragma mark - IBActions

- (void)collectionPressed:(UIGestureRecognizer *)sender {
    if (self.collection) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kCollectionPressedNotificationName
                                                            object:nil
                                                          userInfo:@{ @"collection" : self.collection }];
    }
}

@end

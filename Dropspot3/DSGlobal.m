/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  DSGlobal.m
//  Dropspot3
//
//  Created by Lukas Würzburger on 12.09.13.
//  Copyright (c) 2013 Dropspot. All rights reserved.
//

#import "DSGlobal.h"

#import "GAIHeader.h"

#import "DSDropspot.h"

@interface DSGlobal () {
    NSMutableDictionary * loadingStack;
    
    // Analytics
    NSString * _screenName;
}

@property (nonatomic, strong) id<GAITracker> tracker;

@end

@implementation DSGlobal

+ (DSGlobal *)sharedGlobals {
    static DSGlobal * instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[DSGlobal alloc] init];
    });
    return instance;
}

#pragma mark - Settings variables

- (void)setNotificationFrequency:(DSNotificationFrequency)newNotificationFrequency {
    if (self.notificationFrequency != newNotificationFrequency) {
        NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:[self numberFromFrequency:newNotificationFrequency] forKey:kDSNotificationFrequencyDefaultsKey];
        [defaults synchronize];
    }
}

- (DSNotificationFrequency)notificationFrequency {
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    NSNumber * n = [defaults objectForKey:kDSNotificationFrequencyDefaultsKey];
    if (n) {
        return [n integerValue];
    } else {
        return kDSNotificationFrequencyDefaultValue;
    }
}

- (NSNumber *)numberFromFrequency:(DSNotificationFrequency)frequency {
    return [NSNumber numberWithInteger:frequency];
}

- (void)setMeasurement:(DSMeasurement)measurement {
    if (self.measurement != measurement) {
        NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:[self numberFromMeasurement:measurement] forKey:kDSNotificationMeasurementDefaultsKey];
        [defaults synchronize];
    }
}

- (DSMeasurement)measurement {
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    NSNumber * n = [defaults objectForKey:kDSNotificationMeasurementDefaultsKey];
    if (n) {
        return [n integerValue];
    } else {
        return kDSNotificationMeasurementDefaultValue;
    }
}

- (NSNumber *)numberFromMeasurement:(DSMeasurement)measurement {
    return [NSNumber numberWithInteger:measurement];
}

- (void)setReadaloud:(BOOL)readaloud {
    
    if (self.readaloud != readaloud) {
    
        NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:@(!readaloud) forKey:kDSReadaloudDefaultsKey];
        [defaults synchronize];
    }
}

- (BOOL)readaloud {
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    return ![defaults boolForKey:kDSReadaloudDefaultsKey];
}

#pragma mark - Helper

- (NSMutableArray *)receivedNotificationSpots {
    if (!_receivedNotificationSpots) {
        _receivedNotificationSpots = [NSMutableArray array];
    }
    
    return _receivedNotificationSpots;
}

#pragma mark - Location helper

+ (void)distancePropertiesFromDistance:(float)distance properties:(void (^)(double value, NSString * measurement))properties {

    BOOL useMeters = NO;
    if ([DSGlobal sharedGlobals].measurement == DSMeasurementAuto) {
        id usesMetricSystem = [[NSLocale currentLocale] objectForKey:NSLocaleUsesMetricSystem];
        useMeters = ([usesMetricSystem boolValue]);
    } else {
        useMeters = ([DSGlobal sharedGlobals].measurement == DSMeasurementMeters);
    }
    
    double value = 0.0;
    NSString * measurement = nil;
    
    if (useMeters) {
        if (distance >= 1000) {
            value = distance / 1000.0;
            measurement = @"km";
        } else {
            value = distance;
            measurement = @"m";
        }
    } else {
        double feet = distance * 3.2808399;
        if (feet >= 5280) {
            value = distance * 0.000621371192;
            measurement = @"mi";
        } else {
            value = feet;
            measurement = @"ft";
        }
    }
    
    if (properties) {
        properties(value, measurement);
    }
}

+ (NSString *)lowestMeasurement {
    BOOL useMeters = NO;
    
    if ([DSGlobal sharedGlobals].measurement == DSMeasurementAuto) {
        id usesMetricSystem = [[NSLocale currentLocale] objectForKey:NSLocaleUsesMetricSystem];
        useMeters = ([usesMetricSystem boolValue]);
    } else {
        useMeters = ([DSGlobal sharedGlobals].measurement == DSMeasurementMeters);
    }
    
    if (useMeters) {
        return @"m";
    } else {
        return @"ft";
    }
}

+ (void)lastLocationUpdate:(void (^)(BOOL available, NSDate * date, CLLocation * location))locationUpdate {
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary * dict = [defaults objectForKey:@"locationUpdate"];
    if (locationUpdate) {
        if (dict) {
            CLLocation * location = [[CLLocation alloc] initWithCoordinate:CLLocationCoordinate2DMake([[dict valueForKey:@"lat"] doubleValue], [[dict valueForKey:@"lng"] doubleValue])
                                                                  altitude:0
                                                        horizontalAccuracy:[[dict valueForKey:@"acc"] doubleValue]
                                                          verticalAccuracy:0
                                                                 timestamp:nil];
            locationUpdate(YES, [dict valueForKey:@"date"], location);
        } else {
            locationUpdate(NO, nil, nil);
        }
    }
}

#pragma mark - Syncing

- (void)setIsLoading:(BOOL)isLoading {
    _isLoading = isLoading;
    
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:_isLoading];
}

- (NSDate *)lastSyncDate {
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary * sessionDict = [defaults objectForKey:kDSDropspotSessionDomainKeyName];
    if ([sessionDict objectForKey:kDSDropspotSessionLastSyncKeyName]) {
        return [sessionDict objectForKey:kDSDropspotSessionLastSyncKeyName];
    }
    return nil;
}

- (void)setLastSyncDate:(NSDate *)lastSyncDate {
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary * sessionDict = [[defaults objectForKey:kDSDropspotSessionDomainKeyName] mutableCopy];
    if (!sessionDict) {
        sessionDict = [NSMutableDictionary dictionary];
    }
    if (lastSyncDate) {
        [sessionDict setObject:lastSyncDate forKey:kDSDropspotSessionLastSyncKeyName];
    } else {
        if ([sessionDict objectForKey:kDSDropspotSessionLastSyncKeyName]) {
            [sessionDict removeObjectForKey:kDSDropspotSessionLastSyncKeyName];
        }
    }
    [defaults setObject:sessionDict forKey:kDSDropspotSessionDomainKeyName];
    [defaults synchronize];
}

- (void)addLoadingStackObject:(NSString *)key {
    if (!loadingStack) {
        loadingStack = [NSMutableDictionary dictionary];
    }
    [loadingStack setObject:@"1" forKey:key];
    
    self.isLoading = YES;
}

- (void)removeFromLoadingStack:(NSString *)key {
    if (loadingStack) {
        [loadingStack removeObjectForKey:key];

        if ([loadingStack allKeys].count == 0) {
            self.isLoading = NO;
        }
    }
}

#pragma mark - Analytics

- (id<GAITracker>)tracker {
    if (!_tracker) {
        _tracker = [[GAI sharedInstance] trackerWithTrackingId:kGoogleAnalyticsTrackingID];
    }
    
    return _tracker;
}

- (void)analyticsView:(NSString *)viewString {
    if (![_screenName isEqualToString:viewString]) {
        _screenName = viewString;

        //[self.tracker set:kGAIScreenName value:viewString];
        //[self.tracker send:[[GAIDictionaryBuilder createAppView] build]];
        /*
        if ([DSUser currentUser].username) {
            [[Mixpanel sharedInstance] identify:[DSUser currentUser].username];
        }
        [[Mixpanel sharedInstance] track:[NSString stringWithFormat:@"Screen: %@", _screenName]];
         */
    }
}

- (void)analyticsEvent:(NSString *)eventString category:(NSString *)categoryString {
    [self analyticsEvent:eventString category:categoryString info:nil];
}

- (void)analyticsEvent:(NSString *)eventString category:(NSString *)categoryString info:(NSString *)info {
    [self analyticsEvent:eventString category:categoryString info:info number:nil];
}

- (void)analyticsEvent:(NSString *)eventString category:(NSString *)categoryString info:(NSString *)info number:(NSNumber *)number {
/*
    [self.tracker send:[[GAIDictionaryBuilder createEventWithCategory:categoryString
                                                               action:eventString
                                                                label:info
                                                                value:number] build]];
*/
    NSMutableDictionary * dict = [NSMutableDictionary dictionary];
    
    if (eventString) {
        if (categoryString) {
            [dict setValue:categoryString forKey:@"Category"];
        }
        if (info) {
            [dict setValue:info forKey:@"Info"];
        }
        if (number) {
            [dict setValue:[number stringValue] forKey:@"value"];
        }

        if ([DSUser currentUser].username) {
            [[Mixpanel sharedInstance] identify:[DSUser currentUser].username];
            [dict setValue:[DSUser currentUser].username forKey:@"Username"];
        }
        [[Mixpanel sharedInstance] track:eventString properties:dict];
    }
}

@end

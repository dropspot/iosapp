/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  User.m
//  Dropspot3
//
//  Created by Lukas Würzburger on 4/9/14.
//  Copyright (c) 2014 Dropspot GmbH. All rights reserved.
//

#import "User.h"

#import "Collection.h"
#import "Spot.h"

@implementation User

@dynamic username;
@dynamic displayName;
@dynamic photoFile;
@dynamic photoURL;
@dynamic collections;
@dynamic spots;

+ (User *)userWithUsername:(NSString *)username
    inManagedObjectContext:(NSManagedObjectContext *)context {
    User * user = nil;
    
    NSFetchRequest * request = [NSFetchRequest fetchRequestWithEntityName:@"UserPhoto"];
    request.predicate = [NSPredicate predicateWithFormat:@"username = %@", username];
    
    NSError * error;
    NSArray * matches = [context executeFetchRequest:request error:&error];
    
    if (!matches || matches.count > 1) {
        // Handle error
    } else if (matches.count == 1) {
        user = matches.lastObject;
    } else {
        user = [NSEntityDescription insertNewObjectForEntityForName:@"User" inManagedObjectContext:context];
    }
    
    return user;
}

@end

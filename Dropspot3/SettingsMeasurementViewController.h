/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  SettingsMeasurementViewController.h
//  Dropspot3
//
//  Created by Lukas Würzburger on 2/3/14.
//  Copyright (c) 2014 Dropspot. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsMeasurementViewController : UITableViewController

@end

/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  DSLocationDeniedViewController.h
//  SingleCollection
//
//  Created by Lukas Würzburger on 2/14/14.
//  Copyright (c) 2014 Lukas Würzburger. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DSLocationDeniedViewController : UIViewController

@end

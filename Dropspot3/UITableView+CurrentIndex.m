/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  UITableView+CurrentIndex.m
//  Dropspot3
//
//  Created by Lukas Würzburger on 01.10.13.
//  Copyright (c) 2013 Dropspot. All rights reserved.
//

#import "UITableView+CurrentIndex.h"

@implementation UITableView (CurrentIndex)

- (NSInteger)currentIndex {
    return self.contentOffset.y / self.frame.size.height;
}

@end

/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  DSSyncController.h
//  Dropspot3
//
//  Created by Lukas Würzburger on 2/7/14.
//  Copyright (c) 2014 Dropspot. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^CompletionBlock)(void);

@class DSSyncController, Collection, Spot;

@protocol DSSyncControllerDelegate <NSObject>

@required
/**
 * Called when syncing is finished. Maybe with errors.
 */
- (void)syncController:(DSSyncController *)syncController didFinishWithErrors:(NSArray *)errors;

@optional
/**
 * Called when a single collection were synced.
 */
- (void)syncController:(DSSyncController *)syncController didFinishLoadingCollection:(Collection *)collection;

/**
 * Called when a single spot were synced.
 */
- (void)syncController:(DSSyncController *)syncController didFinishLoadingSpot:(Spot *)spot;

@end

@interface DSSyncController : NSObject

/**
 * The object which receives delegation methods.
 */
@property (nonatomic, strong) id <DSSyncControllerDelegate> delegate;

/**
 * Returns a boolean wether the syncing is running or not.
 */
@property (nonatomic, assign, readonly) BOOL isRunning;

/**
 * Returns a boolean wether the finding of popular collections is running or not.
 */
@property (nonatomic, assign, readonly) BOOL isFindingPopularCollections;

/**
 * Returns the singleton class.
 */
+ (DSSyncController *)sharedController;

/**
 * Starts syncing with delegation methods.
 */
- (void)synchronize;

@end

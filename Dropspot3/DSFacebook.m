/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  DSFacebook.m
//  Dropspot3
//
//  Created by Lukas Würzburger on 11.09.13.
//  Copyright (c) 2013 Dropspot. All rights reserved.
//

#import "DSFacebook.h"

@implementation DSFacebook

+ (DSFacebook *)session {
    static DSFacebook * instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[DSFacebook alloc] init];
    });
    return instance;
}

- (id)init {
    self = [super init];
    if (self) {
        self.initialPermissions = @[@"email"];
        
    }
    return self;
}

- (void)setInitialPermissions:(NSArray *)initialPermissions {
    if (initialPermissions && initialPermissions.count > 0) {
        _initialPermissions = initialPermissions;
        return;
    }
    
    _initialPermissions = @[@"email"];
}

- (void)checkLoginState {
    self.isUserLoggedIn = [self openSession];
}

- (FBSessionState)sessionState {
    return [FBSession activeSession].state;
}

- (BOOL)openSession {
    return [FBSession openActiveSessionWithReadPermissions:self.initialPermissions allowLoginUI:YES completionHandler:^(FBSession *session, FBSessionState state, NSError *error) {
        [self.delegate sessionStateChanged:session state:state error:error];
    }];
}

- (void)closeSession {
    [[FBSession activeSession] closeAndClearTokenInformation];
}

- (void)handleDidBecomeActive {
    [[FBSession activeSession] handleDidBecomeActive];
}

- (BOOL)handleOpenURL:(NSURL *)url {
    return [FBSession.activeSession handleOpenURL:url];
}

#pragma mark - Error code description

+ (NSString *)FBErrorCodeDescription:(FBErrorCode)code {
    switch(code) {
        case FBErrorInvalid:{
            return @"FBErrorInvalid";
        }
        case FBErrorOperationCancelled:{
            return @"FBErrorOperationCancelled";
        }
        case FBErrorLoginFailedOrCancelled:{
            return @"FBErrorLoginFailedOrCancelled";
        }
        case FBErrorRequestConnectionApi:{
            return @"FBErrorRequestConnectionApi";
        }
        case FBErrorProtocolMismatch:{
            return @"FBErrorProtocolMismatch";
        }
        case FBErrorHTTPError:{
            return @"FBErrorHTTPError";
        }
        case FBErrorNonTextMimeTypeReturned:{
            return @"FBErrorNonTextMimeTypeReturned";
        }
        default:
            return @"[Unknown]";
    }
}

@end

/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  User+Dropspot.h
//  Dropspot3
//
//  Created by Lukas Würzburger on 4/9/14.
//  Copyright (c) 2014 Dropspot GmbH. All rights reserved.
//

#import "User.h"
#import "DSAPIUserResponse.h"

@interface User (Dropspot)

+ (User *)userWithAPIResponse:(DSAPIUserResponse *)response
       inManagedObjectContext:(NSManagedObjectContext *)context;

- (void)loadPhotoInManagedObjectContext:(NSManagedObjectContext *)context
                             completion:(void (^)(void))completion;

- (void)deletePhotoFile;

@end

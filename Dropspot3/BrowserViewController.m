/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  BrowserViewController.m
//  Dropspot3
//
//  Created by Lukas Würzburger on 12/20/13.
//  Copyright (c) 2013 Dropspot. All rights reserved.
//

#import "BrowserViewController.h"

#import "DSGlobal.h"
#import "GAIHeader.h"

@interface BrowserViewController () <UIWebViewDelegate, UIActionSheetDelegate> {
    IBOutlet UIWebView * theWebView;
    IBOutlet UIToolbar * theToolbar;
    IBOutlet UIBarButtonItem * backButton;
    IBOutlet UIBarButtonItem * forwardButton;
    IBOutlet UIBarButtonItem * reloadButton;
    IBOutlet UIBarButtonItem * actionButton;
    IBOutlet UIView * progressView;
}

@end

@implementation BrowserViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self loadURL];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[DSGlobal sharedGlobals] analyticsView:kDSScreenNameBrowserView];
    
}

- (void)updateToolbar {
    UIBarButtonItem * flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    [theToolbar setItems:@[flexSpace, backButton, flexSpace, forwardButton, flexSpace, reloadButton, flexSpace, actionButton, flexSpace] animated:NO];
}

- (void)setProgressViewHidden:(BOOL)hidden {
    if (hidden) {
        [UIView animateWithDuration:0.2 animations:^{
            CGRect frame = progressView.frame;
            frame.size.width = self.view.frame.size.width;
            progressView.frame = frame;
            
            progressView.alpha = 0.0;
        } completion:^(BOOL finished) {
            progressView.hidden = YES;
        }];
    } else {
        CGRect frame = progressView.frame;
        frame.size.width = 1;
        progressView.frame = frame;

        progressView.alpha = 0.0;
        progressView.hidden = NO;
        
        [UIView animateWithDuration:0.2 animations:^{
            CGRect frame = progressView.frame;
            frame.size.width = 50;
            progressView.frame = frame;
            
            progressView.alpha = 1.0;
        }];
    }
}

- (void)loadURL {

    if (self.url) {
    
        // Create request
        NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:self.url];
        [request setValue:@"http://dropspot.it" forHTTPHeaderField:@"Referer"];

        [theWebView loadRequest:request];
    }
}

#pragma mark - IBAction

- (IBAction)backButtonPressed:(id)sender {
    [theWebView goBack];
}

- (IBAction)forwardButtonPressed:(id)sender {
    [theWebView goForward];
}

- (IBAction)reloadButtonPressed:(id)sender {
    
    [self loadURL];
}

- (IBAction)actionButtonPressed:(id)sender {
    UIActionSheet * sheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Open in Safari", nil];
    [sheet showInView:self.view];
}

#pragma mark - Webview delegate

- (void)webViewDidStartLoad:(UIWebView *)webView {
    reloadButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemStop target:self action:@selector(reloadButtonPressed:)];
    
    [self updateToolbar];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    reloadButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(reloadButtonPressed:)];

    backButton.enabled = [webView canGoBack];
    forwardButton.enabled = [webView canGoForward];
    
    [self updateToolbar];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    if ([error code] != NSURLErrorCancelled) {
        [[[UIAlertView alloc] initWithTitle:@"Sorry" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    }
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    
    return YES;
}

#pragma mark - UIActionSheet delegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex  {
    if (buttonIndex == 0) {
        [[UIApplication sharedApplication] openURL:self.url];
    }
}

@end

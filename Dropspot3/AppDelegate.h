/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  SCAppDelegate.h
//  Dropspot3
//
//  Created by Lukas Würzburger on 2/19/14.
//  Copyright (c) 2014 Dropspot. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreTelephony/CTCallCenter.h>
#import <MediaPlayer/MediaPlayer.h>
#import <AudioToolbox/AudioToolbox.h>
#import <CoreLocation/CoreLocation.h>

#import "DSDropspot.h"
#import "DSFacebook.h"

#import "GAIHeader.h"

@class _MasterViewController, RootNavigationController;

@interface AppDelegate : UIResponder <UIApplicationDelegate, CLLocationManagerDelegate, DSDropspotDelegate, DSFacebookDelegate, MixpanelDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) RootNavigationController * rootNavigationController;
@property (strong, nonatomic) CLLocationManager * locationManager;
@property (strong, nonatomic) _MasterViewController * masterViewController;
@property (strong, nonatomic) Mixpanel * mixpanel;

@end

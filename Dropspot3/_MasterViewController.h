/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  _MasterViewController.h
//  Dropspot3
//
//  Created by Lukas Würzburger on 4/7/14.
//  Copyright (c) 2014 Dropspot GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

#import "Spot.h"

@class _MasterViewController;

@protocol MasterViewControllerDelegate <NSObject>

- (BOOL)masterViewControllerShouldRefreshInMainView:(_MasterViewController *)masterViewController;

@end

@interface _MasterViewController : UIViewController

@property (nonatomic, assign) id <MasterViewControllerDelegate> delegate;
@property (nonatomic, weak) CLLocationManager * locationManager;

- (void)checkSessionState;
- (void)refresh;
- (void)refreshManually:(BOOL)manually;
- (void)showSpot:(Spot *)spot;
- (void)updateLocation:(CLLocation *)location;

@end
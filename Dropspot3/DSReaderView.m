//
//  DSReaderView.m
//  Dropspot3
//
//  Created by Lukas Würzburger on 6/10/14.
//  Copyright (c) 2014 Dropspot GmbH. All rights reserved.
//

#import <MediaPlayer/MediaPlayer.h>

#import "DSReaderView.h"

#define kMargin 10

@interface DSReaderView () <AVSpeechSynthesizerDelegate> {
    
    // UI
    UILabel * textLabel;
    UIButton * stopButton;
    CGRect finalFrame;
    
    // Text
    NSString * _text;
    
    // Countdown
    NSTimer * countDownTimer;
    NSUInteger currentDelay;
    
    // User interaction
    UIPanGestureRecognizer * panGestureRecognizer;
    UITapGestureRecognizer * tapGestureRecognizer;
    CGPoint gestureStartPoint;
    
    VoidBlock completionBlock;
    VoidBlock tapBlock;
}

@property (nonatomic, strong) AVSpeechSynthesizer * audioSynthesizer;

@end

@implementation DSReaderView

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        
        finalFrame = frame;
        
        CGRect frame = self.frame;
        frame.origin.x = 330;
        self.frame = frame;
        
        [self initialize];
    }
    return self;
}

- (void)initialize {
    
    // UI Elements
    self.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.9];
    self.layer.cornerRadius = 4;
    self.layer.shadowColor = [UIColor blackColor].CGColor;
    self.layer.shadowOffset = CGSizeMake(0, 0);
    self.layer.shadowOpacity = 0.2;
    self.layer.shadowRadius = 3;
    
    stopButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.height, self.frame.size.height)];
    stopButton.backgroundColor = [UIColor clearColor];
    [stopButton setTitle:@"x" forState:UIControlStateNormal];
    [stopButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [stopButton addTarget:self action:@selector(stopButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:stopButton];
    
    textLabel = [[UILabel alloc] initWithFrame:CGRectMake(stopButton.frame.size.width, kMargin, self.frame.size.width - stopButton.frame.size.width - kMargin * 2, self.frame.size.height - kMargin * 2)];
    textLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    textLabel.backgroundColor = [UIColor clearColor];
    textLabel.textColor = [UIColor blackColor];
    textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:15];
    [self addSubview:textLabel];
    
    // Gestures
    panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePanGesture:)];
    [self addGestureRecognizer:panGestureRecognizer];

    tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapGesture:)];
    [self addGestureRecognizer:tapGestureRecognizer];
}

- (void)stop {
    
    // Stop speaking
    [self cancelAudioOutput];
    
    // Stop timer
    [countDownTimer invalidate];
    countDownTimer = nil;
    
    // Hide self
    [self finishAnimationWithHorizontalVelocity:0.0];
}

#pragma mark - Getters

- (AVSpeechSynthesizer *)audioSynthesizer {
    
    if (!_audioSynthesizer) {
        _audioSynthesizer = [[AVSpeechSynthesizer alloc] init];
        _audioSynthesizer.delegate = self;
    }
    
    return _audioSynthesizer;
}

#pragma mark - IB Actions

- (void)stopButtonPressed:(UIButton *)button {
    
    // Stop
    [self stop];
}

#pragma mark - Gestures

- (void)handleTapGesture:(UIGestureRecognizer *)gestureRecognizer {
    
    if (tapBlock) {
        tapBlock();
    }
    
    // Hide self
    [self finishAnimationWithHorizontalVelocity:0.0];
}

- (void)handlePanGesture:(UIPanGestureRecognizer *)gestureRecognizer {
    
    if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
        
        gestureStartPoint = [gestureRecognizer locationInView:self];
    }
    
    CGPoint newPoint = [gestureRecognizer locationInView:self.superview];
    
    double newX = newPoint.x - gestureStartPoint.x;
    
    CGRect frame = self.frame;
    if (newX >= finalFrame.origin.x) {
        frame.origin.x = newX;
    } else {
        frame.origin.x = finalFrame.origin.x - (finalFrame.origin.x - newX) / 3;
    }
    self.frame = frame;
    
    if (gestureRecognizer.state == UIGestureRecognizerStateCancelled ||
        gestureRecognizer.state == UIGestureRecognizerStateEnded) {
        
        CGPoint velocity = [gestureRecognizer velocityInView:self];
        if (velocity.x > 500) {
            
            // Cancel audio ouput
            [self stop];
        } else {
            
            // Reset animation
            [self resetAnimation];
        }
        
        gestureStartPoint = CGPointZero;
    }
}

#pragma mark - Animations

- (void)finishAnimationWithHorizontalVelocity:(double)horizontalVelocity {
    
    [UIView animateWithDuration:0.1 animations:^{
        
        CGRect frame = self.frame;
        frame.origin.x = self.superview.frame.size.width;
        self.frame = frame;
    } completion:^(BOOL finished) {
        
        if (finished) {
            
            // TODO: Remove from superview or delete object?
        }
    }];
}

- (void)resetAnimation {
    
    [UIView animateWithDuration:0.2 animations:^{
        
        // Reset position
        CGRect frame = self.frame;
        frame.origin.x = self.superview.frame.size.width - self.frame.size.width;
        self.frame = frame;
        
    } completion:^(BOOL finished) {
        
        if (finished) {
            
            
        }
    }];
}

#pragma mark - Audio Player

- (void)readText:(NSString *)text {
    
    // Forward
    [self readText:text delay:0 completion:nil tapGesture:nil];
}

- (void)readText:(NSString *)text delay:(NSUInteger)seconds completion:(VoidBlock)completion {
    
    // Forward
    [self readText:text delay:seconds completion:completion tapGesture:nil];
}

- (void)readText:(NSString *)text delay:(NSUInteger)seconds completion:(VoidBlock)completion tapGesture:(VoidBlock)tapGesture {
    
    _text = text;
    
    // Copy completion blocks
    completionBlock = [completion copy];
    tapBlock = [tapGesture copy];
    
    // Cancel previous playbacks
    [self cancelAudioOutput];
    
    [UIView animateWithDuration:0.2 delay:1.0 options:UIViewAnimationOptionCurveEaseIn animations:^{

        self.frame = finalFrame;
    } completion:nil];
    
    // Use timer when delay is set.
    if (seconds > 0) {

        currentDelay = seconds;
        
        if (countDownTimer) {
            [countDownTimer invalidate];
            countDownTimer = nil;
        }
        
        NSLog(@"Timer start");
        countDownTimer = [NSTimer scheduledTimerWithTimeInterval:1
                                                          target:self
                                                        selector:@selector(timerTick:)
                                                        userInfo:nil
                                                         repeats:YES];
        [self timerTick:countDownTimer];
    } else {
        
        [self startAudioOutput];
    }
}

- (void)timerTick:(NSTimer *)timer {
    NSLog(@"Timer tick %@", timer);

    textLabel.text = [NSString stringWithFormat:translate(@"READER_VIEW_READ_ALOUD_IN_SECONDS"), currentDelay];

    if (currentDelay == 0) {
        
        [self startAudioOutput];
        
        [countDownTimer invalidate];
        countDownTimer = nil;
    } else {
        
        currentDelay--;
    }
}

- (void)startAudioOutput {
    NSLog(@"start audio");
    
    // Activate audio session
    [[AVAudioSession sharedInstance] setActive:YES error:nil];
    NSError * error;
    BOOL success = [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:&error];
    if (!success) {
        NSLog(@"Error setting category: %@", error.localizedDescription);
    }
    
    // Create speech utterance
    AVSpeechUtterance * utterance = [AVSpeechUtterance speechUtteranceWithString:_text];
    [utterance setVoice:[AVSpeechSynthesisVoice voiceWithLanguage:@"DE-de"]];
    [utterance setRate:0.25f];
    
    // Start output
    [self.audioSynthesizer speakUtterance:utterance];
    
    // UI
    textLabel.text = translate(@"READER_VIEW_READING_OUT_LOUD");
}

- (void)cancelAudioOutput {
    
    if (_audioSynthesizer && _audioSynthesizer.isSpeaking) {

        // Stop current playback
        [_audioSynthesizer stopSpeakingAtBoundary:AVSpeechBoundaryImmediate];
    }
}

- (void)finishAudioOutput {
    
    // Deactivate audio session
    [[AVAudioSession sharedInstance] setActive:NO
                                   withOptions:AVAudioSessionSetActiveOptionNotifyOthersOnDeactivation
                                         error:nil];

    // Destroy synthesizer
    _audioSynthesizer = nil;
    
    // UI
    textLabel.text = translate(@"READER_VIEW_GO_TO_ARTICLE");
    
    // Completion
    if (completionBlock) {
        completionBlock();
    }
}

#pragma mark - Speech Synthesizer Delegate

- (void)speechSynthesizer:(AVSpeechSynthesizer *)synthesizer didFinishSpeechUtterance:(AVSpeechUtterance *)utterance {

    // Finish output
    [self finishAudioOutput];
}

- (void)speechSynthesizer:(AVSpeechSynthesizer *)synthesizer didCancelSpeechUtterance:(AVSpeechUtterance *)utterance {
    
    // Finish output
    [self finishAudioOutput];
}

@end

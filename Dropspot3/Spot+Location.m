/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  Spot+Location.m
//  Dropspot3
//
//  Created by Lukas Würzburger on 08.10.13.
//  Copyright (c) 2013 Dropspot. All rights reserved.
//

#import "Spot+Location.h"

#define kLatInterval -0.01
#define kLngInterval 0.025

@implementation Spot (Location)

- (CLLocation *)location {
    CLLocation * loc = [[CLLocation alloc] initWithLatitude:self.latitude longitude:self.longitude];
    return loc;
}

+ (NSArray *)spotsSortedByDistanceFromLocation:(CLLocation *)location
                                         limit:(int)limit
                        inManagedObjectContext:(NSManagedObjectContext *)context {
    return [self spotsSortedByDistanceFromLocation:location limit:limit shouldBeActive:NO inManagedObjectContext:context];
}

+ (NSArray *)spotsSortedByDistanceFromLocation:(CLLocation *)location
                                         limit:(int)limit
                                shouldBeActive:(BOOL)shouldBeActive
                        inManagedObjectContext:(NSManagedObjectContext *)context {

    NSArray * spots = [NSArray array];
    
    UIEdgeInsets insets = UIEdgeInsetsMake(location.coordinate.latitude,
                                           location.coordinate.longitude,
                                           location.coordinate.latitude,
                                           location.coordinate.longitude);
    
    int tries = 0;
    
    while (spots.count < limit && tries < 15) {
        tries++;

        insets = UIEdgeInsetsMake(insets.top - kLatInterval, insets.left - kLngInterval, insets.bottom + kLatInterval, insets.right + kLngInterval);
        
        spots = [self spotsInRect:insets limit:limit shouldBeActive:shouldBeActive inManagedObjectContext:context];
    }
    
    return [self sortSpots:spots byDistanceToLocation:location limit:limit];
}

+ (NSArray *)sortSpots:(NSArray *)spots byDistanceToLocation:(CLLocation *)location limit:(int)limit {
    NSMutableArray * array = [NSMutableArray array];
    for (Spot * spot in spots) {
        CLLocation * l = [[CLLocation alloc] initWithLatitude:spot.latitude longitude:spot.longitude];
        [array addObject:@{@"spot" : spot,
                           @"distance" : [NSNumber numberWithFloat:[l distanceFromLocation:location]]}];
    }
    NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"distance" ascending:YES];
    NSArray * sortedSpots = [array sortedArrayUsingDescriptors:@[descriptor]];
    NSMutableArray * limitedArray = [NSMutableArray array];
    for (NSDictionary * dict in sortedSpots) {
        [limitedArray addObject:dict[@"spot"]];
        if (limit > 0 && limitedArray.count >= limit) {
            break;
        }
    }

    return limitedArray;
}

+ (NSArray *)spotsInRect:(UIEdgeInsets)insets limit:(int)limit shouldBeActive:(BOOL)shouldBeActive inManagedObjectContext:(NSManagedObjectContext *)context {
    NSArray * result = [NSArray array];
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Spot"];
    if (shouldBeActive) {
        request.predicate = [NSPredicate predicateWithFormat:@"(latitude < %f and latitude > %f AND longitude > %f and longitude < %f) AND active = 1 AND (expiresAt = NULL OR expiresAt >= %@)", insets.top, insets.bottom, insets.left, insets.right, [NSDate date]];
    } else {
        request.predicate = [NSPredicate predicateWithFormat:@"latitude < %f and latitude > %f AND longitude > %f and longitude < %f", insets.top, insets.bottom, insets.left, insets.right];
    }
    
    NSError *error = nil;
    result = [context executeFetchRequest:request error:&error];
    if (error || !result) {
        NSLog(@"An error occured: %@", error);
    }
    
    return result;
}

@end

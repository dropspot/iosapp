/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  DataHandler.h
//  dropspotApp
//
//  Created by Fritz on 30.10.12.
//  Copyright (c) 2012 dropspot GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>

static NSString * DSURLToFileName(NSString * url) {
    NSString * fileName = url;
    fileName = [fileName stringByReplacingOccurrencesOfString:@"/" withString:@"_"];
    fileName = [fileName stringByReplacingOccurrencesOfString:@"?" withString:@"+"];
    fileName = [fileName stringByReplacingOccurrencesOfString:@"&" withString:@"+"];
    fileName = [fileName stringByReplacingOccurrencesOfString:@"%" withString:@","];
    fileName = [fileName stringByReplacingOccurrencesOfString:@":" withString:@"."];
    fileName = [fileName stringByReplacingOccurrencesOfString:@"#" withString:@"-"];
    return fileName;
}

@interface DataHelper : NSObject

+ (void)writeData:(NSData *)data toFile:(NSString *)fileName ifNotExists:(BOOL)shouldNotExists completion:(void (^)(BOOL success))completion;
+ (BOOL)writeData:(NSData *)data toFile:(NSString *)fileName ifNotExists:(BOOL)shouldNotExists;
+ (BOOL)writeData:(NSData *)data toDocumentFile:(NSString *)fileName ifNotExists:(BOOL)shouldNotExists;
+ (BOOL)writeData:(NSData *)data toTmpFile:(NSString *)fileName ifNotExists:(BOOL)shouldNotExists;
+ (BOOL)deleteFile:(NSString *)fileName;

// JSON
+ (NSDictionary *)dictionaryFromJSONData:(NSData *)jsonData;
+ (NSArray *)arrayFromJSONData:(NSData *)jsonData;
+ (NSArray *)arrayFromJSONString:(NSString *)jsonString;

// File Paths
+ (NSString *)pathForCacheFile:(NSString *)fileName;
+ (NSString *)pathForDocumentFile:(NSString *)fileName;
+ (NSString *)pathForBundleFile:(NSString *)fileName;

// URL Validation
+ (BOOL)validateUrl:(NSString *)candidate;

// Image
+ (void)cropImageFromData:(NSData *)imageData toSize:(CGSize)size blurIfNecessary:(BOOL)blur resultData:(void (^)(NSData *data))result;
+ (UIImage *)createSnapShotOfView:(UIView *)view;

// Acccessing images
+ (UIImage *)imageWithFilePath:(NSString *)filePath;
+ (void)imageWithFilePath:(NSString *)filePath block:(void (^)(UIImage * image))block;

// Preloaded contents
+ (BOOL)copyPreloadedFile:(NSString *)fileName;

@end

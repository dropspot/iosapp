/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  User+Session.h
//  Dropspot3
//
//  Created by Lukas Würzburger on 4/16/14.
//  Copyright (c) 2014 Dropspot GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    DSUserStateTemporary,
    DSUserStateTransitionToReal,
    DSUserStateReal
} DSUserState;

typedef enum {
    DSUserLoginTypeNone,
    DSUserLoginTypeNative,
    DSUserLoginTypeFacebook
} DSUserLoginType;

@interface DSUser : NSObject

@property (nonatomic, assign) DSUserState state;
@property (nonatomic, assign) DSUserLoginType loginType;

@property (nonatomic, strong) NSString * token;

@property (nonatomic, strong) NSString * username;
@property (nonatomic, strong) NSString * displayName;
@property (nonatomic, strong) UIImage * avatar;
@property (nonatomic, strong) NSDate * joined;

+ (DSUser *)currentUser;
+ (DSUser *)newUserWithToken:(NSString *)token;
+ (void)killCurrentUser;

@end

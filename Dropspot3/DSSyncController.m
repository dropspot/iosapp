/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  DSSyncController.m
//  Dropspot3
//
//  Created by Lukas Würzburger on 2/7/14.
//  Copyright (c) 2014 Dropspot. All rights reserved.
//

#import "DSSyncController.h"

#import "DSLocationController.h"
#import "DSDropspot.h"
#import "Collection+Dropspot.h"
#import "Spot+Dropspot.h"
#import "DSDownload.h"
#import "DSGlobal.h"
#import "DSGlobalData.h"
#import "DSServices.h"

#define kDSSyncControllerLoadStackKey @"DSSyncController-Syncing"

@interface DSSyncController () {
    NSMutableArray * _collections;
    NSMutableArray * _spots;
    NSMutableDictionary * _spotResponsesForCollections;
}

@property (nonatomic, strong) NSUserDefaults * defaults;

@end

@implementation DSSyncController

+ (DSSyncController *)sharedController {
    static DSSyncController * instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[DSSyncController alloc] init];
    });
    return instance;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(collectionPhotoLoadingFinishedNotification:) name:kCollectionPhotoLoadingFinished object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(spotPhotoLoadingFinishedNotification:) name:kSpotPhotoLoadingFinished object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userPhotoLoadingFinishedNotification:) name:kUserPhotoLoadingFinished object:nil];
    }
    return self;
}

- (NSUserDefaults *)defaults {
    if (!_defaults) {
        _defaults = [NSUserDefaults standardUserDefaults];
    }
    return _defaults;
}

#pragma mark - Notifications

- (void)collectionPhotoLoadingFinishedNotification:(NSNotification *)notification {
    //[[DSGlobalData sharedData].parentContext save:nil];
}

- (void)spotPhotoLoadingFinishedNotification:(NSNotification *)notification {
    //[[DSGlobalData sharedData].parentContext save:nil];
}

- (void)userPhotoLoadingFinishedNotification:(NSNotification *)notification {
    //[[DSGlobalData sharedData].parentContext save:nil];
}

#pragma mark - Syncing

- (void)synchronize {
    if (!self.isRunning) {
        _isRunning = YES;
        [[DSGlobal sharedGlobals] addLoadingStackObject:kDSSyncControllerLoadStackKey];

        __block NSMutableArray * errors = [NSMutableArray array];
        
        [self loadUserDetailsWithCompletion:^(NSError *error) {
            if (error) {
                [errors addObject:error];
                
                _isRunning = NO;
                [[DSGlobal sharedGlobals] removeFromLoadingStack:kDSSyncControllerLoadStackKey];
                
                if ([self.delegate respondsToSelector:@selector(syncController:didFinishWithErrors:)]) {
                    [self.delegate syncController:self didFinishWithErrors:errors];
                }
            } else if (kDSPreselectedAuthor) {
                
                _collections = [NSMutableArray array];
                _spots = [NSMutableArray array];
                _spotResponsesForCollections = [NSMutableDictionary dictionary];
                
                // Load my stuff
                [self loadCollectionsOfAuthor:kDSPreselectedAuthor withCompletion:^(NSError *error) {
                    if (error) {
                        [errors addObject:error];
                        
                        _isRunning = NO;
                        [[DSGlobal sharedGlobals] removeFromLoadingStack:kDSSyncControllerLoadStackKey];
                        
                        if ([self.delegate respondsToSelector:@selector(syncController:didFinishWithErrors:)]) {
                            [self.delegate syncController:self didFinishWithErrors:errors];
                        }
                    } else {
                        
                        [[DSGlobalData sharedData].childContext performBlock:^{
                            [self compareCollectionsInManagedObjectContext:[DSGlobalData sharedData].childContext];
                            [self compareSpotsInManagedObjectContext:[DSGlobalData sharedData].childContext];
                            [self compareSpotsForCollectionsInManagedObjectContext:[DSGlobalData sharedData].childContext];
                            
                            NSError * error = nil;
                            [[DSGlobalData sharedData].childContext save:&error];
                            if (error) {
                                NSLog(@"Core Data: Child context save error %@", error);
                            }
                            [_collections removeAllObjects];
                            [_spots removeAllObjects];
                            [_spotResponsesForCollections removeAllObjects];
                            
                            // Completion
                            dispatch_sync(dispatch_get_main_queue(), ^{
                                NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
                                NSMutableDictionary * sessionDict = [[defaults objectForKey:kDSDropspotSessionDomainKeyName] mutableCopy];
                                if ([sessionDict objectForKey:kDSDropspotSessionIsTmpUserKeyName] && [sessionDict objectForKey:kDSDropspotSessionIsRealUserKeyName]) {
                                    [sessionDict removeObjectForKey:kDSDropspotSessionIsTmpUserKeyName];
                                    [defaults setObject:sessionDict forKey:kDSDropspotSessionDomainKeyName];
                                    [defaults synchronize];
                                    
                                    [[Mixpanel sharedInstance] identify:[DSUser currentUser].username];
                                    [[Mixpanel sharedInstance] track:@"Late Sign In" properties:@{
                                                                                                  @"Username" : [DSUser currentUser].username
                                                                                                  }];
                                    [[Mixpanel sharedInstance] flush];
                                }
                                

                                _isRunning = NO;
                                [[DSGlobal sharedGlobals] removeFromLoadingStack:kDSSyncControllerLoadStackKey];
                                
                                [[DSGlobalData sharedData].parentContext save:nil];
                                
                                if ([self.delegate respondsToSelector:@selector(syncController:didFinishWithErrors:)]) {
                                    [self.delegate syncController:self didFinishWithErrors:errors];
                                }
                            });
                        }];
                    }
                }];
            } else if (kDSOwnCollectionApp) {
                
                _spotResponsesForCollections = [NSMutableDictionary dictionary];
                
                // Load my stuff
                [self loadOwnCollectionWithCompletion:^(NSError *error) {
                    if (error) {
                        [errors addObject:error];
                        
                        _isRunning = NO;
                        [[DSGlobal sharedGlobals] removeFromLoadingStack:kDSSyncControllerLoadStackKey];
                        
                        if ([self.delegate respondsToSelector:@selector(syncController:didFinishWithErrors:)]) {
                            [self.delegate syncController:self didFinishWithErrors:errors];
                        }
                    } else {
                        
                        [[DSGlobalData sharedData].childContext performBlock:^{
                            [[DSGlobalData sharedData].childContext save:nil];
                            
                            [self compareSpotsForCollectionsInManagedObjectContext:[DSGlobalData sharedData].childContext];
                            [_spotResponsesForCollections removeAllObjects];
                            _spotResponsesForCollections = nil;
                            
                            dispatch_sync(dispatch_get_main_queue(), ^{
                                [[DSGlobalData sharedData].parentContext save:nil];
                                
                                _isRunning = NO;
                                [[DSGlobal sharedGlobals] removeFromLoadingStack:kDSSyncControllerLoadStackKey];
                                
                                if ([self.delegate respondsToSelector:@selector(syncController:didFinishWithErrors:)]) {
                                    [self.delegate syncController:self didFinishWithErrors:errors];
                                }
                            });
                        }];
                    }
                }];
            } else if (kIsSingleCollectionApp) {
                // Load stuff for a single collection
                [self loadSingleCollectionWithCompletion:^(NSError *error) {
                    if (error) {
                        [errors addObject:error];
                        
                        _isRunning = NO;
                        [[DSGlobal sharedGlobals] removeFromLoadingStack:kDSSyncControllerLoadStackKey];
                        
                        if ([self.delegate respondsToSelector:@selector(syncController:didFinishWithErrors:)]) {
                            [self.delegate syncController:self didFinishWithErrors:errors];
                        }
                    } else {
                        _isRunning = NO;
                        
                        [[DSGlobalData sharedData].childContext performBlock:^{
                            [[DSGlobalData sharedData].childContext save:nil];
                            
                            dispatch_sync(dispatch_get_main_queue(), ^{
                                [[DSGlobalData sharedData].parentContext save:nil];
                                
                                [[DSGlobal sharedGlobals] removeFromLoadingStack:kDSSyncControllerLoadStackKey];
                                
                                if ([self.delegate respondsToSelector:@selector(syncController:didFinishWithErrors:)]) {
                                    [self.delegate syncController:self didFinishWithErrors:errors];
                                }
                            });
                        }];
                    }
                }];
                
            } else {
                
                _collections = [NSMutableArray array];
                _spots = [NSMutableArray array];
                _spotResponsesForCollections = [NSMutableDictionary dictionary];
                
                // Load stuff for mom
                [self loadMotherStuffWithCompletion:^(NSError *error) {
                    if (error) {
                        [errors addObject:error];
                        
                        _isRunning = NO;
                        [[DSLog sharedLog] logWithUser:@"lukas" event:@"Syncing (End)"];

                        [[DSGlobal sharedGlobals] removeFromLoadingStack:kDSSyncControllerLoadStackKey];
                        
                        if ([self.delegate respondsToSelector:@selector(syncController:didFinishWithErrors:)]) {
                            [self.delegate syncController:self didFinishWithErrors:errors];
                        }
                    } else {
                        
                        NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
                        NSMutableDictionary * sessionDict = [defaults objectForKey:kDSDropspotSessionDomainKeyName];
                        
                        if ([sessionDict objectForKey:kDSDropspotSessionIsTmpUserKeyName] && [sessionDict objectForKey:kDSDropspotSessionIsRealUserKeyName]) {
                            // subscribe old shit
                            NSLog(@"subscribe old shit");
                            [[DSGlobalData sharedData].childContext performBlock:^{
                                NSFetchRequest * fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"Collection"];
                                fetchRequest.predicate = [NSPredicate predicateWithFormat:@"subscribed = 1"];
                                __block NSArray * matches = [[DSGlobalData sharedData].childContext executeFetchRequest:fetchRequest error:nil];
                                if (matches && matches.count) {
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        
                                        [self subscribeOldCollections:matches completion:^{
                                            [DSUser currentUser].state = DSUserStateReal;
                                            
                                            _isRunning = NO;
                                            [self synchronize];
                                        }];
                                    });
                                } else {
                                    [DSUser currentUser].state = DSUserStateReal;
                                    
                                    if ([self.delegate respondsToSelector:@selector(syncController:didFinishWithErrors:)]) {
                                        [self.delegate syncController:self didFinishWithErrors:errors];
                                    }
                                }
                            }];
                        } else {
                            [[DSGlobalData sharedData].childContext performBlock:^{
                                [self checkPreselectedCollectionsLoaded:NO inManagedObjectContext:[DSGlobalData sharedData].childContext completion:^{
                                    _isRunning = NO;
                                    [[DSLog sharedLog] logWithUser:@"lukas" event:@"Syncing (End) success"];
                                    
                                    [[DSGlobal sharedGlobals] removeFromLoadingStack:kDSSyncControllerLoadStackKey];
                                    
                                    [[DSGlobalData sharedData].childContext performBlock:^{
                                        [[DSGlobalData sharedData].childContext save:nil];
                                    }];
                                    
                                    [[NSNotificationCenter defaultCenter] postNotificationName:kDSDropspotSyncingFinished object:nil];
                                    
                                    if ([self.delegate respondsToSelector:@selector(syncController:didFinishWithErrors:)]) {
                                        [self.delegate syncController:self didFinishWithErrors:errors];
                                    }
                                }];
                            }];
                        }
                    }
                }];
            }
        }];
    }
}

- (void)subscribeOldCollections:(NSArray *)collections completion:(void (^)(void))completion {
    
    __block int numberOfSubscriptions = collections.count;
    
    if (collections.count > 0) {
        for (Collection * collection in collections) {
            [[DSDropspot session] subscribeToCollectionWithSlug:collection.slug block:^(NSDictionary *result, NSError *error) {
                numberOfSubscriptions--;
                
                if (numberOfSubscriptions == 0) {
                    if (completion) {
                        completion();
                    }
                }
            }];
        }
    } else {
        if (completion) {
            completion();
        }
    }
}

- (void)checkPreselectedCollectionsLoaded:(BOOL)preselectedCollectionsLoaded inManagedObjectContext:(NSManagedObjectContext *)context completion:(void (^)(void))completion {
    if (!preselectedCollectionsLoaded && _collections.count == 0 && _spots.count == 0 && _spotResponsesForCollections.count == 0) {
        
        if (kDSPreselectedCollections) {

            NSDictionary * dictionary = kDSPreselectedCollections;
            NSString * langKey = [[NSLocale preferredLanguages] objectAtIndex:0];
            
            NSArray * array = [dictionary objectForKey:langKey];
            
            if (array) {
                [self loadCollectionsWithSlugs:array completion:^(NSError *error) {
                    [context performBlock:^{
                        [self checkPreselectedCollectionsLoaded:YES inManagedObjectContext:context completion:completion];
                    }];
                }];
            }
        } else {
            dispatch_sync(dispatch_get_main_queue(), ^{
                if (completion) {
                    completion();
                }
            });
        }
    } else {
        [self compareCollectionsInManagedObjectContext:context];
        [self compareSpotsInManagedObjectContext:context];
        [self compareSpotsForCollectionsInManagedObjectContext:context];
        
        NSError * error = nil;
        [context save:&error];
        if (error) {
            NSLog(@"Core Data: Child context save error %@", error);
        }
        [_collections removeAllObjects];
        [_spots removeAllObjects];
        [_spotResponsesForCollections removeAllObjects];
        
        // Completion
        dispatch_sync(dispatch_get_main_queue(), ^{
            if (completion) {
                completion();
            }
        });
    }
}

- (void)compareCollectionsInManagedObjectContext:(NSManagedObjectContext *)context {
    NSFetchRequest * request = [NSFetchRequest fetchRequestWithEntityName:@"Collection"];
    request.returnsObjectsAsFaults = NO;
    request.predicate = [NSPredicate predicateWithFormat:@"isSynced = 1"];
    NSSortDescriptor * slugSortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"slug" ascending:YES];
    request.sortDescriptors = @[slugSortDescriptor];
    
    NSError * error = nil;
    NSArray * matches = [context executeFetchRequest:request error:&error];
    
    if (error || !matches) {
        // Handle error...
    } else {
        NSMutableArray * local = [matches mutableCopy];
        NSMutableArray * remote = [[_collections sortedArrayUsingDescriptors:@[slugSortDescriptor]] mutableCopy];
        
        if (remote.count == 0) {
            for (Collection * collection in local) {
                [context deleteObject:collection];
            }
            [local removeAllObjects];
        } else {
            int i;
            for (i = 0; i < remote.count; i++) {
                DSAPICollectionResponse * collectionResponse = remote[i];
                
                if (i < local.count) {
                    Collection * collection = local[i];
                    
                    if ([collectionResponse.slug isEqualToString:collection.slug]) {
                        [collection updateWithAPIResponse:collectionResponse inManagedObjectContext:context];
                        [self collectionReady:collection];
                    } else {
                        NSArray * c = @[collection.slug, collectionResponse.slug];
                        NSArray * orderedC = [c sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
                        if ([orderedC[0] isEqualToString:collectionResponse.slug]) {
                            Collection * collection = [Collection collectionWithAPIResponse:collectionResponse inManagedObjectContext:context];
                            [local insertObject:collection atIndex:i];
                            [self collectionReady:collection];
                        } else {
                            [local removeObject:collection];
                            [context deleteObject:collection];
                            
                            i--;
                        }
                    }
                } else {
                    Collection * collection = [Collection collectionWithAPIResponse:collectionResponse inManagedObjectContext:context];
                    [local addObject:collection];
                    [self collectionReady:collection];
                }
            }
            if (local.count > remote.count) {
                for (int j = (int)remote.count; j < local.count; j++) {
                    Collection * collection = local[j];
                    [local removeObject:collection];
                    [context deleteObject:collection];
                    
                    j--;
                }
            }
        }
    }
}

- (void)compareSpotsInManagedObjectContext:(NSManagedObjectContext *)context {
    NSFetchRequest * request = [NSFetchRequest fetchRequestWithEntityName:@"Spot"];
    request.returnsObjectsAsFaults = NO;
    request.predicate = [NSPredicate predicateWithFormat:@"isSynced = 1 && collections.@count == 0"];
    NSSortDescriptor * slugSortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"slug" ascending:YES];
    request.sortDescriptors = @[slugSortDescriptor];
    
    NSError * error = nil;
    NSArray * matches = [context executeFetchRequest:request error:&error];
    
    if (error || !matches) {
        // Handle error...
    } else {
        NSMutableArray * local = [matches mutableCopy];
        NSMutableArray * remote = [[_spots sortedArrayUsingDescriptors:@[slugSortDescriptor]] mutableCopy];

        if (remote.count == 0) {
            for (Spot * spot in local) {
                [context deleteObject:spot];
            }
            [local removeAllObjects];
        } else {
            int i;
            for (i = 0; i < remote.count; i++) {
                DSAPISpotResponse * spotResponse = remote[i];
                
                if (i < local.count) {
                    Spot * spot = local[i];
                    
                    if ([spotResponse.slug isEqualToString:spot.slug]) {
                        [spot updateWithAPIResponse:spotResponse inManagedObjectContext:context];
                        [self spotReady:spot];
                    } else {
                        NSArray * c = @[spot.slug, spotResponse.slug];
                        NSArray * orderedC = [c sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
                        if ([orderedC[0] isEqualToString:spotResponse.slug]) {
                            Spot * spot = [Spot spotWithAPIResponse:spotResponse inManagedObjectContext:context];
                            [local insertObject:spot atIndex:i];
                            [self spotReady:spot];
                        } else {
                            [local removeObject:spot];
                            [context deleteObject:spot];
                            
                            i--;
                        }
                    }
                } else {
                    Spot * spot = [Spot spotWithAPIResponse:spotResponse inManagedObjectContext:context];
                    [local addObject:spot];
                    [self spotReady:spot];
                }
            }
            if (local.count > remote.count) {
                for (int j = (int)remote.count; j < local.count; j++) {
                    Spot * spot = local[j];

                    [local removeObject:spot];
                    [context deleteObject:spot];
                    j--;
                }
            }
        }
    }
}

- (void)compareSpotsForCollectionsInManagedObjectContext:(NSManagedObjectContext *)context {
    
    for (NSString * cSlug in _spotResponsesForCollections.allKeys) {
        
        NSFetchRequest * request = [NSFetchRequest fetchRequestWithEntityName:@"Spot"];
        request.predicate = [NSPredicate predicateWithFormat:@"isSynced = 1 && ANY collections.slug = %@", cSlug];
        request.returnsObjectsAsFaults = NO;
        NSSortDescriptor * slugSortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"slug" ascending:YES];
        request.sortDescriptors = @[slugSortDescriptor];
        
        NSError * error = nil;
        NSArray * matches = [context executeFetchRequest:request error:&error];
        
        if (error || !matches) {
            // Handle error...
        } else {
            
            NSArray * spots = _spotResponsesForCollections[cSlug];
            
            NSMutableArray * local = [matches mutableCopy];
            NSMutableArray * remote = [[spots sortedArrayUsingDescriptors:@[slugSortDescriptor]] mutableCopy];
            
            if (remote.count == 0) {
                for (Spot * spot in local) {
                    [context deleteObject:spot];
                }
                [local removeAllObjects];
            } else {
                int i;
                for (i = 0; i < remote.count; i++) {
                    DSAPISpotResponse * spotResponse = remote[i];
                    
                    if (i < local.count) {
                        Spot * spot = local[i];
                        
                        if ([spotResponse.slug isEqualToString:spot.slug]) {
                            [spot updateWithAPIResponse:spotResponse inManagedObjectContext:context];
                            [self spotReady:spot];
                        } else {
                            NSArray * c = @[spot.slug, spotResponse.slug];
                            NSArray * orderedC = [c sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
                            if ([orderedC[0] isEqualToString:spotResponse.slug]) {
                                Spot * spot = [Spot spotWithAPIResponse:spotResponse inManagedObjectContext:context];
                                [local insertObject:spot atIndex:i];
                                [self spotReady:spot];
                            } else {
                                [local removeObject:spot];
                                [context deleteObject:spot];
                            }
                            
                            i--;
                        }
                    } else {
                        Spot * spot = [Spot spotWithAPIResponse:spotResponse inManagedObjectContext:context];
                        [local addObject:spot];
                        [self spotReady:spot];
                        
                        i--;
                    }
                }
                if (local.count > remote.count) {
                    for (int j = i; j < local.count; j++) {
                        Spot * spot = local[j];
                        [local removeObject:spot];
                        [context deleteObject:spot];
                    }
                }
            }
        }
    }
}

- (void)collectionReady:(Collection *)collection {
    if ([NSThread isMainThread]) {
        if ([self.delegate respondsToSelector:@selector(syncController:didFinishLoadingCollection:)]) {
            [self.delegate syncController:self didFinishLoadingCollection:collection];
        }
    } else {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self collectionReady:collection];
        });
    }
}

- (void)spotReady:(Spot *)spot {
    if ([NSThread isMainThread]) {
        if ([self.delegate respondsToSelector:@selector(syncController:didFinishLoadingSpot:)]) {
            [self.delegate syncController:self didFinishLoadingSpot:spot];
        }
    } else {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self spotReady:spot];
        });
    }
}

- (void)loadMotherStuffWithCompletion:(void(^)(NSError * error))completion {
    NSMutableDictionary * sessionDict = [[self.defaults objectForKey:kDSDropspotSessionDomainKeyName] mutableCopy];
    
    // Load my collections...
    [[DSDropspot session] collectionsWithURL:[sessionDict objectForKey:kDSDropspotSessionCollectionsURLKeyName] paginatedWithBlock:^(DSAPICollectionResponse *collectionResponse) {
        collectionResponse.isOwnedByMe = YES;
        [_collections addObject:collectionResponse];
    } completion:^(NSError *error) {

        // Load my subscribed collections...
        [[DSDropspot session] subscriptionsWithURL:[sessionDict objectForKey:kDSDropspotSessionSubscriptionsURLKeyName] paginatedWithBlock:^(DSAPICollectionResponse *collectionResponse) {
            collectionResponse.subscribed = YES;
            BOOL exists = NO;
            for (DSAPICollectionResponse * cr in _collections) {
                if ([cr.slug isEqualToString:collectionResponse.slug]) {
                    exists = YES;
                    break;
                }
            }
            if (!exists) {
                [_collections addObject:collectionResponse];
            }
        } completion:^(NSError *error) {
            
            // Load my spots
            [[DSDropspot session] spotsWithURL:[sessionDict objectForKey:kDSDropspotSessionSpotsURLKeyName] paginatedWithBlock:^(DSAPISpotResponse *spotResponse) {
                spotResponse.isOwnedByMe = YES;
                if ([spotResponse.collectionSlugs isEqualToString:@""]) {
                    [_spots addObject:spotResponse];
                } else {
                    NSArray * slugs = [spotResponse.collectionSlugs componentsSeparatedByString:@","];
                    for (NSString * slug in slugs) {
                        if (![_spotResponsesForCollections objectForKey:slug]) {
                            [_spotResponsesForCollections setObject:[NSMutableArray array] forKey:slug];
                        }
                        [_spotResponsesForCollections[slug] addObject:spotResponse];
                    }
                }
            } completion:^(NSError *error) {
                
                // Load spots of subscribed collections
                [self loadSpotsOfSubscriptionsWithCompletion:^{
                    if (completion) {
                        completion(error);
                    }
                }];
            }];
        }];
    }];
}

- (void)loadCollectionsWithSlugs:(NSArray *)slugs completion:(void(^)(NSError * error))completion {
    
    __block int numberOfCollections = slugs.count;
    
    if (numberOfCollections > 0) {
        
        for (NSString * slug in slugs) {
            NSString * url = kCollectionURLWithSlug(slug);
            
            [[DSDropspot session] collectionWithURL:url success:^(DSAPICollectionResponse *collectionResponse) {
                collectionResponse.subscribed = YES;
                [_collections addObject:collectionResponse];
                
                [[DSDropspot session] subscribeToCollectionWithSlug:slug block:^(NSDictionary *result, NSError *error) {
                    numberOfCollections--;
                    
                    if (numberOfCollections == 0) {
                        [self loadSpotsOfSubscriptionsWithCompletion:^{
                            if (completion) {
                                completion(nil);
                            }
                        }];
                    }
                }];
            } completion:^(NSError *error) {
                if (error) {
                    numberOfCollections--;
                    
                    if (numberOfCollections == 0) {
                        [self loadSpotsOfSubscriptionsWithCompletion:^{
                            if (completion) {
                                completion(nil);
                            }
                        }];
                    }
                }
            }];
        }
    } else {

        [self loadSpotsOfSubscriptionsWithCompletion:^{
            if (completion) {
                completion(nil);
            }
        }];
    }
}

- (void)loadSingleCollectionWithCompletion:(void(^)(NSError * error))completion {
    NSMutableDictionary * sessionDict = [[self.defaults objectForKey:kDSDropspotSessionDomainKeyName] mutableCopy];
    
    [[DSDropspot session] collectionWithURL:kCollectionURL success:^(DSAPICollectionResponse *collectionResponse) {
        [[DSGlobalData sharedData].childContext performBlock:^{
            Collection * collection = [Collection collectionWithAPIResponse:collectionResponse inManagedObjectContext:[DSGlobalData sharedData].childContext];
        
            //[[DSGlobalData sharedData].childContext save:nil];
            
            __block BOOL isSubscribed = NO;
        
            // Load subscribed collections...
            dispatch_async(dispatch_get_main_queue(), ^{
                if ([self.delegate respondsToSelector:@selector(syncController:didFinishLoadingSpot:)]) {
                    [self.delegate syncController:self didFinishLoadingCollection:collection];
                }
                [[DSDropspot session] subscriptionsWithURL:[sessionDict objectForKey:kDSDropspotSessionSubscriptionsURLKeyName] paginatedWithBlock:^(DSAPICollectionResponse *collectionResponse) {
                    [[DSGlobalData sharedData].childContext performBlock:^{
                        if ([collectionResponse.slug isEqualToString:kCollectionSlug]) {
                            collection.subscribed = YES;
                            //[[DSGlobalData sharedData].childContext save:nil];
                            isSubscribed = YES;
                        }
                    }];
                } completion:^(NSError *error) {
                    if (!isSubscribed) {
                        
                        // Subscribe to this collection
                        [[DSDropspot session] subscribeToCollectionWithSlug:kCollectionSlug block:nil];
                    }
                    
                    [[DSDropspot session] spotsWithURL:collectionResponse.spotsURL paginatedWithBlock:^(DSAPISpotResponse *spotResponse) {
                        [[DSGlobalData sharedData].childContext performBlock:^{
                            Spot * spot = [Spot spotWithAPIResponse:spotResponse inManagedObjectContext:[DSGlobalData sharedData].childContext];
                            
                            NSMutableSet * set = [spot.collections mutableCopy];
                            [set addObject:collection];
                            spot.collections = set;
                            NSLog(@"set .collections relationship");
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                if ([self.delegate respondsToSelector:@selector(syncController:didFinishLoadingSpot:)]) {
                                    [self.delegate syncController:self didFinishLoadingSpot:spot];
                                }
                            });
                        }];
                    } completion:^(NSError *error) {
                        if (completion) {
                            completion(error);
                        }
                    }];
                }];
            });

        }];
    } completion:^(NSError *error) {
        if (completion) {
            completion(error);
        }
    }];
}

- (void)createCollectionWithCompletion:(void(^)(NSError * error))completion {
    NSMutableDictionary * sessionDict = [[self.defaults objectForKey:kDSDropspotSessionDomainKeyName] mutableCopy];
    
    [[DSGlobalData sharedData].childContext performBlock:^{
        Collection * myCollection = [NSEntityDescription insertNewObjectForEntityForName:@"Collection"
                                                              inManagedObjectContext:[DSGlobalData sharedData].childContext];
        myCollection.slug = [NSString stringWithFormat:@"%@-%i", [sessionDict objectForKey:kDSDropspotSessionUsernameKeyName], (int)[NSDate date].timeIntervalSince1970];
        myCollection.title = [NSString stringWithFormat:@"%@'s %@", [sessionDict objectForKey:kDSDropspotSessionNameKeyName], kDSOwnCollectionSuffix];
    
        //[[DSGlobalData sharedData].childContext save:nil];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [[DSDropspot session] createCollection:myCollection block:^(DSAPICollectionResponse *collection, NSError *error) {
                [[DSGlobalData sharedData].childContext performBlock:^{
                    [[DSGlobalData sharedData].childContext deleteObject:myCollection];
                    //[[DSGlobalData sharedData].childContext save:nil];
                }];
                
                if (!error) {
                    [[DSGlobalData sharedData].childContext performBlock:^{
                        Collection * newCollection = [Collection collectionWithAPIResponse:collection
                                                                    inManagedObjectContext:[DSGlobalData sharedData].childContext];
                        
                        //[[DSGlobalData sharedData].childContext save:nil];
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [sessionDict setObject:newCollection.slug forKey:kDSDropspotSessionCollectionSlugNameKeyName];
                            [self.defaults setObject:sessionDict forKey:kDSDropspotSessionDomainKeyName];
                            [self.defaults synchronize];
                            
                            if ([self.delegate respondsToSelector:@selector(syncController:didFinishLoadingCollection:)]) {
                                [self.delegate syncController:self didFinishLoadingCollection:newCollection];
                            }
                            
                            [self loadOwnCollectionWithCompletion:^(NSError *error) {
                                if (completion) {
                                    completion(error);
                                }
                            }];
                        });
                    }];
                } else {
                    if (completion) {
                        completion(error);
                    }
                }
            }];
        });

    }];
}

- (void)loadUserDetailsWithCompletion:(void(^)(NSError * error))completion {
    [[DSDropspot session] profileWithBlock:^(DSAPIProfileResponse *profile, NSError *error) {
        if (!error) {
            BOOL shouldLoadUserImage = NO;
            
            // Save name and username to user defaults.
            NSMutableDictionary * sessionDict = [[self.defaults objectForKey:kDSDropspotSessionDomainKeyName] mutableCopy];

            if (!sessionDict) {
                sessionDict = [NSMutableDictionary dictionary];
                
                [sessionDict setObject:profile.username forKey:kDSDropspotSessionUsernameKeyName];
                [sessionDict setObject:profile.name forKey:kDSDropspotSessionNameKeyName];
                [sessionDict setObject:profile.primaryAvatarURL forKey:kDSDropspotSessionPrimaryAvatarURLKeyName];
                [sessionDict setObject:profile.collectionsURL forKey:kDSDropspotSessionCollectionsURLKeyName];
                [sessionDict setObject:profile.spotsURL forKey:kDSDropspotSessionSpotsURLKeyName];
                [sessionDict setObject:profile.subscriptionsURL forKey:kDSDropspotSessionSubscriptionsURLKeyName];
                
                [self.defaults setObject:sessionDict forKey:kDSDropspotSessionDomainKeyName];
                [self.defaults synchronize];
            } else {
                BOOL changes = NO;
                if (![profile.username isEqualToString:[sessionDict objectForKey:kDSDropspotSessionUsernameKeyName]]) {
                    [sessionDict setObject:profile.username forKey:kDSDropspotSessionUsernameKeyName];
                    changes = YES;
                }
                if (![profile.name isEqualToString:[sessionDict objectForKey:kDSDropspotSessionNameKeyName]]) {
                    [sessionDict setObject:profile.name forKey:kDSDropspotSessionNameKeyName];
                    changes = YES;
                }
                if (![profile.collectionsURL isEqualToString:[sessionDict objectForKey:kDSDropspotSessionCollectionsURLKeyName]]) {
                    [sessionDict setObject:profile.collectionsURL forKey:kDSDropspotSessionCollectionsURLKeyName];
                    changes = YES;
                }
                if (![profile.spotsURL isEqualToString:[sessionDict objectForKey:kDSDropspotSessionSpotsURLKeyName]]) {
                    [sessionDict setObject:profile.spotsURL forKey:kDSDropspotSessionSpotsURLKeyName];
                    changes = YES;
                }
                if (![profile.subscriptionsURL isEqualToString:[sessionDict objectForKey:kDSDropspotSessionSubscriptionsURLKeyName]]) {
                    [sessionDict setObject:profile.subscriptionsURL forKey:kDSDropspotSessionSubscriptionsURLKeyName];
                    changes = YES;
                }
                if (![profile.primaryAvatarURL isEqualToString:[sessionDict objectForKey:kDSDropspotSessionPrimaryAvatarURLKeyName]]) {
                    if (profile.primaryAvatarURL) {
                        [sessionDict setObject:profile.primaryAvatarURL forKey:kDSDropspotSessionPrimaryAvatarURLKeyName];
                    } else {
                        [sessionDict removeObjectForKey:kDSDropspotSessionPrimaryAvatarURLKeyName];
                    }
                    changes = YES;
                    
                    shouldLoadUserImage = YES;
                }
                NSLog(@"sesstion dict %@", sessionDict);
                
                if (changes) {
                    [self.defaults setObject:sessionDict forKey:kDSDropspotSessionDomainKeyName];
                    [self.defaults synchronize];
                }
            }
            
            if (!shouldLoadUserImage) {
                if ([sessionDict objectForKey:kDSDropspotSessionPrimaryAvatarURLKeyName] && ![sessionDict objectForKey:kDSDropspotSessionImageKeyName]) {
                    [self loadUserAvatarWithCompletion:nil];
                }
            } else {
                [self loadUserAvatarWithCompletion:nil];
            }
            
            // Notify that user information has changed.
            [[NSNotificationCenter defaultCenter] postNotificationName:kDSDropspotSessionUserChangedNotificationName object:nil];
            
            if ([DSUser currentUser].loginType == DSUserLoginTypeFacebook) {
                
                // Check if this is a new user.
                if ([self.defaults objectForKey:kDSFacebookLoginButtonPressedDateKey]) {
                    NSDate * buttonPressDate = [self.defaults objectForKey:kDSFacebookLoginButtonPressedDateKey];
                    //buttonPressDate
                    
                    
                    NSDateFormatter * dateFormatter = [[NSDateFormatter alloc] init];
                    dateFormatter.dateFormat = DRPSPT_DATE_FORMAT;
                    dateFormatter.timeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
                    
                    NSDate * dateJoined = [dateFormatter dateFromString:profile.dateJoined];
                    
                    if (dateJoined && buttonPressDate) {
                        if ([buttonPressDate compare:dateJoined] != NSOrderedDescending) {
                            
                            // Save information
                            //[[DSGlobal sharedGlobals] analyticsEvent:kDSEventNameFBSignUpSuccess category:kDSCategoryNameOnboarding];
                            
                            [[DSDropspot session] profileWithBlock:^(DSAPIProfileResponse *profile, NSError *error) {
                                Mixpanel * mixPanel = [Mixpanel sharedInstance];
                                [mixPanel identify:profile.username];
                                [mixPanel.people set:@{
                                                       @"Sign Up Type" : @"Facebook",
                                                       @"Username" : profile.username
                                                       }];
                                [mixPanel track:@"$signup" properties:@{
                                                                        @"Sign Up Type" : @"Facebook",
                                                                        @"Username" : profile.username
                                                                        }];
                                [mixPanel flush];
                            }];
                            
                            
                            
                            NSLog(@"remove defaults");
                            // Change defaults
                            [self.defaults removeObjectForKey:kDSFacebookLoginButtonPressedDateKey];
                            [self.defaults synchronize];
                        }
                    }
                }
            }
            
            if (completion) {
                completion(nil);
            }
        } else {
            if (completion) {
                completion(error);
            }
        }
    }];
}

- (void)loadUserAvatarWithCompletion:(void(^)(NSError *))completion {
    NSMutableDictionary * sessionDict = [[self.defaults objectForKey:kDSDropspotSessionDomainKeyName] mutableCopy];
    
    // Load Avatar
    if ([sessionDict objectForKey:kDSDropspotSessionPrimaryAvatarURLKeyName]) {
        [[DSDownload download] runWithURL:[sessionDict objectForKey:kDSDropspotSessionPrimaryAvatarURLKeyName] success:^(NSData *data) {
            
            // Save avatar data to user defaults.
            if (data) {
                [sessionDict setObject:data forKey:kDSDropspotSessionImageKeyName];
            } else {
                [sessionDict removeObjectForKey:kDSDropspotSessionImageKeyName];
            }
            [self.defaults setObject:sessionDict forKey:kDSDropspotSessionDomainKeyName];
            [self.defaults synchronize];
            
            // Notify that user avatar has changed.
            [[NSNotificationCenter defaultCenter] postNotificationName:kDSDropspotSessionUserImageChangedNotificationName object:nil];

            if (completion) {
                completion(nil);
            }
        } fail:^(NSError *error) {
            if (completion) {
                completion(error);
            }
        }];
    }
}

- (void)loadSpotsOfSubscriptionsWithCompletion:(void(^)(void))completion {
    
    __block int numberOfSubscriptions = _collections.count;
    
    if (numberOfSubscriptions > 0) {
        
        for (DSAPICollectionResponse * collectionResponse in _collections) {
            if (collectionResponse.subscribed) {
                Collection * collection = [Collection collectionWithSlug:collectionResponse.slug inManagedObjectContext:[DSGlobalData sharedData].childContext];
                
                BOOL loadSpots = NO;
                
                if (collection) {
                    NSDateFormatter * dateFormatter = [[NSDateFormatter alloc] init];
                    [dateFormatter setDateFormat:DRPSPT_DATE_FORMAT];
                    NSDate * modifiedSpots = [dateFormatter dateFromString:collectionResponse.modifiedSpots];
                    
                    if (collectionResponse.modifiedSpots) {
                        if ([collection.modifiedSpots compare:modifiedSpots] != NSOrderedSame) {
                            loadSpots = YES;
                        }
                    }
                } else {
                    loadSpots = YES;
                }
                
                if (loadSpots) {
                    [[DSDropspot session] spotsWithURL:collectionResponse.spotsURL paginatedWithBlock:^(DSAPISpotResponse *spotResponse) {
                        spotResponse.subscribed = YES;
                        if ([spotResponse.collectionSlugs isEqualToString:@""]) {
                            BOOL exists = NO;
                            for (DSAPISpotResponse * sr in _spots) {
                                if ([sr.slug isEqualToString:spotResponse.slug]) {
                                    exists = YES;
                                    break;
                                }
                            }
                            if (!exists) {
                                [_spots addObject:spotResponse];
                            }
                        } else {
                            NSArray * slugs = [spotResponse.collectionSlugs componentsSeparatedByString:@","];
                            for (NSString * slug in slugs) {
                                if (![_spotResponsesForCollections objectForKey:slug]) {
                                    [_spotResponsesForCollections setObject:[NSMutableArray array] forKey:slug];
                                }
                                [_spotResponsesForCollections[slug] addObject:spotResponse];
                            }
                        }
                        
                    } completion:^(NSError *error) {
                        numberOfSubscriptions--;
                        if (numberOfSubscriptions == 0) {
                            if (completion) {
                                completion();
                            }
                        }
                    }];
                } else {
                    numberOfSubscriptions--;
                    if (numberOfSubscriptions == 0) {
                        if (completion) {
                            completion();
                        }
                    }
                }
            } else {
                numberOfSubscriptions--;
                if (numberOfSubscriptions == 0) {
                    if (completion) {
                        completion();
                    }
                }
            }
        }
    } else {
        
        if (completion) {
            completion();
        }
    }
}

- (void)loadCollectionsOfAuthor:(NSString *)author withCompletion:(void(^)(NSError * error))completion {

    // Load collections of author
    [[DSDropspot session] collectionsWithURL:kCollectionsURLWithAuthor(author) paginatedWithBlock:^(DSAPICollectionResponse *collectionResponse) {
        if (collectionResponse.published) {
            collectionResponse.subscribed = YES;
            [_collections addObject:collectionResponse];
        }
    } completion:^(NSError *error) {
        if (error) {
            if (completion) {
                completion(error);
            }
        } else {
            NSString * subscriptionsURL = [[self.defaults objectForKey:kDSDropspotSessionDomainKeyName] objectForKey:kDSDropspotSessionSubscriptionsURLKeyName];
            
            if (subscriptionsURL) {
                __block NSMutableArray * subscriptionSlugs = [NSMutableArray array];
                
                [[DSDropspot session] subscriptionsWithURL:subscriptionsURL paginatedWithBlock:^(DSAPICollectionResponse *collectionResponse) {
                    if (collectionResponse.slug) {
                        [subscriptionSlugs addObject:collectionResponse.slug];
                    }
                } completion:^(NSError *error) {
                    if (!error) {
                        for (DSAPICollectionResponse * collectionResponse in _collections) {
                            BOOL exists = NO;
                            for (NSString * slug in subscriptionSlugs) {
                                if ([slug isEqualToString:collectionResponse.slug]) {
                                    exists = YES;
                                }
                            }
                            if (!exists) {
                                [[DSDropspot session] subscribeToCollectionWithSlug:collectionResponse.slug block:nil];
                            }
                        }
                    }
                }];
            }
            [self loadSpotsOfSubscriptionsWithCompletion:^{
                if (completion) {
                    completion(error);
                }
            }];
        }
    }];
}

- (void)loadOwnCollectionWithCompletion:(void(^)(NSError * error))completion {
        NSMutableDictionary * sessionDict = [[self.defaults objectForKey:kDSDropspotSessionDomainKeyName] mutableCopy];
        
        if (![sessionDict objectForKey:kDSDropspotSessionCollectionSlugNameKeyName]) {
            
            NSString * name = [sessionDict objectForKey:kDSDropspotSessionNameKeyName];
            NSString * myCollectionString = [NSString stringWithFormat:@"%@'s %@", name, kDSOwnCollectionSuffix];
            
            // Check first if the collection already exists.
            [[DSDropspot session] collectionsBySearch:myCollectionString block:^(NSDictionary *result, NSError *error) {
                if (!error) {
                    NSArray * resultArray = result[@"results"];
                    if (resultArray && resultArray.count > 0) {
                        NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"content_object.created" ascending:YES];
                        NSArray * sortedArray = [resultArray sortedArrayUsingDescriptors:@[descriptor]];
                        NSDictionary * myCollection = [sortedArray firstObject][@"content_object"];
                        
                        if (myCollection && myCollection[@"slug"]) {
                            [sessionDict setObject:myCollection[@"slug"] forKey:kDSDropspotSessionCollectionSlugNameKeyName];
                            [self.defaults setObject:sessionDict forKey:kDSDropspotSessionDomainKeyName];
                            [self.defaults synchronize];
                            
                            [self loadOwnCollectionWithCompletion:^(NSError *error) {
                                if (completion) {
                                    completion(error);
                                }
                            }];
                        } else {
                            [self createCollectionWithCompletion:^(NSError *error) {
                                if (completion) {
                                    completion(error);
                                }
                            }];
                        }
                    } else {
                        [self createCollectionWithCompletion:^(NSError *error) {
                            if (completion) {
                                completion(error);
                            }
                        }];
                    }
                } else {
                    [self createCollectionWithCompletion:^(NSError *error) {
                        if (completion) {
                            completion(error);
                        }
                    }];
                }
            }];
        } else {
            NSString * slug = [sessionDict objectForKey:kDSDropspotSessionCollectionSlugNameKeyName];
            
            [[DSDropspot session] collectionWithURL:kCollectionURLWithSlug(slug) success:^(DSAPICollectionResponse *collectionResponse) {
                [[DSGlobalData sharedData].childContext performBlock:^{
                    Collection * collection = [Collection collectionWithAPIResponse:collectionResponse inManagedObjectContext:[DSGlobalData sharedData].childContext];
                    collection.isOwnedByMe = YES;
                    collection.subscribed = YES;
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [[DSDropspot session] spotsWithURL:collection.spotsURL paginatedWithBlock:^(DSAPISpotResponse *spotResponse) {
                            spotResponse.isOwnedByMe = YES;
                            
                            if (!_spotResponsesForCollections[slug]) {
                                [_spotResponsesForCollections setObject:[NSMutableArray array] forKey:slug];
                            }
                            [_spotResponsesForCollections[slug] addObject:spotResponse];

                        } completion:^(NSError *error) {
                            if (completion) {
                                completion(error);
                            }
                        }];
                    });
                }];
            } completion:^(NSError *error) {
//                if (completion) {
//                    completion(error);
//                }
            }];
        }
}

@end

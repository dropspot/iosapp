/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  SettingsViewController.m
//  Dropspot3
//
//  Created by Lukas Würzburger on 11.10.13.
//  Copyright (c) 2013 Dropspot. All rights reserved.
//

#import "SettingsViewController.h"

#import "DSGlobal.h"
#import "DSGlobalData.h"
#import "DSDropspot.h"
#import "DSFacebook.h"

#import "SettingsFrequencyViewController.h"

#import "GAIHeader.h"

#define kSettingsLogoutAlertViewTag 1
#define kSettingsResetSyncDateAlertViewTag 3
#define kSettingsMeasurementAlertViewTag 2

@interface SettingsViewController () <UIAlertViewDelegate>

@end

@implementation SettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = translate(@"SETTINGS_TITLE");
    
    [self.navigationController.navigationBar setTintColor:kDSColorCoral];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleSessionChanged) name:kDSDropspotSessionUserChangedNotificationName object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleSessionChanged) name:kDSDropspotSessionChangedNotificationName object:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [self.tableView reloadData];

    [[DSGlobal sharedGlobals] analyticsView:kDSScreenNameSettingsView];
}

- (BOOL)shouldAutorotate {
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

#pragma mark - IBActions

- (IBAction)doneButtonPressed:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)readaloudButtonPressed:(UISwitch *)switchButton {
    
    [DSGlobal sharedGlobals].readaloud = switchButton.on;
}

#pragma mark - Helper

- (void)handleSessionChanged {
    [self.tableView reloadData];
}

- (UIView *)activeSessionHeaderView {
    UIView * view = [[UIView alloc] initWithFrame:CGRectMake(0.f, 0.f, 320.f, 100.f)];
    view.backgroundColor = [UIColor clearColor];
    
    UIImageView * imageView = [[UIImageView alloc] initWithFrame:CGRectMake(17.f, 20.f, 60.f, 60.f)];
    imageView.contentMode = UIViewContentModeScaleAspectFill;
    imageView.image = [DSUser currentUser].avatar;
    imageView.layer.masksToBounds = YES;
    imageView.layer.cornerRadius = 3.f;
    [view addSubview:imageView];
    
    UILabel * nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(94.f, 30.f, 200.f, 21.f)];
    nameLabel.font = [UIFont systemFontOfSize:17.f];
    nameLabel.textColor = [UIColor blackColor];
    nameLabel.backgroundColor = [UIColor clearColor];
    nameLabel.text = [DSUser currentUser].displayName;
    [view addSubview:nameLabel];
    
    UILabel * userNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(94.f, 50.f, 200.f, 21.f)];
    userNameLabel.font = [UIFont systemFontOfSize:15.f];
    userNameLabel.textColor = [UIColor darkGrayColor];
    userNameLabel.backgroundColor = [UIColor clearColor];
    userNameLabel.text = [DSUser currentUser].username;
    [view addSubview:userNameLabel];
    
    return view;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 2;
    } else if (section == 1) {
        return 1;
    } else {
        return 1;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"cell"];
    }
    
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            cell.textLabel.text = @"Last sync";
            cell.textLabel.textColor = [UIColor blackColor];
            
            NSDate * date = [DSGlobal sharedGlobals].lastSyncDate;
            if (date) {
                NSDateFormatter * dateFormatter = [[NSDateFormatter alloc] init];
                [dateFormatter setDateStyle:NSDateFormatterShortStyle];
                [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
                
                cell.detailTextLabel.text = [dateFormatter stringFromDate:date];
            } else {
                cell.detailTextLabel.text = @"Sync date is reset";
            }
            cell.accessoryType = UITableViewCellAccessoryNone;
        } else {
            cell.textLabel.text = translate(@"SETTINGS_LABEL_SESSION_SIGNOUT");
            cell.textLabel.textColor = [UIColor redColor];
            cell.detailTextLabel.text = nil;
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
        cell.selectionStyle = UITableViewCellSelectionStyleDefault;
    } else if (indexPath.section == 1) {
        cell.textLabel.text = translate(@"SETTINGS_LABEL_MEASUREMENT");
        
        if ([DSGlobal sharedGlobals].measurement == DSMeasurementAuto) {
            cell.detailTextLabel.text = translate(@"SETTINGS_MEASUREMENT_AUTO");
        } else if ([DSGlobal sharedGlobals].measurement == DSMeasurementMiles) {
            cell.detailTextLabel.text = translate(@"SETTINGS_MEASUREMENT_MILES");
        } else {
            cell.detailTextLabel.text = translate(@"SETTINGS_MEASUREMENT_METERS");
        }
        
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.selectionStyle = UITableViewCellSelectionStyleDefault;
    } else if (indexPath.section == 2) {
        cell.textLabel.text = translate(@"SETTINGS_LABEL_READ_ALOUD");
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        UISwitch * switchButton = [[UISwitch alloc] init];
        switchButton.on = [DSGlobal sharedGlobals].readaloud;
        [switchButton addTarget:self action:@selector(readaloudButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        
        cell.accessoryView = switchButton;
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return 100;
        default:
            return 40;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return [self activeSessionHeaderView];
    }
    return nil;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (section == 1) {
        
        return translate(@"SETTINGS_HEADER_LABEL_DISTANCES");
    } else if (section == 2) {

        return translate(@"SETTINGS_HEADER_LABEL_READALOUD");
    }
    return nil;
}

- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {
    if (section == 2) {
        if (kDSForceAboutText) {
            return kDSForceAboutText;
        } else {
            return [NSString stringWithFormat:translate(@"SETTINGS_ABOUT_FOOTER"), kAppName];
        }
    }
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Reset last sync timestamp?"
                                                             message:nil
                                                            delegate:self
                                                   cancelButtonTitle:translate(@"SETTINGS_SESSION_ALERT_SIGNOUT_CANCEL")
                                                   otherButtonTitles:@"OK", nil];
            alert.tag = kSettingsResetSyncDateAlertViewTag;
            [alert show];
        } else {
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:translate(@"SETTINGS_SESSION_ALERT_SIGNOUT_TITLE")
                                                             message:translate(@"SETTINGS_SESSION_ALERT_SIGNOUT_MESSAGE")
                                                            delegate:self
                                                   cancelButtonTitle:translate(@"SETTINGS_SESSION_ALERT_SIGNOUT_CANCEL")
                                                   otherButtonTitles:translate(@"SETTINGS_SESSION_ALERT_SIGNOUT_SIGNOUT"), nil];
            alert.tag = kSettingsLogoutAlertViewTag;
            [alert show];
        }
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    } else if (indexPath.section == 1) {
        [self performSegueWithIdentifier:@"showSettingsMeasurement" sender:nil];
    }
}

#pragma mark - Navigation

- (IBAction)done:(id)sender {
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kDSDropspotSessionUserChangedNotificationName
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kDSDropspotSessionChangedNotificationName
                                                  object:nil];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Alert view

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == kSettingsLogoutAlertViewTag) {
        if (buttonIndex == 1) {
            [self logout];
        }
    } else if (alertView.tag == kSettingsResetSyncDateAlertViewTag) {
        if (buttonIndex == 1) {
            [DSGlobal sharedGlobals].lastSyncDate = nil;
            [self.tableView reloadData];
        }
    }
}

- (void)logout {
    [[DSDropspot session] closeSession];

    NSManagedObjectContext * context = [DSGlobalData sharedData].childContext;
    [context performBlock:^{
        NSFetchRequest * request = [NSFetchRequest fetchRequestWithEntityName:@"Collection"];
        NSArray * array = [context executeFetchRequest:request error:nil];
        for (Collection * collection in array) {
            [context deleteObject:collection];
        }
        request = [NSFetchRequest fetchRequestWithEntityName:@"Spot"];
        array = [context executeFetchRequest:request error:nil];
        for (Spot * spot in array) {
            [context deleteObject:spot];
        }
        request = [NSFetchRequest fetchRequestWithEntityName:@"User"];
        array = [context executeFetchRequest:request error:nil];
        for (Spot * spot in array) {
            [context deleteObject:spot];
        }
        
        [context save:nil];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self dismissViewControllerAnimated:YES completion:^{
                [[DSDropspot session] checkSession];
            }];
        });
    }];
}

@end

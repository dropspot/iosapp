/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  MasterBottomCell.h
//  Dropspot3
//
//  Created by Lukas Würzburger on 3/11/14.
//  Copyright (c) 2014 Dropspot GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MasterBottomCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIButton * settingsButton;

- (void)setLayout;

@end

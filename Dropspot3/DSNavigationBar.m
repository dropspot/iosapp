/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  DSNavigationBar.m
//  Dropspot3
//
//  Created by Lukas Würzburger on 1/20/14.
//  Copyright (c) 2014 Dropspot. All rights reserved.
//

#import "DSNavigationBar.h"

@implementation DSNavigationBar

- (void)layoutSubviews {
    [super layoutSubviews];
    
    for (UIView * subview in self.subviews) {
        if ([subview isKindOfClass:[UIButton class]]) {
            CGRect frame = subview.frame;
            if (frame.origin.x < 160) {
                frame.origin.x = 8;
                subview.frame = frame;
            }
        }
    }
    
}

- (void)setContentAlpha:(float)contentAlpha {
    _contentAlpha = contentAlpha;
    
    for (UIView * subview in self.subviews) {
        subview.alpha = _contentAlpha;
    }
}

@end

/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  DSNavigationBar.h
//  Dropspot3
//
//  Created by Lukas Würzburger on 1/20/14.
//  Copyright (c) 2014 Dropspot. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DSNavigationBar : UINavigationBar

@property (nonatomic, assign) float contentAlpha;

@property (nonatomic, weak) IBOutlet UIView * customTitleView;
@property (nonatomic, weak) IBOutlet UILabel * titleLabel;

@end

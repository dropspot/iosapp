/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  SettingsFrequencyViewController.m
//  Dropspot3
//
//  Created by Lukas Würzburger on 11.10.13.
//  Copyright (c) 2013 Dropspot. All rights reserved.
//

#import "SettingsMeasurementViewController.h"
#import "DSGlobal.h"

#import "GAIHeader.h"

@implementation SettingsMeasurementViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = translate(@"SETTINGS_LABEL_MEASUREMENT");
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:kDSScreenNameSettingsMeasurementView];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
}

- (BOOL)shouldAutorotate {
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    
    if (indexPath.row == 0) {
        cell.textLabel.text = translate(@"SETTINGS_MEASUREMENT_AUTO");
    } else if (indexPath.row == 1) {
        cell.textLabel.text = translate(@"SETTINGS_MEASUREMENT_MILES");
    } else if (indexPath.row == 2) {
        cell.textLabel.text = translate(@"SETTINGS_MEASUREMENT_METERS");
    }
    
    if (indexPath.row == [DSGlobal sharedGlobals].measurement) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [DSGlobal sharedGlobals].measurement = indexPath.row;
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
}

@end

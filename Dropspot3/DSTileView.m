/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  DSTileView.m
//  Dropspot3
//
//  Created by Lukas Würzburger on 2/8/14.
//  Copyright (c) 2014 Dropspot. All rights reserved.
//

#import "DSTileView.h"
#import "UILabel+AutoHeight.h"

#define kImageWrapperViewSizeSmall  CGSizeMake (50., 50.)
#define kImageWrapperViewSizeBig    CGSizeMake (300., 80.)

@interface DSTileView () {
    UILabel * _timeLabel;
    UILabel * _textLabel;
    UILabel * _infoLabel;
    UILabel * _distanceLabel;
    UIView * _imageWrapperView;
    UIImageView * _imageView;
    
    id _target;
    SEL _selector;
    
    UITapGestureRecognizer * _tapGesture;
}

@end

@implementation DSTileView

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initialize];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
    }
    return self;
}

- (void)initialize {
    // Self
    self.backgroundColor = [UIColor colorWithWhite:0.97f alpha:1.f];
    self.layer.cornerRadius = 2.f;
    
    // Border
    UIView * rightBorder = [[UIView alloc] initWithFrame:CGRectMake(self.frame.size.width - 1, 0, 1, self.frame.size.height)];
    rightBorder.backgroundColor = [UIColor colorWithWhite:0.93 alpha:1.0];
    rightBorder.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    [self addSubview:rightBorder];
    
    UIView * bottomBorder = [[UIView alloc] initWithFrame:CGRectMake(0, self.frame.size.height, self.frame.size.width, 1)];
    bottomBorder.backgroundColor = [UIColor colorWithWhite:0.93 alpha:1.0];
    bottomBorder.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
    [self addSubview:bottomBorder];
    
    // Text label
    _textLabel = [[UILabel alloc] initWithFrame:CGRectMake(10.f, 10.f, self.frame.size.width - 20.f, 21.f)];
    _textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:15.f];
    _textLabel.backgroundColor = [UIColor clearColor];
    _textLabel.numberOfLines = 0;
    [self addSubview:_textLabel];
    
    // Gesture
    _tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    [self addGestureRecognizer:_tapGesture];
    
    // Style
    self.style = DSTileViewStyleSmall;
}

#pragma mark - Static functions

+ (float)heightWithStyle:(DSTileViewStyle)style width:(float)width text:(NSString *)text info:(NSString *)info time:(BOOL)time distance:(BOOL)distance image:(BOOL)image {
    
    float height = 0;
    
    float innerWidth = width - 20.f;
    
    if (style == DSTileViewStyleBig) {
        height += kImageWrapperViewSizeBig.height + 10;
    } else {
        height += 10;
    }
    
    if (time) {
        CGRect rect = [self heightOfText:text withSize:17.f width:innerWidth];
        height += round(rect.size.height);
    }
    
    if (style == DSTileViewStyleSmall) {
        if (image) {
            innerWidth -= kImageWrapperViewSizeSmall.width + 10.f;
        }
    }
    
    CGRect textRect = [self heightOfText:text withSize:15.f width:innerWidth];
    height += round(textRect.size.height);
    
    if (![[info stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""] && info != nil) {
        CGRect rect = [self heightOfText:info withSize:14.f width:width - 80.f];
        CGRect distanceRect = [self heightOfText:@"xx" withSize:14.f width:100.f];
        
        if (image) {
            float imageAndDistanceHeight = kImageWrapperViewSizeSmall.height + 5.f + distanceRect.size.height;
            if (textRect.size.height + 10.f + rect.size.height < imageAndDistanceHeight) {
                height += round(imageAndDistanceHeight - textRect.size.height) + 5.f + 7.f;
            } else {
                height += 10.f + round(rect.size.height) + 7.f;
            }
        } else {
            height += 10.f + round(rect.size.height) + 7.f;
        }
    } else {
        CGRect distanceRect = [self heightOfText:@"xx" withSize:14.f width:100.f];
        if (style == DSTileViewStyleSmall) {
            if (image) {
                float imageAndDistanceHeight = kImageWrapperViewSizeSmall.height + 5.f + distanceRect.size.height;
                if (textRect.size.height < imageAndDistanceHeight) {
                    height += round(imageAndDistanceHeight - textRect.size.height) + 5.f;
                } else {
                    height += 5.f;
                }
            } else {
                height += distanceRect.size.height + 10.f;
            }
        } else {
            height += distanceRect.size.height + 10.f;
        }
    }
    
    //height += 10;
    
    return height;
}

+ (CGRect)heightOfText:(NSString *)text withSize:(CGFloat)size width:(CGFloat)width {
    return [text boundingRectWithSize:CGSizeMake(width, CGFLOAT_MAX)
                                     options:(NSStringDrawingUsesFontLeading | NSStringDrawingUsesLineFragmentOrigin)
                                  attributes:@{
                                               NSFontAttributeName : [UIFont fontWithName:@"HelveticaNeue-Light" size:size]
                                               }
                                     context:nil];
}

#pragma mark - Setter & Getter

- (void)setTime:(NSString *)time {
    if ([[time stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]) {
        time = nil;
    }
    _time = time;
    
    if (_time) {
        if (!_timeLabel) {
            
            // Time label
            _timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(10.f, 10.f, self.frame.size.width - 20.f, 21.f)];
            _timeLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:17.f];
            _timeLabel.backgroundColor = [UIColor clearColor];
            [self addSubview:_timeLabel];
        }
        _timeLabel.text = _time;
    }
    
    [self setLayout];
}

- (void)setText:(NSString *)text {
    if ([[text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]) {
        text = nil;
    }
    _text = text;
    
    if (_text) {
        _textLabel.text = _text;
    }
    
    [self setLayout];
}

- (void)setInfo:(NSString *)info {
    if ([[info stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]) {
        info = nil;
    }
    _info = info;
    
    if (_info) {
        if (!_infoLabel) {
            
            // Info label
            _infoLabel = [[UILabel alloc] initWithFrame:CGRectMake(10.f, 10.f, self.frame.size.width - 80.f, 21.f)];
            _infoLabel.numberOfLines = 0;
            [self styleInfoLabel];
            [self addSubview:_infoLabel];
        }
        _infoLabel.text = _info;
    } else {
        if (_infoLabel) {
            _infoLabel.text = @"";
        }
    }
    
    [self setLayout];
}

- (void)setDistance:(NSString *)distance {
    if ([[distance stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]) {
        distance = nil;
    }
    _distance = distance;
    
    if (_distance) {
        if (!_distanceLabel) {
            
            // Distance label
            _distanceLabel = [[UILabel alloc] initWithFrame:CGRectMake(10.f, self.frame.size.height - 29.f, self.frame.size.width - 20.f, 21.f)];
            _distanceLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:14.f];
            _distanceLabel.textColor = [UIColor grayColor];
            _distanceLabel.backgroundColor = [UIColor clearColor];
            _distanceLabel.textAlignment = NSTextAlignmentRight;
            [self addSubview:_distanceLabel];
        }
        _distanceLabel.text = _distance;
    }
    _distanceLabel.hidden = (_distance == nil);
    
    [self setLayout];
}

- (void)setImage:(UIImage *)image {
    _image = image;
    
    if (_image) {
        if (!_imageWrapperView) {
            _imageWrapperView = [[UIView alloc] initWithFrame:CGRectMake(self.frame.size.width - 10.f - kImageWrapperViewSizeBig.width, 10.f, kImageWrapperViewSizeBig.width, kImageWrapperViewSizeBig.height)];
            _imageWrapperView.layer.borderColor = [UIColor colorWithWhite:0.8f alpha:1.f].CGColor;
            _imageWrapperView.layer.borderWidth = 0.5f;
            [self addSubview:_imageWrapperView];
        }
        if (!_imageView) {
            _imageView = [[UIImageView alloc] initWithFrame:CGRectMake(2.f, 2.f, kImageWrapperViewSizeBig.width - 4.f, kImageWrapperViewSizeBig.height - 4.f)];
            _imageView.contentMode = UIViewContentModeScaleAspectFill;
            _imageView.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
            _imageView.layer.masksToBounds = YES;
            [_imageWrapperView addSubview:_imageView];
        }
        
        _imageView.image = image;
    } else {
        if (_style == DSTileViewStyleBig) {
            _imageView.image = [UIImage imageNamed:@"placehold_kicker.png"];
        } else {
            _imageView.image = nil;
        }
    }
    
    [self setLayout];
}

- (void)setStyle:(DSTileViewStyle)style {
    _style = style;
    
    if (style == DSTileViewStyleBig) {
        if (!_imageWrapperView) {
            _imageWrapperView = [[UIView alloc] initWithFrame:CGRectMake(self.frame.size.width - 10.f - kImageWrapperViewSizeBig.width, 10.f, kImageWrapperViewSizeBig.width, kImageWrapperViewSizeBig.height)];
            _imageWrapperView.layer.borderColor = [UIColor colorWithWhite:0.8f alpha:1.f].CGColor;
            _imageWrapperView.layer.borderWidth = 0.5f;
            [self addSubview:_imageWrapperView];
        } else {
            _imageWrapperView.frame = CGRectMake(self.frame.size.width - 10.f - kImageWrapperViewSizeBig.width, 10.f, kImageWrapperViewSizeBig.width, kImageWrapperViewSizeBig.height);
        }
        if (!_imageView) {
            _imageView = [[UIImageView alloc] initWithFrame:CGRectMake(2.f, 2.f, kImageWrapperViewSizeBig.width - 4.f, kImageWrapperViewSizeBig.height - 4.f)];
            _imageView.contentMode = UIViewContentModeScaleAspectFill;
            _imageView.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
            _imageView.layer.masksToBounds = YES;
            _imageView.backgroundColor = [UIColor lightGrayColor];
            [_imageWrapperView addSubview:_imageView];
        } else {
            _imageView.frame = CGRectMake(2.f, 2.f, kImageWrapperViewSizeBig.width - 4.f, kImageWrapperViewSizeBig.height - 4.f);
        }
    } else {
        if (!_imageWrapperView) {
            _imageWrapperView = [[UIView alloc] initWithFrame:CGRectMake(self.frame.size.width - 10.f - kImageWrapperViewSizeSmall.width, 10.f, kImageWrapperViewSizeSmall.width, kImageWrapperViewSizeBig.height)];
            _imageWrapperView.layer.borderColor = [UIColor colorWithWhite:0.8f alpha:1.f].CGColor;
            _imageWrapperView.layer.borderWidth = 0.5f;
            [self addSubview:_imageWrapperView];
        } else {
            _imageWrapperView.frame = CGRectMake(self.frame.size.width - 10.f - kImageWrapperViewSizeSmall.width, 10.f, kImageWrapperViewSizeSmall.width, kImageWrapperViewSizeSmall.height);
        }
        if (!_imageView) {
            _imageView = [[UIImageView alloc] initWithFrame:CGRectMake(2.f, 2.f, kImageWrapperViewSizeSmall.width - 4.f, kImageWrapperViewSizeSmall.height - 4.f)];
            _imageView.contentMode = UIViewContentModeScaleAspectFill;
            _imageView.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
            _imageView.layer.masksToBounds = YES;
            _imageView.backgroundColor = [UIColor lightGrayColor];
            [_imageWrapperView addSubview:_imageView];
        } else {
            _imageView.frame = CGRectMake(2.f, 2.f, kImageWrapperViewSizeSmall.width - 4.f, kImageWrapperViewSizeSmall.height - 4.f);
        }
    }
    
    [self setLayout];
}

- (void)setInfoStyle:(DSTileViewInfoStyle)infoStyle {
    _infoStyle = infoStyle;
    
    [self styleInfoLabel];
}

#pragma mark - Helper

- (void)styleInfoLabel {
    if (_infoStyle == DSTileViewInfoStyleGreenBordered) {
        _infoLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:14.f];
        _infoLabel.textColor = [UIColor whiteColor];
        _infoLabel.backgroundColor = [UIColor colorWithRed:0.3 green:0.8 blue:0.3 alpha:1.0];
        _infoLabel.layer.cornerRadius = 3.f;
        _infoLabel.textAlignment = NSTextAlignmentCenter;
    } else {
        _infoLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:14.f];
        _infoLabel.textColor = [UIColor grayColor];
        _infoLabel.backgroundColor = [UIColor clearColor];
        _infoLabel.layer.cornerRadius = 0.f;
        _infoLabel.textAlignment = NSTextAlignmentLeft;
    }
}

- (void)setLayout {
    [UIView transitionWithView:self duration:0.1 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
        
    _timeLabel.hidden = (_time == nil);
    _textLabel.hidden = (_text == nil);
    _distanceLabel.hidden = (_distance == nil);
    _infoLabel.hidden = (_info == nil);
    
    if (_style == DSTileViewStyleSmall) {
        _imageWrapperView.hidden = (_image == nil);
    }
    
    CGRect frame;
    float positionY = 10.f;
    
    if (_style == DSTileViewStyleBig) {
        frame = _imageWrapperView.frame;
        frame.origin.x = 0.f;
        frame.origin.y = 0.f;
        frame.size.width = self.frame.size.width;
        frame.size.height = kImageWrapperViewSizeBig.height;
        _imageWrapperView.frame = frame;
        
        positionY = _imageWrapperView.frame.origin.y + _imageWrapperView.frame.size.height + 10.f;
        
        if (!_inviteButton) {
            _inviteButton = [UIButton buttonWithType:UIButtonTypeCustom];
            NSString * buttonTitle = translate(@"SPOT_VIEW_CHECKIN_BUTTON_LABEL");
            UIFont * font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:12.f];
            CGRect bounds = [buttonTitle boundingRectWithSize:CGSizeMake(200.f, 30.f) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{ NSFontAttributeName : font } context:nil];
            
            _inviteButton.frame = CGRectMake(self.frame.size.width - bounds.size.width - 20.f - 20.f, 25.f, bounds.size.width + 20.f, 30.f);
            _inviteButton.backgroundColor = [UIColor blueColor];
            _inviteButton.layer.cornerRadius = 3.f;
            [_inviteButton setTitle:buttonTitle forState:UIControlStateNormal];
            _inviteButton.titleLabel.font = font;
            [_inviteButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [self addSubview:_inviteButton];
        }
    }
    
    if (_time) {
        frame = _timeLabel.frame;
        frame.origin.y = positionY;
        _timeLabel.frame = frame;
        positionY = _timeLabel.frame.origin.y + _timeLabel.frame.size.height;
    }
    
    frame = _textLabel.frame;
    frame.origin.y = positionY;
    if (_style == DSTileViewStyleSmall) {
        if (_image) {
            frame.size.width = self.frame.size.width - 2 * 10.f - _imageWrapperView.frame.size.width - 10.f;
        } else {
            frame.size.width = self.frame.size.width - 2 * 10.f;
        }
    }
    _textLabel.frame = frame;
    
    [_textLabel autoAdjustHeight];
    
    if (_image && _style == DSTileViewStyleSmall) {
        if (_textLabel.frame.size.height > _imageWrapperView.frame.size.height) {
            positionY = _textLabel.frame.origin.y + _textLabel.frame.size.height;
        } else {
            positionY = _imageWrapperView.frame.origin.y + _imageWrapperView.frame.size.height;
        }
    } else {
        positionY = _textLabel.frame.origin.y + _textLabel.frame.size.height;
    }
    
    if (_info) {
        if (_infoStyle == DSTileViewInfoStyleGreenBordered) {
            frame = _infoLabel.frame;
            CGRect rect = [_info boundingRectWithSize:CGSizeMake(_infoLabel.frame.size.width, CGFLOAT_MAX)
                                              options:(NSStringDrawingUsesFontLeading | NSStringDrawingUsesLineFragmentOrigin)
                                           attributes:@{ NSFontAttributeName : _infoLabel.font }
                                              context:nil];
            frame.size.width = rect.size.width + 13.f;
            frame.size.height = rect.size.height + 8.f;
            frame.origin.y = positionY + 5.f;
            _infoLabel.frame = frame;
        } else {
            frame = [_infoLabel autoAdjustHeight];
            frame.origin.y = positionY + 5.f;
            _infoLabel.frame = frame;
        }
        positionY = _infoLabel.frame.origin.y + _infoLabel.frame.size.height;
    } else {
        if (_distance) {
            positionY = _textLabel.frame.origin.y + _textLabel.frame.size.height + _distanceLabel.frame.size.height;
        }
    }
    
    if (_distance) {
        frame = [_distanceLabel autoAdjustHeight];
        if (_info) {
            frame.origin.y = _infoLabel.frame.origin.y + _infoLabel.frame.size.height - _distanceLabel.frame.size.height;
        } else {
            if (_style == DSTileViewStyleSmall) {
                if (_image) {
                    if (_textLabel.frame.size.height > _imageWrapperView.frame.size.height) {
                        if (_textLabel.frame.origin.y + _textLabel.frame.size.height > _imageWrapperView.frame.origin.y + _imageWrapperView.frame.size.height + 5.f + _distanceLabel.frame.size.height) {
                            frame.origin.y = _textLabel.frame.origin.y + _textLabel.frame.size.height - _distanceLabel.frame.size.height;
                        } else {
                            frame.origin.y = _imageWrapperView.frame.origin.y + _imageWrapperView.frame.size.height + 5.f;
                        }
                    } else {
                        frame.origin.y = _imageWrapperView.frame.origin.y + _imageWrapperView.frame.size.height + 5.f;
                    }
                } else {
                    frame.origin.y = _textLabel.frame.origin.y + _textLabel.frame.size.height + 5.f;
                }
            } else {
                frame.origin.y = _textLabel.frame.origin.y + _textLabel.frame.size.height + 5.f;
            }
        }
        _distanceLabel.frame = frame;
        
        if (_info && _infoLabel.frame.size.height > _distanceLabel.frame.size.height) {
            positionY = _infoLabel.frame.origin.y + _infoLabel.frame.size.height;
        } else {
            positionY = _distanceLabel.frame.origin.y + _distanceLabel.frame.size.height;
        }
    }
    
    frame = self.frame;
    frame.size.height = positionY + 10.f;
    self.frame = frame;
        
    } completion:nil];
}

#pragma mark - Public functions

- (void)addTarget:(id)target selector:(SEL)selector {
    _target = target;
    _selector = selector;
}

#pragma mark - Gestures

- (void)handleTap:(UIGestureRecognizer *)gestureRecognizer {
    if (gestureRecognizer.state == UIGestureRecognizerStateRecognized) {
        if ([_target respondsToSelector:_selector]) {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
            [_target performSelector:_selector withObject:self];
#pragma clang diagnostic pop
        }
    }
}

@end

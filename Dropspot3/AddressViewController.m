/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  AddressViewController.m
//  Dropspot3
//
//  Created by Lukas Würzburger on 1/20/14.
//  Copyright (c) 2014 Dropspot. All rights reserved.
//

#import <LRResty/LRResty.h>

#import "AddressViewController.h"

#import "DSServices.h"

@interface AddressViewController () <UISearchBarDelegate, UITableViewDataSource, UITableViewDelegate> {
    IBOutlet UISearchBar * theSearchBar;
    IBOutlet UITableView * theTableView;
    
    NSArray * searchResult;
}

@end

@implementation AddressViewController

#pragma mark - Table view dataSource

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = translate(@"ADDRESS_VIEW_TITLE");
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return searchResult.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString * cellIdentifier = @"cell";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];

    cell.textLabel.text = searchResult[indexPath.row][@"description"];
    
    return cell;
}

#pragma mark - Table view data source

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if ([self.delegate respondsToSelector:@selector(addressViewController:didSaveWithAddress:)]) {
        [self.delegate addressViewController:self didSaveWithAddress:searchResult[indexPath.row][@"description"]];
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Search bar

- (BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    NSString * searchString = [searchBar.text stringByReplacingCharactersInRange:range withString:text];
    
    [DSServices addressCompletionBySearch:searchString completion:^(NSArray * array) {
        searchResult = array;
        [theTableView reloadData];
    }];
    
    return YES;
}


@end

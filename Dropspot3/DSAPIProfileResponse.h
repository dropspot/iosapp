/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  DSAPIProfileResponse.h
//  Dropspot3
//
//  Created by Lukas Würzburger on 2/7/14.
//  Copyright (c) 2014 Dropspot. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DSAPIProfileResponse : NSObject

@property (nonatomic, retain) NSString * url;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * username;
@property (nonatomic, retain) NSString * primaryAvatarURL;
@property (nonatomic, retain) NSString * collectionsURL;
@property (nonatomic, retain) NSString * spotsURL;
@property (nonatomic, retain) NSString * subscriptionsURL;
@property (nonatomic, retain) NSString * dateJoined;

@end

/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  WelcomeSlideViewController.m
//  Dropspot3
//
//  Created by Lukas Würzburger on 16.10.13.
//  Copyright (c) 2013 Dropspot. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import <MediaPlayer/MediaPlayer.h>

#import "WelcomeSlideViewController.h"

#import "DSLocationController.h"

@interface WelcomeSlideViewController () {
    IBOutlet UIView * moviePlayerView;
    IBOutlet UIImageView * notificationImageView;
    IBOutlet UIView * iPhoneView;
    IBOutlet UIButton * prevButton;
    IBOutlet UIButton * nextButton;
    IBOutlet UILabel * toolTipLabel;
    IBOutlet UIButton * enableLocationButton;
    IBOutlet UIImageView * moviePlayerBackgroundImageView;
    
    MPMoviePlayerController * moviePlayer;
    NSTimer * animationTimer;
}

@end

@implementation WelcomeSlideViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    moviePlayerBackgroundImageView.layer.masksToBounds = YES;
    
    
    
    [self configureView];
}

- (void)setSlide:(DSWelcomeSlide *)slide {
    if (_slide != slide) {
        _slide = slide;
    
        [self configureView];
    }
}

- (void)configureView {
    NSURL *movieURL = [NSURL fileURLWithPath:self.slide.videoURLString];
    if (moviePlayer) {
        if (moviePlayer.view.superview != nil) {
            [moviePlayer.view removeFromSuperview];
        }
        moviePlayer = nil;
    }
    moviePlayer = [[MPMoviePlayerController alloc] initWithContentURL:movieURL];
    [moviePlayer setControlStyle:MPMovieControlStyleNone];
    
    if (self.slide.shouldRepeatVideo) {
        [moviePlayer setRepeatMode:MPMovieRepeatModeOne];
    } else {
        [moviePlayer setRepeatMode:MPMovieRepeatModeNone];
    }
    
    if (moviePlayer.view.superview != moviePlayerView) {
        moviePlayer.view.frame = CGRectMake(0, 0, moviePlayerView.frame.size.width, moviePlayerView.frame.size.height);
        [moviePlayerView addSubview:moviePlayer.view];
        moviePlayer.scalingMode = MPMovieScalingModeAspectFill;
    }
    
    [moviePlayer prepareToPlay];
    [moviePlayer play];
    
    if (self.slide.showLocationButton) {
        moviePlayerView.hidden = YES;
        [self startWelcomeAnimation];
    }
    
    nextButton.hidden = !self.slide.enableNextButton;
    prevButton.hidden = !self.slide.enablePrevButton;
    toolTipLabel.hidden = self.slide.showLocationButton;
    enableLocationButton.hidden = !self.slide.showLocationButton;
    toolTipLabel.text = self.slide.descriptionText;
}

- (void)startWelcomeAnimation {
    [self stopWelcomeAnimation];
    animationTimer = [NSTimer scheduledTimerWithTimeInterval:3.0 target:self selector:@selector(welcomeTimerTick) userInfo:nil repeats:YES];
}

- (void)stopWelcomeAnimation {
    if (animationTimer != nil) {
        [animationTimer invalidate];
        animationTimer = nil;
    }
    notificationImageView.hidden = YES;
}

- (void)welcomeTimerTick {
    [self hideNotification];
    
    [UIView animateWithDuration:0.05 animations:^{
        iPhoneView.transform = CGAffineTransformMakeRotation(-0.1);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.05 animations:^{
            iPhoneView.transform = CGAffineTransformMakeRotation(0.1);
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.05 animations:^{
                iPhoneView.transform = CGAffineTransformMakeRotation(-0.1);
            } completion:^(BOOL finished) {
                [UIView animateWithDuration:0.05 animations:^{
                    iPhoneView.transform = CGAffineTransformMakeRotation(0.1);
                } completion:^(BOOL finished) {
                    [UIView animateWithDuration:0.05 animations:^{
                        iPhoneView.transform = CGAffineTransformMakeRotation(0.0);
                    } completion:^(BOOL finished) {
                        if (finished) {
                            if (self.slide.index == 0) {
                                [self animateNotification];
                            }
                        }
                    }];
                }];
            }];
        }];
    }];
}

- (void)animateNotification {
    notificationImageView.alpha = 0.0;
    notificationImageView.hidden = NO;
    
    CGRect frame = notificationImageView.frame;
    float ratio = frame.size.height / frame.size.width;
    frame.size.width = 1;
    frame.size.height = frame.size.width * ratio;
    frame.origin.x = 160;
    frame.origin.y = iPhoneView.frame.size.height / 2 + iPhoneView.frame.origin.y;
    [notificationImageView setFrame:frame];
    
    [UIView animateWithDuration:0.1 animations:^{
        CGRect frame = notificationImageView.frame;
        frame.size.width = 124;
        frame.size.height = 34;
        frame.origin.x = 160 - frame.size.width / 2;
        frame.origin.y = (iPhoneView.frame.size.height / 2 + iPhoneView.frame.origin.y) - frame.size.height / 2;
        [notificationImageView setFrame:frame];
        notificationImageView.alpha = 1.0;
    } completion:^(BOOL finished) {
        if (self.slide.index != 0) {
            notificationImageView.hidden = YES;
        }
    }];
}

- (void)hideNotification {
    [UIView animateWithDuration:0.2 animations:^{
        notificationImageView.alpha = 0.0;
    } completion:^(BOOL finished) {
        notificationImageView.hidden = YES;
    }];
}

#pragma mark - IBActions

- (IBAction)prevButtonPressed:(id)sender {
    if ([self.delegate respondsToSelector:@selector(welcomeSlideViewControllerDidPressPrevButton:)]) {
        [self.delegate welcomeSlideViewControllerDidPressPrevButton:self];
    }
}

- (IBAction)nextButtonPressed:(id)sender {
    if ([self.delegate respondsToSelector:@selector(welcomeSlideViewControllerDidPressNextButton:)]) {
        [self.delegate welcomeSlideViewControllerDidPressNextButton:self];
    }
}

- (IBAction)enableLocationButtonPressed:(id)sender {
    //[[DSLocationController controller] checkAuthorizationStatus];
}

@end

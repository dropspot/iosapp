/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  CollectionMapViewController.m
//  Dropspot3
//
//  Created by Lukas Würzburger on 1/30/14.
//  Copyright (c) 2014 Dropspot. All rights reserved.
//

#import "CollectionMapViewController.h"

#import "DSMapView.h"

#import "DSGlobalData.h"
#import "Spot+Location.h"
#import "DSDropspot.h"
#import "DSServices.h"
#import "DSGlobal.h"
#import "UIBarButtonItem+Dropspot.h"

#import "SpotViewController.h"
#import "CreateSpotViewController.h"

@interface CollectionMapViewController () <DSMapViewDelegate, DSMapViewDataSource> {
    DSMapView * _mapView;
}

@property (nonatomic, strong) UIBarButtonItem * searchBarButton;
@property (nonatomic, strong) UIBarButtonItem * locateMeBarButton;

@end

@implementation CollectionMapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"Spots";
    
    UIBarButtonItem *newBackButton = [[UIBarButtonItem alloc] initWithTitle:translate(@"MAP_VIEW_BACK_BUTTON_TITLE")
                                                                      style:UIBarButtonItemStyleBordered
                                                                     target:nil
                                                                     action:nil];
    [[self navigationItem] setBackBarButtonItem:newBackButton];

    
    // Map view
    _mapView = [[DSMapView alloc] initWithFrame:CGRectMake(0.f, 0.f, self.view.frame.size.width, self.view.frame.size.height)];
    _mapView.contentInset = UIEdgeInsetsMake(64.f, 0.f, 0.f, 0.f);
    _mapView.delegate = self;
    _mapView.dataSource = self;
    _mapView.showsUserLocation = YES;

    _mapView.followsUserLocation = NO;
    if (kDSOwnCollectionApp) {
        _mapView.state = DSMapViewStateNormal;
        _mapView.searchBar.infoText = translate(@"COLLECTION_MAP_VIEW_TOOLTIP_HOW_TO_MOVE_SPOT");
        _mapView.showsAuthorView = NO;
    } else {
        _mapView.showsSearchBar = NO;
        _mapView.showsAuthorView = YES;
    }
    [self.view addSubview:_mapView];
    
    self.navigationItem.rightBarButtonItems = @[self.searchBarButton, self.locateMeBarButton];
    
    // Forwarding selected spot to map view
    _mapView.selectedSpot = _selectedSpot;
    
    // Load spots for the current region.
    [self loadSpotsForRegion:_mapView.region completion:^{
        [_mapView update];
    }];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateSpotNotification:) name:kDSDropspotSpotEditedNotificationName object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(spotDeletedNotification) name:kDSDropspotSpotDeletedNotificationName object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateSearchBarTextNotification:) name:kDSSpotDidDetermineAddressNotificationName object:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[DSGlobal sharedGlobals] analyticsView:kDSScreenNameCollectionMapView];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if (!_selectedSpot) {
        _mapView.followsUserLocation = YES;
    }
    
    //[self checkSpots];
}

#pragma mark - Setter & Getter

- (UIBarButtonItem *)locateMeBarButton {
    
    if (!_locateMeBarButton) {
        
        _locateMeBarButton = [UIBarButtonItem locateMeBarButtonItemWithTarget:self action:@selector(locateMeButtonPressed:)];
    }
    return _locateMeBarButton;
}

- (UIBarButtonItem *)searchBarButton {
    
    if (!_searchBarButton) {
        
        _searchBarButton = [UIBarButtonItem searchBarButtonItemWithTarget:self action:@selector(searchButtonPressed:)];
    }
    return _searchBarButton;
}

- (void)setSpots:(NSArray *)spots {
    _spots = spots;
    
    [_mapView update];
}

- (void)setSelectedSpot:(Spot *)selectedSpot {
    _selectedSpot = selectedSpot;
    
    _mapView.selectedSpot = _selectedSpot;
}

#pragma mark - Notifications

- (void)updateSpotNotification:(NSNotification *)notification {
    [_mapView update];
}

- (void)updateSearchBarTextNotification:(NSNotification *)notification {
    Spot * spot = notification.userInfo[@"spot"];
    if (spot) {
        _mapView.searchBar.inputText = spot.address;
    }
}

#pragma mark - IBActions

- (void)locateMeButtonPressed:(id)sender {
    
    // Center the user's current location on the map.
    [_mapView centerUserLocationAnimated:YES];
}

- (void)searchButtonPressed:(UIBarButtonItem *)sender {
    
    [self setSearchBarVisible:!_mapView.showsSearchBar];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"spotDetail"] && [segue.destinationViewController isKindOfClass:[SpotViewController class]]) {
        SpotViewController * spotVC = segue.destinationViewController;
        spotVC.spot = sender;
    } else if ([segue.identifier isEqualToString:@"createSpot"]) {
        CreateSpotViewController * vc = [(UINavigationController *)segue.destinationViewController viewControllers].lastObject;
        vc.spot = sender;
    }
}

#pragma mark - Helper

- (void)setSearchBarVisible:(BOOL)visible {
    
    // Toggles the search bar.
    _mapView.showsSearchBar = visible;
    
    // Change hightlightment
    if (visible) {
        
        [self.searchBarButton setBackgroundImage:[UIImage imageNamed:@"searchButtonBackgroundHighlighted.png"]
                                        forState:UIControlStateNormal
                                      barMetrics:UIBarMetricsDefault];
        
        self.searchBarButton.tintColor = [UIColor whiteColor];
    } else {
        
        [self.searchBarButton setBackgroundImage:[UIImage imageNamed:@"searchButtonBackgroundNormal.png"]
                                        forState:UIControlStateNormal
                                      barMetrics:UIBarMetricsDefault];
        
        self.searchBarButton.tintColor = nil;
    }
}

/*
- (void)spotDeletedNotification {
    [self checkSpots];
}

- (void)removeDroppedSpots {
    [[DSGlobalData sharedData].childContext performBlock:^{
        NSFetchRequest * fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Spot"];
        fetchRequest.predicate = [NSPredicate predicateWithFormat:@"isSynced = 0 && isBeingSynced = 0"];
        
        NSArray * matches = [[DSGlobalData sharedData].childContext executeFetchRequest:fetchRequest error:nil];
        
        if (matches && matches.count > 0) {
            NSMutableArray * spots = [_spots mutableCopy];
            for (Spot * spot in matches) {
                for (Spot * s in spots) {
                    if (s == spot) {
                        [spots removeObject:s];
                        break;
                    }
                }
                
                [[DSGlobalData sharedData].childContext deleteObject:spot];
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                self.spots = spots;
            });
        }
    }];
}

- (void)checkSpots {
    NSMutableArray * spots = [self.spots mutableCopy];
    for (Spot * spot in self.spots) {
        if (spot.isDeleted) {
            [spots removeObject:spot];
        }
    }
    self.spots = spots;
    
    [_mapView update];
}

- (Spot *)createSpotAtLocationCoordinate:(CLLocationCoordinate2D)coordinate inManagedObjectContext:(NSManagedObjectContext *)context {
    // Create new spot with given coordinates
    Spot * spot = [Spot spotWithLatitude:coordinate.latitude
                               longitude:coordinate.longitude
                  inManagedObjectContext:context];
    
    // Add the author and author image.
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    if ([defaults objectForKey:kDSDropspotSessionDomainKeyName]) {
        User * user = [NSEntityDescription insertNewObjectForEntityForName:@"User" inManagedObjectContext:context];
        user.displayName = [[defaults objectForKey:kDSDropspotSessionDomainKeyName] valueForKey:kDSDropspotSessionNameKeyName];
        
        NSData * data = [[defaults objectForKey:kDSDropspotSessionDomainKeyName] valueForKey:kDSDropspotSessionImageKeyName];
        if (data) {
            UserPhoto * photo = [NSEntityDescription insertNewObjectForEntityForName:@"UserPhoto" inManagedObjectContext:context];
            photo.image = [UIImage imageWithData:data];
            user.photo = photo;
        }
        spot.user = user;
    }
    
    return spot;
}
*/
#pragma mark - DSMapView delegate

- (void)mapView:(DSMapView *)mapView didPressSpot:(Spot *)spot {
    
    // Show Spot View Controller
    [self performSegueWithIdentifier:@"spotDetail" sender:spot];
}

- (void)mapView:(DSMapView *)mapView didSelectSpot:(Spot *)spot {
    
    NSString * labelString = nil;
    
    if (spot.collections.count == 0) {
        labelString = spot.slug;
    } else {
        labelString = [NSString stringWithFormat:@"%@/%@", [(Collection *)spot.collections.anyObject slug], spot.slug];
    }
}

- (void)mapView:(DSMapView *)mapView didPressCreateSpot:(Spot *)spot {
    
    // Show Create/Edit Spot View Controller
    [self performSegueWithIdentifier:@"createSpot" sender:spot];
}

- (void)mapView:(DSMapView *)mapView didPressCancelSpot:(Spot *)spot {
    
    // Remove the object from database
    [[DSGlobalData sharedData].childContext performBlock:^{
        [[DSGlobalData sharedData].childContext deleteObject:spot];

        dispatch_async(dispatch_get_main_queue(), ^{

            // Tell the delegate that a spot was canceled.
            if ([self.delegate respondsToSelector:@selector(collectionMapViewControllerDidCancelSpot:)]) {
                [self.delegate collectionMapViewControllerDidCancelSpot:self];
            }
        });
    }];
}
/*
- (void)mapView:(DSMapView *)mapView didRecognizeLongPressAtLocationCoordinate:(CLLocationCoordinate2D)coordinate {
    if (kDSOwnCollectionApp) {
        // Remove spots that has been created befor and not synced.
        [self removeDroppedSpots];
        
        [[DSGlobalData sharedData].childContext performBlock:^{
            Spot * newSpot = [self createSpotAtLocationCoordinate:coordinate inManagedObjectContext:[DSGlobalData sharedData].childContext];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                // Search asynchronously for the address
                [DSServices geocodeBySearch:[NSString stringWithFormat:@"%f,%f", newSpot.latitude, newSpot.longitude] completion:^(NSArray *array) {
                    if (array && array.count > 0) {
                        NSString * address = array[0][@"formatted_address"];
                        [[DSGlobalData sharedData].childContext performBlock:^{
                            newSpot.address = address;
                        }];
                        
                        _mapView.searchBar.inputText = address;
                    }
                    [[DSGlobalData sharedData].childContext performBlock:^{
                        newSpot.isDeterminingAddress = NO;
                    }];
                    [[NSNotificationCenter defaultCenter] postNotificationName:kDSSpotDidDetermineAddressNotificationName object:nil userInfo:@{ @"spot" : newSpot }];
                }];
            });

            [[DSGlobalData sharedData].childContext performBlock:^{
                if (kDSOwnCollectionApp) {
                    if ([self.delegate respondsToSelector:@selector(collectionForCreationInMapViewController:)]) {
                        newSpot.collections = [NSSet setWithObject:[self.delegate collectionForCreationInMapViewController:self]];
                    }
                }
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSMutableArray * spots = [self.spots mutableCopy];
                    [spots addObject:newSpot];
                    self.spots = spots;
                    
                    self.selectedSpot = newSpot;
                });
            }];
        }];
    }
}
*/
- (void)mapView:(DSMapView *)mapView didChangeRegion:(DSMapViewRegion)region {
    
    // Fetch new spots from the database for the new region.
    [self loadSpotsForRegion:region];
}

#pragma mark - Map view dataSource

- (NSUInteger)mapView:(DSMapView *)mapView numberOfItemsForMapInfoView:(DSMapInfoView *)mapInfoView {
    
    NSLog(@"spot count %i", _spots.count);
    
    return _spots.count;
}

- (DSMapInfoItemViewController *)mapView:(DSMapView *)mapView viewControllerAtIndex:(NSUInteger)index {
    
    DSMapInfoItemViewController * viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"spotView"];
    viewController.spot = _spots[index];
    return viewController;
}

- (NSUInteger)mapView:(DSMapView *)mapView indexForSpot:(Spot *)spot {
    
    return [_spots indexOfObject:spot];
}

- (DSMapInfoItemViewController *)mapView:(DSMapView *)mapView viewControllerWithSpot:(Spot *)spot {
    
    DSMapInfoItemViewController * viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"spotView"];
    viewController.spot = spot;
    return viewController;
}

#pragma mark - Map data

- (void)loadSpotsForRegion:(DSMapViewRegion)region {
    
    // Forward to the actual function.
    [self loadSpotsForRegion:region completion:nil];
}

- (void)loadSpotsForRegion:(DSMapViewRegion)region completion:(void (^)(void))completion {
    
    __block NSMutableArray * spots = [NSMutableArray array];
    
    NSUInteger spotslimitPerRegion = 8;
    
    // Create region grid.
    DSMapViewRegionGrid regionGrid = DSMapViewRegionGridMake(4, 4);
    
    // Create a sliced region grid.
    NSArray * subRegions = [self slicedRegionWithRegion:region grid:regionGrid];
    
    for (int i = 0; i < subRegions.count; i++) {
        
        NSDictionary * subRegionDict = subRegions[i];
        
        DSMapViewRegion subRegion = DSMapViewRegionMake([subRegionDict[@"top"] doubleValue],
                                                        [subRegionDict[@"bottom"] doubleValue],
                                                        [subRegionDict[@"left"] doubleValue],
                                                        [subRegionDict[@"right"] doubleValue]);
        
        [self fetchSpotsInRegion:subRegion limit:spotslimitPerRegion completion:^(NSArray * regionSpots){
            
            [spots addObjectsFromArray:regionSpots];
            
            if (i == subRegions.count - 1) {
                
                _mapView.spots = spots;
                
                if (completion) {
                    completion();
                }
            }
        }];
    }
}

- (NSArray *)slicedRegionWithRegion:(DSMapViewRegion)region grid:(DSMapViewRegionGrid)grid {
    NSMutableArray * regions = [NSMutableArray array];
    
    double latitudeStep = (region.topLatitude - region.bottomLatitude) / grid.rows;
    double longitudeStep = (region.rightLongitude - region.leftLongitude) / grid.columns;
    
    for (int i = 0; i < grid.rows; i++) {
        for (int j = 0; j < grid.columns; j++) {
            [regions addObject:@{
                                 @"top" : @(region.topLatitude - (i * latitudeStep)),
                                 @"bottom" : @(region.topLatitude - ((i + 1) * latitudeStep)),
                                 @"left" : @(region.leftLongitude + (j * longitudeStep)),
                                 @"right" : @(region.leftLongitude + ((j + 1) * longitudeStep))
                                 }];
        }
    }
    
    return regions;
}

- (void)fetchSpotsInRegion:(DSMapViewRegion)region limit:(NSUInteger)limit completion:(void (^)(NSArray * regionSpots))completion {
    
    NSManagedObjectContext * context = [DSGlobalData sharedData].childContext;
    
    [context performBlock:^{
        
        NSFetchRequest * fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"Spot"];
        
        fetchRequest.predicate = [NSPredicate predicateWithFormat:@"latitude <= %f AND latitude > %f AND longitude >= %f AND longitude < %f AND ANY collections = %@", region.topLatitude, region.bottomLatitude, region.leftLongitude, region.rightLongitude, self.collection];
        fetchRequest.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"modified" ascending:NO]];
        fetchRequest.fetchLimit = limit;
        
        NSError * error;
        NSArray * result = [context executeFetchRequest:fetchRequest error:&error];
        
        if (error) {
            NSLog(@"fetchSpotsInRegion:limit:completion: An Error occured: %@", error.localizedDescription);
        } else {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (completion) {
                    completion(result);
                }
            });
        }
    }];
}

- (void)sortSpots:(NSArray *)spots byDistanceToLocation:(CLLocation *)location block:(void (^)(NSArray * sortedSpots))block {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        NSMutableArray * array = [NSMutableArray array];
        for (Spot * spot in spots) {
            CLLocation * l = [[CLLocation alloc] initWithLatitude:spot.latitude longitude:spot.longitude];
            [array addObject:@{@"spot" : spot,
                               @"distance" : [NSNumber numberWithFloat:[l distanceFromLocation:location]]}];
        }
        NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"distance" ascending:YES];
        NSArray * sortedSpotDictionaries = [array sortedArrayUsingDescriptors:@[descriptor]];
        
        NSMutableArray * sortedSpots = [NSMutableArray array];
        for (NSDictionary * dict in sortedSpotDictionaries) {
            [sortedSpots addObject:dict[@"spot"]];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if (block) {
                block(sortedSpots);
            }
        });
    });
}


@end

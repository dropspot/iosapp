/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  CollectionPhotoThumbnail.m
//  Dropspot3
//
//  Created by Lukas Würzburger on 17.09.13.
//  Copyright (c) 2013 Dropspot. All rights reserved.
//

#import "UIImageTransformer.h"

@implementation UIImageTransformer

+ (Class)transformedValueClass {
    return [NSData class];
}

+ (BOOL)allowsReverseTransformation {
    return YES;
}

- (id)transformedValue:(id)value {
    if (value == nil)
        return nil;
    
    if ([value isKindOfClass:[NSData class]])
        return value;
    
    return UIImagePNGRepresentation((UIImage *)value);
}

- (id)reverseTransformedValue:(id)value {
    return [UIImage imageWithData:(NSData *)value];
}

@end

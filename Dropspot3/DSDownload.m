/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  DSDownload.m
//  dropspotApp
//
//  Created by Lukas Würzburger on 14.05.13.
//  Copyright (c) 2013 dropspot GmbH. All rights reserved.
//

#import "DSDownload.h"

#import "DSDropspot.h"

@interface DSDownload () {
    DataBlock _successCompletion;
    ErrorBlock _errorCompletion;
    FloatBlock _progressBlock;
    
    NSMutableData * _receivedData;
    CGFloat _expectedSize;
}

@end

@implementation DSDownload

+ (DSDownload *)download {
    return [[self alloc] init];
}

- (void)runWithURL:(NSString *)urlString success:(DataBlock)successBlock fail:(ErrorBlock)error progress:(FloatBlock)progress {
    _url = urlString;
    _successCompletion = [successBlock copy];
    _errorCompletion = [error copy];
    _progressBlock = [progress copy];
    
    NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:_url] cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0];
    [request setValue:@"gzip" forHTTPHeaderField:@"Accept-Encoding"];
    
//    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
//    [request setValue:[NSString stringWithFormat:@"Token %@", [[defaults objectForKey:kDSDropspotSessionDomainKeyName] valueForKey:kDSDropspotSessionTokenKeyName]] forHTTPHeaderField:@"Authorization"];
    
    NSURLConnection * connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    if (connection) {
        //NSLog(@"DSDownload.url %@", _url);
        _receivedData = [[NSMutableData alloc] init];
    }
}

- (void)runWithURL:(NSString *)urlString success:(DataBlock)successBlock fail:(ErrorBlock)error {
    [self runWithURL:urlString success:successBlock fail:error progress:nil];
}

#pragma mark - connection delegate

- (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
    if ([challenge previousFailureCount] == 0) {
        NSURLCredential * newCredential = [NSURLCredential credentialWithUser:@"drpsptapicall" password:@"proudDropspot" persistence:NSURLCredentialPersistenceNone];
        [[challenge sender] useCredential:newCredential forAuthenticationChallenge:challenge];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    _receivedData.length = 0;
    _expectedSize = response.expectedContentLength;
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [_receivedData appendData:data];
    
    if (_progressBlock) {
        if (_expectedSize > 0) {
            _progressBlock(_receivedData.length / _expectedSize);
        } else {
            _progressBlock(NAN);
        }
    }
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    connection = nil;

    _receivedData = nil;
    
    if (_errorCompletion) {
        _errorCompletion(error);
        _errorCompletion = nil;
    }
    _successCompletion = nil;
    _progressBlock = nil;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    connection = nil;

    if (_successCompletion) {
        _successCompletion(_receivedData);
        _successCompletion = nil;
    }
    _errorCompletion = nil;
    _progressBlock = nil;
    
    _receivedData = nil;
    _expectedSize = 0.0f;
}

@end

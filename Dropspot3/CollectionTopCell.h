/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  CollectionTopCell.h
//  Dropspot3
//
//  Created by Lukas Würzburger on 3/5/14.
//  Copyright (c) 2014 Dropspot. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CollectionTopCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel * descriptionLabel;
@property (nonatomic, weak) IBOutlet UIButton * subscribeButton;
@property (nonatomic, weak) IBOutlet UIButton * addSpotButton;
@property (nonatomic, weak) IBOutlet UILabel * activateLabel;
@property (nonatomic, weak) IBOutlet UISwitch * activateSwitch;

- (void)setLayout;

+ (float)heightForCellWithDescription:(NSString *)description subscribeButton:(BOOL)subscribeButton addSpotButton:(BOOL)addSpotButton activateSwitch:(BOOL)activateSwitch;

@end

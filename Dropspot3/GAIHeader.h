/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  GAIHeader.h
//  Dropspot3
//
//  Created by Lukas Würzburger on 4/7/14.
//  Copyright (c) 2014 Dropspot GmbH. All rights reserved.
//

#import "GAI.h"
#import "GAIFields.h"
#import "GAIDictionaryBuilder.h"

#pragma mark - Screens

#define kDSScreenNameMainView                   @"MainView"
#define kDSScreenNameMapView                    @"MapView"
#define kDSScreenNameCollectionDetailView       @"CollectionDetailView"
#define kDSScreenNameCollectionMapView          @"CollectionMapView"
#define kDSScreenNameSpotDetailView             @"SpotDetailView"
#define kDSScreenNameSettingsView               @"SettingsView"
#define kDSScreenNameSettingsMeasurementView    @"SettingsMeasurementView"
#define kDSScreenNameBrowserView                @"BrowserView"
#define kDSScreenNameImageView                  @"SpotImageView"
#define kDSScreenNameSignInView                 @"SignInView"
#define kDSScreenNameNativeSignUpView           @"NativeSignUpView"
#define kDSScreenNameNativeSignInView           @"NativeSignInView"
#define kDSScreenNameLocationDeniedView         @"LocationDeniedView"
#define kDSScreenNameLocationPermissionView     @"LocationPermissionView"


#pragma mark - Categories

#define kDSCategoryNameOnboarding               @"Onboarding"
#define kDSCategoryNameKPI                      @"KPI"


#pragma mark - Events

#define kDSEventNameNativeLoginStart            @"NativeLoginStart"
#define kDSEventNameNativeLoginSuccess          @"NativeLoginSuccess"
#define kDSEventNameNativeLoginFailure          @"NativeLoginFailure"
#define kDSEventNameNativeLoginCancel           @"NativeLoginCancel"
#define kDSEventNameNativeLoginSignUpStart      @"NativeLoginSignUpStart"
#define kDSEventNameFBLoginStart                @"FBLoginStart"
#define kDSEventNameFBLoginSuccess              @"FBLoginSuccess"
#define kDSEventNameFBSignUpSuccess             @"FBSignUpSuccess"
#define kDSEventNameFBLoginFailure              @"FBLoginFailure"
#define kDSEventNameNativeSignUpStart           @"NativeSignUpStart"
#define kDSEventNameNativeSignUpSuccess         @"NativeSignUpSuccess"
#define kDSEventNameNativeSignUpFailure         @"NativeSignUpFailure"
#define kDSEventNameNativeSignUpCancel          @"NativeSignUpCancel"
#define kDSEventNameSkipLogin                   @"SkipLogin"

#define kDSEventNameAllowLocationOK             @"Allow Location"
#define kDSEventNameAllowLocationNo             @"AllowLocationNo"
#define kDSEventNamePrelimAllowLocationYes      @"PrelimAllowLocationYes"
#define kDSEventNamePrelimAllowLocationNo       @"PrelimAllowLocationNo"

#define kDSEventNameNotificationPushed          @"Notification"
#define kDSEventNameNotificationSwiped          @"Swipe Notification"

#define kDSEventNameSpotLinkTapped              @"SpotLinkTapped"


/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  CreateSpotViewController.h
//  Dropspot3
//
//  Created by Lukas Würzburger on 10/27/13.
//  Copyright (c) 2013 Dropspot. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Spot.h"

@class CreateSpotViewController;

@protocol CreateSpotViewControllerDelegate <NSObject>

- (void)createSpotViewControllerDidCancel:(CreateSpotViewController *)createSpotViewController;
- (void)createSpotViewControllerDidSave:(CreateSpotViewController *)createSpotViewController;

@end

@interface CreateSpotViewController : UIViewController

@property (nonatomic, weak) Spot * spot;

@property (nonatomic, assign) id <CreateSpotViewControllerDelegate> delegate;

@end

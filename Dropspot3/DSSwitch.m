/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  DSSwitch.m
//  Dropspot
//
//  Created by Lukas Würzburger on 19.07.13.
//  Copyright (c) 2013 Lukas Würzburger. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

#import "DSSwitch.h"
#import "DSGlobal.h"

@interface DSSwitch ()

@property (strong, nonatomic) UIView * buttonView;
@property (strong, nonatomic) UIView * backgroundSlideViewLeft;
@property (strong, nonatomic) UIView * backgroundSlideViewRight;

@property (assign, nonatomic) id target;
@property (assign, nonatomic) SEL selector;

@property (assign, nonatomic) float panStartButtonX;

@end

@implementation DSSwitch

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:CGRectMake(frame.origin.x, frame.origin.y, 45, 30)];
    if (self) {
        self.layer.masksToBounds = YES;
        self.layer.cornerRadius = 15;
        self.layer.borderColor = [UIColor colorWithWhite:0.9 alpha:1.0].CGColor;
        self.layer.borderWidth = 1;
        
        _on = YES;
        
        _tintColor = kDSColorCoral;//[UIColor colorWithRed:107.0f/255.0f green:180.0f/255.0f blue:70.0f/255.0f alpha:1.0f];
        
        _backgroundSlideViewLeft = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 45, 30)];
        _backgroundSlideViewLeft.backgroundColor = _tintColor;
        [self addSubview:_backgroundSlideViewLeft];
        
        _backgroundSlideViewRight = [[UIView alloc] initWithFrame:CGRectMake(30, 0, 45, 30)];
        _backgroundSlideViewRight.backgroundColor = [UIColor whiteColor];
        [self addSubview:_backgroundSlideViewRight];
        
        _buttonView = [[UIView alloc] initWithFrame:CGRectMake(15, 0, 30, 30)];
        _buttonView.backgroundColor = [UIColor whiteColor];
        _buttonView.layer.borderColor = [UIColor colorWithWhite:0.9 alpha:1.0].CGColor;
        _buttonView.layer.borderWidth = 1.0f;
        _buttonView.layer.masksToBounds = YES;
        _buttonView.layer.cornerRadius = 15;
        [self addSubview:_buttonView];
        
        UIPanGestureRecognizer * panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];
        [_buttonView addGestureRecognizer:panGesture];
        
        UITapGestureRecognizer * tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap)];
        [self addGestureRecognizer:tapGesture];
    }
    return self;
}

- (void)setTintColor:(UIColor *)tintColor {
    if (tintColor != nil && _tintColor != tintColor) {
        _tintColor = tintColor;
        
        // ui
    }
}

- (void)handlePan:(UIPanGestureRecognizer *)panGesture {
    
    CGPoint translation = [panGesture translationInView:self];
    
    if (panGesture.state == UIGestureRecognizerStateBegan) {
        _panStartButtonX = _buttonView.center.x;
    }
    
    float newX = _panStartButtonX + translation.x;
    
    if (newX < 15) {
        newX = 15;
    } else if (newX > self.frame.size.width - 15) {
        newX = self.frame.size.width - 15;
    }
    
    CGPoint center = _buttonView.center;
    center.x = newX;
    [_buttonView setCenter:center];
    
    center = _backgroundSlideViewLeft.center;
    center.x = newX - _backgroundSlideViewLeft.frame.size.width / 2;
    [_backgroundSlideViewLeft setCenter:center];
    
    center = _backgroundSlideViewRight.center;
    center.x = newX + _backgroundSlideViewRight.frame.size.width / 2;
    [_backgroundSlideViewRight setCenter:center];
    
    if (panGesture.state == UIGestureRecognizerStateEnded || panGesture.state == UIGestureRecognizerStateCancelled) {
        
        [UIView animateWithDuration:0.2 animations:^{
            CGPoint center = _buttonView.center;
            if (_buttonView.center.x < self.frame.size.width / 2) {
                center.x = 15;
                _on = NO;
            } else {
                center.x = self.frame.size.width - 15;
                _on = YES;
            }
            [_buttonView setCenter:center];
            
            center = _backgroundSlideViewLeft.center;
            center.x = _buttonView.center.x - _backgroundSlideViewLeft.frame.size.width / 2;
            [_backgroundSlideViewLeft setCenter:center];
            
            center = _backgroundSlideViewRight.center;
            center.x = _buttonView.center.x + _backgroundSlideViewRight.frame.size.width / 2;
            [_backgroundSlideViewRight setCenter:center];
        } completion:^(BOOL finished) {
            [self response];
        }];
    }
}

- (void)handleTap {
    self.on = !_on;
    [self response];
}

- (void)response {
    if (_target && _selector) {
        if ([_target respondsToSelector:_selector]) {
            NSMethodSignature *msig = [_target methodSignatureForSelector:_selector];
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
            if (msig != nil) {
                [_target performSelector:_selector withObject:self];
            } else {
                [_target performSelector:_selector];
            }
#pragma clang diagnostic pop
        }
    }
}

- (void)addTarget:(id)target selector:(SEL)selector {
    _target = target;
    _selector = selector;
}

- (void)setOn:(BOOL)on {
    [self setOn:on animated:YES];
}

- (void)setOn:(BOOL)on animated:(BOOL)animated {
    if (_on != on) {
        _on = on;
        
        void (^VoidBlock)(void) = ^{
            CGPoint center = _buttonView.center;
            if (!_on) {
                center.x = 15;
                _on = NO;
            } else {
                center.x = self.frame.size.width - 15;
                _on = YES;
            }
            [_buttonView setCenter:center];
            
            center = _backgroundSlideViewLeft.center;
            center.x = _buttonView.center.x - _backgroundSlideViewLeft.frame.size.width / 2;
            [_backgroundSlideViewLeft setCenter:center];
            
            center = _backgroundSlideViewRight.center;
            center.x = _buttonView.center.x + _backgroundSlideViewRight.frame.size.width / 2;
            [_backgroundSlideViewRight setCenter:center];
        };
        if (animated) {
            [UIView animateWithDuration:0.2 animations:VoidBlock];
        } else {
            VoidBlock();
        }
    }
}

@end

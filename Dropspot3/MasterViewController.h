/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  MasterViewController.h
//  Dropspot3
//
//  Created by Lukas Würzburger on 04.09.13.
//  Copyright (c) 2013 Dropspot. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import <CoreLocation/CoreLocation.h>

#import "_MasterViewController.h"

#import "Spot.h"
#import "UITableView+CurrentIndex.h"

@interface MasterViewController : _MasterViewController <NSFetchedResultsControllerDelegate, UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView * tableView;

@end

/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  DSAPISpotResponse.m
//  Dropspot3
//
//  Created by Lukas Würzburger on 10/30/13.
//  Copyright (c) 2013 Dropspot. All rights reserved.
//

#import "DSAPISpotResponse.h"

@implementation DSAPISpotResponse

- (NSString *)description {
    return [NSString stringWithFormat:@"<SpotResponse: %@>", self.slug];
}

- (DSAPIUserResponse *)user {
    if (!_user) {
        _user = [[DSAPIUserResponse alloc] init];
    }
    return _user;
}

@end

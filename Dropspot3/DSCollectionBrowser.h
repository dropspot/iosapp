/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  CollectionBrowserCell.h
//  Dropspot3
//
//  Created by Lukas Würzburger on 11/3/13.
//  Copyright (c) 2013 Dropspot. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Collection;

@interface DSCollectionBrowser : UIView

/**
 * Collections that should be displayed.
 */
@property (nonatomic, strong) NSArray * collections;

/**
 * Current selected collection.
 */
@property (nonatomic, weak) Collection * selectedCollection;

@end

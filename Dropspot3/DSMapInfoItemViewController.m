/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  DSMapInfoItemViewController.m
//  Dropspot3
//
//  Created by Lukas Würzburger on 07.10.13.
//  Copyright (c) 2013 Dropspot. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>
#import <QuartzCore/QuartzCore.h>

#import "DSMapInfoItemViewController.h"

#import "DSDropspot.h"
#import "DSLocationController.h"
#import "DSGlobal.h"
#import "DSSwitch.h"
#import "Collection.h"
#import "UILabel+AutoHeight.h"
#import "DataHelper.h"

@interface DSMapInfoItemViewController () {
    IBOutlet UIView * textView;
    IBOutlet UILabel * authorLabel;
    IBOutlet UILabel * textLabel;
    IBOutlet UIView * imageContainerView;
    IBOutlet UIImageView * imageView;
    IBOutlet UIImageView * arrowImageView;

    IBOutlet UIView * cancelView;
    IBOutlet UIView * createView;
    
    // Helper
    BOOL _isSpotNew;
}

- (IBAction)detailButtonTapped:(UIGestureRecognizer *)gesture;
- (IBAction)cancelButtonTapped:(UIGestureRecognizer *)gesture;
- (IBAction)createButtonTapped:(UIGestureRecognizer *)gesture;

@end

@implementation DSMapInfoItemViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor clearColor];
    textView.layer.cornerRadius = 3;
    textView.layer.shadowColor = [UIColor blackColor].CGColor;
    textView.layer.shadowOffset = CGSizeMake(0.f, 2.f);
    textView.layer.shadowOpacity = 0.2f;
    textView.layer.shadowRadius = 3.f;
    
    [self configureView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self configureView];
}

- (void)setSpot:(Spot *)spot {
    _spot = spot;
    
    [self configureView];
}

- (void)configureView {
    if (self.spot) {
        
        _isSpotNew = (self.spot.isOwnedByMe && !self.spot.isSynced && (!self.spot.text || [[self.spot.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]));
        
        if (_isSpotNew) {
            textLabel.text = @"Create Dropspot...";
        } else {
            if (kDSForceAuthorName) {
                authorLabel.text = kDSForceAuthorName;
            } else {
                if (self.spot.collections.count > 0) {
                    Collection * collection = [self.spot.collections anyObject];
                    authorLabel.text = collection.title;
                } else {
                    authorLabel.text = self.spot.user.displayName;
                }
            }
            
            textLabel.text = self.spot.text;

            imageView.image = [DataHelper imageWithFilePath:[DataHelper pathForCacheFile:self.spot.thumbnailFile]];
            if (!imageView.image) {
                imageView.image = [DataHelper imageWithFilePath:[DataHelper pathForCacheFile:self.spot.photoFile]];
            }
            [self setLayout];
        }
    }
}

- (void)setLayout {
    CGRect frame;
    
    textView.hidden = _isSpotNew;
    cancelView.hidden = !_isSpotNew;
    createView.hidden = !_isSpotNew;

    if (_isSpotNew) {
        createView.backgroundColor = kDSColorCoral;
        
        createView.layer.cornerRadius = 3;
        createView.layer.masksToBounds = YES;
        
        cancelView.layer.cornerRadius = 3;
        cancelView.layer.masksToBounds = YES;
        
        frame = self.view.frame;
        frame.size.height = cancelView.frame.origin.y + cancelView.frame.size.height + 5;
        self.view.frame = frame;
    } else {
        imageContainerView.layer.borderColor = [UIColor lightGrayColor].CGColor;
        imageContainerView.layer.borderWidth = 0.5f;
        imageView.layer.masksToBounds = YES;
        
        [authorLabel autoAdjustHeight];

        if (imageView.image) {
            imageContainerView.hidden = NO;
            
            frame = textLabel.frame;
            frame.size.width = imageContainerView.frame.origin.x - frame.origin.x - 10;
            textLabel.frame = frame;
        } else {
            imageContainerView.hidden = YES;
        }
        
        
        if (textLabel.text) {
            frame = [textLabel autoAdjustHeight];
            frame.origin.y = authorLabel.frame.origin.y + authorLabel.frame.size.height + 3;
            textLabel.frame = frame;
        }

        if (imageView.image) {
            frame = imageContainerView.frame;
            if ((textLabel.frame.origin.y + textLabel.frame.size.height + 20) / 2 - imageContainerView.frame.size.height / 2 > authorLabel.frame.origin.y + authorLabel.frame.size.height + 5) {
                frame.origin.y = (textLabel.frame.origin.y + textLabel.frame.size.height + 20) / 2 - imageContainerView.frame.size.height / 2;
            } else {
                frame.origin.y = authorLabel.frame.origin.y + authorLabel.frame.size.height + 5;
            }
            imageContainerView.frame = frame;
        }
        
        frame = textView.frame;
        if (imageView.image) {
            if (imageContainerView.frame.size.height > textLabel.frame.size.height) {
                frame.size.height = imageContainerView.frame.origin.y + imageContainerView.frame.size.height + 20;
            } else {
                frame.size.height = textLabel.frame.origin.y + textLabel.frame.size.height + 20;
            }
        } else {
            frame.size.height = textLabel.frame.origin.y + textLabel.frame.size.height + 20;
        }
        textView.frame = frame;
        
        frame = self.view.frame;
        frame.size.height = textView.frame.origin.y + textView.frame.size.height + 5;
        self.view.frame = frame;
    }
}

- (float)viewHeight {
    if (_isSpotNew) {
        return cancelView.frame.origin.y + cancelView.frame.size.height + 5;
    } else {
        return textView.frame.origin.y + textView.frame.size.height + 5;
    }
}

#pragma mark - IB Actions

- (IBAction)detailButtonTapped:(UIGestureRecognizer *)gesture {
    if ([self.delegate respondsToSelector:@selector(mapInfoItemDidPressSpot:)]) {
        [self.delegate mapInfoItemDidPressSpot:self];
    }
}

- (IBAction)cancelButtonTapped:(UIGestureRecognizer *)gesture {
    if ([self.delegate respondsToSelector:@selector(mapInfoItemDidPressCancelSpot:)]) {
        [self.delegate mapInfoItemDidPressCancelSpot:self.spot];
    }
}

- (IBAction)createButtonTapped:(UIGestureRecognizer *)gesture {
    if ([self.delegate respondsToSelector:@selector(mapInfoItemDidPressCreateSpot:)]) {
        [self.delegate mapInfoItemDidPressCreateSpot:self.spot];
    }
}


@end

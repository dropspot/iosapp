/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  DSAppDelegate.m
//  SingleCollection
//
//  Created by Lukas Würzburger on 2/10/14.
//  Copyright (c) 2014 Lukas Würzburger. All rights reserved.
//

#import "AppDelegate.h"

#import "RootNavigationController.h"
#import "_MasterViewController.h"
#import "OnboardingViewController.h"
#import "DSLocationPermissionViewController.h"
#import "DSLocationDeniedViewController.h"

#import "Spot+Location.h"
#import "Spot+Dropspot.h"
#import "Collection+Dropspot.h"
#import "User+Dropspot.h"
#import "DSGlobal.h"
#import "DSGlobalData.h"
#import "DSSyncController.h"

#import "SpotLink.h"

#import "BrowserViewController.h"
#import "SpotViewController.h"

#import "DSReaderView.h"

#define kDSLocationHitRadius 300

#define kDSRefreshTrackedSpotsRegionIdentifier @"kDSRefreshTrackedSpotsRegionIdentifier"
#define kDSRefreshTrackedSpotsRegionAlternateIdentifier @"kDSRefreshTrackedSpotsRegionAlternateIdentifier"

#define kSyncInterval 21600 // 21600
#define kAnalyticsInterval 21600 // 21600
#define kDifferenceBetweenSameSpotNotification 432000 // 432000
#define kDifferenceBetweenNotifications 300 // 300
#define kNumberOfNotificationsUntilTurnOff 2 // 2
#define kNumberOfNotificationsPerDay 2 // 2

@interface AppDelegate () <MasterViewControllerDelegate, OnboardingViewControllerDelegate, UINavigationControllerDelegate> {
    BOOL _didReceiveLocalNotification;
    NSDate * _lastNotificationDate;
    
    // In case we hit multiple spots at the same time.
    NSMutableArray * _hitRegions;
    BOOL _isCheckingHitRegion;

    BOOL _isLoadingImages;
    
    // Background task used for loading images and syncing.
    UIBackgroundTaskIdentifier _backgroundTask;
}

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    // Application appearance
    [application setStatusBarStyle:UIStatusBarStyleDefault];
    [application.keyWindow setTintColor:kDSColorCoral];
    
    // Interface connections
    self.rootNavigationController = (RootNavigationController *)self.window.rootViewController;
    self.rootNavigationController.navigationBar.tintColor = kDSColorCoral;
    self.masterViewController = (_MasterViewController *)self.rootNavigationController.topViewController;
    self.masterViewController.delegate = self;
    self.masterViewController.locationManager = self.locationManager;
    
    // User session
    [DSDropspot session].delegate = self;
    [[DSDropspot session] checkSession];
    [DSFacebook session].delegate = self;

    
    // Location manager
    
        if (self.locationManager && [CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorized) {
            if (application.applicationState == UIApplicationStateBackground) {
                [self.locationManager startMonitoringSignificantLocationChanges];
            } else {
                [self.locationManager startUpdatingLocation];
            }
        }
    
    
    // Analytics
/*
    NSDictionary *appDefaults = @{ @"allowTracking" : @(kGoogleAnalyticsAllowTracking) };
    [[NSUserDefaults standardUserDefaults] registerDefaults:appDefaults];
    [GAI sharedInstance].optOut = ![[NSUserDefaults standardUserDefaults] boolForKey:@"allowTracking"];
    [GAI sharedInstance].dispatchInterval = kAnalyticsInterval;
    [GAI sharedInstance].trackUncaughtExceptions = YES;
    [[GAI sharedInstance].logger setLogLevel:kGAILogLevelNone];
    */
    // Initialize the MixpanelAPI object
    self.mixpanel = [Mixpanel sharedInstanceWithToken:kMixPanelAnalyticsToken];
    
    self.mixpanel.checkForSurveysOnActive = NO;
    self.mixpanel.showSurveyOnActive = NO; //Change this to NO to show your surveys manually.
    self.mixpanel.checkForNotificationsOnActive = NO;
    self.mixpanel.showNotificationOnActive = NO; //Change this to NO to show your notifs manually.
    
    // Set the upload interval to 20 seconds for demonstration purposes. This would be overkill for most applications.
    self.mixpanel.flushInterval = kAnalyticsInterval; // defaults to 60 seconds
    
    // Launch options
    [self handleLaunchOptions:launchOptions];
    
    // Notification center
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkSessionState) name:kDSDropspotSessionChangedNotificationName object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showLoginView) name:kDSDropspotShowLoginNotificationName object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshTrackedSpots) name:kDSDropspotSyncingFinished object:nil];
    
    return YES;
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    
    _didReceiveLocalNotification = NO;
    
    _backgroundTask = [application beginBackgroundTaskWithExpirationHandler:^ {
        
        // Clean up code. Tell the system that we are done.
        [application endBackgroundTask:_backgroundTask];
        _backgroundTask = UIBackgroundTaskInvalid;
    }];
    
    [[DSGlobalData sharedData] saveContext];
    
    // To make the code block asynchronous
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        // Last location update doesn't matter anymore.
        [self removeLocationUpdate];
        
        if (self.locationManager && [CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorized) {
            [self.locationManager stopUpdatingLocation];
            [self.locationManager startMonitoringSignificantLocationChanges];
            
            [self refreshTrackedSpotsWithCompletion:^{
                
                if (!_isLoadingImages) {
                    
                    // Clean up code. Tell the system that we are done.
                    [application endBackgroundTask:_backgroundTask];
                    _backgroundTask = UIBackgroundTaskInvalid;
                }
            }];
        } else {
            
            if (!_isLoadingImages) {
                
                // Clean up code. Tell the system that we are done.
                [application endBackgroundTask:_backgroundTask];
                _backgroundTask = UIBackgroundTaskInvalid;
            }
        }
    });
    
#ifdef DEBUG
    [self simulateHitSpot];
#endif
}

- (void)applicationWillResignActive:(UIApplication *)application {
    
    // Check audio outputs
    [self.rootNavigationController.readerView stop];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    [[DSFacebook session] handleDidBecomeActive];

    [self checkSessionState];
    
    if (!_didReceiveLocalNotification) {
        [self resumeLoadingOfPhotos];
    }

#ifdef DEBUG
    [application setApplicationIconBadgeNumber:0];
#endif
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        if (self.locationManager && [CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorized) {
            [self.locationManager stopMonitoringSignificantLocationChanges];
            [self.locationManager startUpdatingLocation];
        }
    });

    if ([DSUser currentUser].username) {
        [self.mixpanel identify:[DSUser currentUser].username];
        
        if (!_didReceiveLocalNotification) {
            
            [self.mixpanel track:@"Open Application" properties:@{ @"Action" : @"Normal",
                                                                   @"Username" : [DSUser currentUser].username }];
        } else {

            [self.mixpanel track:@"Open Application" properties:@{ @"Action" : @"Swipe Notification",
                                                                   @"Username" : [DSUser currentUser].username }];
        }
    }
}

- (void)applicationWillTerminate:(UIApplication *)application {
    [self removeLocationUpdate];

    [[DSGlobalData sharedData] saveContext];
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    return [[DSFacebook session] handleOpenURL:url];
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification {

    // Handle the incoming notification.
    [self handleLocalNotification:notification];
}

- (void)handleLaunchOptions:(NSDictionary *)launchOptions {
    
    // Local notifications
    UILocalNotification * notification = [launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey];
    if (notification && [notification isKindOfClass:[UILocalNotification class]]) {
        
        // Handle the incoming notification.
        [self handleLocalNotification:notification];
    }
    
    // Significant location change
    id object = [launchOptions objectForKey:UIApplicationLaunchOptionsLocationKey];
    if (object) {
        [[DSLog sharedLog] logWithUser:@"lukas" event:@"location key"];
        
        // Check if the last syncing date is more than 3 hours ago.
        if ([DSGlobal sharedGlobals].lastSyncDate &&
            [NSDate date].timeIntervalSince1970 - [DSGlobal sharedGlobals].lastSyncDate.timeIntervalSince1970 > kSyncInterval) {
            
            // Start sync.
            [[DSGlobalData sharedData] synchronizeWithCompletion:^(BOOL success, NSError *error) {
                
                // Refresh the tracked spots
                [self refreshTrackedSpots];
            }];
        }
    }
}

#pragma mark - Local notification

- (void)handleLocalNotification:(UILocalNotification *)notification {
    _didReceiveLocalNotification = YES;
    
    [[DSGlobalData sharedData].childContext performBlock:^{

        NSString * slug = notification.userInfo[@"spot.slug"];
        if (slug) {
            Spot * spot = [Spot spotWithSlug:slug inManagedObjectContext:[DSGlobalData sharedData].childContext];
            if (spot) {
                float distance = [spot.location distanceFromLocation:self.locationManager.location];
                float radius = kDSLocationHitRadius;
                float accuracy = self.locationManager.location.horizontalAccuracy;
                if (distance <= radius + accuracy) {
                    [spot addHistoryWithType:kSpotHistoryEventTypeSeen inManagedObjectContext:[DSGlobalData sharedData].childContext];
                    spot.active = NO;
                    [[DSGlobalData sharedData].childContext save:nil];
                    
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                        [self stopTrackingSpot:spot];
                    });
                }
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    if ([DSGlobal sharedGlobals].readaloud) {
                        
                        // Read spot text out loud.
                        [self.rootNavigationController.readerView readText:spot.text delay:4 completion:nil tapGesture:^{
                            
                            if (spot.links.count) {
                                
                                SpotLink * sl = spot.links.anyObject;
                                if (sl && [sl isKindOfClass:[SpotLink class]]) {
                                    
                                    NSURL * url = [NSURL URLWithString:sl.link];
                                    
                                    if (url) {
                                        
                                        BrowserViewController * bVC = [self.masterViewController.storyboard instantiateViewControllerWithIdentifier:@"browserView"];
                                        bVC.url = url;
                                        [self.rootNavigationController pushViewController:bVC animated:YES];
                                        return;
                                    }
                                }
                            }
                            
                            // This will be executed if there is no link
                            SpotViewController * sVC = [self.masterViewController.storyboard instantiateViewControllerWithIdentifier:@"spotViewController"];
                            sVC.spot = spot;
                            [self.rootNavigationController pushViewController:sVC animated:YES];
                        }];
                    }
                    
                    [self.rootNavigationController popToRootViewControllerAnimated:NO];
                    
                    if (self.masterViewController.presentedViewController) {
                        [self.masterViewController.presentedViewController dismissViewControllerAnimated:YES completion:^{
                            [self.masterViewController showSpot:spot];
                        }];
                    } else {
                        [self.masterViewController showSpot:spot];
                    }
                    
                    [[DSGlobalData sharedData] saveContext];
                    
                    
                    NSMutableDictionary * dict = [NSMutableDictionary dictionary];
                    if (spot.collections.count) {
                        [dict setValue:[(Collection *)spot.collections.allObjects.firstObject slug] forKey:@"Collection"];
                        [dict setValue:[NSString stringWithFormat:@"%@/%@",[(Collection *)spot.collections.allObjects.firstObject slug], spot.slug] forKey:@"Info"];
                    } else {
                        [dict setValue:spot.slug forKey:@"Info"];
                    }
                    if ([DSUser currentUser].username) {
                        [[Mixpanel sharedInstance] identify:[DSUser currentUser].username];
                        [dict setValue:[DSUser currentUser].username forKey:@"Username"];
                    }
                    [dict setValue:spot.slug forKey:@"Spot"];
                    [dict setValue:@(distance) forKey:@"Distance"];
                    [self.mixpanel track:kDSEventNameNotificationSwiped properties:dict];
                });
            }
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [[UIApplication sharedApplication] cancelLocalNotification:notification];
        });
    }];
}

#pragma mark - OnboardingViewController delegate

- (void)onboardingViewController:(OnboardingViewController *)onboardingViewController didChangeStatus:(OnboardingStatus)status {
    if (status == OnboardingStatusSignedIn) {
        if ([DSUser currentUser]) {
            [self checkSessionState];
            
            [DSGlobal sharedGlobals].readaloud = YES;
            
            [[DSDropspot session] profileWithBlock:^(DSAPIProfileResponse *profile, NSError *error) {
                
                // Analytics
                [self.mixpanel reset];
                
                NSMutableDictionary * dict = [NSMutableDictionary dictionary];
                
                if ([DSUser currentUser].state == DSUserStateTemporary) {
                    [dict setObject:@"Skip" forKey:@"Login Type"];
                } else {
                    [dict setObject:[self stringFromUserLoginType:[DSUser currentUser].loginType] forKey:@"Login Type"];
                }
                if (profile.username) {
                    [dict setObject:profile.username forKey:@"Username"];
                }
                
                [self.mixpanel.people set:dict];
            }];
        }
    } else if (status == OnboardingStatusComplete) {
        [onboardingViewController dismissViewControllerAnimated:YES completion:^{
            [self checkSessionState];
        }];
    }
}

- (void)onboardingViewControllerDidCancel:(OnboardingViewController *)onboardingViewController {
    if ([DSUser currentUser].state == DSUserStateTemporary) {
        [onboardingViewController dismissViewControllerAnimated:YES completion:^{
            // Do nothing.
        }];
    }
}

#pragma mark - Location permission

- (void)showLocationPermissionView {
    if (![self.window.rootViewController.presentedViewController isKindOfClass:[OnboardingViewController class]] &&
        !self.window.rootViewController.presentedViewController.isBeingPresented &&
        !self.window.rootViewController.presentedViewController.isBeingDismissed) {
        OnboardingViewController * onboardingViewController = [self.masterViewController.storyboard instantiateViewControllerWithIdentifier:@"onboardingViewController"];
        onboardingViewController.delegate = self;
        onboardingViewController.screen = OnboardingScreenLocationPermission;
        [self.window.rootViewController presentViewController:onboardingViewController animated:YES completion:nil];
    }
}

- (void)showLocationPermissionDeniedView {
    if (![self.window.rootViewController.presentedViewController isKindOfClass:[DSLocationDeniedViewController class]] &&
        !self.window.rootViewController.presentedViewController.isBeingPresented &&
        !self.window.rootViewController.presentedViewController.isBeingDismissed) {
        DSLocationDeniedViewController * locationDeniedViewController = [self.masterViewController.storyboard instantiateViewControllerWithIdentifier:@"noLocation"];
        [self.window.rootViewController presentViewController:locationDeniedViewController animated:YES completion:nil];
    }
}

#pragma mark - Master view delegate

- (BOOL)masterViewControllerShouldRefreshInMainView:(_MasterViewController *)masterViewController {
    BOOL should = _didReceiveLocalNotification;
    _didReceiveLocalNotification = NO;
    return should;
}

#pragma mark - Dropspot session

- (void)checkSessionState {
    if ([DSUser currentUser]) {
        if (!_didReceiveLocalNotification) {
            [self.masterViewController refresh];
        }

        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self showLocationPermissionView];
                });
            } else if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorized) {
                if ([self.window.rootViewController.presentedViewController isKindOfClass:[DSLocationDeniedViewController class]]) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.window.rootViewController.presentedViewController dismissViewControllerAnimated:YES completion:nil];
                    });
                }
            } else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self showLocationPermissionDeniedView];
                });
            }
        });
    } else {
        [self showLoginView];
    }
}

- (void)showLoginView {
    if (![self.window.rootViewController.presentedViewController isKindOfClass:[OnboardingViewController class]] &&
        !self.window.rootViewController.presentedViewController.isBeingPresented &&
        !self.window.rootViewController.presentedViewController.isBeingDismissed) {
        OnboardingViewController * onboardingViewController = [self.masterViewController.storyboard instantiateViewControllerWithIdentifier:@"onboardingViewController"];
        onboardingViewController.delegate = self;
        [self.window.rootViewController presentViewController:onboardingViewController animated:YES completion:nil];
    }
}

- (void)dropspotSessionChangedState:(DSDropspot *)dropspot state:(DSDropspotSessionState)state error:(NSError *)error {
    switch (state) {
        case DSDropspotSessionStateOpen:
            
            break;
        case DSDropspotSessionStateLoginFailed: {
            
        }
            break;
        case DSDropspotSessionStateFailed:
        case DSDropspotSessionStateClose: {
            [DSUser killCurrentUser];
        }
            break;
            
        default:
            break;
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kDSDropspotSessionChangedNotificationName object:nil];
}

#pragma mark - Facebook

- (void)sessionStateChanged:(FBSession *)session state:(FBSessionState)state error:(NSError *)error {
    switch (state) {
        case FBSessionStateOpen: {
            if (![DSFacebook session].isUserLoggedIn) {
                [DSFacebook session].isUserLoggedIn = YES;
                [[DSDropspot session] openSessionWithFacebookToken:session.accessTokenData.accessToken block:nil];
            }
        }
            break;
        case FBSessionStateClosed: case FBSessionStateClosedLoginFailed: {
            [FBSession.activeSession closeAndClearTokenInformation];
            [DSFacebook session].isUserLoggedIn = NO;
        }
            break;
        default:
            break;
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kDSFacebookSessionChangedNotificationName object:nil];
}

#pragma mark - Location manager

- (CLLocationManager *)locationManager {
    if (!_locationManager) {
        _locationManager = [[CLLocationManager alloc] init];
        _locationManager.delegate = self;
        _locationManager.desiredAccuracy = kCLLocationAccuracyKilometer;
        _locationManager.distanceFilter = kCLDistanceFilterNone;
    }
    
    return _locationManager;
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    if (status == kCLAuthorizationStatusAuthorized) {
        if (self.locationManager) {
            if ([UIApplication sharedApplication].applicationState == UIApplicationStateBackground) {
                [self.locationManager startMonitoringSignificantLocationChanges];
            } else {
                [self.locationManager startUpdatingLocation];
            }
        }
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    
    if ([UIApplication sharedApplication].applicationState == UIApplicationStateBackground) {
        // Significant location change
        
        // Sync only every 6 hours when app is in background.
        if (![DSGlobal sharedGlobals].lastSyncDate || (
            [DSGlobal sharedGlobals].lastSyncDate &&
            [NSDate date].timeIntervalSince1970 - [DSGlobal sharedGlobals].lastSyncDate.timeIntervalSince1970 > kSyncInterval)) {
            
            // Start syncing
            [[DSGlobalData sharedData] synchronizeWithCompletion:^(BOOL success, NSError *error) {
                
                if (success) {
                    
                    [self refreshTrackedSpots];
                }
            }];
        }
        
        // Refresh tracked spots
        [self refreshTrackedSpots];
        
    } else {
        
        // Get the last location update.
        [DSGlobal lastLocationUpdate:^(BOOL available, NSDate *date, CLLocation *location) {
            
            // Trigger update
            // if last location update is older than 5 minutes,
            // or distance to last location is more than 100 meters,
            // or the location accuracy is bad (> 100)
            
            if (!available || (available && ([NSDate date].timeIntervalSince1970 - date.timeIntervalSince1970 > 300 ||
                                             [self.locationManager.location distanceFromLocation:location] > 100 ||
                                             location.horizontalAccuracy > 100))) {
                
                // Save current location
                [self saveLocationUpdate:self.locationManager.location];
                
                // Post notification for some view controller to update the UI.
                [[NSNotificationCenter defaultCenter] postNotificationName:kDSLocationManagerDidUpdateLocationNotificationName
                                                                    object:nil
                                                                  userInfo:@{ kDSLocationManagerDidUpdateLocationKey : [locations lastObject] }];
            }
        }];
    }
}

- (void)locationManager:(CLLocationManager *)manager didEnterRegion:(CLRegion *)region {
    
    if (!_hitRegions) {
        _hitRegions = [NSMutableArray array];
    }
    
    // Add hit region to array
    [_hitRegions addObject:region];
    
    [self checkHitRegions];
}

- (void)checkHitRegions {
    
    if (!_isCheckingHitRegion) {
        
        if (_hitRegions.count) {
            
            _isCheckingHitRegion = YES;
            
            // Check first region in the array.
            CLRegion * region = _hitRegions.firstObject;
            [self checkRegion:region completion:^(BOOL success){
                
                _isCheckingHitRegion = NO;
                
                if (success) {
                    
                    // If a notification was triggered destroy object.
                    [_hitRegions removeAllObjects];
                    _hitRegions = nil;
                } else {
                    
                    // If this region didn't trigger a notification try next one.
                    [_hitRegions removeObject:region];
                    [self checkHitRegions];
                }
            }];
        } else {
            
            // Destroy array
            _hitRegions = nil;
        }
    }
}

- (void)checkRegion:(CLRegion *)region completion:(void (^)(BOOL success))completion {
    
    [[DSGlobalData sharedData].childContext performBlock:^{
        
        // Fetch spot.
        Spot * spot = [Spot spotWithSlug:region.identifier
                  inManagedObjectContext:[DSGlobalData sharedData].childContext];
        
        // Does the spot still exist?
        if (!spot) {
            
            if (completion) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    completion(NO);
                });
            }
            return;
        }
        
        // Check expiration date of the spot.
        if (spot.expiresAt && [spot.expiresAt compare:[NSDate date]] == NSOrderedAscending) {
            
            // Spot date expired.
            if (completion) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    completion(NO);
                });
            }
            return;
        }
        
        // Check previous notifications of this spot.
        for (History * h in spot.history) {
            
            // If a notification of the same spot was triggered within the last 5 days, return.
            if (h.eventType == kSpotHistoryEventTypeNotification &&
                [NSDate date].timeIntervalSince1970 - h.date.timeIntervalSince1970 < kDifferenceBetweenSameSpotNotification) { // 432000 seconds = 5 days
                
                if (completion) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(NO);
                    });
                }
                return;
            }
        }
        
        // Check if the app has already presented two notifications today.
        NSArray * result = [self historyOfType:kSpotHistoryEventTypeNotification
                                         limit:kNumberOfNotificationsPerDay
                        inManagedObjectContext:[DSGlobalData sharedData].childContext];
        
        if (result && result.count == kNumberOfNotificationsPerDay) {
            int numberOfNotificationsToday = 0;
            
            for (History * h in result) {
                if ([self dateIsToday:h.date]) {
                    numberOfNotificationsToday++;
                }
            }
            
            if (numberOfNotificationsToday >= kNumberOfNotificationsPerDay) {
                
                if (completion) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(NO);
                    });
                }
                return;
            }
        }
        
        // Check distance from the user's current location to the spot.
        if ([spot.location distanceFromLocation:self.locationManager.location] <= kDSLocationHitRadius + self.locationManager.location.horizontalAccuracy) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                // Check if any other notification was triggered within last 5 minutes.
                if ([NSDate date].timeIntervalSince1970 - _lastNotificationDate.timeIntervalSince1970 < kDifferenceBetweenNotifications) { // 300 seconds = 5 minutes
                    
                    if (completion) {
                        completion(NO);
                    }
                    return;
                }
                
                if (completion) {
                    completion(YES);
                }
                
                _lastNotificationDate = [NSDate date];
                
                [[DSGlobalData sharedData].childContext performBlock:^{
                    [spot addHistoryWithType:kSpotHistoryEventTypeVisited inManagedObjectContext:[DSGlobalData sharedData].childContext];
                    [spot addHistoryWithType:kSpotHistoryEventTypeNotification inManagedObjectContext:[DSGlobalData sharedData].childContext];
                    
                    if (spot.visits >= kNumberOfNotificationsUntilTurnOff) {
                        spot.active = NO;
                        [self stopTrackingSpot:spot];
                    }
                    
                    [[DSGlobalData sharedData].childContext save:nil];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [[DSGlobalData sharedData] saveContext];
                        
                        
                        [self refreshTrackedSpots];
                    });
                }];
                
                __block UIBackgroundTaskIdentifier backgroundTask;
                backgroundTask = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{
                    
                    // Clear code
                    [[UIApplication sharedApplication] endBackgroundTask:backgroundTask];
                    backgroundTask = UIBackgroundTaskInvalid;
                    
                }];
                
                // Present a local notification in main thread.
                [self presentNotificationWithSpot:spot];
                
                // Clear code
                [[UIApplication sharedApplication] endBackgroundTask:backgroundTask];
                backgroundTask = UIBackgroundTaskInvalid;
                
            });
        } else {
            if (completion) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    completion(NO);
                });
            }
        }
    }];
}

- (void)locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region {
    
    // Check if this event is triggered by meta-geofence
    if ([region.identifier isEqualToString:kDSRefreshTrackedSpotsRegionIdentifier] ||
        [region.identifier isEqualToString:kDSRefreshTrackedSpotsRegionAlternateIdentifier]) {
        
        // Refresh tracked spots.
        [self refreshTrackedSpots];
    }
}

- (void)presentNotificationWithSpot:(Spot *)spot {
    if (spot) {
        CTCallCenter * callcenter = [[CTCallCenter alloc] init];
        BOOL shouldPlayQuite = ([[MPMusicPlayerController iPodMusicPlayer] playbackState] == MPMusicPlaybackStatePlaying || [callcenter currentCalls] != nil);
        
        // Create local notification
        UILocalNotification * localNotification = [[UILocalNotification alloc] init];
        NSString * notificationBody = [NSString string];
        
        if (self.locationManager.location.horizontalAccuracy <= 100) {

            __block int distance = (int)[self.locationManager.location distanceFromLocation:spot.location];
            __block NSString * mm = @"";
            
            [DSGlobal distancePropertiesFromDistance:distance properties:^(double value, NSString *measurement) {
                distance = (int)floor(value/50.) * 50;
                if (distance == 0) {
                    distance = (int)roundf(value);
                }
                mm = measurement;
            }];
            
            if (distance == 0) {
                notificationBody = [NSString stringWithFormat:@"< 5%@: %@", [DSGlobal lowestMeasurement], spot.text];
            } else {
                notificationBody = [NSString stringWithFormat:@"%i%@: %@", distance, mm, spot.text];
            }
        } else {
            notificationBody = spot.text;
        }
        localNotification.alertBody = notificationBody;
        localNotification.userInfo = @{ @"spot.slug" : spot.slug };
        
        if (shouldPlayQuite) {
            localNotification.soundName = @"leise.caf";
        } else {
            localNotification.soundName = @"laut.caf";
        }
        
        [[UIApplication sharedApplication] presentLocalNotificationNow:localNotification];
        
        double distance = [self.locationManager.location distanceFromLocation:spot.location];
        
        NSMutableDictionary * dict = [NSMutableDictionary dictionary];
        if (spot.collections.count) {
            [dict setValue:[(Collection *)spot.collections.allObjects.firstObject slug] forKey:@"Collection"];
            [dict setValue:[NSString stringWithFormat:@"%@/%@",[(Collection *)spot.collections.allObjects.firstObject slug], spot.slug] forKey:@"Info"];
        } else {
            [dict setValue:spot.slug forKey:@"Info"];
        }
        if ([DSUser currentUser].username) {
            [[Mixpanel sharedInstance] identify:[DSUser currentUser].username];
            [dict setValue:[DSUser currentUser].username forKey:@"Username"];
        }
        [dict setValue:spot.slug forKey:@"Spot"];
        [dict setValue:@(distance) forKey:@"Distance"];
        [self.mixpanel track:kDSEventNameNotificationPushed properties:dict];

        // Notification sound won't be played when application state is UIApplicationStateActive
        
        UIApplicationState state = [UIApplication sharedApplication].applicationState;
        
        if (state == UIApplicationStateActive) {
            NSURL * path;
            if (shouldPlayQuite) {
                path = [[NSBundle mainBundle] URLForResource:@"leise" withExtension:@"caf"];
            } else {
                path = [[NSBundle mainBundle] URLForResource:@"laut" withExtension:@"caf"];
            }
            CFURLRef soundFileURLRef = (__bridge CFURLRef) path;
            SystemSoundID notification_sound;
            AudioServicesCreateSystemSoundID (soundFileURLRef, &notification_sound);
            AudioServicesPlaySystemSound(notification_sound);
        }
        
        [self refreshTrackedSpots];
    }
}

#pragma mark - Helper

- (NSArray *)historyOfType:(double)type limit:(int)limit inManagedObjectContext:(NSManagedObjectContext *)context {
    
    NSFetchRequest * fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"History"];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"eventType = %i", type];
    fetchRequest.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"date" ascending:NO]];
    fetchRequest.fetchLimit = limit;
    
    return [context executeFetchRequest:fetchRequest error:nil];
}

- (BOOL)dateIsToday:(NSDate *)aDate {
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDateComponents *components = [cal components:(NSEraCalendarUnit|NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit) fromDate:[NSDate date]];
    NSDate *today = [cal dateFromComponents:components];
    components = [cal components:(NSEraCalendarUnit|NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit) fromDate:aDate];
    NSDate *otherDate = [cal dateFromComponents:components];
    
    return [today isEqualToDate:otherDate];
}

- (void)stopTrackingSpot:(Spot *)spot {
    
    // Search for the monitored region object related to the spot.
    for (CLRegion * region in self.locationManager.monitoredRegions) {
        if ([region.identifier isEqualToString:spot.slug]) {
            [self.locationManager stopMonitoringForRegion:region];
            break;
        }
    }
}

- (void)refreshTrackedSpots {
    [self refreshTrackedSpotsWithCompletion:nil];
}

- (void)refreshTrackedSpotsWithCompletion:(void (^)(void))completion {
    NSLog(@"Refresh tracked spots");
    [[DSGlobalData sharedData].childContext performBlock:^{
        
        // Get nearby spot sorted by distance.
        NSMutableArray * array = [[Spot spotsSortedByDistanceFromLocation:self.locationManager.location
                                                                    limit:18
                                                           shouldBeActive:YES
                                                   inManagedObjectContext:[DSGlobalData sharedData].childContext] mutableCopy];
        
        NSMutableArray * stopMonitoring = [NSMutableArray array];
        NSMutableArray * startMonitoring = [NSMutableArray array];
        
        for (CLCircularRegion * region in self.locationManager.monitoredRegions) {
            BOOL spotShouldStillBeTracked = NO;
            for (Spot * spot in array) {
                if ([region.identifier isEqualToString:spot.slug]) {
                    spotShouldStillBeTracked = YES;
                    [array removeObject:spot];
                    break;
                }
            }
            if (!spotShouldStillBeTracked) {
                [stopMonitoring addObject:region];
            }
        }
        for (Spot * spot in array) {
            CLCircularRegion * newRegion = [[CLCircularRegion alloc] initWithCenter:spot.location.coordinate radius:spot.radius identifier:spot.slug];
            [startMonitoring addObject:newRegion];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            for (CLRegion * region in stopMonitoring) {
                NSLog(@"UNTRACK: %@", region.identifier);
                [self.locationManager stopMonitoringForRegion:region];
            }
            for (CLRegion * region in startMonitoring) {
                if (self.locationManager.monitoredRegions.count < 20) {
                    BOOL exists = NO;
                    for (CLCircularRegion * oldRegion in self.locationManager.monitoredRegions) {
                        if ([oldRegion.identifier isEqualToString:region.identifier]) {
                            exists = YES;
                        }
                    }
                    if (!exists) {
                        [self.locationManager startMonitoringForRegion:region];
                        NSLog(@"TRACK: %@", region.identifier);
                    }
                }
            }
            
            double maxDistance = 0.0;
            
            for (CLCircularRegion * region in self.locationManager.monitoredRegions) {
                
                // Calculare distance to current location
                CLLocation * regionLocation = [[CLLocation alloc] initWithLatitude:region.center.latitude
                                                                         longitude:region.center.longitude];
                double regionDistance = [regionLocation distanceFromLocation:self.locationManager.location];
                
                if (regionDistance > maxDistance) {
                    maxDistance = regionDistance;
                }
            }
            
            // Set fence regions
            CLCircularRegion * fenceRegion = [[CLCircularRegion alloc] initWithCenter:self.locationManager.location.coordinate
                                                                               radius:maxDistance
                                                                           identifier:kDSRefreshTrackedSpotsRegionIdentifier];
            NSLog(@"track fence with distance %f", maxDistance);
            CLCircularRegion * alternateFenceRegion = [[CLCircularRegion alloc] initWithCenter:self.locationManager.location.coordinate
                                                                               radius:maxDistance + 300
                                                                           identifier:kDSRefreshTrackedSpotsRegionAlternateIdentifier];
            [self.locationManager startMonitoringForRegion:fenceRegion];
            [self.locationManager startMonitoringForRegion:alternateFenceRegion];
            
            if (completion) {
                completion();
            }
        });
    }];
}

- (void)saveLocationUpdate:(CLLocation *)location {
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary * dict = @{
                            @"date" : [NSDate date],
                            @"lat" : [NSNumber numberWithDouble:location.coordinate.latitude],
                            @"lng" : [NSNumber numberWithDouble:location.coordinate.longitude],
                            @"acc" : [NSNumber numberWithDouble:location.horizontalAccuracy]
                            };
    [defaults setObject:dict forKey:@"locationUpdate"];
    [defaults synchronize];
}

- (void)removeLocationUpdate {
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    if ([defaults objectForKey:@"locationUpdate"]) {
        [defaults removeObjectForKey:@"locationUpdate"];
        [defaults synchronize];
    }
}

- (NSString *)stringFromUserLoginType:(DSUserLoginType)loginType {
    if (loginType == DSUserLoginTypeNative) {
        return @"Native";
    } else if (loginType == DSUserLoginTypeFacebook) {
        return @"Facebook";
    } else {
        return @"None";
    }
}

#pragma mark - Photos

- (void)resumeLoadingOfPhotos {
    
    // Query should fetch collection, spot or user. Each of these have to have a photo URL property. In addition to that the photo entity have to have a URL property. To prevent connection loss between objects and photos compare URLs. Build a for-loop for each of the main objects and check the connection to their photos.
    
    if (![DSSyncController sharedController].isRunning) {
    
        NSLog(@"resume laoding photo ");
        
        NSManagedObjectContext * context = [DSGlobalData sharedData].childContext;
        
        _isLoadingImages = YES;
        
        [self resumePhotoLoadingForUsersInManagedObjectContext:context completion:^{
            
            [self resumePhotoLoadingForCollectionsInManagedObjectContext:context completion:^{
                
                [self resumePhotoLoadingForSpotsInManagedObjectContext:context completion:^{
                    
                    if ([[DSGlobalData sharedData].childContext hasChanges]) {
                        [[DSGlobalData sharedData].childContext performBlock:^{
                            [[DSGlobalData sharedData].childContext save:nil];
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                _isLoadingImages = NO;
                                
                                if (_backgroundTask != UIBackgroundTaskInvalid) {
                                    
                                    // Clean up code. Tell the system that we are done.
                                    [[UIApplication sharedApplication] endBackgroundTask:_backgroundTask];
                                    _backgroundTask = UIBackgroundTaskInvalid;
                                }
                            });
                        }];
                    } else {
                        _isLoadingImages = NO;
                        
                        if (_backgroundTask != UIBackgroundTaskInvalid) {
                            
                            // Clean up code. Tell the system that we are done.
                            [[UIApplication sharedApplication] endBackgroundTask:_backgroundTask];
                            _backgroundTask = UIBackgroundTaskInvalid;
                        }
                    }
                }];
            }];
        }];
    }
}

- (void)resumePhotoLoadingForCollectionsInManagedObjectContext:(NSManagedObjectContext *)context completion:(void (^)(void))completion {
    [context performBlock:^{
        
        NSFetchRequest * fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"Collection"];
        fetchRequest.predicate = [NSPredicate predicateWithFormat:@"photoFile = nil OR thumbnailFile = nil"];
        fetchRequest.returnsObjectsAsFaults = NO;
        
        NSArray * result = [context executeFetchRequest:fetchRequest error:nil];
        if (result) {
            
            for (int i = 0; i < result.count; i++) {
                
                Collection * collection = result[i];
                
                if (!collection.thumbnailFile) {
                    
                    [collection resumeLoadingThumbnailInManagedObjectContext:context completion:^{
                        
                        if ([UIApplication sharedApplication].applicationState != UIApplicationStateBackground) {
                            
                            [[NSNotificationCenter defaultCenter] postNotificationName:kCollectionPhotoLoadingFinished
                                                                                object:nil];
                        }
                        
                        if (!collection.photoFile) {

                            [collection resumeLoadingPhotoInManagedObjectContext:context completion:^{
                                
                                if ([UIApplication sharedApplication].applicationState != UIApplicationStateBackground) {
                                    
                                    [[NSNotificationCenter defaultCenter] postNotificationName:kCollectionPhotoLoadingFinished
                                                                                        object:nil];
                                }
                            }];
                        }
                    }];
                } else {
                    
                    if (!collection.photoFile) {
                        
                        [collection resumeLoadingPhotoInManagedObjectContext:context completion:^{
                            
                            if ([UIApplication sharedApplication].applicationState != UIApplicationStateBackground) {
                                
                                [[NSNotificationCenter defaultCenter] postNotificationName:kCollectionPhotoLoadingFinished
                                                                                    object:nil];
                            }
                        }];
                    }
                }
            }
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            if (completion) {
                completion();
            }
        });
    }];
}

- (void)resumePhotoLoadingForSpotsInManagedObjectContext:(NSManagedObjectContext *)context completion:(void (^)(void))completion {

    [context performBlock:^{
        
        NSFetchRequest * fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"Spot"];
        fetchRequest.predicate = [NSPredicate predicateWithFormat:@"photoFile = nil OR thumbnailFile = nil"];
        fetchRequest.returnsObjectsAsFaults = NO;

        NSArray * result = [context executeFetchRequest:fetchRequest error:nil];
        if (result) {
            
            for (int i = 0; i < result.count; i++) {
                
                Spot * spot = result[i];
                
                if (!spot.thumbnailFile) {
                    
                    [spot resumeLoadingThumbnailInManagedObjectContext:context completion:^{
                        
                        if ([UIApplication sharedApplication].applicationState != UIApplicationStateBackground) {
                            
                            [[NSNotificationCenter defaultCenter] postNotificationName:kSpotPhotoLoadingFinished
                                                                                object:nil];
                        }
                    }];
                }
            }
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            if (completion) {
                completion();
            }
        });
    }];
}

- (void)resumePhotoLoadingForUsersInManagedObjectContext:(NSManagedObjectContext *)context completion:(void (^)(void))completion {

    [context performBlock:^{
        
        NSFetchRequest * fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"User"];
        fetchRequest.predicate = [NSPredicate predicateWithFormat:@"photoFile = nil"];
        fetchRequest.returnsObjectsAsFaults = NO;
        
        NSArray * result = [context executeFetchRequest:fetchRequest error:nil];
        if (result) {
            
            for (int i = 0; i < result.count; i++) {
                
                User * user = result[i];
                [user loadPhotoInManagedObjectContext:context completion:^{
                    
                    if ([UIApplication sharedApplication].applicationState != UIApplicationStateBackground) {
                        
                        [[NSNotificationCenter defaultCenter] postNotificationName:kUserPhotoLoadingFinished
                                                                            object:nil];
                    }
                }];
            }
        }
        if (completion) {
            dispatch_async(dispatch_get_main_queue(), ^{
                completion();
            });
        }
    }];
}

#ifdef DEBUG

#pragma mark - Simulation

- (void)simulateHitSpot {
    
    [[DSGlobalData sharedData].childContext performBlock:^{

        NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Spot"];
        NSError * error = nil;
        NSArray * matches = [[DSGlobalData sharedData].childContext executeFetchRequest:request error:&error];
        
        if (matches && matches.count > 0) {
            Spot * spot = matches[arc4random() % [matches count]];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self presentNotificationWithSpot:spot];
            });
        }
    }];
    //NSLog(@"simulate hit spot");
    // ------
    /*
    UILocalNotification * localNotification = [[UILocalNotification alloc] init];
    localNotification.alertBody = @"Hier wird die neue Fachhochschule gebaut – mit Mehrkosten von über 100 Milionen Euro";
    localNotification.userInfo = @{ @"spot.slug" : @"hier-ist-die-von-fluchtlingen-besetzte-schule-in-der-es-immer-wieder-gewalt" };
    localNotification.fireDate = [NSDate dateWithTimeIntervalSinceNow:1];
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    */
    
}

#endif

@end

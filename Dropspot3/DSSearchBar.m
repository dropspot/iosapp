/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  DSSearchBar.m
//  Dropspot3
//
//  Created by Lukas Würzburger on 1/30/14.
//  Copyright (c) 2014 Dropspot. All rights reserved.
//

#import "DSSearchBar.h"

#import "UIImage+Tint.h"

#import "DSGlobal.h"

#define kDSSearchBarDefaultHeight 44
#define kDSSearchBarDefaultButtonWidth 45

@interface DSSearchBar () {
    UISearchBar * _searchBar;
    UILabel * _infoLabel;
}

@end

@implementation DSSearchBar

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, kDSSearchBarDefaultHeight)];
    if (self) {
        // View
        self.backgroundColor = [UIColor clearColor];
        
        // Blur view
        UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:self.bounds];
        toolbar.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        toolbar.barTintColor = nil;
        [self insertSubview:toolbar atIndex:0];
        
        // Searchbar
        _searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
        _searchBar.barStyle = UIBarStyleDefault;
        _searchBar.barTintColor = nil;
        _searchBar.backgroundImage = [UIImage imageWithColor:[UIColor clearColor]];
        _searchBar.tintColor = kDSColorCoral;
        _searchBar.placeholder = translate(@"MAP_VIEW_SEARCHBAR_PLACEHOLDER");
        
        [self addSubview:_searchBar];
    }
    return self;
}

#pragma mark - Setter & Getter

- (BOOL)becomeFirstResponder {
    return [_searchBar becomeFirstResponder];
}

- (BOOL)resignFirstResponder {
    return [_searchBar resignFirstResponder];
}

- (void)setDelegate:(id<UISearchBarDelegate>)delegate {
    if (_delegate != delegate) {
        _delegate = delegate;
        _searchBar.delegate = delegate;
    }
}

- (void)setLeftButtonItem:(UIButton *)leftButtonItem {
    if (_leftButtonItem != leftButtonItem) {
        if (_leftButtonItem.superview) {
            [_leftButtonItem removeFromSuperview];
        }

        _leftButtonItem = leftButtonItem;
        
        if (_leftButtonItem) {
            [self addSubview:_leftButtonItem];
        }
    }
    
    [self setLayout];
}

- (void)setInputText:(NSString *)inputText {
    _inputText = inputText;
    
    _searchBar.text = _inputText;
}

- (NSString *)text {
    return _searchBar.text;
}

- (void)setInfoText:(NSString *)infoText {
    _infoText = infoText;
    
    if (_infoText) {
        if (!_infoLabel) {
            _infoLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, _searchBar.frame.origin.y + _searchBar.frame.size.height, self.frame.size.width - 10, 21)];
            _infoLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:14.0];
            _infoLabel.textColor = [UIColor darkGrayColor];
            _infoLabel.backgroundColor = [UIColor clearColor];
            _infoLabel.textAlignment = NSTextAlignmentCenter;
            [self addSubview:_infoLabel];
        }
        
        _infoLabel.text = _infoText;
    } else {
        if (_infoLabel) {
            [_infoLabel removeFromSuperview];
            _infoLabel = nil;
        }
    }
    
    [self setLayout];
}

#pragma mark - Helper

- (void)setLayout {
    CGRect frame;
    
    // Left button item
    frame = _leftButtonItem.frame;
    frame.origin.x = 0;
    frame.origin.y = 0;
    frame.size.width = kDSSearchBarDefaultButtonWidth;
    frame.size.height = kDSSearchBarDefaultHeight;
    _leftButtonItem.frame = frame;
    
    // Search bar positioning
    frame = _searchBar.frame;

    if (self.leftButtonItem) {
        frame.origin.x = 40;
        frame.size.width = self.frame.size.width - 40;
    } else {
        frame.origin.x = 0;
        frame.size.width = self.frame.size.width;
    }

    _searchBar.frame = frame;
    
    // Adjust view size
    if (_infoText) {
        frame = self.frame;
        frame.size.height = _searchBar.frame.size.height + _infoLabel.frame.size.height + 10;
        self.frame = frame;
    }
}

@end

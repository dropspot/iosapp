/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  DSSignInTextField.m
//  Dropspot3
//
//  Created by Lukas Würzburger on 3/4/14.
//  Copyright (c) 2014 Dropspot. All rights reserved.
//

#import "DSSignInTextField.h"

@implementation DSSignInTextField

- (CGRect)textRectForBounds:(CGRect)bounds {
    return CGRectInset(bounds, 10, 10);
}

- (CGRect)editingRectForBounds:(CGRect)bounds {
    return CGRectInset(bounds, 10, 10);
}

- (void) drawPlaceholderInRect:(CGRect)rect {
    [[UIColor lightGrayColor] setFill];
    [self.placeholder drawInRect:CGRectInset(rect, 0, 3)
                  withAttributes:@{
                                   NSFontAttributeName : self.font,
                                   NSForegroundColorAttributeName : [UIColor lightGrayColor]
                                   }];
}

@end

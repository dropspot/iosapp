/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  DSLocationPermissionViewController.m
//  Dropspot3
//
//  Created by Lukas Würzburger on 4/15/14.
//  Copyright (c) 2014 Dropspot GmbH. All rights reserved.
//

#import "DSLocationPermissionViewController.h"

#import "DSGlobal.h"
#import "DSUser.h"

@interface DSLocationPermissionViewController () <CLLocationManagerDelegate, UIAlertViewDelegate> {
    NSArray * _demoImagesArray;
    int _currentDemoImageIndex;
    NSTimer * _timer;
    
    CLLocationManager * _locationManager;
}

@property (nonatomic, weak) IBOutlet UIImageView * demoImageView;
@property (nonatomic, weak) IBOutlet UILabel * headLabel;
@property (nonatomic, weak) IBOutlet UILabel * subLabel;
@property (nonatomic, weak) IBOutlet UIButton * enableLocationButton;
@property (nonatomic, weak) IBOutlet UIButton * denyLocationButton;

- (IBAction)enableLocationButtonPressed:(id)sender;
- (IBAction)denyLocationButtonPressed:(id)sender;

@end

@implementation DSLocationPermissionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSLog(@"call location permission view");

    // Localize labels
    self.headLabel.text = translate(@"LOCATION_PERMISSION_VIEW_HEAD_LABEL");
    self.subLabel.text = translate(@"LOCATION_PERMISSION_VIEW_SUB_LABEL");
    [self.enableLocationButton setTitle:translate(@"LOCATION_PERMISSION_VIEW_ENABLE_LOCATION_BUTTON") forState:UIControlStateNormal];
    [self.denyLocationButton setTitle:translate(@"LOCATION_PERMISSION_VIEW_DENY_LOCATION_BUTTON") forState:UIControlStateNormal];
    
    self.enableLocationButton.layer.borderColor = self.enableLocationButton.titleLabel.textColor.CGColor;
    self.enableLocationButton.layer.borderWidth = 1.0;
    
    if (kDSForceLocationPermissionDemoImages) {
        _demoImagesArray = [NSArray arrayWithArray:kDSForceLocationPermissionDemoImages];
        
        self.demoImageView.image = [UIImage imageNamed:_demoImagesArray[0]];
        
        [self startTimer];
    } else {
        _demoImagesArray = @[@"onboarding-location.png", @"onboarding-location2.png", @"onboarding-location3.png"];

        [self startTimer];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[DSGlobal sharedGlobals] analyticsView:kDSScreenNameLocationPermissionView];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationFade];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];

    if (_timer) {
        [_timer invalidate];
        _timer = nil;
    }
}

- (BOOL)shouldAutorotate {
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

#pragma mark - Helper

- (void)shakeAllowButton {
    [UIView transitionWithView:self.enableLocationButton duration:0.05 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.enableLocationButton.transform = CGAffineTransformMakeRotation(-0.06);
    } completion:^(BOOL finished) {
        [UIView transitionWithView:self.enableLocationButton duration:0.05 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            self.enableLocationButton.transform = CGAffineTransformMakeRotation(0.06);
        } completion:^(BOOL finished) {
            [UIView transitionWithView:self.enableLocationButton duration:0.05 options:UIViewAnimationOptionCurveEaseInOut animations:^{
                self.enableLocationButton.transform = CGAffineTransformMakeRotation(-0.06);
            } completion:^(BOOL finished) {
                [UIView transitionWithView:self.enableLocationButton duration:0.05 options:UIViewAnimationOptionCurveEaseInOut animations:^{
                    self.enableLocationButton.transform = CGAffineTransformMakeRotation(0.06);
                } completion:^(BOOL finished) {
                    [UIView transitionWithView:self.enableLocationButton duration:0.05 options:UIViewAnimationOptionCurveEaseInOut animations:^{
                        self.enableLocationButton.transform = CGAffineTransformMakeRotation(-0.06);
                    } completion:^(BOOL finished) {
                        [UIView transitionWithView:self.enableLocationButton duration:0.05 options:UIViewAnimationOptionCurveEaseInOut animations:^{
                            self.enableLocationButton.transform = CGAffineTransformMakeRotation(0.06);
                        } completion:^(BOOL finished) {
                            self.enableLocationButton.transform = CGAffineTransformMakeRotation(0.0);
                        }];
                    }];
                }];
            }];
        }];
    }];
}

#pragma mark - Timer

- (void)startTimer {
    if (_timer) {
        [_timer invalidate];
        _timer = nil;
    }
    _currentDemoImageIndex = 0;
    
    _timer = [NSTimer scheduledTimerWithTimeInterval:3.0 target:self selector:@selector(handleTimerTick:) userInfo:nil repeats:YES];
}

- (void)handleTimerTick:(NSTimer *)timer {
    _currentDemoImageIndex++;
    if (_currentDemoImageIndex >= _demoImagesArray.count) {
        _currentDemoImageIndex = 0;
    }
    
    [UIView transitionWithView:self.demoImageView duration:0.6 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
        self.demoImageView.image = [UIImage imageNamed:_demoImagesArray[_currentDemoImageIndex]];
    } completion:nil];
}

#pragma mark - IBAction

- (IBAction)enableLocationButtonPressed:(id)sender {
//    [[DSGlobal sharedGlobals] analyticsEvent:kDSEventNamePrelimAllowLocationYes category:kDSCategoryNameOnboarding];

    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.delegate = self;
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    _locationManager.distanceFilter = kCLDistanceFilterNone;
    [_locationManager startUpdatingLocation];
}

- (IBAction)denyLocationButtonPressed:(id)sender {
//    [[DSGlobal sharedGlobals] analyticsEvent:kDSEventNamePrelimAllowLocationNo category:kDSCategoryNameOnboarding];

    [[[UIAlertView alloc] initWithTitle:translate(@"LOCATION_PERMISSION_VIEW_DENY_ALERT")
                                message:nil
                               delegate:self
                      cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
}

#pragma mark - Location manager delegate

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    if (status != kCLAuthorizationStatusNotDetermined) {
        
        CLLocationCoordinate2D coords;
        
        if (manager.location) {
            coords = manager.location.coordinate;
        }
        
        [manager stopUpdatingLocation];
        
        if (status == kCLAuthorizationStatusAuthorized) {
            if ([DSUser currentUser].username) {
                [[Mixpanel sharedInstance] identify:[DSUser currentUser].username];
                
                [[Mixpanel sharedInstance] track:kDSEventNameAllowLocationOK properties:@{
                                                                                             @"Username" : [DSUser currentUser].username,
                                                                                             @"Location" : [NSString stringWithFormat:@"%f,%f", coords.latitude, coords.longitude]
                                                                                             }];
            }
            
            if ([self.delegate respondsToSelector:@selector(locationPermissionViewControllerDidAllow:)]) {
                [self.delegate locationPermissionViewControllerDidAllow:self];
            }
        } else if (status == kCLAuthorizationStatusDenied || status == kCLAuthorizationStatusRestricted) {
            //[[DSGlobal sharedGlobals] analyticsEvent:kDSEventNameAllowLocationNo category:kDSCategoryNameOnboarding];

            if ([self.delegate respondsToSelector:@selector(locationPermissionViewControllerDidDeny:)]) {
                [self.delegate locationPermissionViewControllerDidDeny:self];
            }
        }
    }
}

#pragma mark - Alert view delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    [self shakeAllowButton];
}

@end

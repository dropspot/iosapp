/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  _MasterViewController.m
//  Dropspot3
//
//  Created by Lukas Würzburger on 4/7/14.
//  Copyright (c) 2014 Dropspot GmbH. All rights reserved.
//

#import "_MasterViewController.h"

@interface _MasterViewController ()

@end

@implementation _MasterViewController

- (void)checkSessionState {}
- (void)refresh {}
- (void)refreshManually:(BOOL)manually {}
- (void)showSpot:(Spot *)spot {}
- (void)updateLocation:(CLLocation *)location {}

@end

/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  WelcomeViewController.m
//  Dropspot3
//
//  Created by Lukas Würzburger on 16.10.13.
//  Copyright (c) 2013 Dropspot. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>

#import "DSLocationController.h"

#import "WelcomeViewController.h"

#import "WelcomeSlideViewController.h"

@interface WelcomeViewController () <UIPageViewControllerDataSource, UIPageViewControllerDelegate, WelcomeSlideViewControllerDelegate> {
    UIPageViewController * thePageViewController;
}

@end

@implementation WelcomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(locationPermissionDidChange) name:kDSLocationControllerAuthorizationStatusDidChangeNotificationName object:nil];
    
    thePageViewController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
    thePageViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    thePageViewController.dataSource = self;
    thePageViewController.delegate = self;
    [self.view addSubview:thePageViewController.view];
    
    WelcomeSlideViewController * vc = [self slideViewControllerForIndex:0];
    [thePageViewController setViewControllers:@[vc] direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
}

- (WelcomeSlideViewController *)slideViewControllerForIndex:(NSInteger)index {
    if (index < 0 || index > 4) {
        return nil;
    }
    
    WelcomeSlideViewController * slideViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"welcomeSlide"];
    slideViewController.delegate = self;
    
    DSWelcomeSlide * slide = [[DSWelcomeSlide alloc] init];
    slide.index = index;
    NSString * translationKey = [NSString stringWithFormat:@"WELCOME_SCREEN_DESCRIPTION_%d", (index + 1)];
    slide.descriptionText = translate(translationKey);
    slide.shouldRepeatVideo = (index == 0);
    slide.showLocationButton = (index == 0 && [CLLocationManager authorizationStatus] != kCLAuthorizationStatusAuthorized);
    slide.enableNextButton = !slide.showLocationButton;
    slide.enablePrevButton = (index > 0);
    slide.videoURLString = [[[NSBundle mainBundle] bundlePath] stringByAppendingPathComponent:[NSString stringWithFormat:@"%lu.mp4", (long)index]];
    
    slideViewController.slide = slide;
    
    return slideViewController;
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(WelcomeSlideViewController *)viewController {
    return [self slideViewControllerForIndex:viewController.slide.index + 1];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(WelcomeSlideViewController *)viewController {
    return [self slideViewControllerForIndex:viewController.slide.index - 1];
}

- (void)welcomeSlideViewControllerDidPressNextButton:(WelcomeSlideViewController *)welcomeSlideViewController {
    WelcomeSlideViewController * vc = [self slideViewControllerForIndex:welcomeSlideViewController.slide.index + 1];
    if (vc) {
        [thePageViewController setViewControllers:@[vc] direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:nil];
    } else {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)welcomeSlideViewControllerDidPressPrevButton:(WelcomeSlideViewController *)welcomeSlideViewController {
    [thePageViewController setViewControllers:@[[self slideViewControllerForIndex:welcomeSlideViewController.slide.index - 1]] direction:UIPageViewControllerNavigationDirectionReverse animated:YES completion:nil];
}

- (void)locationPermissionDidChange {
    if ([(WelcomeSlideViewController *)[thePageViewController.viewControllers lastObject] slide].index == 0) {
        [self welcomeSlideViewControllerDidPressNextButton:[thePageViewController.viewControllers lastObject]];
    }
}

@end

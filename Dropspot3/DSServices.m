/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  DSServices.m
//  Dropspot3
//
//  Created by Lukas Würzburger on 1/29/14.
//  Copyright (c) 2014 Dropspot. All rights reserved.
//

#import <LRResty/LRResty.h>

#import "DSServices.h"

@implementation DSServices

+ (void)geocodeBySearch:(NSString *)searchString completion:(void(^)(NSArray * array))completion {
    __block NSArray * results = [NSArray array];
    
    NSString * url = @"http://maps.googleapis.com/maps/api/geocode/";
    NSString * output = @"json";
    NSString * search = [searchString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSString * urlString = [NSString stringWithFormat:@"%@%@?sensor=true&address=%@", url, output, search];
    
    LRRestyClient * client = [LRResty client];
    [client get:urlString withBlock:^(LRRestyResponse *response) {
        if (response.status == 200) {
            id json = [NSJSONSerialization JSONObjectWithData:response.responseData options:kNilOptions error:nil];
            if ([json isKindOfClass:[NSDictionary class]]) {
                if ([json[@"status"] isEqualToString:@"OK"]) {
                    results = [NSArray arrayWithArray:json[@"results"]];
                } else {
                    results = @[];
                }
            } else {
                results = @[];
            }
        } else {
            results = @[];
        }
        
        if (completion) {
            completion(results);
        }
    }];
}

+ (void)addressCompletionBySearch:(NSString *)searchString completion:(void(^)(NSArray * array))completion {
    NSString * url = @"https://maps.googleapis.com/maps/api/place/autocomplete/";
    NSString * output = @"json";
    NSString * apiKey = @"AIzaSyD94o4FYzi2ZWyrMYht_PQ42pORjjkVSLA";
    NSString * search = [searchString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSString * urlString = [NSString stringWithFormat:@"%@%@?sensor=true&input=%@&key=%@", url, output, search, apiKey];
    
    __block NSArray * result = [NSArray array];
    
    LRRestyClient * client = [LRResty client];
    [client get:urlString withBlock:^(LRRestyResponse *response) {
        if (response.status == 200) {
            id json = [NSJSONSerialization JSONObjectWithData:response.responseData options:kNilOptions error:nil];
            
            if ([json isKindOfClass:[NSDictionary class]]) {
                if ([[json valueForKey:@"status"] isEqualToString:@"OK"]) {
                    result = [NSArray arrayWithArray:[json objectForKey:@"predictions"]];
                } else {
                    result = @[];
                }
            } else {
                result = @[];
            }
        } else {
            result = @[];
        }
        if (completion) {
            completion(result);
        }
    }];
}


@end

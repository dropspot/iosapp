/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  UILabel+AutoHeight.h
//  Dropspot3
//
//  Created by Lukas Würzburger on 11/8/13.
//  Copyright (c) 2013 Dropspot. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (AutoHeight)

- (CGRect)autoAdjustHeight;

@end

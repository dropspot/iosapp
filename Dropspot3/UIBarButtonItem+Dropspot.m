/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  UIBarButtonItem+Dropspot.m
//  Dropspot3
//
//  Created by Lukas Würzburger on 5/26/14.
//  Copyright (c) 2014 Dropspot GmbH. All rights reserved.
//

#import "UIBarButtonItem+Dropspot.h"

#import "UIImage+Tint.h"
#import "DSGlobal.h"

@implementation UIBarButtonItem (Dropspot)

+ (UIBarButtonItem *)mapBarButtonItemWithTarget:(id)target action:(SEL)action {
    
    UIButton * button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    button.backgroundColor = [UIColor clearColor];
    [button setImage:[[UIImage imageNamed:@"mapButton.png"] tintedImageWithColor:kDSColorCoral] forState:UIControlStateNormal];
    [button setImage:[[UIImage imageNamed:@"mapButton.png"] tintedImageWithColor:[kDSColorCoral colorWithAlphaComponent:0.25]] forState:UIControlStateHighlighted];
    [button addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    
    UIView * view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [view addSubview:button];
    
    return [[UIBarButtonItem alloc] initWithCustomView:view];
}

+ (UIBarButtonItem *)refreshBarButtonItemWithTarget:(id)target action:(SEL)action {
    
    UIButton * button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    button.backgroundColor = [UIColor clearColor];
    [button setImage:[[UIImage imageNamed:@"reloadButton.png"] tintedImageWithColor:kDSColorCoral] forState:UIControlStateNormal];
    [button setImage:[[UIImage imageNamed:@"reloadButton.png"] tintedImageWithColor:[kDSColorCoral colorWithAlphaComponent:0.25]] forState:UIControlStateHighlighted];
    [button addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    
    UIView * view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [view addSubview:button];
    
    return [[UIBarButtonItem alloc] initWithCustomView:view];
}

+ (UIBarButtonItem *)backBarButtonItemWithTarget:(id)target action:(SEL)action {
    
    CGRect frame = [translate(@"MAP_VIEW_BACK_BUTTON_TITLE") boundingRectWithSize:CGSizeMake(200, CGFLOAT_MAX)
                                                                          options:(NSStringDrawingUsesFontLeading | NSStringDrawingUsesLineFragmentOrigin)
                                                                       attributes:@{ NSFontAttributeName : [UIFont boldSystemFontOfSize:17.0] }
                                                                          context:nil];
    
    UIImage * image = [[UIImage imageNamed:@"backButtonArrow.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 13.5f, 20, 13.5f)];
    
    UIButton * button = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, frame.size.width + 40, 21.0f)];
    [button setBackgroundImage:image  forState:UIControlStateNormal];
    [button setBackgroundImage:[[image tintedImageWithColor:[kDSColorCoral colorWithAlphaComponent:0.25]] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 13.5f, 20, 13.5f)] forState:UIControlStateHighlighted];
    [button setTitle:translate(@"MAP_VIEW_BACK_BUTTON_TITLE") forState:UIControlStateNormal];
    [button setTitleColor:kDSColorCoral forState:UIControlStateNormal];
    [button setTitleColor:[kDSColorCoral colorWithAlphaComponent:0.25] forState:UIControlStateHighlighted];
    [button addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    
    return [[UIBarButtonItem alloc] initWithCustomView:button];
}

+ (UIBarButtonItem *)locateMeBarButtonItemWithTarget:(id)target action:(SEL)action {
    
    return [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"arrow.png"]
                                            style:UIBarButtonItemStyleBordered
                                           target:target
                                           action:action];
}


+ (UIBarButtonItem *)searchBarButtonItemWithTarget:(id)target action:(SEL)action {
    
    UIBarButtonItem * barButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSearch
                                                                                target:target
                                                                                action:action];
    [barButton setBackgroundImage:[UIImage imageNamed:@"searchButtonBackgroundNormal.png"]
                         forState:UIControlStateNormal
                       barMetrics:UIBarMetricsDefault];
    return barButton;
}

@end

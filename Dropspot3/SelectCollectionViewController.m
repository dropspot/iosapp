/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  SelectCollectionViewController.m
//  Dropspot3
//
//  Created by Lukas Würzburger on 11/16/13.
//  Copyright (c) 2013 Dropspot. All rights reserved.
//

#import "SelectCollectionViewController.h"

#import "DSGlobalData.h"
#import "Collection.h"

@interface SelectCollectionViewController () {
    NSArray * fetchedCollections;
}

@end

@implementation SelectCollectionViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [[DSGlobalData sharedData].childContext performBlock:^{
        fetchedCollections = [self fetchMyCollectionsInManagedObjectContext:[DSGlobalData sharedData].childContext];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableView reloadData];
        });
    }];
}

- (void)setCollections:(NSMutableArray *)collections {
    _collections = [collections mutableCopy];
}

- (NSArray *)fetchMyCollectionsInManagedObjectContext:(NSManagedObjectContext *)context {
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Collection"];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"isOwnedByMe = 1"];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"created" ascending:NO];
    [fetchRequest setSortDescriptors:@[sortDescriptor]];
    
    return [context executeFetchRequest:fetchRequest error:nil];
}

#pragma mark - IBActions

- (IBAction)cancelButtonPressed:(id)sender {
    [self.delegate selectCollectionViewControllerDidCancel:self];
}

- (IBAction)doneButtonPressed:(id)sender {
    [self.delegate selectCollectionViewController:self didSelectCollections:self.collections];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return fetchedCollections.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    Collection * collection = fetchedCollections[indexPath.row];
    
    BOOL selected = NO;

    for (Collection * c in _collections) {
        if (c == collection) {
            selected = YES;
            break;
        }
    }
    
    if (selected) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    cell.textLabel.text = collection.title;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    BOOL selected = NO;
    
    for (Collection * c in _collections) {
        if (c == fetchedCollections[indexPath.row]) {
            selected = YES;
            break;
        }
    }
    
    if (selected) {
        [self.collections removeObject:fetchedCollections[indexPath.row]];
        [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    } else {
        [self.collections addObject:fetchedCollections[indexPath.row]];
        [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    }
}

@end

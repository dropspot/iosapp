/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  NearbyCell.h
//  Dropspot3
//
//  Created by Lukas Würzburger on 11/1/13.
//  Copyright (c) 2013 Dropspot. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "DSTileView.h"

@class Spot;

@interface SpotCell : UITableViewCell

@property (nonatomic, weak) IBOutlet DSTileView * tileView;

+ (float)heightForCellWithSpot:(Spot *)spot style:(DSTileViewStyle)style info:(NSString *)info time:(BOOL)time;

@end

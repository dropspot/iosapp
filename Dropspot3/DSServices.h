/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  DSServices.h
//  Dropspot3
//
//  Created by Lukas Würzburger on 1/29/14.
//  Copyright (c) 2014 Dropspot. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DSServices : NSObject

/**
 * Returns address for given search string searching google. searchString can contain Addresses, names, coodinates.
 */
+ (void)geocodeBySearch:(NSString *)searchString completion:(void(^)(NSArray * array))completion;

/**
 * Returns auto completed address for given search string searching google.
 */
+ (void)addressCompletionBySearch:(NSString *)searchString completion:(void(^)(NSArray * array))completion;

@end

/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  DSAPICollectionResponse.h
//  Dropspot3
//
//  Created by Lukas Würzburger on 10/30/13.
//  Copyright (c) 2013 Dropspot. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "DSAPIUserResponse.h"

@interface DSAPICollectionResponse : NSObject

@property (nonatomic, retain) NSString * url;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * text;
@property (nonatomic, retain) NSString * photoURL;
@property (nonatomic, retain) NSString * thumbnailURL;
@property (nonatomic, strong) NSString * slug;
@property (nonatomic, retain) NSString * subscribeURL;
@property (nonatomic, retain) NSString * spotsURL;
@property (nonatomic, retain) NSString * created;
@property (nonatomic, retain) NSString * modified;
@property (nonatomic, retain) NSString * modifiedSpots;
@property (nonatomic, assign) BOOL isOwnedByMe;
@property (nonatomic, assign) BOOL subscribed;
@property (nonatomic, retain) DSAPIUserResponse * user;
@property (nonatomic, assign) BOOL published;

@end

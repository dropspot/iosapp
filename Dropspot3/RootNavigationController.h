/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  RootNavigationController.h
//  Dropspot3
//
//  Created by Lukas Würzburger on 1/10/14.
//  Copyright (c) 2014 Dropspot. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DSReaderView;

@interface RootNavigationController : UINavigationController

@property (nonatomic, strong) DSReaderView * readerView;

@end

/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  User.h
//  Dropspot3
//
//  Created by Lukas Würzburger on 4/9/14.
//  Copyright (c) 2014 Dropspot GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

#define kUserPhotoLoadingFinished @"kUserPhotoLoadingFinished"

@class Collection, Spot;

@interface User : NSManagedObject

@property (nonatomic, retain) NSString * username;
@property (nonatomic, retain) NSString * displayName;
@property (nonatomic, retain) NSString * photoFile;
@property (nonatomic, retain) NSString * photoURL;
@property (nonatomic, retain) NSSet *collections;
@property (nonatomic, retain) NSSet *spots;

+ (User *)userWithUsername:(NSString *)username inManagedObjectContext:(NSManagedObjectContext *)context;

@end

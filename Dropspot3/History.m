/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  History.m
//  Dropspot3
//
//  Created by Lukas Würzburger on 1/8/14.
//  Copyright (c) 2014 Dropspot. All rights reserved.
//

#import "History.h"
#import "Spot.h"


@implementation History

@dynamic date;
@dynamic eventType;
@dynamic spot;

+ (NSArray *)historyWithLimit:(int)limit inManagedObjectContext:(NSManagedObjectContext *)context {
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"History"];
    request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"date" ascending:NO]];
    if (limit > 0) {
        request.fetchLimit = limit;
    }
    
    NSError *error = nil;
    NSArray *matches = [context executeFetchRequest:request error:&error];
    
    if (matches) {
        return matches;
    }
    return @[];
}

+ (NSString *)timeStringFromDate:(NSDate *)date {
    NSDate * currentDate = [NSDate date];
    int currentTimestamp = (int)[currentDate timeIntervalSince1970];
    
    int gap = currentTimestamp - (int)[date timeIntervalSince1970];
    
    if (gap < 60) {
        return translate(@"MENU_NOTIFICATION_HISTORY_TIME_LABEL_NOW");
    } else if (gap < 3600) {
        if (floor(gap/60.0) == 1.0) {
            return translate(@"MENU_NOTIFICATION_HISTORY_TIME_LABEL_MINUTE_AGO");
        } else {
            return [NSString stringWithFormat:translate(@"MENU_NOTIFICATION_HISTORY_TIME_LABEL_MINUTES_AGO"), (int)floor(gap/60.0)];
        }
    } else if (gap < 86400) {
        if (floor(gap/3600.0) == 1.0) {
            return translate(@"MENU_NOTIFICATION_HISTORY_TIME_LABEL_HOUR_AGO");
        } else {
            return [NSString stringWithFormat:translate(@"MENU_NOTIFICATION_HISTORY_TIME_LABEL_HOURS_AGO"), (int)floor(gap/3600.0)];
        }
    } else if (gap < 2592000) {
        if (floor(gap/86400.0) == 1.0) {
            return translate(@"MENU_NOTIFICATION_HISTORY_TIME_LABEL_DAY_AGO");
        } else {
            return [NSString stringWithFormat:translate(@"MENU_NOTIFICATION_HISTORY_TIME_LABEL_DAYS_AGO"), (int)floor(gap/86400.0)];
        }
    } else {
        if (floor(gap/2592000.0) == 1.0) {
            return translate(@"MENU_NOTIFICATION_HISTORY_TIME_LABEL_MONTH_AGO");
        } else {
            return [NSString stringWithFormat:translate(@"MENU_NOTIFICATION_HISTORY_TIME_LABEL_MONTHS_AGO"), (int)floor(gap/2592000.0)];
        }
    }
}


@end

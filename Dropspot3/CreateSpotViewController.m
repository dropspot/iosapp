/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  CreateSpotViewController.m
//  Dropspot3
//
//  Created by Lukas Würzburger on 10/27/13.
//  Copyright (c) 2013 Dropspot. All rights reserved.
//

#import <LRResty/LRResty.h>

#import "CreateSpotViewController.h"

#import "DSDropspot.h"
#import "Spot+Dropspot.h"
#import "DSGlobal.h"
#import "DSGlobalData.h"
#import "DSAPISpotResponse.h"
#import "UILabel+AutoHeight.h"
#import "SelectCollectionViewController.h"
#import "AddressViewController.h"
#import "DSServices.h"
#import "DataHelper.h"

#import "DSButton.h"

#define kAlertViewTagNoText 1

#define kActionSheetTagImageNotSet 2
#define kActionSheetTagImageSet 3
#define kActionSheetTagDeleteSpot 4

@interface CreateSpotViewController () <UIAlertViewDelegate, UIActionSheetDelegate, SelectCollectionViewControllerDelegate, UITextFieldDelegate, UITextViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, AddressViewControllerDelegate> {
    IBOutlet UIScrollView * theScrollView;
    
    IBOutlet UIImageView * sourceImageView;
    IBOutlet UILabel * sourceLabel;
    
    IBOutlet UILabel * seperatorLabelAddress;
    IBOutlet DSButton * addressButton;
    
    IBOutlet UILabel * seperatorLabelText;
    IBOutlet UITextView * textView;
    
    IBOutlet UILabel * seperatorLabelURL;
    IBOutlet UITextField * linkTextField;
    
    IBOutlet UILabel * seperatorLabelCollections;
    IBOutlet DSButton * collectionButton;
    IBOutlet UIView * collectionsContainerView;
    
    IBOutlet UILabel * seperatorLabelPicture;
    IBOutlet UIImageView * photoImageView;
    IBOutlet DSButton * photoButton;
    
    IBOutlet UIButton * deleteButton;
    
    DSPhotoStatus photoStatus;
    NSMutableDictionary * spotData;
}

@end

@implementation CreateSpotViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.navigationBar.tintColor = kDSColorCoral;
    
    sourceImageView.layer.cornerRadius = 3.f;
    sourceImageView.layer.masksToBounds = YES;
    
    photoImageView.layer.masksToBounds = YES;
    
    if (kDSForceSpotText) {
        seperatorLabelText.hidden = YES;
        textView.hidden = YES;
    } else {
        textView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    }
    textView.layer.borderWidth = .5;
    textView.layer.masksToBounds = YES;
    textView.layer.cornerRadius = 5;
    
    seperatorLabelAddress.textColor = kDSColorCoral;
    seperatorLabelText.textColor = kDSColorCoral;
    seperatorLabelURL.textColor = kDSColorCoral;
    seperatorLabelCollections.textColor = kDSColorCoral;
    seperatorLabelPicture.textColor = kDSColorCoral;
    
    spotData = [NSMutableDictionary dictionary];
    photoStatus = DSPhotoStatusNoChange;
    
    spotData[DRPSPT_CREATE_SPOT_LATITUDE] = [NSNumber numberWithDouble:self.spot.latitude];
    spotData[DRPSPT_CREATE_SPOT_LONGITUDE] = [NSNumber numberWithDouble:self.spot.longitude];
    if (!self.spot.address) {
        self.spot.address = @"";
    }
    spotData[DRPSPT_CREATE_SPOT_ADDRESS] = self.spot.address;
    
    self.navigationItem.rightBarButtonItem.enabled = (!self.spot.isBeingSynced);
    
    [photoButton setTitle:translate(@"SPOT_EDIT_VIEW_CONTROL_BUTTON_PICK_IMAGE") forState:UIControlStateNormal];
    seperatorLabelText.text = translate(@"SPOT_EDIT_VIEW_LABEL_TITLE_TEXT");
    seperatorLabelAddress.text = translate(@"SPOT_EDIT_VIEW_LABEL_TITLE_ADDRESS");
    seperatorLabelPicture.text = translate(@"SPOT_EDIT_VIEW_LABEL_TITLE_PICTURE");
    
    [deleteButton setTitle:translate(@"SPOT_EDIT_VIEW_CONTROL_BUTTON_DELETE_SPOT") forState:UIControlStateNormal];
    [deleteButton setTitleColor:kDSColorCoral forState:UIControlStateNormal];
    deleteButton.layer.borderWidth = 1.f;
    deleteButton.layer.borderColor = kDSColorCoral.CGColor;
    deleteButton.layer.cornerRadius = 6.f;
    
    [self configureView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateCreatorImage)
                                                 name:kDSDropspotSessionUserImageChangedNotificationName
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateAddressField:)
                                                 name:kDSSpotDidDetermineAddressNotificationName
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(checkSpot:)
                                                 name:kDSDropspotSpotCreatedRemoteNotificationName
                                               object:nil];
    
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kDSDropspotSessionUserImageChangedNotificationName
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kDSSpotDidDetermineAddressNotificationName
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kDSDropspotSpotCreatedRemoteNotificationName
                                                  object:nil];
}

- (void)checkSpot:(NSNotification *)notification {
    Spot * spot = notification.userInfo[@"spot"];
    if (spot) {
        if (spot == self.spot) {
            self.navigationItem.rightBarButtonItem.enabled = (!self.spot.isBeingSynced);
        }
    }
}

- (void)updateAddressField:(NSNotification *)notification {
    if (self.spot == notification.userInfo[@"spot"]) {
        if (self.spot.address) {
            [addressButton setTitle:self.spot.address forState:UIControlStateNormal];
            spotData[DRPSPT_CREATE_SPOT_ADDRESS] = self.spot.address;
            if (kDSForceSpotText) {
                if (self.spot.address) {
                    textView.text = [NSString stringWithFormat:@"%@ %@", kDSForceSpotText, self.spot.address];
                } else {
                    textView.text = kDSForceSpotText;
                }
            }
        } else {
            [addressButton setTitle:translate(@"SPOT_EDIT_VIEW_ERROR_COULDNT_DETERMINE_ADDRESS") forState:UIControlStateNormal];
            spotData[DRPSPT_CREATE_SPOT_ADDRESS] = @"";
            if (kDSForceSpotText) {
                textView.text = kDSForceSpotText;
            }
        }
        addressButton.enabled = YES;

        self.navigationItem.rightBarButtonItem.enabled = YES;
        
        [self setLayout];
    }
}

- (void)configureView {
    
    if (!self.spot.isSynced) {
        self.navigationItem.title = translate(@"SPOT_EDIT_VIEW_TITLE_CREATE_SPOT");
    } else {
        self.navigationItem.title = translate(@"SPOT_EDIT_VIEW_TITLE_EDIT_SPOT");
    }
    
    sourceLabel.text = self.spot.user.displayName;
    [DataHelper imageWithFilePath:[DataHelper pathForCacheFile:self.spot.user.photoFile] block:^(UIImage *image) {
        sourceImageView.image = image;
    }];
    
    if (self.spot.isDeterminingAddress) {
        [addressButton setTitle:translate(@"SPOT_EDIT_VIEW_CONTROL_BUTTON_ADDRESS_DETERMINING") forState:UIControlStateNormal];
        addressButton.enabled = NO;
        self.navigationItem.rightBarButtonItem.enabled = NO;
    } else {
        if (spotData[DRPSPT_CREATE_SPOT_ADDRESS] == nil || [[spotData[DRPSPT_CREATE_SPOT_ADDRESS] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]) {
            [addressButton setTitle:translate(@"SPOT_EDIT_VIEW_ERROR_COULDNT_DETERMINE_ADDRESS") forState:UIControlStateNormal];
            spotData[DRPSPT_CREATE_SPOT_ADDRESS] = @"";
        } else {
            [addressButton setTitle:spotData[DRPSPT_CREATE_SPOT_ADDRESS] forState:UIControlStateNormal];
        }
        addressButton.enabled = YES;
        self.navigationItem.rightBarButtonItem.enabled = YES;
    }
    
    if (kDSForceSpotText) {
        if (self.spot.address) {
            textView.text = [NSString stringWithFormat:@"%@ %@", kDSForceSpotText, self.spot.address];
        } else {
            textView.text = kDSForceSpotText;
        }
    } else {
        textView.text = self.spot.text;
    }

//    linkTextField.text = self.spot.link;
    
    [DataHelper imageWithFilePath:[DataHelper pathForCacheFile:self.spot.photoFile] block:^(UIImage *image) {
        photoImageView.image = image;
    }];
    
    if (!self.spot.isOwnedByMe || !self.spot.isSynced) {
        deleteButton.hidden = YES;
    }
    
    [self setLayout];
}

- (void)setLayout {
    CGRect frame;
    CGFloat positionY;
    
    positionY = 20;
    
    // Source image & label
    
    frame = sourceImageView.frame;
    frame.origin.y = positionY;
    sourceImageView.frame = frame;
    
    frame = sourceLabel.frame;
    if (sourceImageView.image) {
        frame.origin.x = sourceImageView.frame.origin.x + sourceImageView.frame.size.width + 10;
        frame.size.width = self.view.frame.size.width - 2 * 20 - sourceImageView.frame.size.width - 10;
        sourceLabel.frame = frame;
        frame = [sourceLabel autoAdjustHeight];
        frame.origin.y = sourceImageView.frame.origin.y + sourceImageView.frame.size.height / 2 - frame.size.height / 2;
        sourceLabel.frame = frame;
    } else {
        frame.origin.x = 20;
        frame.origin.y = positionY;
        frame.size.width = self.view.frame.size.width - 2 * 20;
        sourceLabel.frame = frame;
        [sourceLabel autoAdjustHeight];
    }
    
    positionY = sourceImageView.frame.origin.y + sourceImageView.frame.size.height + 20;
    
    if (!seperatorLabelText.hidden) {
        // Text
        
        frame = seperatorLabelText.frame;
        frame.origin.y = positionY;
        seperatorLabelText.frame = frame;
        
        positionY = seperatorLabelText.frame.origin.y + seperatorLabelText.frame.size.height + 10;
        
        frame = textView.frame;
        frame.origin.y = positionY;
        textView.frame = frame;
        
        positionY = textView.frame.origin.y + textView.frame.size.height + 10;
    }
    // Address
    
    frame = seperatorLabelAddress.frame;
    frame.origin.y = positionY;
    seperatorLabelAddress.frame = frame;
    
    positionY = seperatorLabelAddress.frame.origin.y + seperatorLabelAddress.frame.size.height + 10;
    
    frame = addressButton.frame;
    frame.origin.y = positionY;
    addressButton.frame = frame;
    
    positionY = addressButton.frame.origin.y + addressButton.frame.size.height + 20;
    
    if (kDSOwnCollectionApp) {
        seperatorLabelURL.hidden = YES;
        linkTextField.hidden = YES;
        seperatorLabelCollections.hidden = YES;
        collectionsContainerView.hidden = YES;
        collectionButton.hidden = YES;
    } else {
        // URL
        
        frame = seperatorLabelURL.frame;
        frame.origin.y = positionY;
        seperatorLabelURL.frame = frame;
        
        positionY = seperatorLabelURL.frame.origin.y + seperatorLabelURL.frame.size.height + 10;
        
        frame = linkTextField.frame;
        frame.origin.y = positionY;
        linkTextField.frame = frame;
        
        positionY = linkTextField.frame.origin.y + linkTextField.frame.size.height + 20;
    }
    
    // Picture
    
    frame = seperatorLabelPicture.frame;
    frame.origin.y = positionY;
    seperatorLabelPicture.frame = frame;
    
    positionY = seperatorLabelPicture.frame.origin.y + seperatorLabelPicture.frame.size.height + 10;
    
    photoImageView.hidden = !photoImageView.image;
    
    if (photoImageView.image) {
        frame = photoImageView.frame;
        frame.origin.y = positionY;
        photoImageView.frame = frame;
        
        positionY = photoImageView.frame.origin.y + photoImageView.frame.size.height + 20;
    }
    
    frame = photoButton.frame;
    frame.origin.y = positionY;
    photoButton.frame = frame;
    
    positionY = photoButton.frame.origin.y + photoButton.frame.size.height + 20;
    
    if (!kDSOwnCollectionApp) {
        // Collections
        
        frame = seperatorLabelCollections.frame;
        frame.origin.y = positionY;
        seperatorLabelCollections.frame = frame;
        
        positionY = seperatorLabelCollections.frame.origin.y + seperatorLabelCollections.frame.size.height + 10;
        
        frame = collectionButton.frame;
        frame.origin.y = positionY;
        collectionButton.frame = frame;
        
        positionY = collectionButton.frame.origin.y + collectionButton.frame.size.height + 20;
        
        for (UIView * v in collectionsContainerView.subviews) {
            [v removeFromSuperview];
        }
        
        NSArray * collections;
        collections = self.spot.collections.allObjects;
        
        frame = collectionsContainerView.frame;
        frame.origin.y = positionY - 10;
        collectionsContainerView.frame = frame;
        
        if (collections.count > 0) {
            float innerPositionY = 0;
            for (int i = 0; i < collections.count; i++) {
                UIView * cv = [self collectionViewWithCollection:collections[i]];
                CGRect f = cv.frame;
                f.origin.y = innerPositionY;
                cv.frame = f;
                [collectionsContainerView addSubview:cv];
                innerPositionY = cv.frame.origin.y + cv.frame.size.height + 10;
            }
            frame = collectionsContainerView.frame;
            frame.size.height = innerPositionY;
            collectionsContainerView.frame = frame;
            
            positionY = collectionsContainerView.frame.origin.y + collectionsContainerView.frame.size.height + 20;
        } else {
            // Maybe display text 'No collections selected'
        }
        
    } else {
        if (!deleteButton.hidden) {
            frame = deleteButton.frame;
            frame.origin.y = positionY;
            deleteButton.frame = frame;
            
            positionY = deleteButton.frame.origin.y + deleteButton.frame.size.height + 20;
        }
    }
    
    theScrollView.contentSize = CGSizeMake(self.view.frame.size.width, positionY);
}

- (void)updateCreatorImage {
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    if ([defaults objectForKey:kDSDropspotSessionDomainKeyName]) {
        sourceLabel.text = [[defaults objectForKey:kDSDropspotSessionDomainKeyName] valueForKey:kDSDropspotSessionNameKeyName];
        NSData * data = [[defaults objectForKey:kDSDropspotSessionDomainKeyName] valueForKey:kDSDropspotSessionImageKeyName];
        sourceImageView.image = [UIImage imageWithData:data];
    }
}

- (UIView *)collectionViewWithCollection:(Collection *)collection {
    UIView * view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 280, 50)];
    
    view.backgroundColor = [UIColor colorWithWhite:0.98 alpha:1.0];
    view.layer.shadowOffset = CGSizeMake(1, 1);
    view.layer.shadowColor = [UIColor blackColor].CGColor;
    view.layer.shadowOpacity = 0.1;
    view.layer.shadowRadius = 0;
    
    UIImageView * iv = [[UIImageView alloc] initWithFrame:CGRectMake(5, 5, 40, 40)];
//    iv.image = collection.photo.image;
    iv.contentMode = UIViewContentModeScaleAspectFill;
    iv.layer.masksToBounds = YES;
    [view addSubview:iv];
    
    UILabel * t = [[UILabel alloc] initWithFrame:CGRectMake(55, 5, view.frame.size.width - 60, view.frame.size.height - 10)];
    t.numberOfLines = 0;
    t.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:15.0];
    t.textColor = [UIColor darkGrayColor];
    t.backgroundColor = [UIColor clearColor];
    t.text = collection.title;
    [view addSubview:t];
    
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    [view addGestureRecognizer:tap];
    
    return view;
}

- (NSString *)collectionSlugsFromCollections:(NSArray *)collections {
    NSMutableString * string = [NSMutableString string];
    
    for (Collection * collection in collections) {
        if (string.length > 0) {
            [string appendString:@","];
        }
        [string appendString:collection.slug];
    }
    
    return string;
}

- (NSArray *)collectionsFromCollectionSlugs:(NSString *)slugs inManagedObjectContext:(NSManagedObjectContext *)context {
    NSArray * slugsArray = [slugs componentsSeparatedByString:@","];
    
    NSMutableArray * array = [NSMutableArray array];
    
    for (NSString * slug in slugsArray) {
        Collection * c = [Collection collectionWithSlug:slug inManagedObjectContext:context];
        [array addObject:c];
    }
    
    return array;
}

- (NSDictionary *)getCoordinatesOfAddressString:(NSString *)addressString {
    [DSServices geocodeBySearch:addressString completion:^(NSArray *array) {
        if (array.count > 0) {
            spotData[DRPSPT_CREATE_SPOT_LATITUDE] = array[0][@"geometry"][@"location"][@"lat"];
            spotData[DRPSPT_CREATE_SPOT_LONGITUDE] = array[0][@"geometry"][@"location"][@"lng"];
            
            NSLog(@"spot %@", spotData);
        }
    }];
    
    return nil;
}

- (void)deleteSpot {
    NSManagedObjectContext * context = [DSGlobalData sharedData].childContext;
    
    if (self.spot.url && [NSURL URLWithString:self.spot.url]) {
        LRRestyClient * client = [LRResty client];
        [client setHandlesCookiesAutomatically:NO];
        
        [client attachRequestModifier:^(LRRestyRequest *request) {
            NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
            [request addHeader:@"Authorization" value:[NSString stringWithFormat:@"Token %@", [[defaults objectForKey:kDSDropspotSessionDomainKeyName] valueForKey:kDSDropspotSessionTokenKeyName]]];
        }];
        
        self.navigationItem.rightBarButtonItem = nil;
        
        [client delete:self.spot.url withBlock:^(LRRestyResponse *response) {
            if (response.status == 204 || response.status == 404) {
                [context performBlock:^{
                    [context deleteObject:self.spot];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [[NSNotificationCenter defaultCenter] postNotificationName:kDSDropspotSpotDeletedNotificationName object:nil];
                        [self dismissViewControllerAnimated:YES completion:nil];
                    });
                }];
                
                NSLog(@"delete remote");
            } else {
                UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Oops..." message:@"There was a problem deleting this spot, please try again later." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
                
                NSLog(@"status code %lu", (unsigned long)response.status);
            }
        }];
    } else {
        [context performBlock:^{
            [context deleteObject:self.spot];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [[NSNotificationCenter defaultCenter] postNotificationName:kDSDropspotSpotDeletedNotificationName object:nil];
                [self dismissViewControllerAnimated:YES completion:nil];
            });
        }];
        
        NSLog(@"delete local");
    }
}

#pragma mark - IBActions

- (IBAction)addressButtonPressed:(id)sender {
    [self performSegueWithIdentifier:@"showAddressViewController" sender:nil];
}

- (IBAction)saveButtonPressed:(id)sender {
    spotData[DRPSPT_CREATE_SPOT_TEXT] = textView.text;
    spotData[DRPSPT_CREATE_SPOT_LINK] = linkTextField.text;
    
    if ([[spotData[DRPSPT_CREATE_SPOT_TEXT] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]) {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:translate(@"SPOT_EDIT_VIEW_ERROR_NO_TEXT_TITLE") message:translate(@"SPOT_EDIT_VIEW_ERROR_NO_TEXT_MESSAGE") delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        alert.tag = kAlertViewTagNoText;
        [alert show];
        
        return;
    }
    
    [[DSGlobalData sharedData].childContext performBlock:^{
        if (!self.spot.isSynced) {
            self.spot.text = spotData[DRPSPT_CREATE_SPOT_TEXT];
            self.spot.latitude = [spotData[DRPSPT_CREATE_SPOT_LATITUDE] doubleValue];
            self.spot.longitude = [spotData[DRPSPT_CREATE_SPOT_LONGITUDE] doubleValue];
            self.spot.address = spotData[DRPSPT_CREATE_SPOT_ADDRESS];
            /*
            self.spot.photo = [NSEntityDescription insertNewObjectForEntityForName:@"SpotPhoto"
                                                            inManagedObjectContext:[DSGlobalData sharedData].childContext];
            self.spot.photo.image = spotData[DRPSPT_CREATE_SPOT_PHOTO];
            self.spot.photo.spot = self.spot;
            */
            self.spot.isBeingSynced = YES;
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [[NSNotificationCenter defaultCenter] postNotificationName:kDSDropspotSpotEditedNotificationName
                                                                    object:nil
                                                                  userInfo:@{ @"spot" : self.spot }];
                
                [[DSDropspot session] createSpot:self.spot block:^(DSAPISpotResponse *response, NSError *error) {
                    self.spot.isBeingSynced = NO;
                    
                    if (!error) {
                        self.spot.isSynced = YES;
                        self.spot.slug = response.slug;
                        self.spot.url = response.url;
                        
                        if ([self.delegate respondsToSelector:@selector(createSpotViewControllerDidSave:)]) {
                            [self.delegate createSpotViewControllerDidSave:self];
                        }
                    } else {
                        NSLog(@"error: %@", error.localizedDescription);
                    }
                }];
            });
        } else {
            self.spot.text = spotData[DRPSPT_CREATE_SPOT_TEXT];
//            self.spot.link = spotData[DRPSPT_CREATE_SPOT_LINK];
            self.spot.address = spotData[DRPSPT_CREATE_SPOT_ADDRESS];
            self.spot.latitude = [spotData[DRPSPT_CREATE_SPOT_LATITUDE] doubleValue];
            self.spot.longitude = [spotData[DRPSPT_CREATE_SPOT_LONGITUDE] doubleValue];
            
            if (photoStatus != DSPhotoStatusNoChange) {
                /*
                self.spot.photo = [NSEntityDescription insertNewObjectForEntityForName:@"SpotPhoto"
                                                                inManagedObjectContext:[DSGlobalData sharedData].childContext];
                self.spot.photo.image = spotData[DRPSPT_CREATE_SPOT_PHOTO];
                self.spot.photo.spot = self.spot;
                 */
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [[NSNotificationCenter defaultCenter] postNotificationName:kDSDropspotSpotEditedNotificationName
                                                                    object:nil
                                                                  userInfo:@{ @"spot" : self.spot }];
                
                [[DSDropspot session] editSpot:self.spot imageStatus:photoStatus block:^(DSAPISpotResponse *spot, NSError *error) {
                    if (!error) {
                        if ([self.delegate respondsToSelector:@selector(createSpotViewControllerDidSave:)]) {
                            [self.delegate createSpotViewControllerDidSave:self];
                        }
                    } else {
                        NSLog(@"error: %@", error.localizedDescription);
                    }
                }];
            });
        }
        
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
}

- (IBAction)photoButtonPressed:(id)sender {
    UIActionSheet * sheet = [[UIActionSheet alloc] initWithTitle:@"Select Source" delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:@"Camera", @"Library", nil];
    /*
    if ((self.spot && self.spot.photo) || spotData[DRPSPT_CREATE_SPOT_PHOTO]) {
        sheet.tag = kActionSheetTagImageSet;
        
        [sheet addButtonWithTitle:@"Remove photo"];
        [sheet setDestructiveButtonIndex:sheet.numberOfButtons-1];
    } else {
        sheet.tag = kActionSheetTagImageNotSet;
    }
     */
    
    [sheet addButtonWithTitle:@"Cancel"];
    [sheet setCancelButtonIndex:sheet.numberOfButtons-1];
    
    [sheet showInView:self.view];
}

- (void)showImagePickerControllerWithSourceType:(UIImagePickerControllerSourceType)type {
    UIImagePickerController * pickerController = [[UIImagePickerController alloc] init];
    pickerController.sourceType = type;
    pickerController.delegate = self;
    pickerController.navigationBar.tintColor = kDSColorCoral;
    [self.navigationController presentViewController:pickerController animated:YES completion:nil];
}

- (IBAction)cancelButtonPressed:(id)sender {
    if ([self.delegate respondsToSelector:@selector(createSpotViewControllerDidCancel:)]) {
        [self.delegate createSpotViewControllerDidCancel:self];
    }
}

- (IBAction)deleteButtonPressed:(id)sender {
    UIActionSheet * sheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Delete" otherButtonTitles:nil];
    sheet.tag = kActionSheetTagDeleteSpot;
    [sheet showInView:self.view];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"collectionPicker"]) {
        id vc = [[(UINavigationController *)segue.destinationViewController viewControllers] lastObject];
        if ([vc isKindOfClass:[SelectCollectionViewController class]]) {
            [(SelectCollectionViewController *)vc setDelegate:self];
            [(SelectCollectionViewController *)vc navigationController].navigationBar.tintColor = kDSColorCoral;
            [(SelectCollectionViewController *)vc setCollections:self.spot.collections.allObjects.mutableCopy];
        }
    } else if ([segue.identifier isEqualToString:@"showAddressViewController"]) {
        AddressViewController * addressVC = segue.destinationViewController;
        if (addressVC) {
            addressVC.delegate = self;
        }
    }
}

#pragma mark - AddressViewController delegate

- (void)addressViewController:(AddressViewController *)addressViewController didSaveWithAddress:(NSString *)addressString {
    spotData[DRPSPT_CREATE_SPOT_ADDRESS] = addressString;
    [addressButton setTitle:addressString forState:UIControlStateNormal];
    
    [self getCoordinatesOfAddressString:addressString];
}

#pragma mark - UIAlertView delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == kAlertViewTagNoText) {
        [textView becomeFirstResponder];
    }
}

#pragma mark - UIActionSheet delegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (actionSheet.tag == kActionSheetTagImageNotSet || actionSheet.tag == kActionSheetTagImageSet) {
        if (buttonIndex == 0) {
            [self showImagePickerControllerWithSourceType:UIImagePickerControllerSourceTypeCamera];
        } else if (buttonIndex == 1) {
            [self showImagePickerControllerWithSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
        } else if (buttonIndex == 2) {
            if (actionSheet.tag == kActionSheetTagImageSet) {
                //self.spot.photo = nil;
                photoStatus = DSPhotoStatusRemoveImage;
            }
        }
    } else if (actionSheet.tag == kActionSheetTagDeleteSpot) {
        if (buttonIndex == 0) {
            [self deleteSpot];
        }
    }
}

#pragma mark - UIImagePickerController delegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    if ([info objectForKey:UIImagePickerControllerOriginalImage]) {
        UIImage * image = [info objectForKey:UIImagePickerControllerOriginalImage];
        if (image) {
            spotData[DRPSPT_CREATE_SPOT_PHOTO] = image;
            photoStatus = DSPhotoStatusNewImage;
            photoImageView.image = image;
            [self setLayout];
        }
    }
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UITextField delegate

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    [self setViewOffsetForKeyboard:YES];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [self setViewOffsetForKeyboard:NO];
    
    if (![self isUrlValid:textField.text]) {
        textField.text = [NSString stringWithFormat:@"http://%@", textField.text];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (void)setViewOffsetForKeyboard:(BOOL)keyboardVisible {
    float speed = 0.25;
    
    CGRect frame = theScrollView.frame;
    if (keyboardVisible) {
        if (frame.size.height == self.view.frame.size.height) {
            frame.size.height = self.view.frame.size.height - 216;
            [UIView animateWithDuration:speed animations:^{
                theScrollView.frame = frame;
            }];
        }
    } else {
        if (frame.size.height < self.view.frame.size.height) {
            frame.size.height = self.view.frame.size.height;
            [UIView animateWithDuration:speed animations:^{
                theScrollView.frame = frame;
            }];
        }
    }
}

- (BOOL)isUrlValid:(NSString *)urlString {
    if ([urlString rangeOfString:@"://"].location != NSNotFound) {
        return YES;
    }
    return NO;
}

#pragma mark - TextView delegate

- (void)textViewDidBeginEditing:(UITextView *)textView {
    [self setViewOffsetForKeyboard:YES];
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    [self setViewOffsetForKeyboard:NO];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if ([text isEqualToString:@"\n"]) {
        return NO;
    }
    return YES;
}

- (void)textViewDidChange:(UITextView *)atextView {
    atextView.text = [atextView.text stringByReplacingOccurrencesOfString:@"\n" withString:@" "];
}

#pragma mark - SelectCollectionViewController delegate

- (void)selectCollectionViewController:(SelectCollectionViewController *)selectCollectionViewController didSelectCollections:(NSArray *)collections {
    self.spot.collections = [NSSet setWithArray:collections];
    [self setLayout];
    [selectCollectionViewController dismissViewControllerAnimated:YES completion:nil];
}

- (void)selectCollectionViewControllerDidCancel:(SelectCollectionViewController *)selectCollectionViewController {
    [selectCollectionViewController dismissViewControllerAnimated:YES completion:nil];
}

@end

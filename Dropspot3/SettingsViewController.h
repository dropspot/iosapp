/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  SettingsViewController.h
//  Dropspot3
//
//  Created by Lukas Würzburger on 11.10.13.
//  Copyright (c) 2013 Dropspot. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsViewController : UITableViewController

- (IBAction)doneButtonPressed:(id)sender;

@end

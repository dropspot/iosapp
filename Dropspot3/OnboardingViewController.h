/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  OnboardingViewController.h
//  Dropspot3
//
//  Created by Lukas Würzburger on 14.10.13.
//  Copyright (c) 2013 Dropspot. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DSUser.h"

typedef enum {
    OnboardingStatusNotSignedIn,
    OnboardingStatusSignedIn,
    OnboardingStatusLocationPermissionDetermined,
    OnboardingStatusComplete
} OnboardingStatus;

typedef enum {
    OnboardingScreenSignIn,
    OnboardingScreenLocationPermission
} OnboardingScreen;

@class OnboardingViewController;

@protocol OnboardingViewControllerDelegate <NSObject>

@optional
- (void)onboardingViewController:(OnboardingViewController *)onboardingViewController didChangeStatus:(OnboardingStatus)status;
- (void)onboardingViewControllerDidCancel:(OnboardingViewController *)onboardingViewController;

@end

@interface OnboardingViewController : UINavigationController

@property (nonatomic, assign) id <OnboardingViewControllerDelegate, UINavigationControllerDelegate> delegate;
@property (nonatomic, assign) OnboardingStatus status;
@property (nonatomic, assign) OnboardingScreen screen;

@end

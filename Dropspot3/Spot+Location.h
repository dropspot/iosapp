/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  Spot+Location.h
//  Dropspot3
//
//  Created by Lukas Würzburger on 08.10.13.
//  Copyright (c) 2013 Dropspot. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>

#import "Spot.h"

@interface Spot (Location)

- (CLLocation *)location;


+ (NSArray *)spotsSortedByDistanceFromLocation:(CLLocation *)location
                                         limit:(int)limit
                        inManagedObjectContext:(NSManagedObjectContext *)context;

+ (NSArray *)spotsSortedByDistanceFromLocation:(CLLocation *)location
                                         limit:(int)limit
                                shouldBeActive:(BOOL)shouldBeActive
                        inManagedObjectContext:(NSManagedObjectContext *)context;

+ (NSArray *)sortSpots:(NSArray *)spots byDistanceToLocation:(CLLocation *)location limit:(int)limit;

@end

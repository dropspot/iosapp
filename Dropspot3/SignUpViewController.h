/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  SignUpViewController.h
//  Dropspot3
//
//  Created by Lukas Würzburger on 10/23/13.
//  Copyright (c) 2013 Dropspot. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SignInViewController.h"

@class SignUpViewController, DSUser;

@protocol SignUpViewControllerDelegate <NSObject>

- (void)signUpViewControllerDidCancel:(SignUpViewController *)signUpViewController;
- (void)signUpViewController:(SignUpViewController *)signUpViewController didRegisterAndLoginWithUser:(DSUser *)user;

@end

@interface SignUpViewController : UIViewController

@property (nonatomic, assign) id <SignUpViewControllerDelegate> delegate;

@end

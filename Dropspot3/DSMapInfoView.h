/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  DSMapInfoView.h
//  Dropspot3
//
//  Created by Lukas Würzburger on 07.10.13.
//  Copyright (c) 2013 Dropspot. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DSMapInfoItemViewController.h"

@class DSMapInfoView;

@protocol DSMapInfoViewDataSource <NSObject>

@required
- (NSUInteger)numberOfItemsInMapInfoView:(DSMapInfoView *)mapInfoView;
- (DSMapInfoItemViewController *)mapInfoItemViewControllerForIndex:(NSInteger)index;
- (DSMapInfoItemViewController *)mapInfoItemViewControllerForSpot:(Spot *)spot;

@end

@protocol DSMapInfoViewDelegate <NSObject>

@optional
- (void)mapViewInfo:(DSMapInfoView *)mapViewInfo didSelectItemAtIndex:(NSInteger)index;
- (void)mapViewInfo:(DSMapInfoView *)mapViewInfo didSelectItemWithSpot:(Spot *)spot;

@end

@interface DSMapInfoView : UIView

@property (nonatomic, assign) id <DSMapInfoViewDataSource> dataSource;
@property (nonatomic, assign) id <DSMapInfoViewDelegate> delegate;

- (void)reloadData;
- (void)selectSpot:(Spot *)spot atIndex:(NSUInteger)index;
- (void)selectSpot:(Spot *)spot atIndex:(NSUInteger)index animated:(BOOL)animated;
- (void)adjustHeight;

@end

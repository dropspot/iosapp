/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  User+Dropspot.m
//  Dropspot3
//
//  Created by Lukas Würzburger on 4/9/14.
//  Copyright (c) 2014 Dropspot GmbH. All rights reserved.
//

#import "User+Dropspot.h"

#import "DSGlobalData.h"
#import "DataHelper.h"
#import "DSDropspot.h"

@implementation User (Dropspot)

+ (User *)userWithAPIResponse:(DSAPIUserResponse *)response
       inManagedObjectContext:(NSManagedObjectContext *)context {
    User * user = nil;
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"User"];
    request.predicate = [NSPredicate predicateWithFormat:@"username = %@", response.username];
    
    NSError *error = nil;
    NSArray *matches = [context executeFetchRequest:request error:&error];
    
    if (!matches || ([matches count] > 1)) {
        NSLog(@"User matches %@", matches);
    } else if ([matches count]) {
        user = [matches lastObject];
    } else {
        user = [NSEntityDescription insertNewObjectForEntityForName:@"User" inManagedObjectContext:context];
    }
    
    user.username = response.username;
    user.displayName = response.displayName;
    
    [user comparePhoto:response.avatarImageURL inManagedObjectContext:context];

    return user;
}

- (void)comparePhoto:(NSString *)photoURL inManagedObjectContext:(NSManagedObjectContext *)context {
    
    if (![self.photoURL isEqualToString:photoURL]) {
        
        self.photoURL = photoURL;
        
        [self loadPhotoInManagedObjectContext:context completion:^{
            
            [[NSNotificationCenter defaultCenter] postNotificationName:kUserPhotoLoadingFinished object:nil];
        }];
    }
}

- (void)loadPhotoInManagedObjectContext:(NSManagedObjectContext *)context
                             completion:(void (^)(void))completion {
    
    if (self.photoURL) {
        
        NSString * fileName = DSURLToFileName(self.photoURL);
        NSString * filePath = [DataHelper pathForCacheFile:fileName];
        
        // Check if the file exists and there is no connection.
        if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
            
            // Connect if the file exists on disk
            self.photoFile = fileName;
        } else {
            
            // Download image
            [[DSGlobalData sharedData] downloadStuffFromURL:[NSURL URLWithString:self.photoURL] withCompletion:^(NSData *data, NSError *error) {
                
                [context performBlock:^{
                    
                    // Check just in case self is deleted due to syncing.
                    if (self && !self.isDeleted) {
                        
                        if (error) {
                            
                            // Image couln't be loaded. Handle error.
                            
                            // Delete old file
                            [self deletePhotoFile];
                        } else {
                            
                            // New image incoming.
                            UIImage * image = [UIImage imageWithData:data];
                            if (image) {
                                
                                // Delete old file.
                                [self deletePhotoFile];
                                
                                // Write to disk.
                                BOOL writeToFileSuccess = [data writeToFile:filePath
                                                                 atomically:NO];
                                if (writeToFileSuccess) {
                                    
                                    // If writing was successful update the path to file.
                                    self.photoFile = fileName;
                                    
                                    // TODO: Maybe generate thumbnail.
                                }
                            } else {
                                
                                // If received data is not an image delete local file too.
                                [self deletePhotoFile];
                            }
                        }
                    }
                }];
            }];
        }
    } else {
        
        // File is nil. Delete old file.
        [self deletePhotoFile];
    }
}

- (void)deletePhotoFile {
    
    // Delete old file if it exists.
    if (self.photoFile) {
        
        NSString * path = [DataHelper pathForCacheFile:self.photoFile];
        
        if ([[NSFileManager defaultManager] fileExistsAtPath:path]) {
            
            // Remove old file
            NSError * error;
            [[NSFileManager defaultManager] removeItemAtPath:path error:&error];
            if (error) {
                
                NSLog(@"Spot+Dropspot comparePhoto: Error %@", error.localizedDescription);
            }
        }
        self.photoFile = nil;
    }
}

@end

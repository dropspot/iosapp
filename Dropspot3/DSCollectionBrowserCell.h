/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  CollectionViewController.h
//  Dropspot3
//
//  Created by Lukas Würzburger on 11/3/13.
//  Copyright (c) 2013 Dropspot. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Collection.h"

@interface DSCollectionBrowserCell : UIViewController

@property (nonatomic, strong) Collection * collection;

- (id)initWithCollection:(Collection *)collection;

- (void)updatePhoto;

@end

/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  DSButton.m
//  Dropspot3
//
//  Created by Lukas Würzburger on 12/20/13.
//  Copyright (c) 2013 Dropspot. All rights reserved.
//

#import "DSButton.h"

@implementation DSButton

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.layer.masksToBounds = YES;
        self.layer.cornerRadius = 4.0f;
        
        self.backgroundColor = [UIColor colorWithWhite:0.93 alpha:1.0];
        
        self.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 10);
    }
    return self;
}

- (void)setHighlighted:(BOOL)highlighted {
    super.highlighted = highlighted;
    
    if (highlighted) {
        self.backgroundColor = [UIColor grayColor];
    } else {
        self.backgroundColor = [UIColor colorWithWhite:0.93 alpha:1.0];
    }
}

- (void)setTitle:(NSString *)title forState:(UIControlState)state {
    [super setTitle:title forState:state];
    
    UIFont * font = self.titleLabel.font;
    float width = 0;
    float maxWith = self.frame.size.width - self.contentEdgeInsets.left - self.contentEdgeInsets.right;
    
    CGRect frame = [title boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, self.frame.size.height)
                                       options:NSStringDrawingUsesFontLeading
                                    attributes:@{ NSFontAttributeName : font }
                                       context:nil];
    width = frame.size.width;

    while (width > maxWith) {
        font = [font fontWithSize:font.pointSize - 1];
        CGRect frame = [title boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, self.frame.size.height)
                                           options:NSStringDrawingUsesFontLeading
                                        attributes:@{ NSFontAttributeName : font }
                                           context:nil];
        width = frame.size.width;
    }
    
    [self.titleLabel setFont:font];
}

@end

/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  CollectionTopCell.m
//  Dropspot3
//
//  Created by Lukas Würzburger on 3/5/14.
//  Copyright (c) 2014 Dropspot. All rights reserved.
//

#import "CollectionBottomCell.h"

#import "DSDropspot.h"
#import "UILabel+AutoHeight.h"

@implementation CollectionBottomCell

- (void)setLayout {
    float positionY = 10;
    
    CGRect frame;
    if (!self.unsubscribeButton.hidden) {
        self.unsubscribeButton.backgroundColor = [UIColor redColor];
        [self.unsubscribeButton setTitle:translate(@"DETAIL_VIEW_CONTROL_BUTTON_TITLE_UNSUBSCRIBE") forState:UIControlStateNormal];
        
        frame = self.unsubscribeButton.frame;
        frame.origin.y = positionY;
        self.unsubscribeButton.frame = frame;
        
        positionY = frame.origin.y + frame.size.height + 20;
    }
    
    if (!self.deleteButton.hidden) {
        [self.deleteButton setTitle:translate(@"DETAIL_VIEW_CONTROL_BUTTON_TITLE_DELETE") forState:UIControlStateNormal];
        
        frame = self.deleteButton.frame;
        frame.origin.y = positionY;
        self.deleteButton.frame = frame;
        
        positionY = frame.origin.y + frame.size.height + 20;
    }
    
    if (!self.settingsButton.hidden) {
        NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
        NSString * settingsButtonTitle = @"";
        if ([defaults objectForKey:kDSDropspotSessionDomainKeyName]) {
            settingsButtonTitle = [[defaults objectForKey:kDSDropspotSessionDomainKeyName] valueForKey:kDSDropspotSessionNameKeyName];
        }
        [self.settingsButton setTitle:[NSString stringWithFormat:translate(@"DETAIL_VIEW_CONTROL_BUTTON_TITLE_SETTINGS"), settingsButtonTitle] forState:UIControlStateNormal];

        [self.settingsButton setTitleColor:[UIColor colorWithWhite:0.8f alpha:1.0] forState:UIControlStateNormal];
        [self.settingsButton setTitleColor:[UIColor colorWithWhite:0.3f alpha:1.0] forState:UIControlStateHighlighted];
        self.settingsButton.layer.borderWidth = 1.f;
        self.settingsButton.layer.borderColor = [UIColor colorWithWhite:0.8f alpha:1.0].CGColor;
        self.settingsButton.layer.cornerRadius = 6.f;

        frame = self.settingsButton.frame;
        frame.origin.y = positionY;
        self.settingsButton.frame = frame;
        
        positionY = frame.origin.y + frame.size.height + 20;
    }
    
}

+ (float)heightForCellUnsubscribeButton:(BOOL)unsubscribeButton settingsButton:(BOOL)settingsButton {
    float height = 0;
    
    if (unsubscribeButton) {
        height += 64;
    }
    
    if (settingsButton) {
        height += 64;
    }
    
    return height;
}

@end

/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  OnboardingViewController.m
//  Dropspot3
//
//  Created by Lukas Würzburger on 14.10.13.
//  Copyright (c) 2013 Dropspot. All rights reserved.
//

#import "OnboardingViewController.h"

#import "StartViewController.h"
#import "DSLocationPermissionViewController.h"

@interface OnboardingViewController () <StartViewControllerDelegate, DSLocationPermissionViewControllerDelegate> {
}

@property (nonatomic, strong) StartViewController * startViewController;
@property (nonatomic, strong) DSLocationPermissionViewController * locationPermissionViewController;

@end

@implementation OnboardingViewController

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initialize];
    }
    return self;
}

- (id)init {
    self = [super init];
    if (self) {
        [self initialize];
    }
    return self;
}

- (void)initialize {
    if ([self.viewControllers.firstObject isKindOfClass:[StartViewController class]]) {
        self.startViewController = self.viewControllers.firstObject;
        self.startViewController.delegate = self;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
}

- (void)setScreen:(OnboardingScreen)screen {
    _screen = screen;
    
    if (screen == OnboardingScreenLocationPermission) {
        if (![self.viewControllers.lastObject isKindOfClass:[DSLocationPermissionViewController class]]) {
            if (!self.locationPermissionViewController) {
                self.locationPermissionViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"locationPermissionViewController"];
            }
            self.locationPermissionViewController.delegate = self;
            [self pushViewController:self.locationPermissionViewController animated:YES];
        }
    }
}

#pragma mark - StartViewController delegate

- (void)startViewController:(StartViewController *)startViewController didSignInWithUser:(DSUser *)currentUser {
    if ([self.delegate respondsToSelector:@selector(onboardingViewController:didChangeStatus:)]) {
        [self.delegate onboardingViewController:self didChangeStatus:OnboardingStatusSignedIn];
    }
    
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined) {
        self.screen = OnboardingScreenLocationPermission;
    } else {
        if ([self.delegate respondsToSelector:@selector(onboardingViewController:didChangeStatus:)]) {
            [self.delegate onboardingViewController:self didChangeStatus:OnboardingStatusComplete];
        }
    }
}

- (void)startViewControllerDidCancel:(StartViewController *)startViewController {
    if ([self.delegate respondsToSelector:@selector(onboardingViewControllerDidCancel:)]) {
        [self.delegate onboardingViewControllerDidCancel:self];
    }
}

#pragma mark - Location permission delegate

- (void)locationPermissionViewControllerDidAllow:(DSLocationPermissionViewController *)locationPermissionViewController {
    if ([self.delegate respondsToSelector:@selector(onboardingViewController:didChangeStatus:)]) {
        [self.delegate onboardingViewController:self didChangeStatus:OnboardingStatusLocationPermissionDetermined];
        [self.delegate onboardingViewController:self didChangeStatus:OnboardingStatusComplete];
    }
}

- (void)locationPermissionViewControllerDidDeny:(DSLocationPermissionViewController *)locationPermissionViewController {
    if ([self.delegate respondsToSelector:@selector(onboardingViewController:didChangeStatus:)]) {
        [self.delegate onboardingViewController:self didChangeStatus:OnboardingStatusLocationPermissionDetermined];
        [self.delegate onboardingViewController:self didChangeStatus:OnboardingStatusComplete];
    }
}

@end
/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  NearbyCell.m
//  Dropspot3
//
//  Created by Lukas Würzburger on 11/1/13.
//  Copyright (c) 2013 Dropspot. All rights reserved.
//

#import "SpotCell.h"

#import "Spot.h"
#import "DataHelper.h"

@implementation SpotCell

+ (float)heightForCellWithSpot:(Spot *)spot style:(DSTileViewStyle)style info:(NSString *)info time:(BOOL)time {
    float height = [DSTileView heightWithStyle:style width:300 text:spot.text info:info time:time distance:YES image:(spot.photoFile != nil || spot.thumbnailFile != nil)];
    return height + 15;
}

@end

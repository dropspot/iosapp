/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  DSMapAuthorView.m
//  Dropspot3
//
//  Created by Lukas Würzburger on 3/26/14.
//  Copyright (c) 2014 Dropspot GmbH. All rights reserved.
//

#import "DSMapAuthorView.h"

#define kPadding 5.

@interface DSMapAuthorView () {
    UIImageView * _imageView;
    UILabel * _label;
}

@end

@implementation DSMapAuthorView

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        
        // Self
        self.userInteractionEnabled = NO;
        
        // Layer Style
        self.layer.cornerRadius = 3.f;
        self.layer.shadowColor = [UIColor blackColor].CGColor;
        self.layer.shadowOffset = CGSizeMake(0.f, 0.f);
        self.layer.shadowOpacity = 0.15f;
        self.layer.shadowRadius = 2.f;
        
        // Image View
        _imageView = [[UIImageView alloc] initWithFrame:CGRectMake(kPadding, kPadding, self.frame.size.height - kPadding - kPadding, self.frame.size.height - kPadding - kPadding)];
        _imageView.backgroundColor = [UIColor clearColor];
        _imageView.layer.masksToBounds = YES;
        _imageView.layer.cornerRadius = 2;
        [self addSubview:_imageView];
        
        // Label
        float xPosition = kPadding + _imageView.frame.size.width + kPadding + kPadding;
        _label = [[UILabel alloc] initWithFrame:CGRectMake(xPosition, kPadding, self.frame.size.width - xPosition - kPadding, self.frame.size.height - kPadding - kPadding)];
        _label.backgroundColor = [UIColor clearColor];
        _label.font = [UIFont fontWithName:@"HelveticaNeue" size:14.];
        _label.textColor = [UIColor whiteColor];
        _label.numberOfLines = 0;
        
        // Label shadow
        _label.layer.shadowColor = [UIColor blackColor].CGColor;
        _label.layer.shadowOffset = CGSizeMake(0.f, 0.f);
        _label.layer.shadowOpacity = 0.35f;
        _label.layer.shadowRadius = 2.f;
        
        [self addSubview:_label];
    }
    return self;
}

- (void)setImage:(UIImage *)image {
    [self setImage:image animated:NO];
}

- (void)setImage:(UIImage *)image animated:(BOOL)animated {
    if (_image != image) {
        _image = image;
        
        // Transfer the image to the image view.
        if (animated) {
            
            // Animate with fade.
            [UIView transitionWithView:_imageView duration:0.25 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
                _imageView.image = _image;
            } completion:nil];
        } else {
            _imageView.image = _image;
        }
    }
}

- (void)setName:(NSString *)name {
    [self setName:name animated:NO];
}

- (void)setName:(NSString *)name animated:(BOOL)animated {
    if (_name != name) {
        _name = name;
        
        // Transfer the name to the label.
        if (animated) {
            
            // Animate with fade.
            [UIView transitionWithView:_label duration:0.25 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
                _label.text = _name;
            } completion:nil];
        } else {
            _label.text = _name;
        }
    }
}

@end

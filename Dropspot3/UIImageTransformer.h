/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  CollectionPhotoThumbnail.h
//  Dropspot3
//
//  Created by Lukas Würzburger on 17.09.13.
//  Copyright (c) 2013 Dropspot. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIImageTransformer : NSValueTransformer

@end

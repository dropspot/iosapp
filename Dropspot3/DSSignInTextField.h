/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  DSSignInTextField.h
//  Dropspot3
//
//  Created by Lukas Würzburger on 3/4/14.
//  Copyright (c) 2014 Dropspot. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DSSignInTextField : UITextField

@end

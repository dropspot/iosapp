/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  CollectionDetailViewController.h
//  Dropspot3
//
//  Created by Lukas Würzburger on 11/13/13.
//  Copyright (c) 2013 Dropspot. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "_MasterViewController.h"

#import "Collection.h"

@interface CollectionDetailViewController : _MasterViewController

@property (nonatomic, strong) Collection * collection;

@end

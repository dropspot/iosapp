/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  DSWelcomeSlide.h
//  Dropspot3
//
//  Created by Lukas Würzburger on 17.10.13.
//  Copyright (c) 2013 Dropspot. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DSWelcomeSlide : NSObject

@property (nonatomic) NSInteger index;
@property (nonatomic, retain) NSString * descriptionText;
@property (nonatomic, retain) NSString * videoURLString;
@property (nonatomic) BOOL shouldRepeatVideo;
@property (nonatomic) BOOL showLocationButton;
@property (nonatomic) BOOL enableNextButton;
@property (nonatomic) BOOL enablePrevButton;

@end

/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  DSGlobalData.m
//  Dropspot3
//
//  Created by Lukas Würzburger on 10/28/13.
//  Copyright (c) 2013 Dropspot. All rights reserved.
//

#import "DSGlobalData.h"

#import "DataHelper.h"
#import "DSGlobal.h"

#import "DSSyncController.h"

@interface DSGlobalData () <DSSyncControllerDelegate> {
    SyncingCompletion _syncingCompletion;
}

@property (nonatomic, strong) DSSyncController * syncController;

@property (nonatomic, strong) NSMutableDictionary * downloadStack;
@property (nonatomic, strong) dispatch_queue_t downloadQueue;
@property (nonatomic, strong) NSURLSession * urlSession;

@end

@implementation DSGlobalData

@synthesize parentContext = _parentContext;
@synthesize childContext = _childContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

+ (DSGlobalData *)sharedData {
    static DSGlobalData * instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[DSGlobalData alloc] init];
    });
    return instance;
}

- (dispatch_queue_t)downloadQueue {
    if (!_downloadQueue) {
        _downloadQueue = dispatch_queue_create("com.dropspot.imageDownloader", DISPATCH_QUEUE_SERIAL);
    }
    return _downloadQueue;
}

- (NSMutableDictionary *)downloadStack {
    if (!_downloadStack) {
        _downloadStack = [NSMutableDictionary dictionary];
    }
    return _downloadStack;
}

- (NSURLSession *)urlSession {
    
    if (!_urlSession) {
        
        // Set up url session configuration
        NSURLSessionConfiguration * configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        configuration.timeoutIntervalForRequest = 300;
        configuration.HTTPMaximumConnectionsPerHost = 2;
        configuration.HTTPShouldUsePipelining = YES;
        
        // Create URL Session object.
        _urlSession = [NSURLSession sessionWithConfiguration:configuration];
    }
    return _urlSession;
}

- (DSSyncController *)syncController {
    
    if (!_syncController) {
        
        _syncController = [DSSyncController sharedController];
        _syncController.delegate = self;
    }
    
    return _syncController;
}

#pragma mark - Download

- (void)downloadStuffFromURL:(NSURL *)url withCompletion:(void (^)(NSData *data, NSError *error))completion {
    
    // Check if url is valid
    if (url) {
        
        // Check if a connection with this url is already in the stack
        if (![self.downloadStack objectForKey:url]) {
            
            // Add object
            [self.downloadStack setObject:@"1" forKey:url];
            
            NSLog(@"start download");
            // Create data task
            NSURLSessionDataTask * task = [self.urlSession dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                NSLog(@"finish download");
                
                if (error) {
                    
                    // Handle error
                    NSLog(@"Error downloading %@ %@ %@", response, url.absoluteString, error.localizedDescription);
                    
                    // Call back
                    if (completion) {
                        completion(nil, error);
                    }
                } else {
                    
                    // Remove item from stack
                    [self.downloadStack removeObjectForKey:url];
                    
                    // Call back
                    if (completion) {
                        completion(data, nil);
                    }
                }
            }];
            
            // Start task
            [task resume];
        } else {
            
            // Call back
            if (completion) {
                NSError * error = [NSError errorWithDomain:@"com.dropspot.errorDomain"
                                                      code:DSDownloadErrorKeyItemAlreadyInStack
                                                  userInfo:@{ NSLocalizedDescriptionKey : @"Item already in stack." }];
                completion(nil, error);
            }
        }
    } else {
        
        // Call back
        if (completion) {
            NSError * error = [NSError errorWithDomain:@"com.dropspot.errorDomain"
                                                  code:DSDownloadErrorKeyFailed
                                              userInfo:@{ NSLocalizedDescriptionKey : @"No valid URL." }];
            completion(nil, error);
        }
    }
}

- (void)downloadFileFromURL:(NSURL *)url withCompletion:(void (^)(NSURL * location, NSError *error))completion {
    
    // Check if url is valid
    if (url) {
        
        // Check if a connection with this url is already in the stack
        if (![self.downloadStack objectForKey:url]) {
            
            // Add object
            [self.downloadStack setObject:@"1" forKey:url];
            
            NSLog(@"start download");
            
            // Create request object
            NSURLRequest * urlRequest = [NSURLRequest requestWithURL:url];
            
            // Create data task
            NSURLSessionDownloadTask * task = [self.urlSession downloadTaskWithRequest:urlRequest completionHandler:^(NSURL *location, NSURLResponse *response, NSError *error) {
                NSLog(@"finish download");
                
                if (error) {
                    
                    // Handle error
                    NSLog(@"Error downloading %@ %@ %@", response, url.absoluteString, error.localizedDescription);
                    
                    // Call back
                    if (completion) {
                        completion(nil, error);
                    }
                } else {
                    
                    // Remove item from stack
                    [self.downloadStack removeObjectForKey:url];
                    
                    // Call back
                    if (completion) {
                        completion(location, nil);
                    }
                }
            }];
            
            // Start task
            [task resume];
            
            /*
            NSURLSessionDataTask * task = [self.urlSession dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                NSLog(@"finish download");
                
                if (error) {
                    
                    // Handle error
                    NSLog(@"Error downloading %@ %@ %@", response, url.absoluteString, error.localizedDescription);
                    
                    // Call back
                    if (completion) {
                        completion(nil, error);
                    }
                } else {
                    
                    // Remove item from stack
                    [self.downloadStack removeObjectForKey:url];
                    
                    // Call back
                    if (completion) {
                        completion(data, nil);
                    }
                }
            }];
            
            // Start task
            [task resume];
             */
        } else {
            
            // Call back
            if (completion) {
                NSError * error = [NSError errorWithDomain:@"com.dropspot.errorDomain"
                                                      code:DSDownloadErrorKeyItemAlreadyInStack
                                                  userInfo:@{ NSLocalizedDescriptionKey : @"Item already in stack." }];
                completion(nil, error);
            }
        }
    } else {
        
        // Call back
        if (completion) {
            NSError * error = [NSError errorWithDomain:@"com.dropspot.errorDomain"
                                                  code:DSDownloadErrorKeyFailed
                                              userInfo:@{ NSLocalizedDescriptionKey : @"No valid URL." }];
            completion(nil, error);
        }
    }
}

- (void)loadFileFromURL:(NSURL *)url
     filePathForStorage:(NSString *)filePath
            oldFilePath:(NSString *)oldFilePath
             completion:(void (^)(BOOL success))completion {
    
    if (url) {
        
        // Check if the file exists and there is no connection.
        BOOL isDir;
        BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:filePath isDirectory:&isDir];
        if (fileExists && !isDir) {
            
            // Photo is already downloaded
            if (completion) {
                completion(YES);
            }
        } else {
            
            // Download image
            [[DSGlobalData sharedData] downloadFileFromURL:url withCompletion:^(NSURL *location, NSError *error) {
                
                if (error) {
                    
                    if (error.code == DSDownloadErrorKeyItemAlreadyInStack) {
                    
                        // Download canceled because another download with same url was started befor.
                        if (completion) {
                            completion(NO);
                        }
                    } else {
                        
                        // Image couldn't be loaded.
                        
                        // Delete old file
                        [self deleteFile:oldFilePath];

                        if (completion) {
                            completion(NO);
                        }
                    }
                    
                    
                } else {
                    
                    // Delete old file
                    [self deleteFile:oldFilePath];
                    
                    // Move file to new location
                    NSURL * newLocation = [NSURL fileURLWithPath:filePath];
                    NSLog(@"%@ - %@", filePath, newLocation.absoluteString);
                    NSError * error;
                    BOOL move = [[NSFileManager defaultManager] moveItemAtURL:location toURL:newLocation error:&error];
                    if (move) {
                        
                        // If moving was successful update the path to file.
                        if (completion) {
                            completion(YES);
                        }
                    } else {
                        
                        // Handle error
                        if (error) {
                            
                            NSLog(@"Move file error: %@", error.localizedDescription);
                            
                            if (completion) {
                                completion(NO);
                            }
                        }
                    }
                }
            }];
        }
    } else {
        
        // Delete old file
        [self deleteFile:oldFilePath];
        
        if (completion) {
            completion(NO);
        }
    }
}

- (void)deleteFile:(NSString *)filePath {
    
    if (filePath) {

        // Delete old file if it exists.
        BOOL isDir;
        BOOL exists = [[NSFileManager defaultManager] fileExistsAtPath:filePath isDirectory:&isDir];
        if (filePath && exists && !isDir) {
            
            // Remove old file
            NSError * error;
            [[NSFileManager defaultManager] removeItemAtPath:filePath error:&error];
            if (error) {
                
                NSLog(@"Spot+Dropspot comparePhoto: Error %@", error.localizedDescription);
            }
        }
    }
}

- (BOOL)isDownloadingObjectWithURL:(NSString *)urlString {
    return ([self.downloadStack objectForKey:urlString] != nil);
}

#pragma mark - Syncing

- (void)synchronizeWithCompletion:(SyncingCompletion)completion {
    _syncingCompletion = [completion copy];
    
    [self.syncController synchronize];
}

- (void)syncController:(DSSyncController *)syncController didFinishWithErrors:(NSArray *)errors {
    
    if (errors.count) {
        
        if (_syncingCompletion) {
            _syncingCompletion(NO, errors[0]);
        }
    } else {
        
        // Set the last sync date
        [DSGlobal sharedGlobals].lastSyncDate = [NSDate date];
        
        if (_syncingCompletion) {
            _syncingCompletion(YES, nil);
        }
    }
}

#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)parentContext {
    if (_parentContext != nil) {
        return _parentContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];

    if (coordinator != nil) {
        _parentContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
        [_parentContext setPersistentStoreCoordinator:coordinator];
    }
    return _parentContext;
}

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)childContext {
    if (_childContext != nil) {
        return _childContext;
    }
    
    _childContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    [_childContext setParentContext:self.parentContext];

    return _childContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel {
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Dropspot3" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [NSURL fileURLWithPath:[DataHelper pathForCacheFile:@"Dropspot3.sqlite"]];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:@{ NSMigratePersistentStoresAutomaticallyOption : @YES, NSInferMappingModelAutomaticallyOption : @YES } error:&error]) {
        [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil];
        
        _persistentStoreCoordinator = nil;

        return self.persistentStoreCoordinator;
    }
    
    return _persistentStoreCoordinator;
}

- (void)saveContext {
    if ([self.parentContext hasChanges]) {
        NSError * error;
        [self.parentContext save:&error];
        if (error) {
            NSLog(@"Error: %@", error.localizedDescription);
        }
    }
}

@end

/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  CollectionBrowserCell.m
//  Dropspot3
//
//  Created by Lukas Würzburger on 11/3/13.
//  Copyright (c) 2013 Dropspot. All rights reserved.
//

#import "DSCollectionBrowser.h"

#import "DSCollectionBrowserCell.h"

@interface DSCollectionBrowser () <UIPageViewControllerDelegate, UIPageViewControllerDataSource> {
    UIPageControl * _pageControl;
}

@property (nonatomic, strong) UIPageViewController * pageViewController;

@end

@implementation DSCollectionBrowser

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initialize];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
    }
    return self;
}

- (void)initialize {
    
    // Page control
    _pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(10.f, self.frame.size.height - 10.f, self.frame.size.width - 20.f, 10.f)];
    _pageControl.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
    [_pageControl addTarget:self action:@selector(pageControlPressed:) forControlEvents:UIControlEventValueChanged];
    [_pageControl setPageIndicatorTintColor:[UIColor colorWithWhite:0.9 alpha:1.0]];
    [_pageControl setCurrentPageIndicatorTintColor:[UIColor colorWithWhite:0.7 alpha:1.0]];
    [self addSubview:_pageControl];
}

- (UIPageViewController *)pageViewController {
    if (!_pageViewController) {
        // Page view controller
        _pageViewController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll
                                                              navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal
                                                                            options:@{ UIPageViewControllerOptionInterPageSpacingKey : @0.f}];
        _pageViewController.view.layer.masksToBounds = YES;
        _pageViewController.view.frame = CGRectMake(0.f, 0.f, self.frame.size.width, self.frame.size.width);
        _pageViewController.delegate = self;
        _pageViewController.dataSource = self;
        
        [self addSubview:_pageViewController.view];
    }
    
    return _pageViewController;
}

- (void)setCollections:(NSArray *)collections {
    _collections = collections;
    
    if (_selectedCollection) {
        NSUInteger indexOfSelectedCollection = [_collections indexOfObject:_selectedCollection];
        
        if (indexOfSelectedCollection != NSNotFound) {
            _pageControl.currentPage = indexOfSelectedCollection;
        } else {
            _pageControl.currentPage = 0;
        }
    } else {
        _pageControl.currentPage = 0;
    }
    
    [self setUp];
}

- (void)setUp {
    if (self.collections && self.collections.count > 0) {
        DSCollectionBrowserCell * vc = [self collectionViewControllerForIndex:_pageControl.currentPage];
        if (vc) {
            if (vc.collection != _selectedCollection) {
                [self.pageViewController setViewControllers:@[vc]
                                                  direction:UIPageViewControllerNavigationDirectionForward
                                                   animated:NO
                                                 completion:nil];
            }
        }
        _pageControl.numberOfPages = self.collections.count;
    } else {
        [_pageViewController.view removeFromSuperview];
        _pageViewController = nil;
        _pageControl.numberOfPages = 0;
    }
}

#pragma mark - UIPageViewController

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(DSCollectionBrowserCell *)viewController {
    int index = (int)[self.collections indexOfObject:viewController.collection];
    
    return [self collectionViewControllerForIndex:index + 1];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(DSCollectionBrowserCell *)viewController {
    
    int index = (int)[self.collections indexOfObject:viewController.collection];
    
    return [self collectionViewControllerForIndex:index - 1];
}

- (DSCollectionBrowserCell *)collectionViewControllerForIndex:(NSInteger)index {
    if (index < 0 || index >= self.collections.count) {
        return nil;
    }
    
    Collection * newCollection = self.collections[index];
    
    if (newCollection) {
        DSCollectionBrowserCell * controller = [[DSCollectionBrowserCell alloc] initWithCollection:newCollection];
        return controller;
    }
    return nil;
}

- (void)pageViewController:(UIPageViewController *)thePageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray *)previousViewControllers transitionCompleted:(BOOL)completed {
    if (!completed) {
        return;
    }
    
    _pageControl.currentPage = [self.collections indexOfObject:[(DSCollectionBrowserCell *)thePageViewController.viewControllers.lastObject collection]];
    _selectedCollection = self.collections[_pageControl.currentPage];
    
    [(DSCollectionBrowserCell *)thePageViewController.viewControllers.lastObject updatePhoto];
}

#pragma mark - IBAction

- (void)pageControlPressed:(UIPageControl *)sender {
    NSUInteger currentPage = [self.collections indexOfObject:[(DSCollectionBrowserCell *)_pageViewController.viewControllers.lastObject collection]];
    
    UIPageViewControllerNavigationDirection direction;
    
    if (currentPage < sender.currentPage) {
        direction = UIPageViewControllerNavigationDirectionForward;
    } else {
        direction = UIPageViewControllerNavigationDirectionReverse;
    }
    
    [_pageViewController setViewControllers:@[[self collectionViewControllerForIndex:sender.currentPage]] direction:direction animated:YES completion:nil];
}

@end

/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  SettingsFrequencyViewController.m
//  Dropspot3
//
//  Created by Lukas Würzburger on 11.10.13.
//  Copyright (c) 2013 Dropspot. All rights reserved.
//

#import "SettingsFrequencyViewController.h"
#import "DSGlobal.h"

@interface SettingsFrequencyViewController ()

@end

@implementation SettingsFrequencyViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.navigationItem.title = translate(@"SETTINGS_LABEL_FREQUENCY");
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotate {
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    
    if (indexPath.row == 0) {
        cell.textLabel.text = translate(@"SETTINGS_NOTIFICATION_FREQUENCY_OFF");
    } else if (indexPath.row == 1) {
        cell.textLabel.text = translate(@"SETTINGS_NOTIFICATION_FREQUENCY_FEW");
    } else if (indexPath.row == 2) {
        cell.textLabel.text = translate(@"SETTINGS_NOTIFICATION_FREQUENCY_MANY");
    }
    
    if (indexPath.row == [DSGlobal sharedGlobals].notificationFrequency) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [DSGlobal sharedGlobals].notificationFrequency = indexPath.row;
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
}

@end

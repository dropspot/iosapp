/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  DSWelcomeSlide.m
//  Dropspot3
//
//  Created by Lukas Würzburger on 17.10.13.
//  Copyright (c) 2013 Dropspot. All rights reserved.
//

#import "DSWelcomeSlide.h"

@implementation DSWelcomeSlide

@end

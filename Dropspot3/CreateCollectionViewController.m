/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  CreateCollectionViewController.m
//  Dropspot3
//
//  Created by Lukas Würzburger on 10/27/13.
//  Copyright (c) 2013 Dropspot. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

#import "CreateCollectionViewController.h"

#import "Collection+Dropspot.h"
#import "DSDropspot.h"
#import "DSGlobal.h"
#import "DSGlobalData.h"
#import "DataHelper.h"

#define kActionSheetTagImageNotSet 2
#define kActionSheetTagImageSet 3

@interface CreateCollectionViewController () <UINavigationControllerDelegate, UIImagePickerControllerDelegate, UITextFieldDelegate, UIActionSheetDelegate> {
    IBOutlet UINavigationBar * navigationBar;
    IBOutlet UIButton * previewButton;
    IBOutlet UILabel * nameLabel;
    IBOutlet UITextField * nameTextField;
    IBOutlet UIButton * selectPhotoButton;
    IBOutlet UIImageView * photoImageView;
    
    NSMutableDictionary * collectionData;
    
    DSPhotoStatus photoStatus;
}

- (IBAction)saveButtonPressed:(id)sender;
- (IBAction)cancelButtonPressed:(id)sender;
- (IBAction)selectPhotoButtonPressed:(id)sender;

@end

@implementation CreateCollectionViewController


- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        collectionData = [NSMutableDictionary dictionary];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.navigationBar.tintColor = kDSColorCoral;
    nameLabel.textColor = kDSColorCoral;
    
    navigationBar.topItem.title = translate(@"CREATE_COLLECTION_NAVIGATION_BAR_TITLE");
    [previewButton setTitle:translate(@"CREATE_COLLECTION_BUTTON_PREVIEW") forState:UIControlStateNormal];
    nameLabel.text = translate(@"CREATE_COLLECTION_LABEL_COLLECTION_NAME");
    nameTextField.placeholder = translate(@"CREATE_COLLECTION_LABEL_COLLECTION_NAME_PLACEHOLDER");
    [selectPhotoButton setTitle:translate(@"CREATE_COLLECTION_BUTTON_SELECT_PHOTO") forState:UIControlStateNormal];
    
    photoImageView.layer.masksToBounds = YES;
    
    [self configureView];
}

- (void)configureView {
    NSLog(@"collection %@", collectionData);
    
    nameTextField.text = collectionData[DRPSPT_CREATE_COLLECTION_TITLE];
    photoImageView.image = collectionData[DRPSPT_CREATE_COLLECTION_PHOTO];
}

#pragma mark - Setter & Getter

- (void)setCollection:(Collection *)collection {
    if (_collection != collection) {
        _collection = collection;
    }
    
    collectionData[DRPSPT_CREATE_COLLECTION_TITLE] = self.collection.title;
    [DataHelper imageWithFilePath:[DataHelper pathForCacheFile:self.collection.photoFile] block:^(UIImage *image) {
        collectionData[DRPSPT_CREATE_COLLECTION_PHOTO] = image;
    }];
}

#pragma mark - IBActions

- (IBAction)selectPhotoButtonPressed:(id)sender {
    UIActionSheet * sheet = [[UIActionSheet alloc] initWithTitle:@"Select Source" delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:@"Camera", @"Library", nil];
    
    if ((self.collection && self.collection.photoFile) || collectionData[DRPSPT_CREATE_COLLECTION_PHOTO]) {
        sheet.tag = kActionSheetTagImageSet;
        
        [sheet addButtonWithTitle:@"Remove photo"];
        [sheet setDestructiveButtonIndex:sheet.numberOfButtons-1];
    } else {
        sheet.tag = kActionSheetTagImageNotSet;
    }
    
    [sheet addButtonWithTitle:@"Cancel"];
    [sheet setCancelButtonIndex:sheet.numberOfButtons-1];
    
    [sheet showInView:self.view];
}

- (IBAction)saveButtonPressed:(id)sender {
    if ([self saveTitleWhileFocus]) {
        [[DSGlobalData sharedData].childContext performBlock:^{
            if (!self.collection.isSynced) {
                
                self.collection.title = collectionData[DRPSPT_CREATE_COLLECTION_TITLE];
                /*
                self.collection.photo = [NSEntityDescription insertNewObjectForEntityForName:@"CollectionPhoto"
                                                                      inManagedObjectContext:[DSGlobalData sharedData].childContext];
                self.collection.photo.image = collectionData[DRPSPT_CREATE_COLLECTION_PHOTO];
                self.collection.photo.collection = self.collection;
                */
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[NSNotificationCenter defaultCenter] postNotificationName:kDSDropspotCollectionCreatedLocalNotificationName object:nil];
                    
                    [[DSDropspot session] createCollection:self.collection block:^(DSAPICollectionResponse *collection, NSError *error) {
                        if (!error) {
                            [[DSGlobalData sharedData].childContext performBlock:^{
                                self.collection.slug = collection.slug;
                                self.collection.url = collection.url;
                                self.collection.isSynced = YES;
                                
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    [[NSNotificationCenter defaultCenter] postNotificationName:kDSDropspotCollectionCreatedRemoteNotificationName object:nil];
                                    [[DSDropspot session] subscribeToCollectionWithSlug:self.collection.slug block:^(NSDictionary *result, NSError *error) {
                                        [[NSNotificationCenter defaultCenter] postNotificationName:kDSDropspotCollectionSubscriptionChanged object:nil userInfo:@{ @"collection" : self.collection }];
                                    }];
                                });
                            }];
                        } else {
                            NSLog(@"error: %@", error.localizedDescription);
                        }
                    }];
                });
            } else {
                
                self.collection.title = collectionData[DRPSPT_CREATE_COLLECTION_TITLE];
                
                if (photoStatus != DSPhotoStatusNoChange) {
                    /*
                    self.collection.photo = [NSEntityDescription insertNewObjectForEntityForName:@"CollectionPhoto"
                                                                          inManagedObjectContext:[DSGlobalData sharedData].childContext];
                    self.collection.photo.image = collectionData[DRPSPT_CREATE_COLLECTION_PHOTO];
                    self.collection.photo.collection = self.collection;
                     */
                }
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[DSDropspot session] editCollection:self.collection imageStatus:photoStatus block:^(DSAPICollectionResponse *spot, NSError *error) {
                        if (!error) {
                            [[NSNotificationCenter defaultCenter] postNotificationName:kDSDropspotCollectionEditedNotificationName
                                                                                object:nil
                                                                              userInfo:@{ @"collection" : self.collection }];
                        } else {
                            NSLog(@"error: %@", error.localizedDescription);
                        }
                    }];
                });
            }
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
    } else {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Please enter a name" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
}

- (BOOL)saveTitleWhileFocus {
    if (nameTextField.text && ![[nameTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:@""]) {
        collectionData[DRPSPT_CREATE_COLLECTION_TITLE] = nameTextField.text;
        return YES;
    }
    return NO;
}

- (IBAction)cancelButtonPressed:(id)sender {
    if (!self.collection.isSynced && !self.collection.isBeingSynced) {
        [[DSGlobalData sharedData].childContext performBlock:^{
            [[DSGlobalData sharedData].childContext deleteObject:self.collection];
        }];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UIActionSheet delegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (actionSheet.tag == kActionSheetTagImageNotSet || actionSheet.tag == kActionSheetTagImageSet) {
        if (buttonIndex == 0) {
            [self showImagePickerControllerWithSourceType:UIImagePickerControllerSourceTypeCamera];
        } else if (buttonIndex == 1) {
            [self showImagePickerControllerWithSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
        } else if (buttonIndex == 2) {
            if (actionSheet.tag == kActionSheetTagImageSet) {
                //self.collection.photo = nil;
                photoStatus = DSPhotoStatusRemoveImage;
            }
        }
    }
}

#pragma mark - UIImagePickerControllerDelegate

- (void)showImagePickerControllerWithSourceType:(UIImagePickerControllerSourceType)type {
    UIImagePickerController * pickerController = [[UIImagePickerController alloc] init];
    pickerController.sourceType = type;
    pickerController.delegate = self;
    pickerController.navigationBar.tintColor = kDSColorCoral;
    [self.navigationController presentViewController:pickerController animated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    if ([info objectForKey:UIImagePickerControllerOriginalImage]) {
        UIImage * image = [info objectForKey:UIImagePickerControllerOriginalImage];
        if (image) {
            collectionData[DRPSPT_CREATE_COLLECTION_PHOTO] = image;
            photoStatus = DSPhotoStatusNewImage;
            [self configureView];
        }
    }

    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UITextField

- (NSString *)slugFromTitle:(NSString *)title {
    
    NSString * string = [[NSString alloc] initWithString:title];
    string = [string lowercaseString];
    string = [string stringByReplacingOccurrencesOfString:@"ä" withString:@"ae"];
    string = [string stringByReplacingOccurrencesOfString:@"ü" withString:@"ue"];
    string = [string stringByReplacingOccurrencesOfString:@"ö" withString:@"oe"];
    string = [string stringByReplacingOccurrencesOfString:@"ß" withString:@"ss"];
    string = [string stringByReplacingOccurrencesOfString:@" " withString:@"-"];
    string = [string stringByReplacingOccurrencesOfString:@"_" withString:@"-"];
    
    return string;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [collectionData setObject:textField.text forKey:DRPSPT_CREATE_COLLECTION_TITLE];
    [collectionData setObject:[self slugFromTitle:textField.text] forKey:DRPSPT_CREATE_COLLECTION_SLUG];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

@end

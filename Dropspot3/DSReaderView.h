//
//  DSReaderView.h
//  Dropspot3
//
//  Created by Lukas Würzburger on 6/10/14.
//  Copyright (c) 2014 Dropspot GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

// Completion Blocks
typedef void(^VoidBlock)(void);

@interface DSReaderView : UIView

- (void)readText:(NSString *)text;
- (void)readText:(NSString *)text delay:(NSUInteger)seconds completion:(VoidBlock)completion;
- (void)readText:(NSString *)text delay:(NSUInteger)seconds completion:(VoidBlock)completion tapGesture:(VoidBlock)tapGesture;

- (void)stop;

@end

/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  SpotViewController.h
//  Dropspot3
//
//  Created by Lukas Würzburger on 18.10.13.
//  Copyright (c) 2013 Dropspot. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Spot.h"

#import "GAIHeader.h"

@interface SpotViewController : UIViewController

@property (nonatomic, strong) Spot * spot;

@end

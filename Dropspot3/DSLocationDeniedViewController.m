/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  DSLocationDeniedViewController.m
//  SingleCollection
//
//  Created by Lukas Würzburger on 2/14/14.
//  Copyright (c) 2014 Lukas Würzburger. All rights reserved.
//

#import "DSLocationDeniedViewController.h"

#import "DSGlobal.h"

@interface DSLocationDeniedViewController () {
    IBOutlet UILabel * label;
}

@end

@implementation DSLocationDeniedViewController

- (void)viewDidLoad {
    [super viewDidLoad];
	
    label.text = [NSString stringWithFormat:translate(@"LOCATION_DENIED_INFO_TEXT"), kAppName, kAppName];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[DSGlobal sharedGlobals] analyticsView:kDSScreenNameLocationDeniedView];
}

- (BOOL)shouldAutorotate {
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

@end

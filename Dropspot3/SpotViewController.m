/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  SpotViewController.m
//  Dropspot3
//
//  Created by Lukas Würzburger on 18.10.13.
//  Copyright (c) 2013 Dropspot. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import <LRResty/LRResty.h>
#import <MessageUI/MessageUI.h>
#import <MapKit/MapKit.h>

#import "SpotViewController.h"

#import "DSDropspot.h"
#import "Collection.h"
#import "UILabel+AutoHeight.h"
#import "CollectionDetailViewController.h"
#import "ImageViewerViewController.h"
#import "Spot+Location.h"
#import "DSLocationController.h"
#import "DSGlobal.h"
#import "DSGlobalData.h"
#import "BrowserViewController.h"
#import "DSTileView.h"
#import "CreateSpotViewController.h"
#import "SpotLink+Dropspot.h"
#import "DSLinkPreview.h"
#import "DataHelper.h"
#import "Spot+Dropspot.h"

@interface SpotViewController () <UITextViewDelegate, UIActionSheetDelegate, MFMessageComposeViewControllerDelegate, CreateSpotViewControllerDelegate, DSLinkPreviewDelegate> {

    // Shared
    IBOutlet UIScrollView * theScrollView;
    IBOutlet UIImageView * sourceImageView;
    IBOutlet UIActivityIndicatorView * activityIndicatorView;
    IBOutlet UILabel * sourceLabel;
    IBOutlet UIImageView * imageView;
    IBOutlet UIView * linksView;
    IBOutlet UILabel * partOfCollectionLabel;
    IBOutlet UIView * collectionsView;
    IBOutlet UIButton * deleteButton;
    IBOutlet UIView * deleteOverlayView;
    DSTileView * titleView;
    
    // Single collection app
    IBOutlet UIButton * checkInButton;
    IBOutlet UIButton * directionsButton;
    
    // Dropspot general
    IBOutlet UILabel * addressLabel;
}

@end

@implementation SpotViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"Detail";
    
    UIBarButtonItem *newBackButton = [[UIBarButtonItem alloc] initWithTitle:translate(@"MAP_VIEW_BACK_BUTTON_TITLE")
                                                                      style:UIBarButtonItemStyleBordered
                                                                     target:nil
                                                                     action:nil];
    [[self navigationItem] setBackBarButtonItem:newBackButton];

    if (kDSCheckInFeature) {
        [checkInButton setTitle:translate(@"SPOT_VIEW_CHECKIN_BUTTON_LABEL") forState:UIControlStateNormal];
        checkInButton.layer.cornerRadius = 4;
    }
    
    sourceImageView.layer.cornerRadius = 3.f;
    
    sourceImageView.layer.masksToBounds = YES;
    imageView.layer.masksToBounds = YES;
    [deleteButton setTintColor:kDSColorCoral];
    
    [self configureView];

    // Load photo if necessary
    [self checkSpotPhoto];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateAddressField:) name:kDSSpotDidDetermineAddressNotificationName object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateLocationNotification:) name:kDSLocationManagerDidUpdateLocationNotificationName object:nil];
    
    [[DSGlobal sharedGlobals] analyticsView:kDSScreenNameSpotDetailView];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if (!self.spot || self.spot.isDeleted) {
        [self.navigationController popViewControllerAnimated:YES];
    }
    [self configureView];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kDSSpotDidDetermineAddressNotificationName object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kDSLocationManagerDidUpdateLocationNotificationName object:nil];
}

- (BOOL)shouldAutorotate {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationPortrait;
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleDefault;
}

#pragma mark - Notifications

- (void)updateLocationNotification:(NSNotification *)notification {
    [self updateDistanceField];
}

#pragma mark - Helper

- (void)updateDistanceField {
    [DSGlobal lastLocationUpdate:^(BOOL available, NSDate *date, CLLocation *location) {
        if (location) {
            [DSGlobal distancePropertiesFromDistance:[location distanceFromLocation:self.spot.location] properties:^(double value, NSString *measurement) {
                if ([measurement isEqualToString:[DSGlobal lowestMeasurement]]) {
                    titleView.distance = [NSString stringWithFormat:@"%.0f%@", value, measurement];
                } else {
                    titleView.distance = [NSString stringWithFormat:@"%.2f%@", value, measurement];
                }
            }];
        }
    }];
}

- (void)updateAddressField:(NSNotification *)notification {
    if (self.spot == notification.userInfo[@"spot"]) {
        addressLabel.text = self.spot.address;
        
        [self setLayout];
    }
}

- (void)setSpot:(Spot *)spot {
    if (_spot != spot) {
        _spot = spot;
    }
    
    // Load link previews if necessary
    [self checkLinkPreviews];
}

- (void)checkSpotPhoto {
    
    // Get managed object context.
    NSManagedObjectContext * context = [[DSGlobalData sharedData] childContext];
    [context performBlock:^{

        if (!self.spot.photoFile) {
            
            [self startLoadingAnimation];
            
            // If no photo file is saved load it.
            [self.spot resumeLoadingPhotoInManagedObjectContext:context completion:^{

                [self stopLoadingAnimation];
                [context performBlock:^{
                    
                    NSError * error;
                    [context save:&error];
                    if (error) {
                        NSLog(@"Error %@", error.localizedDescription);
                    }
                }];
        
                if ([self respondsToSelector:@selector(configureView)]) {
                    [self configureView];
                }
                
                if ([self.presentedViewController isKindOfClass:[ImageViewerViewController class]]) {
                    
                    ImageViewerViewController * imageViewerViewController = (ImageViewerViewController *)self.presentedViewController;
                    [DataHelper imageWithFilePath:[DataHelper pathForCacheFile:self.spot.photoFile] block:^(UIImage *image) {

                        imageViewerViewController.image = image;
                    }];
                }
            }];
        }
    }];
}

- (void)startLoadingAnimation {
    
    CGRect frame = activityIndicatorView.frame;
    frame.origin.y = imageView.frame.origin.y + imageView.frame.size.height / 2 - activityIndicatorView.frame.size.height / 2;
    activityIndicatorView.frame = frame;
    
    [activityIndicatorView startAnimating];
}

- (void)stopLoadingAnimation {
    
    [activityIndicatorView stopAnimating];
}

- (void)checkLinkPreviews {
    
    if (self.spot.links) {
        
        BOOL shouldLoadPreviews = NO;
        
        // Load link previews
        for (SpotLink * link in self.spot.links) {
            
            if (!link.isComplete) {
                
                // A link is incomplete, previews should be loaded.
                shouldLoadPreviews = YES;
                break;
            } else if ([link.modified compare:self.spot.modified] == NSOrderedAscending) {
                
                // Spot was updated
                shouldLoadPreviews = YES;
                break;
            }
        }
        
        if (shouldLoadPreviews) {
            
            // Download links
            [self downloadLinks];
        }
    }
}

- (void)downloadLinks {
    
    NSManagedObjectContext * context = [DSGlobalData sharedData].childContext;
    
    // Start download
    [[DSDropspot session] linksWithURL:self.spot.linksURL paginatedWithBlock:^(DSAPISpotLinkResponse * linkResponse) {
        
        [context performBlock:^{
            
            // Create or update SpotLink object.
            [SpotLink spotLinkWithAPIResponse:linkResponse
                       inManagedObjectContext:context];
        }];
    } completion:^(NSError *error) {
        
        [context performBlock:^{
            
            NSError * error;
            [context save:&error];
            if (error) {
                NSLog(@"Error saving %@", error.localizedDescription);
            }
            
            [self checkSpotLinkImagesInManagedObjectContext:context];

            dispatch_async(dispatch_get_main_queue(), ^{
                
                // We need to put this into this child context to
                // make sure that database operations are done.
                
                // Configure the view again to show previews.
                if ([self respondsToSelector:@selector(configureView)] && self == self.navigationController.viewControllers.lastObject) {
                    [self configureView];
                }
            });
        }];
    }];
}

- (void)checkSpotLinkImagesInManagedObjectContext:(NSManagedObjectContext *)context {
    
    for (SpotLink * spotLink in self.spot.links) {
        
        if (!spotLink.imageFile) {
            
            NSURL * url = [NSURL URLWithString:spotLink.imageURL];
            NSString * fileName = DSURLToFileName(url.absoluteString);
            NSString * filePath = [DataHelper pathForCacheFile:fileName];
            dispatch_async(dispatch_get_main_queue(), ^{
                
                // Download preview image
                [[DSGlobalData sharedData] loadFileFromURL:url filePathForStorage:filePath oldFilePath:nil completion:^(BOOL success) {
                    
                    if (!success) {
                        
                        // Handle error
                        NSLog(@"Error downloading preview image");
                    } else {
                        
                        [context performBlock:^{
                            
                            if (spotLink && !spotLink.isDeleted) {
                                
                                spotLink.imageFile = fileName;
                            }
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                if ([self respondsToSelector:@selector(configureView)] && self == self.navigationController.viewControllers.lastObject) {
                                    [self configureView];
                                }
                            });
                        }];
                    }
                }];
            });
        }

        if (!spotLink.iconFile) {
            
            NSURL * url2 = [NSURL URLWithString:spotLink.iconURL];
            NSString * fileName2 = DSURLToFileName(url2.absoluteString);
            NSString * filePath2 = [DataHelper pathForCacheFile:fileName2];
            dispatch_async(dispatch_get_main_queue(), ^{
                
                // Download preview image
                [[DSGlobalData sharedData] loadFileFromURL:url2 filePathForStorage:filePath2 oldFilePath:nil completion:^(BOOL success) {
                    
                    if (!success) {
                        
                        // Handle error
                        NSLog(@"Error downloading preview image");
                    } else {
                        
                        [context performBlock:^{
                            
                            if (spotLink && !spotLink.isDeleted) {
                                
                                spotLink.iconFile = fileName2;
                            }
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                if ([self respondsToSelector:@selector(configureView)] && self == self.navigationController.viewControllers.lastObject) {
                                    [self configureView];
                                }
                            });
                        }];
                    }
                }];
            });
        }
    }
}

- (void)configureView {
    if (self.spot && !self.spot.isDeleted) {
        if (!titleView) {
            titleView = [[DSTileView alloc] initWithFrame:CGRectMake(20.f, 0.f, self.view.frame.size.width - 40.f, 10.f)];
            [titleView addTarget:self selector:@selector(handleSpotTap:)];
            [theScrollView addSubview:titleView];
            
            [self updateDistanceField];
        }

        titleView.text = self.spot.text;
        
        if (kDSHideBeenThereDoneThat) {
            titleView.infoStyle = DSTileViewInfoStyleNormal;
            titleView.info = self.spot.address;
            addressLabel.hidden = YES;
        } else {
            titleView.infoStyle = DSTileViewInfoStyleGreenBordered;
            addressLabel.text = self.spot.address;
            if (self.spot.seen) {
                //titleView.info = translate(@"TILE_VIEW_LABEL_BEEN_THERE");
            }
        }
        
        if (kDSForceAuthorName) {
            sourceLabel.text = kDSForceAuthorName;
        } else {
            sourceLabel.text = self.spot.user.displayName;
        }
        
        if (kDSForceAuthorImageName) {
            sourceImageView.image = [UIImage imageNamed:kDSForceAuthorImageName];
        } else {
            sourceImageView.image = [DataHelper imageWithFilePath:[DataHelper pathForCacheFile:self.spot.user.photoFile]];
        }
        
        if (!self.spot.links.count) {
            [DataHelper imageWithFilePath:[DataHelper pathForCacheFile:self.spot.photoFile] block:^(UIImage *image) {
                if (image) {
                    imageView.image = image;
                    [self setLayout];
                } else {
                    [DataHelper imageWithFilePath:[DataHelper pathForCacheFile:self.spot.thumbnailFile] block:^(UIImage *image) {
                        if (image) {
                            imageView.image = image;
                            [self setLayout];
                        }
                    }];
                }
            }];
        }
        
        linksView.hidden = !(self.spot.links.count);
        
        [self configureLinksView];
        
        [directionsButton setTitle:translate(@"SPOT_VIEW_DIRECTIONS_BUTTON") forState:UIControlStateNormal];
        
        if (!kIsSingleCollectionApp || kDSOwnCollectionApp) {
            if (self.spot.isOwnedByMe) {
                //UIBarButtonItem * editButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(editButtonPressed:)];
                //self.navigationItem.rightBarButtonItem = editButton;
            }
        }
        
        if (!kIsSingleCollectionApp) {
            if (self.spot.collections.count > 0) {
                partOfCollectionLabel.text = translate(@"SPOT_VIEW_PART_OF_LABEL");
            } else {
                partOfCollectionLabel.hidden = YES;
            }
            
            deleteButton.hidden = YES;//!self.spot.isOwnedByMe;
        } else {
            partOfCollectionLabel.hidden = YES;
            collectionsView.hidden = YES;
            deleteButton.hidden = YES;
        }
        [self setLayout];
    } else {
        UIView * overlay = [[UIView alloc] initWithFrame:self.view.bounds];
        overlay.backgroundColor = [UIColor whiteColor];
        [self.view addSubview:overlay];
    }
}

- (void)configureLinksView {
    __block double lastY = 0;
    
    [[DSGlobalData sharedData].childContext performBlock:^{
        NSLog(@"self.spot.links %@ %@", NSStringFromClass(self.spot.links.class), self.spot.links);
        
        if (self.spot.links) {
            NSArray * spotLinks = self.spot.links.allObjects;
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                for (NSInteger i = 0; i < spotLinks.count; i++) {
                    
                    SpotLink * link = spotLinks[i];
                    
                    if ([link isKindOfClass:[SpotLink class]]) {
                        
                        DSLinkPreview * preview = [self linkPreviewWithLink:link.link];
                        preview.delegate = self;
                        preview.url = [NSURL URLWithString:link.link];
                        preview.urlHost = link.shortHost;
                        preview.placeholderImage = [DataHelper imageWithFilePath:[DataHelper pathForCacheFile:self.spot.user.photoFile]];
                        
                        if (link.isComplete) {
                            preview.title = link.title;
                            [DataHelper imageWithFilePath:[DataHelper pathForCacheFile:link.imageFile] block:^(UIImage *image) {
                                if (image) {
                                    preview.image = image;
                                } else {
                                    if (![[DSGlobalData sharedData] isDownloadingObjectWithURL:link.link]) {
                                        preview.image = [DataHelper imageWithFilePath:[DataHelper pathForCacheFile:self.spot.photoFile]];
                                    }
                                }
                            }];
                            preview.favicon = [DataHelper imageWithFilePath:[DataHelper pathForCacheFile:link.iconFile]];
                            
                            [preview stopLoadingAnimation];
                        } else {
                            preview.title = link.link;
                            
                            [preview startLoadingAnimation];
                        }
                        
                        CGRect frame = preview.frame;
                        frame.origin.y = lastY;
                        preview.frame = frame;
                        [linksView addSubview:preview];
                        
                        lastY = preview.frame.origin.y + preview.frame.size.height + 20;
                    }
                }
                
                if (linksView.subviews.count > spotLinks.count) {
                    for (UIView * view in linksView.subviews) {
                        if (view.tag > spotLinks.count) {
                            [view removeFromSuperview];
                        }
                    }
                }
                
                CGRect frame = linksView.frame;
                frame.size.height = lastY - 20;
                linksView.frame = frame;
                
                [self setLayout];
            });
        }
    }];
}

- (DSLinkPreview *)linkPreviewWithLink:(NSString *)link {
    
    DSLinkPreview * linkPreview = nil;
    
    for (DSLinkPreview * lp in linksView.subviews) {
        if ([lp isKindOfClass:[DSLinkPreview class]]) {
            if ([lp.url.absoluteString isEqualToString:link]) {
                linkPreview = lp;
            }
        }
    }
    
    if (!linkPreview) {
        linkPreview = [[DSLinkPreview alloc] initWithFrame:CGRectMake(0, 0, 280, 1)];
        linkPreview.url = [NSURL URLWithString:link];
    }
    
    return linkPreview;
}

- (void)setLayout {
    float positionY = 20;
    CGRect frame;
    
    frame = sourceImageView.frame;
    frame.origin.y = positionY;
    sourceImageView.frame = frame;
    
    frame = sourceLabel.frame;
    if (sourceImageView.image) {
        frame.origin.x = sourceImageView.frame.origin.x + sourceImageView.frame.size.width + 10;
        frame.size.width = self.view.frame.size.width - 2 * 20 - sourceImageView.frame.size.width - 10;
        sourceLabel.frame = frame;
        frame = [sourceLabel autoAdjustHeight];
        frame.origin.y = sourceImageView.frame.origin.y + sourceImageView.frame.size.height / 2 - frame.size.height / 2;
        sourceLabel.frame = frame;
    } else {
        frame.origin.x = 20;
        frame.origin.y = positionY;
        frame.size.width = self.view.frame.size.width - 2 * 20;
        sourceLabel.frame = frame;
        [sourceLabel autoAdjustHeight];
    }
    
    positionY = sourceImageView.frame.origin.y + sourceImageView.frame.size.height + 20;
    
    if (imageView.image) {
        
        //CGFloat ratio = imageView.image.size.height / imageView.image.size.width;
        frame = imageView.frame;
        frame.origin.y = positionY;
        //frame.size.height = frame.size.width * ratio;
        frame.size.height = frame.size.width;
        //        if (frame.size.height > frame.size.width) {
        //            frame.size.height = frame.size.width;
        //        }
        
        imageView.frame = frame;
        
        positionY = imageView.frame.origin.y + imageView.frame.size.height + 20;
    }
    
    imageView.hidden = (!imageView.image);
    
    if (!kDSHideBeenThereDoneThat) {
        if (self.spot.address && ![[self.spot.address stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]) {
            [addressLabel autoAdjustHeight];
            
            frame = addressLabel.frame;
            frame.origin.y = positionY;
            if (imageView.image) {
                frame.origin.y -= 10;
            }
            addressLabel.frame = frame;
            
            positionY = addressLabel.frame.origin.y + addressLabel.frame.size.height + 20;
        }
    }
    
    frame = titleView.frame;
    frame.origin.y = positionY;
    titleView.frame = frame;
    
    positionY = titleView.frame.origin.y + titleView.frame.size.height + 20;
    
    if (!linksView.hidden) {
        frame = linksView.frame;
        frame.origin.y = positionY;
        linksView.frame = frame;
        
        positionY = linksView.frame.origin.y + linksView.frame.size.height + 20;
    }
    
    frame = directionsButton.frame;
    frame.origin.y = positionY;
    directionsButton.frame = frame;
    
    positionY = directionsButton.frame.origin.y + directionsButton.frame.size.height + 20;
    
    if (!kIsSingleCollectionApp) {
        for (UIView * view in collectionsView.subviews) {
            [view removeFromSuperview];
        }
        
        if (self.spot.collections.count > 0) {
            partOfCollectionLabel.hidden = NO;
            frame = [partOfCollectionLabel autoAdjustHeight];
            frame.origin.y = positionY;
            partOfCollectionLabel.frame = frame;
            
            positionY = partOfCollectionLabel.frame.origin.y + partOfCollectionLabel.frame.size.height + 20;
            
            CGFloat collectionsPositionY = 0;
            
            for (int i = 0; i < self.spot.collections.allObjects.count; i++) {
                Collection * collection = self.spot.collections.allObjects[i];
                
                UIView * view = [self collectionViewWithCollection:collection];
                view.tag = i;
                
                CGRect frame = view.frame;
                frame.origin.x = 20;
                frame.origin.y = collectionsPositionY;
                view.frame = frame;
                
                [collectionsView addSubview:view];
                
                collectionsPositionY = view.frame.origin.y + view.frame.size.height + 5;
            }
            
            frame = collectionsView.frame;
            frame.origin.y = positionY;
            frame.size.height = collectionsPositionY;
            collectionsView.frame = frame;
            
            positionY = collectionsView.frame.origin.y + collectionsView.frame.size.height + 20;
        } else {
            partOfCollectionLabel.hidden = YES;
        }
    }
    
    if (kDSCheckInFeature && self.spot.address && ![[self.spot.address stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]) {
        frame = checkInButton.frame;
        frame.origin.y = positionY;
        checkInButton.frame = frame;
        
        positionY = checkInButton.frame.origin.y + checkInButton.frame.size.height + 20;
    } else {
        checkInButton.hidden = YES;
    }
    
    if (!kIsSingleCollectionApp) {
        frame = deleteButton.frame;
        frame.origin.y = positionY;
        deleteButton.frame = frame;
        
        positionY = deleteButton.frame.origin.y + deleteButton.frame.size.height + 20;
    }
    
    [theScrollView setContentSize:CGSizeMake(theScrollView.frame.size.width, positionY)];
}

- (UIView *)collectionViewWithCollection:(Collection *)collection {
    UIView * view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width - (partOfCollectionLabel.frame.origin.x * 2), 50)];
    view.backgroundColor = [UIColor colorWithWhite:0.98 alpha:1.0];
    view.layer.shadowOffset = CGSizeMake(1, 1);
    view.layer.shadowColor = [UIColor blackColor].CGColor;
    view.layer.shadowOpacity = 0.1;
    view.layer.shadowRadius = 0;
    
    UIImageView * iv = [[UIImageView alloc] initWithFrame:CGRectMake(5, 5, 40, 40)];
    [DataHelper imageWithFilePath:[DataHelper pathForCacheFile:collection.photoFile] block:^(UIImage *image) {
        if (image) {
            iv.image = image;
        } else {
            [DataHelper imageWithFilePath:[DataHelper pathForCacheFile:collection.thumbnailFile] block:^(UIImage *image) {
                if (image) {
                    iv.image = image;
                }
            }];
        }
    }];
    iv.contentMode = UIViewContentModeScaleAspectFill;
    iv.layer.masksToBounds = YES;
    [view addSubview:iv];
    
    UILabel * t = [[UILabel alloc] initWithFrame:CGRectMake(55, 5, view.frame.size.width - 60, view.frame.size.height - 10)];
    t.numberOfLines = 0;
    t.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:15.0];
    t.textColor = [UIColor darkGrayColor];
    t.backgroundColor = [UIColor clearColor];
    t.text = collection.title;
    [view addSubview:t];
    
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    [view addGestureRecognizer:tap];
    
    return view;
}

- (void)deleteSpot {
    if (self.spot.url && [NSURL URLWithString:self.spot.url]) {
        LRRestyClient * client = [LRResty client];
        [client setHandlesCookiesAutomatically:NO];
        
        [client attachRequestModifier:^(LRRestyRequest *request) {
            NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
            [request addHeader:@"Authorization" value:[NSString stringWithFormat:@"Token %@", [[defaults objectForKey:kDSDropspotSessionDomainKeyName] valueForKey:kDSDropspotSessionTokenKeyName]]];
        }];
        
        deleteOverlayView.hidden = NO;
        deleteOverlayView.alpha = 0.0;
        [self.view bringSubviewToFront:deleteOverlayView];
        
        [UIView animateWithDuration:0.25 animations:^{
            deleteOverlayView.alpha = 1.0;
        }];
        
        self.navigationItem.rightBarButtonItem = nil;
        
        [client delete:self.spot.url withBlock:^(LRRestyResponse *response) {
            if (response.status == 204 || response.status == 404) {
                [[DSGlobalData sharedData].childContext performBlock:^{
                    [[DSGlobalData sharedData].childContext deleteObject:self.spot];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [[NSNotificationCenter defaultCenter] postNotificationName:kDSDropspotSpotDeletedNotificationName object:nil];
                        [self.navigationController popViewControllerAnimated:YES];
                    });
                }];
            } else {
                UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Oops..." message:@"There was a problem deleting this spot, please try again later." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
                
                NSLog(@"status code %lu", (unsigned long)response.status);
            }
        }];
    } else {
        [[DSGlobalData sharedData].childContext performBlock:^{
            [[DSGlobalData sharedData].childContext deleteObject:self.spot];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [[NSNotificationCenter defaultCenter] postNotificationName:kDSDropspotSpotDeletedNotificationName object:nil];
                [self.navigationController popViewControllerAnimated:YES];
            });
        }];
    }
}

#pragma mark - Gesture Recognizer

- (void)handleTap:(UIGestureRecognizer *)gestureRecognizer {
    Collection * collection = self.spot.collections.allObjects[gestureRecognizer.view.tag];
    [self performSegueWithIdentifier:@"spotToCollection" sender:collection];
}

#pragma mark - IBActions

- (IBAction)photoTapped:(UIGestureRecognizer *)gesture {
    if (gesture.state == UIGestureRecognizerStateRecognized) {
        [self presentImageViewerController];
    }
}

- (void)editButtonPressed:(id)sender {
    [self performSegueWithIdentifier:@"editSpotSegue" sender:nil];
}

- (IBAction)deleteButtonPressed:(id)sender {
    UIActionSheet * sheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Delete" otherButtonTitles:nil];
    [sheet showInView:self.view];
}

- (void)handleSpotTap:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)checkInButtonPressed:(id)sender {
    MFMessageComposeViewController * viewController = [[MFMessageComposeViewController alloc] init];
    if ([MFMessageComposeViewController canSendText]) {
        NSArray * addressParts = [self.spot.address componentsSeparatedByString:@","];
        if (addressParts.count >= 2) {
            NSMutableString * shortAddressString = [[addressParts[0] stringByAppendingFormat:@", %@", [addressParts[1] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]] mutableCopy];
            viewController.body = [NSString stringWithFormat:@"Ich geh kickern! Wo? %@.\nBiste dabei?", shortAddressString];
        } else {
            viewController.body = [NSString stringWithFormat:@"Ich geh kickern! Wo? %@.\nBiste dabei?", self.spot.address];
        }
        viewController.messageComposeDelegate = self;
        [self presentViewController:viewController animated:YES completion:nil];
    }
}

- (IBAction)directionsButtonPressed:(id)sender {
        CLLocationCoordinate2D locb = self.spot.location.coordinate;
        
        Class mapItemClass = [MKMapItem class];
        if (mapItemClass && [mapItemClass respondsToSelector:@selector(openMapsWithItems:launchOptions:)]) {
            MKPlacemark * placemark = [[MKPlacemark alloc] initWithCoordinate:locb addressDictionary:nil];
            MKMapItem * mapItem = [[MKMapItem alloc] initWithPlacemark:placemark];
            [mapItem setName:self.spot.text];
            NSDictionary * launchOptions = @{MKLaunchOptionsDirectionsModeKey:MKLaunchOptionsDirectionsModeWalking};
            MKMapItem * currentLocationMapItem = [MKMapItem mapItemForCurrentLocation];
            [MKMapItem openMapsWithItems:@[currentLocationMapItem, mapItem] launchOptions:launchOptions];
        }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"editSpotSegue"]) {
        CreateSpotViewController * theEditSpotViewController = [[(UINavigationController *)[segue destinationViewController] viewControllers] lastObject];
        if (theEditSpotViewController) {
            theEditSpotViewController.spot = self.spot;
            theEditSpotViewController.delegate = self;
        }
    } else if ([segue.identifier isEqualToString:@"spotToCollection"]) {
        CollectionDetailViewController * collectionViewController = segue.destinationViewController;
        if (collectionViewController) {
            collectionViewController.collection = sender;
        }
    } else if ([segue.identifier isEqualToString:@"showBrowser"]) {
        BrowserViewController * theBrowserViewController = segue.destinationViewController;
        if (theBrowserViewController) {
            theBrowserViewController.url = sender;
        }
    } else if ([segue.identifier isEqualToString:@"showImageViewer"]) {
        ImageViewerViewController * imageViewer = [[(UINavigationController *)[segue destinationViewController] viewControllers] lastObject];
        if (imageViewer) {
            [DataHelper imageWithFilePath:[DataHelper pathForCacheFile:self.spot.photoFile] block:^(UIImage *image) {
                imageViewer.image = image;
            }];
        }
    }
}

#pragma mark - Link preview delegate

- (void)linkPreviewTapped:(DSLinkPreview *)linkPreview {
    [self performSegueWithIdentifier:@"showBrowser" sender:linkPreview.url];
    

    // Mixpanel Analytics
    [[Mixpanel sharedInstance] identify:[DSUser currentUser].username];
    [[Mixpanel sharedInstance] track:@"Open Article" properties:@{
                                                                  @"Spot" : self.spot.slug,
                                                                  @"URL" : [linkPreview.url absoluteString],
                                                                  @"Username" : [DSUser currentUser].username
                                                                  }];
}

#pragma mark - Create Spot View Controller

- (void)createSpotViewControllerDidSave:(CreateSpotViewController *)createSpotViewController {
    [createSpotViewController dismissViewControllerAnimated:YES completion:nil];
}

- (void)createSpotViewControllerDidCancel:(CreateSpotViewController *)createSpotViewController {
    [createSpotViewController dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Animations

- (void)presentImageViewerController {
    ImageViewerViewController * imageController = [self.storyboard instantiateViewControllerWithIdentifier:@"ImageViewer"];
    imageController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    
    imageController.image = imageView.image;
    
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationFade];
    [self presentViewController:imageController animated:YES completion:nil];
}

#pragma mark - UIActionSheet delegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        [self deleteSpot];
    }
}

#pragma mark - Message Compose Delegate

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result {
    if (result == MessageComposeResultFailed) {
        [[[UIAlertView alloc] initWithTitle:@"Sorry" message:@"Something went wrong. Please try again." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    } else if (result == MessageComposeResultSent) {
        [[[UIAlertView alloc] initWithTitle:@"Success!" message:@"Message sent." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        [controller dismissViewControllerAnimated:YES completion:nil];
    } else {
        [controller dismissViewControllerAnimated:YES completion:nil];
    }
}

@end

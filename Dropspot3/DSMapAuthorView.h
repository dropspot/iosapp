/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  DSMapAuthorView.h
//  Dropspot3
//
//  Created by Lukas Würzburger on 3/26/14.
//  Copyright (c) 2014 Dropspot GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DSMapAuthorView : UIView

/**
 * The image to be displayed.
 */
@property (nonatomic, strong) UIImage * image;

/**
 * The name to be displayed.
 */
@property (nonatomic, strong) NSString * name;

/**
 * Changes the image wether animated or not.
 */
- (void)setImage:(UIImage *)image animated:(BOOL)animated;

/**
 * Changes the name wether animated or not.
 */
- (void)setName:(NSString *)name animated:(BOOL)animated;

@end

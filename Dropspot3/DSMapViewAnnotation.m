/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  DSAnnotation.m
//  Dropspot
//
//  Created by Lukas Würzburger on 20.06.13.
//  Copyright (c) 2013 Lukas Würzburger. All rights reserved.
//

#import "DSMapViewAnnotation.h"
#import "Spot+Location.h"

@implementation DSMapViewAnnotation

- (id)initWithSpot:(Spot *)spot {
    if (self = [super init]) {
        _spot = spot;
        
        _selectable = YES;
        
        // Check if the coordinates are valid, if not return nil.
        if (CLLocationCoordinate2DIsValid(_spot.location.coordinate)) {
            _coordinate = CLLocationCoordinate2DMake(self.spot.latitude, self.spot.longitude);
        } else {
            return nil;
        }
    }
    return self;
}

@end

/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  MasterBottomCell.m
//  Dropspot3
//
//  Created by Lukas Würzburger on 3/11/14.
//  Copyright (c) 2014 Dropspot GmbH. All rights reserved.
//

#import "MasterBottomCell.h"

#import "DSDropspot.h"

@implementation MasterBottomCell

- (void)setLayout {
    float positionY = 10;
    
    CGRect frame;
    
    [self.settingsButton setTitleColor:[UIColor colorWithWhite:0.8f alpha:1.0] forState:UIControlStateNormal];
    [self.settingsButton setTitleColor:[UIColor colorWithWhite:0.3f alpha:1.0] forState:UIControlStateHighlighted];
    self.settingsButton.layer.borderWidth = 1.f;
    self.settingsButton.layer.borderColor = [UIColor colorWithWhite:0.8f alpha:1.0].CGColor;
    self.settingsButton.layer.cornerRadius = 6.f;
    
    frame = self.settingsButton.frame;
    frame.origin.y = positionY;
    self.settingsButton.frame = frame;
}

@end

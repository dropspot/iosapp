/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  DSFacebook.h
//  Dropspot3
//
//  Created by Lukas Würzburger on 11.09.13.
//  Copyright (c) 2013 Dropspot. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <FacebookSDK/FacebookSDK.h>

#define kDSFacebookSessionChangedNotificationName @"DSFacebookSessionChangedNotificationName"

@class DSFacebook;

@protocol DSFacebookDelegate <NSObject>

@required
- (void)sessionStateChanged:(FBSession *)session state:(FBSessionState)state error:(NSError *)error;

@end

@interface DSFacebook : NSObject

@property (nonatomic, strong) NSArray * initialPermissions;

@property (nonatomic) BOOL isUserLoggedIn;

@property (nonatomic, assign) id<DSFacebookDelegate> delegate;

+ (DSFacebook *)session;

- (void)checkLoginState;
- (BOOL)openSession;
- (void)closeSession;
- (FBSessionState)sessionState;

- (void)handleDidBecomeActive;
- (BOOL)handleOpenURL:(NSURL *)url;

+ (NSString *)FBErrorCodeDescription:(FBErrorCode)code;

@end

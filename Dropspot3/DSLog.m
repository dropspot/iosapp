/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  DSLog.m
//  Dropspot3
//
//  Created by Lukas Würzburger on 4/2/14.
//  Copyright (c) 2014 Dropspot GmbH. All rights reserved.
//

#import "DSLog.h"

@implementation DSLog

+ (instancetype)sharedLog {
    static DSLog * instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[DSLog alloc] init];
        
        [Parse setApplicationId:@"DR1DoRz76aoGlbLdSzimFwZVmtnzQkvsxiOJnK2E" clientKey:@"D4l4lL2kTbqF8tr7aJM0ju9qr0asQXI1tFncqdP0"];
    });
    return instance;
}

- (void)logWithUser:(NSString *)username event:(NSString *)event {
    [self logWithUser:username event:event spot:@"" comment:@""];
}

- (void)logWithUser:(NSString *)username event:(NSString *)event spot:(NSString *)spot {
    [self logWithUser:username event:event spot:spot comment:@""];
}

- (void)logWithUser:(NSString *)username event:(NSString *)event comment:(NSString *)comment {
    [self logWithUser:username event:event spot:@"" comment:comment];
}

- (void)logWithUser:(NSString *)username event:(NSString *)event spot:(NSString *)spot comment:(NSString *)comment {
/*
    PFObject * log = [PFObject objectWithClassName:@"Log"];
    [log setObject:username forKey:@"username"];
    [log setObject:event forKey:@"event"];
    [log setObject:spot forKey:@"spot"];
    [log setObject:comment forKey:@"comment"];
    
    NSLog(@"saveloginbackground");
    [log saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        NSLog(@"success %i error %@", succeeded, error);
    }];
    */
#if DEBUG
    [self log:[NSString stringWithFormat:@"%@ - %@ - %@ - %@", username, event, spot, comment]];
#endif
}

- (NSString*)locationPath
{
	NSString* path = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
	return [path stringByAppendingPathComponent:@"locationPath.txt"];
}

- (void) clearLog
{
	NSString * content = @"";
	NSString * fileName = [self locationPath];
	[content writeToFile:fileName
              atomically:NO
                encoding:NSStringEncodingConversionAllowLossy
                   error:nil];
}

- (void) log:(NSString*)msg
{
	NSDateFormatter * formatter = [[NSDateFormatter alloc] init];
	[formatter setTimeStyle:NSDateFormatterMediumStyle];
	NSString * logMessage = [NSString stringWithFormat:@"%@ %@", [formatter stringFromDate:[NSDate date]], msg];
    
	NSString * fileName = [self locationPath];
    NSMutableString * string = [NSMutableString stringWithContentsOfFile:fileName encoding:NSUTF8StringEncoding error:nil];
    if (!string) {
        string = [NSMutableString string];
    }
    [string appendFormat:@"%@\n", logMessage];
    [string writeToFile:fileName atomically:NO encoding:NSUTF8StringEncoding error:nil];
}


@end

/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  ImageViewerViewController.m
//  Dropspot3
//
//  Created by Lukas Würzburger on 1/10/14.
//  Copyright (c) 2014 Dropspot. All rights reserved.
//

#import "ImageViewerViewController.h"

#import "DSGlobal.h"
#import "GAIHeader.h"

@interface ImageViewerViewController () {
    IBOutlet UIImageView * imageView;
}

- (IBAction)doneButtonPressed:(id)sender;

@end

@implementation ImageViewerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor blackColor];

    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;

    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    [self.view addGestureRecognizer:tap];
    
    imageView.image = self.image;
}

- (void)setImage:(UIImage *)image {
    if (_image != image) {
        _image = image;
        
        imageView.image = _image;
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[DSGlobal sharedGlobals] analyticsView:kDSScreenNameImageView];
}

- (BOOL)shouldAutorotate {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAllButUpsideDown;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationPortrait;
}

- (void)dismiss {
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Gestures

- (void)handleTap:(UIGestureRecognizer *)gesture {
    if (gesture.state == UIGestureRecognizerStateRecognized) {
        [self dismiss];
    }
}

#pragma mark - IBActions

- (IBAction)doneButtonPressed:(id)sender {
    [self dismiss];
}

@end

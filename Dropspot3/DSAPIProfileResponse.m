/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  DSAPIProfileResponse.m
//  Dropspot3
//
//  Created by Lukas Würzburger on 2/7/14.
//  Copyright (c) 2014 Dropspot. All rights reserved.
//

#import "DSAPIProfileResponse.h"

@implementation DSAPIProfileResponse

@end

/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  NoNewsNearbyCell.m
//  Dropspot3
//
//  Created by Lukas Würzburger on 4/11/14.
//  Copyright (c) 2014 Dropspot GmbH. All rights reserved.
//

#import "NoNewsNearbyCell.h"

@implementation NoNewsNearbyCell

@end

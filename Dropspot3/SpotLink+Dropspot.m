/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  SpotLink+Dropspot.m
//  Dropspot3
//
//  Created by Lukas Würzburger on 5/14/14.
//  Copyright (c) 2014 Dropspot GmbH. All rights reserved.
//

#import "SpotLink+Dropspot.h"

#import "DataHelper.h"

@implementation SpotLink (Dropspot)

+ (SpotLink *)spotLinkWithAPIResponse:(DSAPISpotLinkResponse *)response
               inManagedObjectContext:(NSManagedObjectContext *)context {
    
    SpotLink * spotLink = nil;
    
    NSFetchRequest * fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"SpotLink"];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"link = %@", response.link];
    NSError * error = nil;
    NSArray * matches = [context executeFetchRequest:fetchRequest error:&error];
    if (error || !matches || matches.count > 1) {
        
        // Handle error
    } else if (matches.count == 1) {
        
        spotLink = matches.firstObject;
    } else {
        
        // Create new object.
        spotLink = [NSEntityDescription insertNewObjectForEntityForName:@"SpotLink"
                                                 inManagedObjectContext:context];
        NSLog(@"Create SpotLink %@", response.link);
    }
    
    // For the case that we use modified timestamp
    
    BOOL shouldUpdate = NO;
    if (spotLink.modified) {
        
        // Compare modified dates
        NSDate * modified = DSDateFromString(response.modified);
        if ([modified compare:spotLink.modified] == NSOrderedDescending) {
            shouldUpdate = YES;
        }
    } else {
        shouldUpdate = YES;
    }
    
    NSLog(@"SpotLink should update %i %@", shouldUpdate, response.link);

    if (shouldUpdate) {
        
        // Update attributes.
        spotLink.identifier = response.identifier;
        spotLink.url = response.url;
        spotLink.link = response.link;
        spotLink.title = response.title;
        spotLink.modified = DSDateFromString(response.modified);
        spotLink.shortHost = DSShortHostFromURL([NSURL URLWithString:spotLink.link]);
        spotLink.isComplete = YES;
        
        if (![spotLink.imageURL isEqualToString:response.photoURL]) {
            
            spotLink.imageURL = response.photoURL;
            
            // Check if an old image exists.
            NSString * filePath = [DataHelper pathForCacheFile:spotLink.imageFile];
            BOOL isDir;
            BOOL exists = [[NSFileManager defaultManager] fileExistsAtPath:filePath isDirectory:&isDir];
            if (exists && !isDir) {
                
                // Delete file
                NSError * error;
                [[NSFileManager defaultManager] removeItemAtPath:filePath error:&error];
                if (error) {
                    
                    // Handle error
                    NSLog(@"removeItemAtPath:error: %@", error.localizedDescription);
                }
            }
            
            spotLink.imageFile = nil;
        }

        if (![spotLink.iconURL isEqualToString:response.iconURL]) {
            
            spotLink.iconURL = response.iconURL;
            
            // Check if an old image exists.
            NSString * filePath = [DataHelper pathForCacheFile:spotLink.iconFile];
            BOOL isDir;
            BOOL exists = [[NSFileManager defaultManager] fileExistsAtPath:filePath isDirectory:&isDir];
            if (exists && !isDir) {
                
                // Delete file
                NSError * error;
                [[NSFileManager defaultManager] removeItemAtPath:filePath error:&error];
                if (error) {
                    
                    // Handle error
                    NSLog(@"removeItemAtPath:error: %@", error.localizedDescription);
                }
            }
            
            spotLink.iconFile = nil;
        }

    }
    
    return spotLink;
}

@end

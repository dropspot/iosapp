/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  WelcomeSlideViewController.h
//  Dropspot3
//
//  Created by Lukas Würzburger on 16.10.13.
//  Copyright (c) 2013 Dropspot. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DSWelcomeSlide.h"

@class WelcomeSlideViewController;

@protocol WelcomeSlideViewControllerDelegate <NSObject>

- (void)welcomeSlideViewControllerDidPressNextButton:(WelcomeSlideViewController *)welcomeSlideViewController;
- (void)welcomeSlideViewControllerDidPressPrevButton:(WelcomeSlideViewController *)welcomeSlideViewController;

@end

@interface WelcomeSlideViewController : UIViewController

@property (nonatomic, retain) DSWelcomeSlide * slide;

@property (nonatomic, assign) id <WelcomeSlideViewControllerDelegate> delegate;

@end

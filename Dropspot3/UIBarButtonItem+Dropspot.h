/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  UIBarButtonItem+Dropspot.h
//  Dropspot3
//
//  Created by Lukas Würzburger on 5/26/14.
//  Copyright (c) 2014 Dropspot GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIBarButtonItem (Dropspot)

+ (UIBarButtonItem *)mapBarButtonItemWithTarget:(id)target action:(SEL)action;
+ (UIBarButtonItem *)backBarButtonItemWithTarget:(id)target action:(SEL)action;
+ (UIBarButtonItem *)searchBarButtonItemWithTarget:(id)target action:(SEL)action;
+ (UIBarButtonItem *)refreshBarButtonItemWithTarget:(id)target action:(SEL)action;
+ (UIBarButtonItem *)locateMeBarButtonItemWithTarget:(id)target action:(SEL)action;

@end

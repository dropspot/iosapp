/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  SpotLink.m
//  Dropspot3
//
//  Created by Lukas Würzburger on 4/17/14.
//  Copyright (c) 2014 Dropspot GmbH. All rights reserved.
//

#import "SpotLink.h"
#import "Spot.h"

#import "DSDropspot.h"

@implementation SpotLink

@dynamic identifier;
@dynamic url;
@dynamic link;
@dynamic imageURL;
@dynamic imageFile;
@dynamic title;
@dynamic iconURL;
@dynamic iconFile;
@dynamic shortHost;
@dynamic modified;
@dynamic isComplete;

@dynamic spot;

+ (SpotLink *)spotLinkWithLink:(NSString *)linkString
        inManagedObjectContext:(NSManagedObjectContext *)context {
    
    SpotLink * spotLink = nil;
    
    NSFetchRequest * fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"SpotLink"];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"link = %@", linkString];
    NSArray * matches = [context executeFetchRequest:fetchRequest error:nil];
    if (!matches || matches.count > 1) {
        
        // Handle error
    } else if (matches.count) {
        
        spotLink = matches[0];
    } else {
        
        // Create new object
        spotLink = [NSEntityDescription insertNewObjectForEntityForName:@"SpotLink" inManagedObjectContext:context];
        spotLink.isComplete = NO;
        spotLink.link = linkString;
        spotLink.shortHost = DSShortHostFromURL([NSURL URLWithString:spotLink.link]);
    }
    
    return spotLink;
}

@end

/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  Spot+Dropspot.m
//  Dropspot3
//
//  Created by Lukas Würzburger on 16.09.13.
//  Copyright (c) 2013 Dropspot. All rights reserved.
//

#import "Spot+Dropspot.h"
#import "DSDropspot.h"
#import "User+Dropspot.h"
#import "SpotLink+Dropspot.h"
#import "DSGlobalData.h"
#import "DataHelper.h"

@implementation Spot (Dropspot)

+ (Spot *)spotWithAPIResponse:(DSAPISpotResponse *)response
       inManagedObjectContext:(NSManagedObjectContext *)context {
    
    Spot * spot = nil;
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Spot"];
    request.predicate = [NSPredicate predicateWithFormat:@"slug = %@", response.slug];
    
    NSError *error = nil;
    NSArray *matches = [context executeFetchRequest:request error:&error];
    
    if (!matches || ([matches count] > 1)) {
        NSLog(@"spot matches %@", matches);
    } else {
        NSDateFormatter * dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:DRPSPT_DATE_FORMAT];
        NSDate * modified = [dateFormatter dateFromString:response.modified];
        
        if (![matches count]) {
            spot = [NSEntityDescription insertNewObjectForEntityForName:@"Spot" inManagedObjectContext:context];
            spot.active = YES;
        } else {
            spot = [matches lastObject];
            
            if ([spot.modified compare:modified] == NSOrderedSame) {
                return spot;
            }
        }
        
        [spot fillWithResponse:response inManagedObjectContext:context];
    }
    
    return spot;
}

- (void)updateWithAPIResponse:(DSAPISpotResponse *)response
       inManagedObjectContext:(NSManagedObjectContext *)context {
    NSDateFormatter * dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:DRPSPT_DATE_FORMAT];
    NSDate * modified = [dateFormatter dateFromString:response.modified];
    
    if (self.modified && [self.modified compare:modified] == NSOrderedSame) {
        return;
    }
    
    [self fillWithResponse:response inManagedObjectContext:context];
}

- (void)checkDependencyForSpot:(Spot *)spot
        inManagedObjectContext:(NSManagedObjectContext *)context {
    
    NSMutableArray * slugs = [[spot.collectionSlugs componentsSeparatedByString:@","] mutableCopy];
    
    if (slugs.count > 0 && [slugs[0] isEqualToString:@""]) {
        [slugs removeObjectAtIndex:0];
    }
    
    NSMutableSet * collectionSet = [spot.collections mutableCopy];
    
    for (NSString * slug in slugs) {
        
        BOOL exists = NO;
        
        for (Collection * c in spot.collections) {
            if ([c.slug isEqualToString:slug]) {
                exists = YES;
            }
        }
        
        if (!exists) {
            [collectionSet addObject:[Collection collectionWithSlug:slug inManagedObjectContext:context]];
        }
    }
    
    for (Collection * c in spot.collections) {
        BOOL exists = NO;
        
        for (NSString * slug in slugs) {
            if ([c.slug isEqualToString:slug]) {
                exists = YES;
            }
        }
        
        if (!exists) {
            [collectionSet removeObject:c];
        }
    }
    
    spot.collections = collectionSet;
}

- (void)fillWithResponse:(DSAPISpotResponse *)response
  inManagedObjectContext:(NSManagedObjectContext *)context {
    
    NSDateFormatter * dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:DRPSPT_DATE_FORMAT];
    NSDate * modified = [dateFormatter dateFromString:response.modified];

    self.url = response.url;
    self.slug = response.slug;
    self.text = response.name;
    self.latitude = response.latitude;
    self.longitude = response.longitude;
    self.modified = modified;
    self.created = [dateFormatter dateFromString:response.created];
    self.expiresAt = [dateFormatter dateFromString:response.expiresAt];
    
    self.address = response.address;
    self.isSynced = YES;
    self.isOwnedByMe = response.isOwnedByMe;
    self.subscribed = response.subscribed;
    self.collectionSlugs = response.collectionSlugs;

    self.linksURL = response.linksURL;
    self.links = [self checkLinks:response.links inManagedObjectContext:context];
    
    self.user = [User userWithAPIResponse:response.user inManagedObjectContext:context];
    
    // Photos
    [self compareThumbnail:response.thumbnailURL inManagedObjectContext:context completion:nil];
    [self comparePhoto:response.photoURL inManagedObjectContext:context completion:nil];
    
    [self checkDependencyForSpot:self inManagedObjectContext:context];
}

- (NSSet *)checkLinks:(NSArray *)newLinks inManagedObjectContext:(NSManagedObjectContext *)context {
    NSMutableArray * oldLinks = [self.links.allObjects mutableCopy];
    
    NSMutableArray * spotsToDelete = [NSMutableArray array];
    
    for (SpotLink * link in oldLinks) {
        BOOL exists = NO;
        for (NSString * url in newLinks) {
            if ([link.url isEqualToString:url]) {
                exists = YES;
            }
        }
        if (!exists) {
            [spotsToDelete addObject:link];
        }
    }

    for (SpotLink * link in spotsToDelete) {
        if ([oldLinks indexOfObject:link] != NSNotFound) {
            [oldLinks removeObject:link];
            [context deleteObject:link];
        }
    }
    
    [spotsToDelete removeAllObjects];
    spotsToDelete = nil;
    
    for (NSString * url in newLinks) {
        BOOL exists = NO;
        for (SpotLink * link in oldLinks) {
            if ([link.url isEqualToString:url]) {
                exists = YES;
            }
        }
        if (!exists) {
            [oldLinks addObject:[SpotLink spotLinkWithLink:url inManagedObjectContext:context]];
        }
    }
    
    return [NSSet setWithArray:oldLinks];
}

#pragma mark - Images

- (void)compareThumbnail:(NSString *)thumbnailURL inManagedObjectContext:(NSManagedObjectContext *)context completion:(void (^)(void))completion {
    
    if (![self.thumbnailURL isEqualToString:thumbnailURL]) {
        
        self.thumbnailURL = thumbnailURL;
        
        [self resumeLoadingThumbnailInManagedObjectContext:context completion:^{
            
            [[NSNotificationCenter defaultCenter] postNotificationName:kSpotPhotoLoadingFinished object:nil];
            
            if (completion) {
                completion();
            }
        }];
    }
}

- (void)comparePhoto:(NSString *)photoURL inManagedObjectContext:(NSManagedObjectContext *)context completion:(void (^)(void))completion {
    
    if (![self.photoURL isEqualToString:photoURL]) {
        
        self.photoURL = photoURL;
        
        [[DSGlobalData sharedData] deleteFile:self.photoFile];
        self.photoFile = nil;

        if (completion) {
            completion();
        }
    }
}

- (void)resumeLoadingThumbnailInManagedObjectContext:(NSManagedObjectContext *)context completion:(void (^)(void))completion {
    
    __block NSString * fileName = DSURLToFileName(self.thumbnailURL);
    __block NSString * filePath = [DataHelper pathForCacheFile:fileName];
    __block NSString * oldFilePath = [DataHelper pathForCacheFile:self.thumbnailFile];
    
    __weak typeof(self) weakSelf = self;
    
    [[DSGlobalData sharedData] loadFileFromURL:[NSURL URLWithString:self.thumbnailURL] filePathForStorage:filePath oldFilePath:oldFilePath completion:^(BOOL success) {
        
        [context performBlock:^{
            if (weakSelf && !weakSelf.isDeleted) {
                
                // Save new file name.
                if (success) {
                    weakSelf.thumbnailFile = fileName;
                } else {
                    weakSelf.thumbnailFile = nil;
                }
            }
            
            NSError * error;
            [context save:&error];
            if (error) {
                NSLog(@"ABC %@", error.localizedDescription);
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if (completion) {
                    completion();
                }
            });
        }];
    }];
}

- (void)resumeLoadingPhotoInManagedObjectContext:(NSManagedObjectContext *)context completion:(void (^)(void))completion {
    
    __block NSString * fileName = DSURLToFileName(self.photoURL);
    __block NSString * filePath = [DataHelper pathForCacheFile:fileName];
    __block NSString * oldFilePath = [DataHelper pathForCacheFile:self.photoFile];
    
    __weak typeof(self) weakSelf = self;

    [[DSGlobalData sharedData] loadFileFromURL:[NSURL URLWithString:self.photoURL] filePathForStorage:filePath oldFilePath:oldFilePath completion:^(BOOL success) {
        
        [context performBlock:^{
            if (weakSelf && !weakSelf.isDeleted) {
                
                // Save new file name.
                if (success) {
                    weakSelf.photoFile = fileName;
                } else {
                    weakSelf.photoFile = nil;
                }
            }
            
            NSError * error;
            [context save:&error];
            if (error) {
                NSLog(@"ABC %@", error.localizedDescription);
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if (completion) {
                    completion();
                }
            });
        }];
    }];
}

@end




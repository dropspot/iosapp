/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  DSAPISpotLinkResponse.h
//  Dropspot3
//
//  Created by Lukas Würzburger on 5/15/14.
//  Copyright (c) 2014 Dropspot GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DSAPISpotLinkResponse : NSObject

@property (nonatomic) int32_t identifier;
@property (nonatomic, retain) NSString * url;
@property (nonatomic, retain) NSString * link;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * photoURL;
@property (nonatomic, retain) NSString * iconURL;
@property (nonatomic, retain) NSString * modified;

@end

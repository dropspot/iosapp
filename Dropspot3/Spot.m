/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  Spot.m
//  Dropspot3
//
//  Created by Lukas Würzburger on 04.09.13.
//  Copyright (c) 2013 Dropspot. All rights reserved.
//

#import "Spot.h"
#import "Collection.h"


@implementation Spot

@dynamic latitude;
@dynamic longitude;
@dynamic radius;
@dynamic collectionSlugs;
@dynamic text;
@dynamic slug;
@dynamic collections;
@dynamic thumbnailFile;
@dynamic thumbnailURL;
@dynamic photoURL;
@dynamic photoFile;
@dynamic active;
@dynamic modified;
@dynamic created;
@dynamic url;
@dynamic subscribed;
@dynamic isOwnedByMe;
@dynamic history;
@dynamic links;
@dynamic linksURL;
@dynamic address;
@dynamic isSynced;
@dynamic isBeingSynced;
@dynamic isDeterminingAddress;
@dynamic user;
@dynamic expiresAt;

- (NSString *)description {
    return [NSString stringWithFormat:@"<Spot: %@, collections: %i>", self.slug, self.collections.count];
}

+ (Spot *)spotWithSlug:(NSString *)slug inManagedObjectContext:(NSManagedObjectContext *)context {
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Spot"];
    request.predicate = [NSPredicate predicateWithFormat:@"slug = %@", slug];
    
    NSError *error = nil;
    NSArray *matches = [context executeFetchRequest:request error:&error];
    
    if (matches && [matches count] == 1) {
        return matches[0];
    }
    
    return nil;
}

+ (Spot *)spotWithLatitude:(float)latitude longitude:(float)longitude inManagedObjectContext:(NSManagedObjectContext *)context {
    Spot * spot = [NSEntityDescription insertNewObjectForEntityForName:@"Spot" inManagedObjectContext:context];
    
    // Temporary identifier
    spot.slug = [NSString stringWithFormat:@"createdSpot{%.0f}", [NSDate date].timeIntervalSince1970];
    
    // Coordinates
    spot.latitude = latitude;
    spot.longitude = longitude;
    
    // Other
    spot.isSynced = NO;
    spot.isBeingSynced = NO;
    spot.isDeterminingAddress = YES;
    spot.isOwnedByMe = YES;
    spot.active = YES;
    
    return spot;
}

- (History *)addHistoryWithType:(double)type inManagedObjectContext:(NSManagedObjectContext *)context {
    NSLog(@"spot %@ addHistoryWithType %.0f", self, type);
    History * h = [NSEntityDescription insertNewObjectForEntityForName:@"History" inManagedObjectContext:context];
    
    h.date = [NSDate date];
    h.eventType = type;
    h.spot = self;
    
    NSLog(@"h %@", h);
    
    return h;
}

- (int)visits {
    int v = 0;
    for (History * h in self.history) {
        if (h.eventType == kSpotHistoryEventTypeVisited) {
            v++;
        }
    }
    return v;
}

- (BOOL)seen {
    for (History * h in self.history) {
        if (h.eventType == kSpotHistoryEventTypeSeen) {
            return YES;
            break;
        }
    }
    return NO;
}


@end

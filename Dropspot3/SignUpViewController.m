/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  SignUpViewController.m
//  Dropspot3
//
//  Created by Lukas Würzburger on 10/23/13.
//  Copyright (c) 2013 Dropspot. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

#import "SignUpViewController.h"

#import "DSGlobal.h"
#import "DSFacebook.h"
#import "DSDropspot.h"
#import "DSSignInTextField.h"
#import "DSUser.h"

#import "GAIHeader.h"

@interface SignUpViewController () <UITextFieldDelegate, UIAlertViewDelegate> {
    IBOutlet UIImageView * backgroundImageView;
    
    IBOutlet UIScrollView * scrollView;
    IBOutlet DSSignInTextField * emailTextField;
    IBOutlet DSSignInTextField * usernameTextField;
    IBOutlet DSSignInTextField * passwordTextField;
    IBOutlet UIButton * registerButton;
    IBOutlet UIButton * cancelButton;
    IBOutlet UIView * progressOverlayView;
    IBOutlet UILabel * progressOverlayLabel;
}

@end

@implementation SignUpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [scrollView setContentSize:CGSizeMake(320, registerButton.frame.origin.y + registerButton.frame.size.height + 20)];
    
    [self localizeAndLayout];
    
    [emailTextField becomeFirstResponder];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[DSGlobal sharedGlobals] analyticsView:kDSScreenNameNativeSignUpView];
}

- (BOOL)shouldAutorotate {
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (void)localizeAndLayout {
    emailTextField.placeholder = translate(@"SIGNUP_VIEW_TEXTFIELD_EMAIL_PLACEHOLDER");
    usernameTextField.placeholder = translate(@"SIGNUP_VIEW_TEXTFIELD_USERNAME_PLACEHOLDER");
    passwordTextField.placeholder = translate(@"SIGNUP_VIEW_TEXTFIELD_PASSWORD_PLACEHOLDER");
    [cancelButton setTitle:translate(@"SIGNUP_VIEW_BUTTON_BACK") forState:UIControlStateNormal];
    [registerButton setTitle:translate(@"SIGNUP_VIEW_BUTTON_REGISTER") forState:UIControlStateNormal];

    progressOverlayLabel.text = translate(@"SIGNUP_OVERLAY_LABEL");
}

- (void)setViewOffsetForKeyboard:(BOOL)keyboardVisible {
    float speed = 0.25;
    
    CGRect frame = scrollView.frame;
    if (keyboardVisible) {
        if (frame.size.height == self.view.frame.size.height) {
            frame.size.height = self.view.frame.size.height - 216;
            [UIView animateWithDuration:speed animations:^{
                scrollView.frame = frame;
            } completion:^(BOOL finished) {
                [scrollView flashScrollIndicators];
            }];
        }
    } else {
        if (frame.size.height < self.view.frame.size.height) {
            frame.size.height = self.view.frame.size.height;
            [UIView animateWithDuration:speed animations:^{
                scrollView.frame = frame;
            }];
        }
    }
}

#pragma mark - Formluar processing

- (void)checkEmailForUsername {
    if (emailTextField.text && [self validateEmail:emailTextField.text]) {
        if (usernameTextField.text == nil || (usernameTextField.text && [[usernameTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:@""])) {
            NSArray * parts = [emailTextField.text componentsSeparatedByString:@"@"];
            if (parts.count >= 1) {
                usernameTextField.text = parts[0];
            }
        }
    }
}

- (BOOL)validateEmail:(NSString *)candidate {
    NSString *emailRegex =
    @"(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}"
    @"~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"
    @"x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"
    @"z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5"
    @"]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"
    @"9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"
    @"-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES[c] %@", emailRegex];
    
    return [emailTest evaluateWithObject:candidate];
}

#pragma mark - IBActions

- (IBAction)registerButtonPressed:(id)sender {
    [self submitRegistration];
}

- (IBAction)cancelButtonPressed:(id)sender {
    if ([self.delegate respondsToSelector:@selector(signUpViewControllerDidCancel:)]) {
        [self.delegate signUpViewControllerDidCancel:self];
    }
}

#pragma mark - UITextField delegate

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    [self setViewOffsetForKeyboard:YES];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    if (textField == emailTextField) {
        [self checkEmailForUsername];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    
    if (textField == emailTextField) {
        [usernameTextField becomeFirstResponder];
    } else if (textField == usernameTextField) {
        [passwordTextField becomeFirstResponder];
    } else if (textField == passwordTextField) {
        [self setViewOffsetForKeyboard:NO];
        
        [self submitRegistration];
    }
    
    return YES;
}

#pragma mark - API

- (void)submitRegistration {
    if ([self validateFields]) {
        [self setProgressOverlayViewHidden:NO];
        [[DSDropspot session] registerNewUserWithEmail:emailTextField.text username:usernameTextField.text password:passwordTextField.text block:^(NSDictionary *result, NSError *error) {
            if (!error) {
                //[[DSGlobal sharedGlobals] analyticsEvent:kDSEventNameNativeSignUpSuccess category:kDSCategoryNameOnboarding];

                [[DSDropspot session] openSessionWithUsername:emailTextField.text password:passwordTextField.text block:^(DSUser *user, NSError *error) {
                    if (!error) {
                        //[[DSGlobal sharedGlobals] analyticsEvent:kDSEventNameNativeLoginSuccess category:kDSCategoryNameOnboarding];
                        
                        if ([self.delegate respondsToSelector:@selector(signUpViewController:didRegisterAndLoginWithUser:)]) {
                            [self.delegate signUpViewController:self didRegisterAndLoginWithUser:user];
                        }
                    } else {
//                        [[DSGlobal sharedGlobals] analyticsEvent:kDSEventNameNativeLoginFailure category:kDSCategoryNameOnboarding];
                        
                        if (error.code == kDSDropspotAPIErrorCodeNoResponse) {
                            UIAlertView * alert = [[UIAlertView alloc]
                                                   initWithTitle:translate(@"SIGNUP_VIEW_REGISTRATION_FAILED_ALERT_TITLE")
                                                   message:translate(@"SIGNUP_VIEW_REGISTRATION_FAILED_ALERT_NO_CONNECTION_MESSAGE")
                                                   delegate:self
                                                   cancelButtonTitle:translate(@"SIGNUP_VIEW_REGISTRATION_FAILED_ALERT_DISMISS_BUTTON")
                                                   otherButtonTitles:nil];
                            [alert show];
                        } else {
                            UIAlertView * alert = [[UIAlertView alloc]
                                                   initWithTitle:translate(@"SIGNIN_NATIVE_LOGIN_FAILED_ALERT_TITLE")
                                                   message:error.localizedDescription
                                                   delegate:nil
                                                   cancelButtonTitle:translate(@"SIGNUP_VIEW_REGISTRATION_FAILED_ALERT_DISMISS_BUTTON")
                                                   otherButtonTitles:nil];
                            [alert show];
                        }
                    }
                    [self setProgressOverlayViewHidden:YES];
                }];
            } else {
                //[[DSGlobal sharedGlobals] analyticsEvent:kDSEventNameNativeSignUpFailure category:kDSCategoryNameOnboarding];

                if (error.code == kDSDropspotAPIErrorCodeRegistrationFailed) {
                    UIAlertView * alert = [[UIAlertView alloc]
                                           initWithTitle:error.localizedDescription
                                           message:[self buildErrorMessage:result]
                                           delegate:nil
                                           cancelButtonTitle:translate(@"SIGNUP_VIEW_REGISTRATION_FAILED_ALERT_DISMISS_BUTTON")
                                           otherButtonTitles:nil];
                    [alert show];

                } else if (error.code == kDSDropspotAPIErrorCodeNoResponse) {
                    UIAlertView * alert = [[UIAlertView alloc]
                                           initWithTitle:translate(@"SIGNUP_VIEW_REGISTRATION_FAILED_ALERT_TITLE")
                                           message:translate(@"SIGNUP_VIEW_REGISTRATION_FAILED_ALERT_NO_CONNECTION_MESSAGE")
                                           delegate:self
                                           cancelButtonTitle:translate(@"SIGNUP_VIEW_REGISTRATION_FAILED_ALERT_DISMISS_BUTTON")
                                           otherButtonTitles:nil];
                    [alert show];
                } else {
                    UIAlertView * alert = [[UIAlertView alloc]
                                           initWithTitle:error.localizedDescription
                                           message:error.localizedDescription
                                           delegate:nil
                                           cancelButtonTitle:translate(@"SIGNUP_VIEW_REGISTRATION_FAILED_ALERT_DISMISS_BUTTON")
                                           otherButtonTitles:nil];
                    [alert show];
                }
                [self setProgressOverlayViewHidden:YES];
            }
        }];
    } else {
        //[[DSGlobal sharedGlobals] analyticsEvent:kDSEventNameNativeSignUpFailure category:kDSCategoryNameOnboarding];

        UIAlertView * alert = [[UIAlertView alloc]
                               initWithTitle:translate(@"SIGNUP_VIEW_REGISTRATION_FAILED_ALERT_TITLE")
                               message:translate(@"SIGNUP_VIEW_REGISTRATION_FAILED_ALERT_ENTER_FIELDS_MESSAGE")
                               delegate:nil
                               cancelButtonTitle:translate(@"SIGNUP_VIEW_REGISTRATION_FAILED_ALERT_DISMISS_BUTTON")
                               otherButtonTitles:nil];
        [alert show];
    }
}

- (void)setProgressOverlayViewHidden:(BOOL)hidden {
    if (hidden) {
        [UIView animateWithDuration:0.25f animations:^{
            progressOverlayView.alpha = 0.f;
        } completion:^(BOOL finished) {
            progressOverlayView.hidden = YES;
        }];
    } else {
        progressOverlayView.alpha = 0.f;
        progressOverlayView.hidden = NO;
        [UIView animateWithDuration:0.25f animations:^{
            progressOverlayView.alpha = 0.6f;
        }];
    }
}

- (BOOL)validateFields {
    if (!emailTextField.text || !usernameTextField.text || !passwordTextField.text) {
        return NO;
    }
    if ([[emailTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:@""] ||
        [[usernameTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:@""] ||
        [[passwordTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:@""]) {
        return NO;
    }
    if (passwordTextField.text.length < 6) {
        return NO;
    }
    return YES;
}

- (NSString *)buildErrorMessage:(NSDictionary *)message {
    NSMutableString * string = [NSMutableString string];
    for (NSString * key in message.allKeys) {
        [string appendFormat:@"%@: ", key];
        for (NSString * subMessage in message[key]) {
            [string appendFormat:@"- %@\n", subMessage];
        }
        [string appendString:@"\n"];
    }
    return string;
}

@end

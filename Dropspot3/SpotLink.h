/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  SpotLink.h
//  Dropspot3
//
//  Created by Lukas Würzburger on 4/17/14.
//  Copyright (c) 2014 Dropspot GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Spot;

@interface SpotLink : NSManagedObject

@property (nonatomic) int32_t identifier;
@property (nonatomic, retain) NSString * url;
@property (nonatomic, retain) NSString * link;
@property (nonatomic, retain) NSString * imageURL;
@property (nonatomic, retain) NSString * imageFile;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * iconURL;
@property (nonatomic, retain) NSString * iconFile;
@property (nonatomic, retain) NSString * shortHost;
@property (nonatomic, retain) NSDate * modified;
@property (nonatomic) BOOL isComplete;

@property (nonatomic, retain) Spot * spot;

+ (SpotLink *)spotLinkWithLink:(NSString *)linkString
        inManagedObjectContext:(NSManagedObjectContext *)context;

@end

/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  Collection+Dropspot.m
//  Dropspot3
//
//  Created by Lukas Würzburger on 04.09.13.
//  Copyright (c) 2013 Dropspot. All rights reserved.
//

#import "Collection+Dropspot.h"
#import "Spot+Dropspot.h"
#import "User+Dropspot.h"

#import "DSGlobalData.h"
#import "DataHelper.h"

@implementation Collection (Dropspot)

+ (Collection *)collectionWithAPIResponse:(DSAPICollectionResponse *)response
                   inManagedObjectContext:(NSManagedObjectContext *)context {
    Collection * collection = nil;
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Collection"];
    request.predicate = [NSPredicate predicateWithFormat:@"slug = %@", response.slug];
    
    NSError *error = nil;
    NSArray *matches = [context executeFetchRequest:request error:&error];
    
    if (!matches || ([matches count] > 1)) {
        NSLog(@"matches %@", matches);
    } else {
        NSDateFormatter * dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:DRPSPT_DATE_FORMAT];
        NSDate * modified = [dateFormatter dateFromString:response.modified];
        
        if (![matches count]) {
            collection = [NSEntityDescription insertNewObjectForEntityForName:@"Collection" inManagedObjectContext:context];
            collection.active = YES;
            collection.subscribed = NO;
        } else {
            collection = [matches lastObject];
            
            if (collection.modified && [collection.modified compare:modified] == NSOrderedSame) {
                return collection;
            }
        }
        
        [collection fillWithResponse:response inManagedObjectContext:context];
    }
    
    return collection;
}

- (void)updateWithAPIResponse:(DSAPICollectionResponse *)response
       inManagedObjectContext:(NSManagedObjectContext *)context {
    NSDateFormatter * dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:DRPSPT_DATE_FORMAT];
    NSDate * modified = [dateFormatter dateFromString:response.modified];
    
    if (self.modified && [self.modified compare:modified] == NSOrderedSame) {
        return;
    }

    [self fillWithResponse:response inManagedObjectContext:context];
}

- (void)fillWithResponse:(DSAPICollectionResponse *)response inManagedObjectContext:(NSManagedObjectContext *)context {
    NSDateFormatter * dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:DRPSPT_DATE_FORMAT];
    NSDate * modified = [dateFormatter dateFromString:response.modified];
    
    self.url = response.url;
    self.spotsURL = response.spotsURL;
    self.slug = response.slug;
    self.title = response.title;
    self.text = response.text;
    self.modified = modified;
    if (response.modifiedSpots == nil) {
        self.modifiedSpots = [NSDate date];
    } else {
        self.modifiedSpots = [dateFormatter dateFromString:response.modifiedSpots];
    }
    self.created = [dateFormatter dateFromString:response.created];
    self.isSynced = YES;
    self.isBeingSynced = NO;
    self.isOwnedByMe = response.isOwnedByMe;
    self.subscribed = response.subscribed;
    
    self.user = [User userWithAPIResponse:response.user inManagedObjectContext:context];
    
    [self compareThumbnail:response.thumbnailURL inManagedObjectContext:context completion:nil];
    [self comparePhoto:response.photoURL inManagedObjectContext:context completion:nil];
}

#pragma mark - Images

- (void)compareThumbnail:(NSString *)thumbnailURL inManagedObjectContext:(NSManagedObjectContext *)context completion:(void (^)(void))completion {
    
    if (![self.thumbnailURL isEqualToString:thumbnailURL]) {
        
        self.thumbnailURL = thumbnailURL;
        
        [self resumeLoadingThumbnailInManagedObjectContext:context completion:^{
            
            [[NSNotificationCenter defaultCenter] postNotificationName:kCollectionPhotoLoadingFinished object:nil];
            
            if (completion) {
                completion();
            }
        }];
    }
}

- (void)comparePhoto:(NSString *)photoURL inManagedObjectContext:(NSManagedObjectContext *)context completion:(void (^)(void))completion {
    
    if (![self.photoURL isEqualToString:photoURL]) {
        
        self.photoURL = photoURL;
        
        [self resumeLoadingPhotoInManagedObjectContext:context completion:^{
            
            [[NSNotificationCenter defaultCenter] postNotificationName:kCollectionPhotoLoadingFinished object:nil];
            
            if (completion) {
                completion();
            }
        }];
    }
}

- (void)resumeLoadingThumbnailInManagedObjectContext:(NSManagedObjectContext *)context completion:(void (^)(void))completion {
    
    __block NSString * fileName = DSURLToFileName(self.thumbnailURL);
    __block NSString * filePath = [DataHelper pathForCacheFile:fileName];
    __block NSString * oldFilePath = [DataHelper pathForCacheFile:self.thumbnailFile];
    
    __weak typeof(self) weakSelf = self;
    
    [[DSGlobalData sharedData] loadFileFromURL:[NSURL URLWithString:self.thumbnailURL] filePathForStorage:filePath oldFilePath:oldFilePath completion:^(BOOL success) {
        
        [context performBlock:^{
            
            if (weakSelf && !weakSelf.isDeleted) {
                
                // Save new file name.
                if (success) {
                    weakSelf.thumbnailFile = fileName;
                } else {
                    weakSelf.thumbnailFile = nil;
                }
            }
            
            [context save:nil];

            dispatch_async(dispatch_get_main_queue(), ^{

                if (completion) {
                    completion();
                }
            });
        }];
    }];
}

- (void)resumeLoadingPhotoInManagedObjectContext:(NSManagedObjectContext *)context completion:(void (^)(void))completion {
    
    NSLog(@"resume with url %@", self.photoURL);
    
    __block NSString * fileName = DSURLToFileName(self.photoURL);
    __block NSString * filePath = [DataHelper pathForCacheFile:fileName];
    __block NSString * oldFilePath = [DataHelper pathForCacheFile:self.photoFile];
    
    __weak typeof(self) weakSelf = self;
    
    [[DSGlobalData sharedData] loadFileFromURL:[NSURL URLWithString:self.photoURL] filePathForStorage:filePath oldFilePath:oldFilePath completion:^(BOOL success) {
        [context performBlock:^{
            
            if (weakSelf && !weakSelf.isDeleted) {
                
                // Save new file name.
                if (success) {
                    weakSelf.photoFile = fileName;
                } else {
                    weakSelf.photoFile = nil;
                }
            }

            [context save:nil];
            
            NSLog(@"photoFile %i %@", success, weakSelf.photoFile);

            dispatch_async(dispatch_get_main_queue(), ^{
                
                if (completion) {
                    completion();
                }
            });
        }];
    }];
}

@end

/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  StartViewController.h
//  Dropspot3
//
//  Created by Lukas Würzburger on 09.05.14.
//  Copyright (c) 2014 Dropspot. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DSUser.h"

@class StartViewController;

@protocol StartViewControllerDelegate <NSObject>

- (void)startViewController:(StartViewController *)startViewController didSignInWithUser:(DSUser *)currentUser;
- (void)startViewControllerDidCancel:(StartViewController *)startViewController;

@end

@interface StartViewController : UIViewController

@property (nonatomic, assign) id <StartViewControllerDelegate> delegate;

@end

/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  DSAPIUserResponse.h
//  Dropspot3
//
//  Created by Lukas Würzburger on 4/9/14.
//  Copyright (c) 2014 Dropspot GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DSAPIUserResponse : NSObject

@property (nonatomic, retain) NSString * username;
@property (nonatomic, retain) NSString * displayName;
@property (nonatomic, retain) NSString * avatarImageURL;

@end

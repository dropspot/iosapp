/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  DSAnnotationView.h
//  dropspotApp
//
//  Created by Fritz on 22.05.13.
//  Copyright (c) 2013 dropspot GmbH. All rights reserved.
//

#import <MapKit/MapKit.h>

#define kDSMapAnnotationViewDragOffset CGPointMake(1.0f, -47.0f)

#define kDSMapAnnotationViewAlphaNotSelectable  0.2f
#define kDSMapAnnotationViewAlphaNormal         0.6f
#define kDSMapAnnotationViewAlphaSelected       1.0f

@interface DSMapAnnotationView : MKAnnotationView

@property (nonatomic, assign) BOOL calledOut;
@property (nonatomic, assign) BOOL isDragging;

@end

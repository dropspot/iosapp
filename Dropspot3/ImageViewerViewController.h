/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  ImageViewerViewController.h
//  Dropspot3
//
//  Created by Lukas Würzburger on 1/10/14.
//  Copyright (c) 2014 Dropspot. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageViewerViewController : UIViewController

@property (nonatomic, strong) UIImage * image;

@end

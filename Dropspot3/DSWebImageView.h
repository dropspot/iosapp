/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  DSWebImageView.h
//  Dropspot3
//
//  Created by Lukas Würzburger on 5/13/14.
//  Copyright (c) 2014 Dropspot GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DSWebImageView : UIImageView

- (void)setImageWithURL:(NSURL *)url placeholder:(UIImage *)placeholder;
- (void)setImageWithURL:(NSURL *)url placeholder:(UIImage *)placeholder completion:(void(^)(void))completion;

@end

/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  CollectionTopCell.h
//  Dropspot3
//
//  Created by Lukas Würzburger on 3/5/14.
//  Copyright (c) 2014 Dropspot. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CollectionBottomCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIButton * unsubscribeButton;
@property (nonatomic, weak) IBOutlet UIButton * deleteButton;
@property (nonatomic, weak) IBOutlet UIButton * settingsButton;

- (void)setLayout;

+ (float)heightForCellUnsubscribeButton:(BOOL)unsubscribeButton settingsButton:(BOOL)settingsButton;

@end

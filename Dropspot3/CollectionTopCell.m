/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  CollectionTopCell.m
//  Dropspot3
//
//  Created by Lukas Würzburger on 3/5/14.
//  Copyright (c) 2014 Dropspot. All rights reserved.
//

#import "CollectionTopCell.h"

#import "UILabel+AutoHeight.h"
#import "DSGlobal.h"

@implementation CollectionTopCell

- (void)setLayout {
    float positionY = 20;
    CGRect frame;
    
    if (![[self.descriptionLabel.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]) {
        frame = [self.descriptionLabel autoAdjustHeight];
        positionY = frame.origin.y + frame.size.height + 20;
    }
    
    if (!self.activateLabel.hidden) {
        self.activateLabel.text = translate(@"DETAIL_VIEW_ACTIVATE_REMINDERS_LABEL");
        
        frame = self.activateLabel.frame;
        frame.origin.y = positionY;
        self.activateLabel.frame = frame;
        
        frame = self.activateSwitch.frame;
        frame.origin.y = positionY;
        self.activateSwitch.frame = frame;
        
        positionY = frame.origin.y + frame.size.height + 20;
    }
    
    if (!self.addSpotButton.hidden) {
        if (kDSForceAddSpotButtonTitle) {
            [self.addSpotButton setTitle:kDSForceAddSpotButtonTitle forState:UIControlStateNormal];
        } else {
            [self.addSpotButton setTitle:translate(@"DETAIL_VIEW_CONTROL_BUTTON_TITLE_CREATE_SPOT") forState:UIControlStateNormal];
        }
        [self.addSpotButton setTitleColor:kDSColorCoral forState:UIControlStateNormal];
        self.addSpotButton.layer.borderWidth = 1.f;
        self.addSpotButton.layer.borderColor = kDSColorCoral.CGColor;
        self.addSpotButton.layer.cornerRadius = 6.f;
        
        
        frame = self.addSpotButton.frame;
        frame.origin.y = positionY;
        self.addSpotButton.frame = frame;
        
        positionY = frame.origin.y + frame.size.height + 20;
    }
    
    if (!self.subscribeButton.hidden) {
        self.subscribeButton.backgroundColor = [UIColor blueColor];
        [self.subscribeButton setTitle:translate(@"DETAIL_VIEW_CONTROL_BUTTON_TITLE_SUBSCRIBE") forState:UIControlStateNormal];

        frame = self.subscribeButton.frame;
        frame.origin.y = positionY;
        self.subscribeButton.frame = frame;
        
        positionY = frame.origin.y + frame.size.height + 20;
    }
    
}

+ (float)heightForCellWithDescription:(NSString *)description subscribeButton:(BOOL)subscribeButton addSpotButton:(BOOL)addSpotButton activateSwitch:(BOOL)activateSwitch {
    float height = 0;
    
    if (![[description stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]) {
        NSDictionary * textStringAttributes = @{ NSFontAttributeName : [UIFont fontWithName:@"HelveticaNeue-Light" size:15.f] };
        
        CGRect rect = [description boundingRectWithSize:CGSizeMake(280, CGFLOAT_MAX)
                                                options:(NSStringDrawingUsesFontLeading | NSStringDrawingUsesLineFragmentOrigin)
                                             attributes:textStringAttributes
                                                context:nil];
        
        height += 20 + round(rect.size.height);
    }
    
    if (subscribeButton) {
        height += 64;
    }
    
    if (addSpotButton) {
        height += 64;
    }
    
    if (activateSwitch) {
        height += 64;
    }
    
    height += 10;
    
    return height;
}

@end

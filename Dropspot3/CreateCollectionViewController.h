/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  CreateCollectionViewController.h
//  Dropspot3
//
//  Created by Lukas Würzburger on 10/27/13.
//  Copyright (c) 2013 Dropspot. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Collection;

@interface CreateCollectionViewController : UIViewController

@property (nonatomic, weak) Collection * collection;

@end

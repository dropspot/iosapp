/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  DataHandler.m
//  dropspotApp
//
//  Created by Fritz on 30.10.12.
//  Copyright (c) 2012 dropspot GmbH. All rights reserved.
//

#import "DataHelper.h"

#import <QuartzCore/QuartzCore.h>

#import "UIImage+StackBlur.h"

@implementation DataHelper

+ (void)writeData:(NSData *)data toFile:(NSString *)fileName ifNotExists:(BOOL)shouldNotExists completion:(void (^)(BOOL success))completion {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        BOOL success = NO;
        NSString * fullPathToFile = [self pathForCacheFile:fileName];
        NSError *error = nil;
        if (shouldNotExists) {
            if (![[NSFileManager defaultManager] fileExistsAtPath:fullPathToFile]) {
                [data writeToFile:fullPathToFile options:NSDataWritingAtomic error:&error];
            }
        } else {
            if ([[NSFileManager defaultManager] fileExistsAtPath:fullPathToFile]) {
                [[NSFileManager defaultManager] removeItemAtPath:fullPathToFile error:nil];
            }
            [data writeToFile:fullPathToFile options:NSDataWritingAtomic error:&error];
        }
        if (error) {
            NSLog(@"error %@", error.description);
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            if (completion) {
                completion(success);
            }
        });
    });
}

+ (BOOL)writeData:(NSData *)data toFile:(NSString *)fileName ifNotExists:(BOOL)shouldNotExists {
    BOOL success = NO;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSString * fullPathToFile = [self pathForCacheFile:fileName];
        NSError *error = nil;
        if (shouldNotExists) {
            if (![[NSFileManager defaultManager] fileExistsAtPath:fullPathToFile]) {
                [data writeToFile:fullPathToFile options:NSDataWritingAtomic error:&error];
            }
        } else {
            if ([[NSFileManager defaultManager] fileExistsAtPath:fullPathToFile]) {
                [[NSFileManager defaultManager] removeItemAtPath:fullPathToFile error:nil];
            }
            [data writeToFile:fullPathToFile options:NSDataWritingAtomic error:&error];
        }
        if (error) {
            NSLog(@"error %@", error.description);
        }
    });
    return success;
}

+ (BOOL)writeData:(NSData *)data toDocumentFile:(NSString *)fileName ifNotExists:(BOOL)shouldNotExists {
    BOOL success = NO;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSString * fullPathToFile = [self pathForDocumentFile:fileName];
        NSError *error = nil;
        if (shouldNotExists) {
            if (![[NSFileManager defaultManager] fileExistsAtPath:fullPathToFile]) {
                [data writeToFile:fullPathToFile options:NSDataWritingAtomic error:&error];
            }
        } else {
            if ([[NSFileManager defaultManager] fileExistsAtPath:fullPathToFile]) {
                [[NSFileManager defaultManager] removeItemAtPath:fullPathToFile error:nil];
            }
            [data writeToFile:fullPathToFile options:NSDataWritingAtomic error:&error];
        }
        if (error) {
            NSLog(@"error %@", error.description);
        }
    });
    return success;
}

+ (BOOL)writeData:(NSData *)data toTmpFile:(NSString *)fileName ifNotExists:(BOOL)shouldNotExists {
    BOOL success = NO;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
    NSString * fullPathToFile = [self pathForCacheFile:fileName];
    NSError *error = nil;
    if (shouldNotExists) {
        if (![[NSFileManager defaultManager] fileExistsAtPath:fullPathToFile]) {
            [data writeToFile:fullPathToFile options:NSDataWritingAtomic error:&error];
        }
    } else {
        [data writeToFile:fullPathToFile options:NSDataWritingAtomic error:&error];
    }
    });
    return success;
}

+ (BOOL)deleteFile:(NSString *)fileName {
    BOOL success = NO;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSString * fullFileName = [self pathForCacheFile:fileName];
        if ([[NSFileManager defaultManager] fileExistsAtPath:[self pathForCacheFile:fileName]]) {
            NSError * errors;
            [[NSFileManager defaultManager] removeItemAtPath:fullFileName error:&errors];
        }
    });
    return success;
}

#pragma mark - JSON Processing

+ (NSDictionary *)dictionaryFromJSONData:(NSData *)jsonData {
    NSError *error = nil;
    if (jsonData) {
        return [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:&error];
    } else {
        return nil;
    }
}

+ (NSArray *)arrayFromJSONData:(NSData *)jsonData {
    NSError *error = nil;
    if (jsonData) {
        return [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:&error];
    } else {
        return nil;
    }
}

+ (NSArray *)arrayFromJSONString:(NSString *)jsonString {
    return [self arrayFromJSONData:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
}

#pragma mark - Paths

+ (NSString *)documentsDirectory
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    return basePath;
}

+ (NSString *)cacheDirectory
{
    NSArray* cachePathArray = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString* cachePath = [cachePathArray lastObject];
    
    return cachePath;
}

+ (NSString *)pathForDocumentFile:(NSString *)fileName {
    return [[self documentsDirectory] stringByAppendingPathComponent:fileName];
}

+ (NSString *)pathForBundleFile:(NSString *)fileName {
    return [[[NSBundle mainBundle] bundlePath] stringByAppendingPathComponent:fileName];
}

+ (NSString *)pathForCacheFile:(NSString *)fileName {
    return [[self cacheDirectory] stringByAppendingPathComponent:fileName];
}

#pragma mark - URL handling

+ (BOOL)validateUrl:(NSString *)candidate {
    NSString *urlRegEx = @"(http|https)://((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+";
    NSPredicate *urlTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", urlRegEx];
    return [urlTest evaluateWithObject:candidate];
}

#pragma mark - UIImage

+ (void)cropImageFromData:(NSData *)imageData toSize:(CGSize)size blurIfNecessary:(BOOL)blur resultData:(void (^)(NSData *data))result {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        UIImage * image = [UIImage imageWithData:imageData];
        CGSize imageSize = image.size;
        
        UIImage * resultImage;

        if (size.width != 0.0 && size.height != 0.0) {
            CGSize sizeToFit = CGSizeMake(size.width * 2, size.height * 2);
            
            if (image.size.width != sizeToFit.width) {
                float ratio = image.size.height / image.size.width;
                imageSize = CGSizeMake(sizeToFit.width, sizeToFit.width * ratio);
                if (imageSize.height < sizeToFit.height) {
                    ratio = image.size.width / image.size.height;
                    imageSize = CGSizeMake(sizeToFit.height * ratio, sizeToFit.height);
                }
            } else {
                if (image.size.height < sizeToFit.height) {
                    float ratio = image.size.width / image.size.height;
                    imageSize = CGSizeMake(sizeToFit.height * ratio, sizeToFit.height);
                }
            }
        
            UIGraphicsBeginImageContext(imageSize);
            [image drawInRect:CGRectMake(0,0,imageSize.width,imageSize.height)];
            UIImage * newImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            
            CGImageRef imageRef = CGImageCreateWithImageInRect(newImage.CGImage, CGRectMake((imageSize.width / 2 - sizeToFit.width / 2), (imageSize.height / 2 - sizeToFit.height / 2), imageSize.width, imageSize.height));
            resultImage = [UIImage imageWithCGImage:imageRef scale:1.0 orientation:UIImageOrientationUp];
            CGImageRelease(imageRef);
        } else {
            resultImage = image;
        }
        
        if (blur) {
            if (image.size.width < 200 || image.size.height < 300) {
                UIImage * blurredImage = [resultImage stackBlur:15];
                if (blurredImage) {
                    resultImage = blurredImage;
                }
            }
        }
        
        NSString * type = [self contentTypeForImageData:imageData];
        
        NSData * returnImage;
        
        if ([type isEqualToString:@"image/jpeg"]) {
            returnImage = UIImageJPEGRepresentation(resultImage, 0.85);
        } else {
            returnImage = UIImagePNGRepresentation(resultImage);
        }

        if (result) {
            dispatch_async(dispatch_get_main_queue(), ^{
                result(returnImage);
            });
        }
    });
}

+ (UIImage *)createSnapShotOfView:(UIView *)captureView {
    CGRect rect = [captureView bounds];
    UIGraphicsBeginImageContextWithOptions(rect.size, NO, 0.0f);
    CGContextRef context = UIGraphicsGetCurrentContext();
    [captureView.layer renderInContext:context];
    UIImage *capturedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return capturedImage;
}

+ (NSString *)contentTypeForImageData:(NSData *)data {
    uint8_t c;
    [data getBytes:&c length:1];
    
    switch (c) {
        case 0xFF:
            return @"image/jpeg";
        case 0x89:
            return @"image/png";
        case 0x47:
            return @"image/gif";
        case 0x49:
        case 0x4D:
            return @"image/tiff";
    }
    return nil;
}

#pragma mark - Preloaded Contents

+ (BOOL)copyPreloadedFile:(NSString *)fileName {
    if ([[NSFileManager defaultManager] fileExistsAtPath:[self pathForBundleFile:fileName]]) {
        [[NSFileManager defaultManager] copyItemAtPath:[self pathForBundleFile:fileName] toPath:[DataHelper pathForCacheFile:fileName] error:nil];
        return YES;
    }
    return NO;
}

#pragma mark - Accessing Images

+ (UIImage *)imageWithFilePath:(NSString *)filePath {
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
        
        NSError * error = nil;
        NSData * data = [NSData dataWithContentsOfFile:filePath options:NSDataReadingMapped error:&error];
        if (error) {
            
            return nil;
        } else {
            
            return [UIImage imageWithData:data];
        }
    }
    return nil;
}

+ (void)imageWithFilePath:(NSString *)filePath block:(void (^)(UIImage * image))block {
    
    dispatch_queue_t queue = dispatch_queue_create("com.dropspot.dropspot:ImageReader", DISPATCH_QUEUE_SERIAL);
    dispatch_async(queue, ^{
        
        BOOL isDir;
        BOOL exists = [[NSFileManager defaultManager] fileExistsAtPath:filePath isDirectory:&isDir];
        if (exists && !isDir) {
            
            NSError * error = nil;
            NSData * data = [NSData dataWithContentsOfFile:filePath options:NSDataReadingMapped error:&error];
            if (error) {
                
                NSLog(@"NSData error: %@", error.localizedDescription);
                NSLog(@"FilePath: %@", filePath);
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (block) {
                        block(nil);
                    }
                });
            } else {
                
                UIImage * image = [UIImage imageWithData:data];
                if (!image) {
                    
                    NSLog(@"UIImage error: Couldn't read NSData");
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if (block) {
                            block(nil);
                        }
                    });
                } else {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if (block) {
                            block(image);
                        }
                    });
                }
            }
        } else {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                if (block) {
                    block(nil);
                }
            });
        }
    });
}



@end

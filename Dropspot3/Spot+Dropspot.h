/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  Spot+Dropspot.h
//  Dropspot3
//
//  Created by Lukas Würzburger on 16.09.13.
//  Copyright (c) 2013 Dropspot. All rights reserved.
//

#import "Spot.h"
#import "DSAPISpotResponse.h"

@interface Spot (Dropspot)

+ (Spot *)spotWithAPIResponse:(DSAPISpotResponse *)response
       inManagedObjectContext:(NSManagedObjectContext *)context;

- (void)updateWithAPIResponse:(DSAPISpotResponse *)response
       inManagedObjectContext:(NSManagedObjectContext *)context;

- (void)fillWithResponse:(DSAPISpotResponse *)response
  inManagedObjectContext:(NSManagedObjectContext *)context;

// Photos
- (void)resumeLoadingThumbnailInManagedObjectContext:(NSManagedObjectContext *)context completion:(void (^)(void))completion;
- (void)resumeLoadingPhotoInManagedObjectContext:(NSManagedObjectContext *)context completion:(void (^)(void))completion;

@end

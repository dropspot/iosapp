/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  DSDownload.h
//  dropspotApp
//
//  Created by Lukas Würzburger on 14.05.13.
//  Copyright (c) 2013 dropspot GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^DataBlock)(NSData * data);
typedef void(^ErrorBlock)(NSError * error);
typedef void(^FloatBlock)(float progress);

@interface DSDownload : NSObject <NSURLConnectionDelegate>

/**
 * The specified url as a string.
 */
@property (nonatomic, strong) NSString * url;

/**
 * Returns a instance of DSDownload class
 */
+ (DSDownload *)download;

/**
 * Initializes and runs a get request. The progress block is called each time data is received with a value between 0.0 and 1.0. If the size couldn't determined the value will be NAN.
 */
- (void)runWithURL:(NSString *)urlString success:(DataBlock)successBlock fail:(ErrorBlock)error progress:(FloatBlock)progress;

/**
 * Initializes and runs a get request.
 */
- (void)runWithURL:(NSString *)urlString success:(DataBlock)successBlock fail:(ErrorBlock)error;

@end

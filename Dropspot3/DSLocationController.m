/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  DSLocationManager.m
//  Dropspot3
//
//  Created by Lukas Würzburger on 18.09.13.
//  Copyright (c) 2013 Dropspot. All rights reserved.
//

#import "DSLocationController.h"
#import "Spot+Location.h"
#import "DSGlobalData.h"
#import "DSGlobal.h"
#import "DSDropspot.h"

@interface DSLocationController () {
    BOOL applicationInBackground;
    
    NSDate * _lastNotificationDate;
}

@end

@implementation DSLocationController

+ (DSLocationController *)controller {
    static DSLocationController * instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[DSLocationController alloc] init];
    });
    return instance;
}

+ (NSString *)distanceStringFromDistance:(float)distance {
    NSString * distanceString = @"";
    
    BOOL useMeters = NO;
    
    if ([DSGlobal sharedGlobals].measurement == DSMeasurementAuto) {
        id usesMetricSystem = [[NSLocale currentLocale] objectForKey:NSLocaleUsesMetricSystem];
        useMeters = ([usesMetricSystem boolValue]);
    } else {
        useMeters = ([DSGlobal sharedGlobals].measurement == DSMeasurementMeters);
    }
    
    if (useMeters) {
        if (distance >= 1000) {
            distance /= 1000;
            if (distance > 10) {
                distanceString = [NSString stringWithFormat:@"%0.0f km", distance];
            } else {
                distanceString = [NSString stringWithFormat:@"%0.2f km", distance];
            }
        } else {
            distanceString = [NSString stringWithFormat:@"%0.0f m", distance];
        }
    } else {
        double feet = distance * 3.2808399;
        if (feet >= 5280) {
            double miles = distance * 0.000621371192;
            if (miles > 10) {
                distanceString = [NSString stringWithFormat:@"%0.0f mi", miles];
            } else {
                distanceString = [NSString stringWithFormat:@"%0.2f mi", miles];
            }
        } else {
            distanceString = [NSString stringWithFormat:@"%0.0f ft", feet];
        }
    }
    
    return distanceString;
}

+ (void)distancePropertiesFromDistance:(float)distance properties:(void (^)(double value, NSString * measurement))properties {
    BOOL useMeters = NO;
    
    if ([DSGlobal sharedGlobals].measurement == DSMeasurementAuto) {
        id usesMetricSystem = [[NSLocale currentLocale] objectForKey:NSLocaleUsesMetricSystem];
        useMeters = ([usesMetricSystem boolValue]);
    } else {
        useMeters = ([DSGlobal sharedGlobals].measurement == DSMeasurementMeters);
    }
    
    double value = 0.0;
    NSString * measurement = nil;
    
    if (useMeters) {
        if (distance >= 1000) {
            value /= 1000;
            measurement = @"km";
        } else {
            value = distance;
            measurement = @"m";
        }
    } else {
        double feet = distance * 3.2808399;
        if (feet >= 5280) {
            value = distance * 0.000621371192;
            measurement = @"mi";
        } else {
            value = feet;
            measurement = @"ft";
        }
    }
    
    if (properties) {
        properties(value, measurement);
    }
}

+ (NSString *)lowestMeasurement {
    BOOL useMeters = NO;
    
    if ([DSGlobal sharedGlobals].measurement == DSMeasurementAuto) {
        id usesMetricSystem = [[NSLocale currentLocale] objectForKey:NSLocaleUsesMetricSystem];
        useMeters = ([usesMetricSystem boolValue]);
    } else {
        useMeters = ([DSGlobal sharedGlobals].measurement == DSMeasurementMeters);
    }
    
    if (useMeters) {
        return @"m";
    } else {
        return @"ft";
    }
}

#pragma mark - Controller

- (id)init {
    self = [super init];
    if (self) {
        _lastNotificationDate = [NSDate dateWithTimeIntervalSince1970:0];
        [[DSGlobalData sharedData].childContext performBlock:^{
            NSFetchRequest * fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"History"];
            fetchRequest.predicate = [NSPredicate predicateWithFormat:@"eventType = %i", kSpotHistoryEventTypeNotification];
            fetchRequest.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"date" ascending:NO]];
            fetchRequest.fetchLimit = 1;
            NSArray * result = [[DSGlobalData sharedData].childContext executeFetchRequest:fetchRequest error:nil];
            if (result.count == 1) {
                History * h = result[0];
                NSLog(@"history date %@", h.date);
                _lastNotificationDate = h.date;
            }
        }];
    }
    return self;
}

- (void)setDelegate:(id<DSLocationControllerDelegate>)delegate {
    _delegate = delegate;
    
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorized) {
        [self.locationManager startUpdatingLocation];
    }
}

- (CLLocationManager *)locationManager {
    if (!_locationManager) {
        if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorized) {
            _locationManager = [[CLLocationManager alloc] init];
            _locationManager.delegate = self;
            _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
            _locationManager.distanceFilter = kCLDistanceFilterNone;
        }
    }
    
    return _locationManager;
}

- (void)handleApplicationState:(UIApplicationState)state {
    applicationInBackground = (state == UIApplicationStateBackground);
    
    if (applicationInBackground) {
        if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorized) {
            [[DSLog sharedLog] logWithUser:@"lukas" event:@"startMonitoringSignificantLocationChanges"];

            [self.locationManager stopUpdatingLocation];
            [self.locationManager startMonitoringSignificantLocationChanges];
            [self refreshTrackedSpots];
        }
    } else {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorized) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.locationManager stopMonitoringSignificantLocationChanges];
                    [self.locationManager startUpdatingLocation];
                });
            }
        });
    }
}

#pragma mark - CLLocationManager delegate

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    [[NSNotificationCenter defaultCenter] postNotificationName:kDSLocationControllerAuthorizationStatusDidChangeNotificationName object:nil];
}

- (void)locationManager:(CLLocationManager *)manager didEnterRegion:(CLRegion *)region {
    if (kDSNotificationTimeFrom && kDSNotificationTimeTill) {
        
        NSDate * currentDate = [NSDate date];
        
        NSDateFormatter * formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd'T'"];
        
        NSString * fromDateString = [NSString stringWithFormat:@"%@15:00:00.000Z", [formatter stringFromDate:currentDate]];
        NSString * tillDateString = [NSString stringWithFormat:@"%@20:00:00.000Z", [formatter stringFromDate:currentDate]];
        
        [formatter setDateFormat:DRPSPT_DATE_FORMAT];
        
        NSDate * fromDate = [formatter dateFromString:fromDateString];
        NSDate * tillDate = [formatter dateFromString:tillDateString];
        
        if ([currentDate compare:fromDate] != NSOrderedDescending ||
            [currentDate compare:tillDate] != NSOrderedAscending) {
            return;
        }
    }
    
    [[DSGlobalData sharedData].childContext performBlock:^{
        Spot * spot = [Spot spotWithSlug:region.identifier inManagedObjectContext:[DSGlobalData sharedData].childContext];
        
        if (spot.expiresAt) {
            if ([spot.expiresAt compare:[NSDate date]] == NSOrderedAscending) {
                return;
            }
        }
        
        if (kDSNotificationEveryXDay) {
            NSNumber * notificationEveryXDay = (NSNumber *)kDSNotificationEveryXDay;
            
            NSArray * historyArray = spot.history.allObjects;
            if (historyArray.count > 0) {
                NSSortDescriptor * descriptor = [NSSortDescriptor sortDescriptorWithKey:@"date" ascending:NO];
                historyArray = [historyArray sortedArrayUsingDescriptors:@[descriptor]];
                
                History * h = historyArray[0];
                
                if ([NSDate date].timeIntervalSince1970 - h.date.timeIntervalSince1970 < notificationEveryXDay.integerValue * 60 * 60 * 24) {
                    return;
                }
            }
        } else {
            
            // Check same spot notifications to be longer than a 5 days ago.
            
            for (History * h in spot.history) {
                if (h.eventType == kSpotHistoryEventTypeNotification) {
                    if ([NSDate date].timeIntervalSince1970 - h.date.timeIntervalSince1970 < 432000) { // 432000 = 5 days
                        NSLog(@"Same spot was triggered within last 5 days");
                        return;
                    }
                }
            }
            
            if ([NSDate date].timeIntervalSince1970 - _lastNotificationDate.timeIntervalSince1970 < 300) {
                NSLog(@"last notification within last 5 minutes");
                return;
            }
            
            NSFetchRequest * fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"History"];
            fetchRequest.predicate = [NSPredicate predicateWithFormat:@"eventType = %i", kSpotHistoryEventTypeNotification];
            fetchRequest.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"date" ascending:NO]];
            fetchRequest.fetchLimit = 2;
            NSArray * result = [[DSGlobalData sharedData].childContext executeFetchRequest:fetchRequest error:nil];
            if (result.count == 2) {
                History * h = result[0];
                History * h2 = result[1];
                
                if ([self dateIsToday:h.date] && [self dateIsToday:h2.date]) {
                    NSLog(@"had already 2 notifications today");
                    return;
                }
            }
        }
        
        if ([spot.location distanceFromLocation:manager.location] <= kDSLocationHitRadius + manager.location.horizontalAccuracy) {
            [spot addHistoryWithType:kSpotHistoryEventTypeVisited inManagedObjectContext:[DSGlobalData sharedData].childContext];
            [spot addHistoryWithType:kSpotHistoryEventTypeNotification inManagedObjectContext:[DSGlobalData sharedData].childContext];
            
            if (!kDSIgnoreMaxAlertsPerSpot) {
                if (spot.visits == 2) {
                    spot.active = NO;
                    [self stopTrackingSpot:spot];
                    [self refreshTrackedSpots];
                }
            }
            
            [[DSGlobalData sharedData].childContext save:nil];
            
            _lastNotificationDate = [NSDate date];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                if ([self.delegate respondsToSelector:@selector(locationController:hitSpot:)]) {
                    [self.delegate locationController:self hitSpot:spot];
                }
            });
        }
    }];
}

- (BOOL)dateIsToday:(NSDate *)aDate {
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDateComponents *components = [cal components:(NSEraCalendarUnit|NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit) fromDate:[NSDate date]];
    NSDate *today = [cal dateFromComponents:components];
    components = [cal components:(NSEraCalendarUnit|NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit) fromDate:aDate];
    NSDate *otherDate = [cal dateFromComponents:components];
    
    return [today isEqualToDate:otherDate];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    if (applicationInBackground) {
        
        // TODO: Add condition to refresh only every 3 hours when in background
        // Last refresh should be stored in user defaults
        
        [self refreshTrackedSpots];

        if ([self.delegate respondsToSelector:@selector(locationController:didUpdateLocation:)]) {
            [self.delegate locationController:self didReceiveSignificantLocationChange:locations.lastObject];
        }
    } else {
        if ([self.delegate respondsToSelector:@selector(locationController:didUpdateLocation:)]) {
            [self.delegate locationController:self didUpdateLocation:locations.lastObject];
        }
    }
}

- (void)refreshTrackedSpots {
    [[DSGlobalData sharedData].childContext performBlock:^{
        NSMutableArray * array = [[Spot spotsSortedByDistanceFromLocation:self.locationManager.location limit:20 shouldBeActive:YES inManagedObjectContext:[DSGlobalData sharedData].childContext] mutableCopy];
        
        NSMutableArray * stopMonitoring = [NSMutableArray array];
        NSMutableArray * startMonitoring = [NSMutableArray array];
        
        for (CLRegion * region in self.locationManager.monitoredRegions) {
            BOOL spotShouldStillBeTracked = NO;
            for (Spot * spot in array) {
                if ([region.identifier isEqualToString:spot.slug]) {
                    spotShouldStillBeTracked = YES;
                    [array removeObject:spot];
                    break;
                }
            }
            if (!spotShouldStillBeTracked) {
                [stopMonitoring addObject:region];
            }
        }
        for (Spot * spot in array) {
            CLRegion * newRegion = [[CLCircularRegion alloc] initWithCenter:spot.location.coordinate radius:spot.radius identifier:spot.slug];
            [startMonitoring addObject:newRegion];
        }

        for (CLRegion * region in stopMonitoring) {
            NSLog(@"UNTRACK: %@", region.identifier);
            [self.locationManager stopMonitoringForRegion:region];
        }
        for (CLRegion * region in startMonitoring) {
            if (self.locationManager.monitoredRegions.count < 20) {
                BOOL exists = NO;
                for (CLRegion * oldRegion in self.locationManager.monitoredRegions) {
                    if ([oldRegion.identifier isEqualToString:region.identifier]) {
                        exists = YES;
                    }
                }
                if (!exists) {
                    [self.locationManager startMonitoringForRegion:region];
                    NSLog(@"TRACK: %@", region.identifier);
                }
            }
        }
    }];
}

- (void)stopTrackingSpot:(Spot *)spot {

    for (CLRegion * region in _locationManager.monitoredRegions) {
        if ([region.identifier isEqualToString:spot.slug]) {
            [_locationManager stopMonitoringForRegion:region];
            break;
        }
    }
}

#pragma mark - Data

- (NSArray *)fetchSpotsAroundLocation:(CLLocation *)location inManagedObjectContext:(NSManagedObjectContext *)context {
    if (!context) {
        return @[];
    }
    
    NSMutableArray * array = [NSMutableArray array];
    
    NSFetchRequest * request = [NSFetchRequest fetchRequestWithEntityName:@"Spot"];
    request.predicate = [NSPredicate predicateWithFormat:@"active = 1 AND ANY collections.active = 1"];
    
    NSError * error = nil;
    NSArray * matches = [context executeFetchRequest:request error:&error];
    
    if ([matches count] > 0) {
        for (Spot * spot in matches) {
            double distance = [location distanceFromLocation:spot.location];
            if (distance < 5000) {
                [array addObject:spot];
            }
        }
    }
    
    return array;
}

@end

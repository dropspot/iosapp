/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  Collection.h
//  Dropspot3
//
//  Created by Lukas Würzburger on 04.09.13.
//  Copyright (c) 2013 Dropspot. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <CoreData/CoreData.h>

#import "User.h"
#import "Spot.h"

#define kCollectionPhotoLoadingFinished @"kCollectionPhotoLoadingFinished"

@interface Collection : NSManagedObject

@property (nonatomic) BOOL active;
@property (nonatomic, retain) NSString * photoURL;
@property (nonatomic, retain) NSString * photoFile;
@property (nonatomic, retain) NSString * thumbnailURL;
@property (nonatomic, retain) NSString * thumbnailFile;
@property (nonatomic, retain) NSDate * savedDate;
@property (nonatomic, retain) NSString * slug;
@property (nonatomic, retain) NSString * text;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSDate * modified;
@property (nonatomic, retain) NSDate * modifiedSpots;
@property (nonatomic, retain) NSDate * created;
@property (nonatomic, retain) NSSet * spots;
@property (nonatomic, retain) User * user;
@property (nonatomic, retain) NSString * spotsURL;
@property (nonatomic, retain) NSString * url;
@property (nonatomic) BOOL subscribed;
@property (nonatomic) BOOL isOwnedByMe;
@property (nonatomic) BOOL isSynced;
@property (nonatomic) BOOL isBeingSynced;

+ (Collection *)collectionWithSlug:(NSString *)slug inManagedObjectContext:(NSManagedObjectContext *)context;

+ (Collection *)createEmptyCollectionInManagedObjectContext:(NSManagedObjectContext *)context;

- (NSArray *)spotsSortedByDistanceFromLocation:(CLLocation *)location inManagedObjectContext:(NSManagedObjectContext *)context;

@end

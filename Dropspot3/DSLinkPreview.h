/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  DSLinkPreview.h
//  Dropspot3
//
//  Created by Lukas Würzburger on 5/13/14.
//  Copyright (c) 2014 Dropspot GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DSLinkPreview;

@protocol DSLinkPreviewDelegate <NSObject>

@optional
/**
 * Called when the view was tapped anywhere.
 */
- (void)linkPreviewTapped:(DSLinkPreview *)linkPreview;

@end

@interface DSLinkPreview : UIView

/**
 * The object which receives all delegate functions.
 */
@property (nonatomic, assign) id <DSLinkPreviewDelegate> delegate;

/**
 * The image to be displayed.
 */
@property (nonatomic, strong) UIImage * image;

/**
 * Placeholder for the image.
 */
@property (nonatomic, strong) UIImage * placeholderImage;

/**
 * The url of the website
 */
@property (nonatomic, strong) NSURL * url;

/**
 * The title to be displayed
 */
@property (nonatomic, strong) NSString * title;

/**
 * The icon to be displayed next to the url host.
 */
@property (nonatomic, strong) UIImage * favicon;

/**
 * The URL host to be displayed.
 */
@property (nonatomic, strong) NSString * urlHost;

/**
 * Starts the loading animation.
 */
- (void)startLoadingAnimation;

/**
 * Stops the loading animation.
 */
- (void)stopLoadingAnimation;

@end

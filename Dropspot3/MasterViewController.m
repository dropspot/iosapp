/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  MasterViewController.m
//  Dropspot3
//
//  Created by Lukas Würzburger on 04.09.13.
//  Copyright (c) 2013 Dropspot. All rights reserved.
//

#import <MapKit/MapKit.h>

#import "MasterViewController.h"

#import "AppDelegate.h"
#import "DSGlobal.h"
#import "DataHelper.h"
#import "DSGlobalData.h"
#import "Collection+Dropspot.h"
#import "Spot+Dropspot.h"
#import "Spot+Location.h"
#import "History.h"
#import "UIImage+Tint.h"
#import "DSDownload.h"
#import "DSServices.h"
#import "DSSyncController.h"
#import "DSNavigationBar.h"
#import "UIBarButtonItem+Dropspot.h"

#import "SignInViewController.h"
#import "SpotViewController.h"
#import "DSMapView.h"
#import "CreateSpotViewController.h"
#import "CreateCollectionViewController.h"
#import "CollectionDetailViewController.h"

#import "DSLocationPermissionViewController.h"
#import "DSLocationDeniedViewController.h"

#import "SpotCell.h"
#import "NoNewsNearbyCell.h"
#import "CollectionCell.h"
#import "MasterBottomCell.h"

#import "Reachability.h"

#import "GAIHeader.h"

#define kPullRefreshYOffset -50

#define kAlertViewTagShowSpot 1

@interface MasterViewController () <DSMapViewDelegate, DSMapViewDataSource, UIScrollViewDelegate, UIAlertViewDelegate> {
    SignInViewController * signInViewController;
    
    DSMapView * _mapView;
    
    IBOutlet UILabel * titleBarLabel;

    IBOutlet UIView * mapCoveringView;
    
    IBOutlet UIView * loadingOverlayView;
    IBOutlet UILabel * loadingOverlayLabel;
    IBOutlet UIActivityIndicatorView * loadingOverlayActivityIndicatorView;
    
    NSArray * nearbySpots;
    NSArray * spotHistory;
    NSArray * popularCollections;
    NSArray * mySubscribedCollections;
    NSArray * userCollections;
    NSArray * myCollections;
    
    BOOL scrollViewIsDisappearing;
    BOOL isShowingMap;
    
    CGFloat scrollViewDraggingStartY;
    CGFloat navBarDraggingStartOffset;
    BOOL scrollViewIsDragging;
    
    NSDate * _lastLocationUpdate;
    
    Spot * _pushedSpot;
    
    UIViewController * noLocationViewController;

    NSArray * _allSpots;
}

@property (nonatomic, strong) NSFetchedResultsController * fetchedResultsControllerForAllSpots;

@property (nonatomic, assign) BOOL isRefreshing;

@property (nonatomic, strong) UIBarButtonItem * refreshBarButtonItem;
@property (nonatomic, strong) UIBarButtonItem * mapBarButtonItem;
@property (nonatomic, strong) UIBarButtonItem * backBarButtonItem;
@property (nonatomic, strong) UIBarButtonItem * searchBarButton;
@property (nonatomic, strong) UIBarButtonItem * locateMeBarButton;

@end

@implementation MasterViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.navigationController.navigationBar.tintColor = kDSColorCoral;
    
    if (kDSForceMainViewTitle) {
        [self setMainTitle:kDSForceMainViewTitle];
    }
    
    UIBarButtonItem * newBackButton = [[UIBarButtonItem alloc] initWithTitle:translate(@"MAP_VIEW_BACK_BUTTON_TITLE") style:UIBarButtonItemStyleBordered target:nil action:nil];
    self.navigationItem.backBarButtonItem = newBackButton;

    self.navigationItem.rightBarButtonItem = self.refreshBarButtonItem;
    self.navigationItem.leftBarButtonItem = self.mapBarButtonItem;
    
    [self checkSessionState];
    
    _mapView = [[DSMapView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    _mapView.delegate = self;
    _mapView.dataSource = self;
    _mapView.showsAuthorView = YES;
    _mapView.contentInset = UIEdgeInsetsMake(64.f, 0.f, 0.f, 0.f);
    [self.view insertSubview:_mapView belowSubview:mapCoveringView];
    
    mapCoveringView.frame = CGRectMake(0, _tableView.frame.origin.y + _tableView.contentInset.top - _tableView.contentOffset.y, mapCoveringView.frame.size.width, _tableView.frame.size.height - _tableView.contentInset.top);

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tableRefresh)                 name:kDSDropspotSessionUserChangedNotificationName object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refresh)                      name:kDSDropspotSpotCreatedRemoteNotificationName object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(localRefresh)                 name:kDSDropspotSpotDeletedNotificationName object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refresh)                      name:kDSDropspotCollectionCreatedRemoteNotificationName object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(localRefresh)                 name:kDSDropspotCollectionEditedNotificationName object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkCollectionSubscription:) name:kDSDropspotCollectionSubscriptionChanged object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showCollectionDetail:)        name:kCollectionPressedNotificationName object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(selectSpotNotification:)      name:kSelectSpotNotificationName object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateLocationNotification:)  name:kDSLocationManagerDidUpdateLocationNotificationName object:nil];
}

- (UIBarButtonItem *)mapBarButtonItem {
    
    if (!_mapBarButtonItem) {
        
        _mapBarButtonItem = [UIBarButtonItem mapBarButtonItemWithTarget:self action:@selector(switchToMap)];
    }
    return _mapBarButtonItem;
}

- (UIBarButtonItem *)refreshBarButtonItem {
    
    if (!_refreshBarButtonItem) {
        
        _refreshBarButtonItem = [UIBarButtonItem refreshBarButtonItemWithTarget:self action:@selector(reloadButtonPressed:)];
    }
    return _refreshBarButtonItem;
}

- (UIBarButtonItem *)backBarButtonItem {
    
    if (!_backBarButtonItem) {
        
        _backBarButtonItem = [UIBarButtonItem backBarButtonItemWithTarget:self action:@selector(backFromMap)];
    }
    return _backBarButtonItem;
}

- (UIBarButtonItem *)locateMeBarButton {
    
    if (!_locateMeBarButton) {
        
        _locateMeBarButton = [UIBarButtonItem locateMeBarButtonItemWithTarget:self action:@selector(locateMeButtonPressed:)];
    }
    return _locateMeBarButton;
}

- (UIBarButtonItem *)searchBarButton {
    
    if (!_searchBarButton) {
        
        _searchBarButton = [UIBarButtonItem searchBarButtonItemWithTarget:self action:@selector(searchButtonPressed:)];
    }
    return _searchBarButton;
}

- (void)setMainTitle:(NSString *)titleString {
    UIView * customView = self.navigationItem.titleView;
    UILabel * titleLabel = [[customView subviews] firstObject];
    
    titleLabel.text = titleString;
    
    CGRect boundingRect = [titleLabel.text boundingRectWithSize:CGSizeMake(200, 30)
                                                        options:NSStringDrawingUsesFontLeading
                                                     attributes:@{ NSFontAttributeName : titleLabel.font }
                                                        context:nil];
    
    CGRect frame = customView.frame;
    frame.size.width = boundingRect.size.width + 1;
    customView.frame = frame;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
    if (signInViewController && !signInViewController.isBeingDismissed && !signInViewController.isBeingPresented && !(signInViewController == self.presentedViewController && self.presentedViewController != nil)) {
        signInViewController = nil;
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // Is not called after login when switch to browser.
    
    if ([DSUser currentUser] && [CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorized) {
        [[DSGlobal sharedGlobals] analyticsView:(isShowingMap ? kDSScreenNameMapView : kDSScreenNameMainView)];
        
        _mapView.showsUserLocation = YES;
    } else {
        [self.tableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
    }
    
    [self localRefresh];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self setNavigationBarHidden:NO];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self checkSessionState];
    
    [self checkReceivedNotifications];
}

- (BOOL)shouldAutorotate {
    return self.presentedViewController.shouldAutorotate;
}

- (NSUInteger)supportedInterfaceOrientations {
    return self.presentedViewController.supportedInterfaceOrientations;
}

#pragma Setter & Getter

- (void)setIsRefreshing:(BOOL)isRefreshing {
    _isRefreshing = isRefreshing;
    
    NSString * key = @"isRefreshing";
    
    if (self.isRefreshing) {
        [[DSGlobal sharedGlobals] addLoadingStackObject:key];
    } else {
        [[DSGlobal sharedGlobals] removeFromLoadingStack:key];
    }
    [self updateFeedView];
}

#pragma mark - Notification

- (void)selectSpotNotification:(NSNotification *)notification {
    Spot * spot = notification.userInfo[@"spot"];
    if (spot) {
        [self switchToMapWithSpot:spot];
    }
}

- (void)checkCollectionSubscription:(NSNotification *)notification {
    Collection * collection = notification.userInfo[@"collection"];
    if (collection) {
        if (!collection.subscribed) {
            [[DSGlobalData sharedData].childContext performBlock:^{
                for (Spot * spot in collection.spots) {
                    [[DSGlobalData sharedData].childContext deleteObject:spot];
                }
                
                [[DSGlobalData sharedData].childContext deleteObject:collection];
                [[DSGlobalData sharedData].childContext save:nil];
                
                [self fetchMyCollections];

                dispatch_async(dispatch_get_main_queue(), ^{
                    [self updateMapView];
                    [self.tableView reloadData];
                });
            }];
        }
    }
}

#pragma mark - Helper

- (void)showSpot:(Spot *)spot {
    if (self.presentedViewController != nil) {
        [self.presentedViewController dismissViewControllerAnimated:YES completion:^{
            [self switchToMapWithSpot:spot];
        }];
    } else {
        [self switchToMapWithSpot:spot];
    }
}

- (void)checkReceivedNotifications {
    
    // Check if the array of received notifications has more than zero objects.
    if ([DSGlobal sharedGlobals].receivedNotificationSpots.count) {
        
        // Get the first spot in the array
        Spot * spot = [DSGlobal sharedGlobals].receivedNotificationSpots.firstObject;
        
        // Show the received spot.
        [self showSpot:spot];
        
        // Remove the spot from received notifications.
        [[DSGlobal sharedGlobals].receivedNotificationSpots removeObject:spot];
    }
}

- (void)createSpotAtLocationCoordinate:(CLLocationCoordinate2D)coordinate {
    NSManagedObjectContext * context = [DSGlobalData sharedData].childContext;
    [context performBlock:^{
        Spot * spot = [Spot spotWithLatitude:coordinate.latitude
                                   longitude:coordinate.longitude
                      inManagedObjectContext:[DSGlobalData sharedData].childContext];
        
        // Add the author and author image.
        NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
        if ([defaults objectForKey:kDSDropspotSessionDomainKeyName]) {
            User * user = [NSEntityDescription insertNewObjectForEntityForName:@"User" inManagedObjectContext:context];
            user.displayName = [[defaults objectForKey:kDSDropspotSessionDomainKeyName] valueForKey:kDSDropspotSessionNameKeyName];
            
            NSData * data = [[defaults objectForKey:kDSDropspotSessionDomainKeyName] valueForKey:kDSDropspotSessionImageKeyName];
            if (data) {
                
                NSString * fileName = @"user-avatar.png";
                [data writeToFile:[DataHelper pathForCacheFile:fileName] atomically:NO];
                user.photoFile = fileName;
            }
            spot.user = user;
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self updateMapView];
            
            _mapView.selectedSpot = spot;
            
            // Search asynchronously for the address
            [DSServices geocodeBySearch:[NSString stringWithFormat:@"%f,%f", spot.latitude, spot.longitude] completion:^(NSArray *array) {
                if (array && array.count > 0) {
                    NSString * address = array[0][@"formatted_address"];
                    spot.address = address;
                }
                spot.isDeterminingAddress = NO;
                [[NSNotificationCenter defaultCenter] postNotificationName:kDSSpotDidDetermineAddressNotificationName object:nil userInfo:@{ @"spot" : spot }];
            }];
        });
    }];
}

- (void)removeDroppedSpots {
    NSManagedObjectContext * context = [DSGlobalData sharedData].childContext;
    [context performBlock:^{
        NSFetchRequest * fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Spot"];
        fetchRequest.predicate = [NSPredicate predicateWithFormat:@"isSynced = 0 && isBeingSynced = 0"];
        
        NSArray * matches = [context executeFetchRequest:fetchRequest error:nil];

        if (matches && matches.count > 0) {
            for (Spot * spot in matches) {
                [context deleteObject:spot];
            }
            
            [self localRefresh];
        }
        [context save:nil];
        dispatch_async(dispatch_get_main_queue(), ^{
            [[DSGlobalData sharedData].parentContext save:nil];
        });
    }];
}

- (void)updateLocationNotification:(NSNotification *)notification {
    [self localRefresh];
}

- (void)updateMapView {
    [self updateMapViewWithCompletion:nil];
}

- (void)updateMapViewWithCompletion:(void (^)(void))completion {
    
    // Get new spot count
    [[DSGlobalData sharedData].childContext performBlock:^{
        
        NSFetchRequest * fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"Spot"];
        fetchRequest.returnsObjectsAsFaults = NO;
        NSArray * matches = [[DSGlobalData sharedData].childContext executeFetchRequest:fetchRequest error:nil];
        
        dispatch_async(dispatch_get_main_queue(), ^{

            [self sortSpots:matches byDistanceToLocation:self.locationManager.location block:^(NSArray *sortedSpots) {
                _allSpots = sortedSpots;
                
                [_mapView update];
            }];
        });
    }];
    
    // Load spots for the current region.
    [self loadSpotsForRegion:_mapView.region completion:completion];
}

- (void)checkNearbySpots:(NSArray *)spots inManagedObjectContext:(NSManagedObjectContext *)context {
    if (self.locationManager.location) {
        for (Spot * spot in spots) {
            if (spot.isSynced) {
                if ([spot.location distanceFromLocation:self.locationManager.location] < 300) {
                    if (!spot.seen) {
                        [spot addHistoryWithType:kSpotHistoryEventTypeSeen inManagedObjectContext:context];
                    }
                } else {
                    break;
                }
            }
        }
    }
}

- (void)switchToMap {
    [self switchToMapWithSpot:nil];
}

- (void)switchToMapWithSpot:(Spot *)spot {
    if (spot) {
        _mapView.selectedSpot = spot;
    }
    
    [self setNavigationBarHidden:NO];
    
    if (!isShowingMap) {
        isShowingMap = YES;
        scrollViewIsDisappearing = YES;
        
        self.navigationItem.leftBarButtonItem = self.backBarButtonItem;
        
        self.navigationItem.rightBarButtonItems = @[self.searchBarButton, self.locateMeBarButton];
        
        _mapView.state = DSMapViewStateNormal;
        
        [UIView animateWithDuration:0.3f delay:0.0f options:UIViewAnimationOptionCurveLinear animations:^{
            [_tableView setContentOffset:CGPointMake(0.0f, -self.view.frame.size.height)];
            [self setMainTitle:@"Map"];
            self.navigationItem.backBarButtonItem.title = @"Map";

        } completion:^(BOOL finished) {
            scrollViewIsDisappearing = NO;
            [_tableView setFrame:CGRectMake(0.0f, self.view.frame.size.height, _tableView.frame.size.width, _tableView.frame.size.height)];
            [_tableView setContentOffset:CGPointZero];
            mapCoveringView.hidden = YES;
            
            if (spot) {
                _mapView.selectedSpot = spot;
            } else {
                if (!_mapView.selectedSpot) {
                    if (_allSpots.count > 0) {
                        _mapView.selectedSpot = _allSpots[0];
                    }
                }
            }
            
            [[DSGlobal sharedGlobals] analyticsView:kDSScreenNameMapView];
        }];
    }
}

- (void)backFromMap {
    
    if ([self.delegate respondsToSelector:@selector(masterViewControllerShouldRefreshInMainView:)]) {
        if ([self.delegate masterViewControllerShouldRefreshInMainView:self]) {
            [self refresh];
        }
    }
    
    [self setSearchBarVisible:NO];
    
    self.navigationItem.leftBarButtonItem = self.mapBarButtonItem;
    self.navigationItem.rightBarButtonItems = @[self.refreshBarButtonItem];
    
    mapCoveringView.hidden = NO;
    mapCoveringView.frame = CGRectMake(0, _tableView.frame.origin.y + _tableView.contentInset.top, mapCoveringView.frame.size.width, 1);
    
    _mapView.state = DSMapViewStateBackground;
    
    [UIView animateWithDuration:0.3 animations:^{
        CGRect frame = _tableView.frame;
        frame.origin = CGPointMake(0, 0);
        _tableView.frame = frame;
        [_tableView setContentOffset:CGPointMake(0, -_tableView.contentInset.top)];
        
        if (kDSForceMainViewTitle) {
            [self setMainTitle:kDSForceMainViewTitle];
        } else {
            [self setMainTitle:@"Dropspot"];
        }
        self.navigationItem.backBarButtonItem.title = translate(@"MAP_VIEW_BACK_BUTTON_TITLE");

        mapCoveringView.frame = CGRectMake(0, _tableView.frame.origin.y + _tableView.contentInset.top - _tableView.contentOffset.y, mapCoveringView.frame.size.width, _tableView.frame.size.height - _tableView.contentInset.top);
    } completion:^(BOOL finished) {
        isShowingMap = NO;
        
        if (!_mapView.selectedSpot) {
            _mapView.followsUserLocation = YES;
        }
        
        [[DSGlobal sharedGlobals] analyticsView:kDSScreenNameMainView];
    }];
}

- (void)setNavigationBarOffset:(CGFloat)offset {

    CGRect frame = self.navigationController.navigationBar.layer.frame;
    float newoffset = navBarDraggingStartOffset - offset;
    if (newoffset > 20) {
        newoffset = 20;
    } else if (newoffset < -24) {
        newoffset = -24;
    }
    frame.origin.y = newoffset;
    [self.navigationController.navigationBar.layer setFrame:frame];
    float alpha = (newoffset + 14) / 34;
    
    titleBarLabel.alpha = alpha;
    self.refreshBarButtonItem.customView.alpha = alpha;
    self.mapBarButtonItem.customView.alpha = alpha;
}

- (void)setNavigationBarHidden:(BOOL)hidden {
    navBarDraggingStartOffset = 0;
    if (hidden) {
        [self setNavigationBarOffset:24];
    } else {
        [self setNavigationBarOffset:-20];
    }
}

- (void)fetchMyCollections {
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Collection"];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"isOwnedByMe = 1 OR subscribed = 1"];
    fetchRequest.fetchLimit = 20;
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"created" ascending:NO];
    [fetchRequest setSortDescriptors:@[sortDescriptor]];
    
    myCollections = [[DSGlobalData sharedData].childContext executeFetchRequest:fetchRequest error:nil];
}

- (void)updateFeedView {
    [self updateFeedViewWithCompletion:nil];
}

- (void)updateFeedViewWithCompletion:(void (^)(void))completion {
    [[DSGlobalData sharedData].childContext performBlock:^{
        nearbySpots = [Spot spotsSortedByDistanceFromLocation:self.locationManager.location
                                                        limit:3
                                       inManagedObjectContext:[DSGlobalData sharedData].childContext];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableView reloadData];
        });
        
        [self fetchMyCollections];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (completion) {
                completion();
            }
            [self.tableView reloadData];
        });
    }];
}

- (void)setLoadingOverlayHidden:(BOOL)hidden {

    if (hidden) {
        [UIView animateWithDuration:0.25 animations:^{
            loadingOverlayView.alpha = 0.0;
        } completion:^(BOOL finished) {
            [loadingOverlayActivityIndicatorView stopAnimating];
            loadingOverlayView.hidden = YES;
        }];
    } else {
        if (loadingOverlayView.hidden) {
            loadingOverlayLabel.text = translate(@"MASTER_VIEW_LOADING_OVERLAY_LABEL");
            [loadingOverlayActivityIndicatorView startAnimating];

            loadingOverlayView.alpha = 0.0;
            loadingOverlayView.hidden = NO;
            [UIView animateWithDuration:0.25 animations:^{
                loadingOverlayView.alpha = 1.0;
            }];
        }
    }
    [self.navigationController setNavigationBarHidden:!hidden animated:YES];
}

- (void)setSearchBarVisible:(BOOL)visible {

    // Toggles the search bar.
    _mapView.showsSearchBar = visible;
    
    // Change hightlightment
    if (visible) {
        
        [self.searchBarButton setBackgroundImage:[UIImage imageNamed:@"searchButtonBackgroundHighlighted.png"]
                          forState:UIControlStateNormal
                        barMetrics:UIBarMetricsDefault];
        
        self.searchBarButton.tintColor = [UIColor whiteColor];
    } else {
        
        [self.searchBarButton setBackgroundImage:[UIImage imageNamed:@"searchButtonBackgroundNormal.png"]
                          forState:UIControlStateNormal
                        barMetrics:UIBarMetricsDefault];
        
        self.searchBarButton.tintColor = nil;
    }
}

#pragma mark - AlertView

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == kAlertViewTagShowSpot) {
        if (buttonIndex == 1) {
            [self switchToMapWithSpot:_pushedSpot];
            _pushedSpot = nil;
        }
    }
}

#pragma mark - IBAction

- (IBAction)mapGradientPressed:(id)sender {
    [self switchToMap];
}

- (void)tileViewPressed:(DSTileView *)sender {
    Spot * spot = nearbySpots[sender.tag];
    [self switchToMapWithSpot:spot];
}

- (void)reloadButtonPressed:(id)sender {
    [self refreshManually:YES];
}

- (void)backButtonPressed:(id)sender {
    [self backFromMap];
}

- (void)addButtonPressed:(id)sender {
    [self createSpotAtLocationCoordinate:self.locationManager.location.coordinate];
    
    [self switchToMap];
}

- (void)settingsButtonPressed:(id)sender {
    [self performSegueWithIdentifier:@"showSettings" sender:nil];
}

- (void)signUpButtonPressed:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:kDSDropspotShowLoginNotificationName object:nil];
}

- (void)locateMeButtonPressed:(id)sender {
    
    // Center the user's current location on the map.
    [_mapView centerUserLocationAnimated:YES];
}

- (void)searchButtonPressed:(UIBarButtonItem *)sender {
    
    [self setSearchBarVisible:!_mapView.showsSearchBar];
}

- (void)showCollectionDetail:(NSNotification *)notification {
    [self setNavigationBarHidden:NO];
    [self performSegueWithIdentifier:@"collectionDetail" sender:notification.userInfo[@"collection"]];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"spotDetail"] && [segue.destinationViewController isKindOfClass:[SpotViewController class]]) {
        SpotViewController * spotVC = segue.destinationViewController;
        spotVC.spot = sender;
    } else if ([segue.identifier isEqualToString:@"collectionDetail"] && [segue.destinationViewController isKindOfClass:[CollectionDetailViewController class]]) {
        CollectionDetailViewController * collectionVC = segue.destinationViewController;
        collectionVC.collection = sender;
        collectionVC.locationManager = self.locationManager;
    } else if ([segue.identifier isEqualToString:@"createSpotSegue"]) {
        CreateSpotViewController * theCreateSpotViewController = [[(UINavigationController *)[segue destinationViewController] viewControllers] lastObject];
        if (theCreateSpotViewController) {
            theCreateSpotViewController.spot = sender;
        }
    } else if ([segue.identifier isEqualToString:@"createCollection"]) {
        CreateCollectionViewController * theCreatecollectionViewController = [[(UINavigationController *)[segue destinationViewController] viewControllers] lastObject];
        theCreatecollectionViewController.collection = sender;
        [self setNavigationBarHidden:NO];
    }
}

#pragma mark - Handle data

- (void)refresh {
    [self refreshManually:NO];
}

- (void)refreshManually:(BOOL)manually {
    [self checkIfContentAvailableLocal:^(BOOL isContentAvailable) {
        if (isContentAvailable) {
            if (!manually && [DSGlobal sharedGlobals].lastSyncDate) {
                if ([NSDate date].timeIntervalSince1970 - [DSGlobal sharedGlobals].lastSyncDate.timeIntervalSince1970 < 10800) {
                    return;
                }
            }
        }
        
        Reachability *reachability = [Reachability reachabilityForInternetConnection];
        NetworkStatus internetStatus = [reachability currentReachabilityStatus];
        if (internetStatus != NotReachable) {
            
            [[DSGlobalData sharedData] synchronizeWithCompletion:^(BOOL success, NSError *error) {
                
                if (error) {
                    
                    if (error.code == kDSDropspotAPIErrorCodeNotAuthorized) {
                        
                        [[DSDropspot session] closeSession];
                        [[DSDropspot session] checkSession];
                    }
                } else {

                    [self localRefreshWithCompletion:^{
                        [self setLoadingOverlayHidden:YES];
                    }];
                }
            }];
            
            if (nearbySpots.count == 0 && myCollections.count == 0) {
                [self setLoadingOverlayHidden:NO];
            }
        } else {
            NSLog(@"You have no internet connection");
        }
    }];
}

- (void)checkIfContentAvailableLocal:(void(^)(BOOL isContentAvailable))block {
    [[DSGlobalData sharedData].childContext performBlock:^{
        [self fetchMyCollections];
        NSArray * spots = [Spot spotsSortedByDistanceFromLocation:self.locationManager.location
                                                            limit:3
                                           inManagedObjectContext:[DSGlobalData sharedData].childContext];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (spots.count == 0 && myCollections.count == 0) {
                if (block) {
                    block(NO);
                }
            } else {
                if (block) {
                    block(YES);
                }
            }
        });
    }];
}

- (void)tableRefresh {
    [self.tableView reloadData];
}

- (void)localRefresh {
    [self localRefreshWithCompletion:nil];
}

- (void)localRefreshWithCompletion:(void (^)(void))completion {
    [self updateFeedViewWithCompletion:^{
        [self updateMapViewWithCompletion:^{
            if (completion) {
                completion();
            }
        }];
    }];
}

#pragma mark - Map view delegate

- (void)mapView:(DSMapView *)mapView didDeselectSpot:(Spot *)spot {
    //self.navigationItem.rightBarButtonItem = addBarButton;
}

- (void)mapView:(DSMapView *)mapView didPressSpot:(Spot *)spot {
    [self performSegueWithIdentifier:@"spotDetail" sender:spot];
}

- (void)mapView:(DSMapView *)mapView didSelectSpot:(Spot *)spot {
    
    NSString * labelString = nil;
    
    if (spot.collections.count == 0) {
        labelString = spot.slug;
    } else {
        labelString = [NSString stringWithFormat:@"%@/%@", [(Collection *)spot.collections.anyObject slug], spot.slug];
    }
}

- (void)mapView:(DSMapView *)mapView didRecognizeLongPressAtLocationCoordinate:(CLLocationCoordinate2D)coordinate {
    
    // Remove spots that has been created befor and not committed.
    [self removeDroppedSpots];
    
    // Create spot with given coordinates
    //  [self createSpotAtLocationCoordinate:coordinate];
}

- (void)mapViewDidRecognizeSwipeUp:(DSMapView *)mapView {
    
    // Come back from map
    [self backFromMap];
}

- (void)mapView:(DSMapView *)mapView didPressCreateSpot:(Spot *)spot {
    [self performSegueWithIdentifier:@"createSpotSegue" sender:spot];
}

- (void)mapView:(DSMapView *)mapView didPressCancelSpot:(Spot *)spot {
    [[DSGlobalData sharedData].childContext performBlock:^{
    
        // Remove the object from database
        [[DSGlobalData sharedData].childContext deleteObject:spot];
    }];
}

- (void)mapView:(DSMapView *)mapView didChangeRegion:(DSMapViewRegion)region {
    
    // Fetch new spots from the database for the new region.
    [self loadSpotsForRegion:region];
}

#pragma mark - Map view dataSource

- (NSUInteger)mapView:(DSMapView *)mapView numberOfItemsForMapInfoView:(DSMapInfoView *)mapInfoView {
    
    return _allSpots.count;
}

- (DSMapInfoItemViewController *)mapView:(DSMapView *)mapView viewControllerAtIndex:(NSUInteger)index {

    DSMapInfoItemViewController * viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"spotView"];
    viewController.spot = _allSpots[index];
    return viewController;
}

- (NSUInteger)mapView:(DSMapView *)mapView indexForSpot:(Spot *)spot {
    
    return [_allSpots indexOfObject:spot];
}

- (DSMapInfoItemViewController *)mapView:(DSMapView *)mapView viewControllerWithSpot:(Spot *)spot {
    
    DSMapInfoItemViewController * viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"spotView"];
    viewController.spot = spot;
    return viewController;
}

#pragma mark - Map data

- (void)loadSpotsForRegion:(DSMapViewRegion)region {
    
    // Forward to the actual function.
    [self loadSpotsForRegion:region completion:nil];
}

- (void)loadSpotsForRegion:(DSMapViewRegion)region completion:(void (^)(void))completion {
    
    __block NSMutableArray * spots = [NSMutableArray array];
    
    NSUInteger spotslimitPerRegion = 8;
    
    // Create region grid.
    DSMapViewRegionGrid regionGrid = DSMapViewRegionGridMake(4, 4);
    
    // Create a sliced region grid.
    NSArray * subRegions = [self slicedRegionWithRegion:region grid:regionGrid];
    
    for (int i = 0; i < subRegions.count; i++) {
        
        NSDictionary * subRegionDict = subRegions[i];
        
        DSMapViewRegion subRegion = DSMapViewRegionMake([subRegionDict[@"top"] doubleValue],
                                                        [subRegionDict[@"bottom"] doubleValue],
                                                        [subRegionDict[@"left"] doubleValue],
                                                        [subRegionDict[@"right"] doubleValue]);
        
        [self fetchSpotsInRegion:subRegion limit:spotslimitPerRegion completion:^(NSArray * regionSpots){
            
            [spots addObjectsFromArray:regionSpots];
            
            if (i == subRegions.count - 1) {
                
                _mapView.spots = spots;
                
                if (completion) {
                    completion();
                }
            }
        }];
    }
}

- (NSArray *)slicedRegionWithRegion:(DSMapViewRegion)region grid:(DSMapViewRegionGrid)grid {
    NSMutableArray * regions = [NSMutableArray array];
    
    double latitudeStep = (region.topLatitude - region.bottomLatitude) / grid.rows;
    double longitudeStep = (region.rightLongitude - region.leftLongitude) / grid.columns;
    
    for (int i = 0; i < grid.rows; i++) {
        for (int j = 0; j < grid.columns; j++) {
            [regions addObject:@{
                                 @"top" : @(region.topLatitude - (i * latitudeStep)),
                                 @"bottom" : @(region.topLatitude - ((i + 1) * latitudeStep)),
                                 @"left" : @(region.leftLongitude + (j * longitudeStep)),
                                 @"right" : @(region.leftLongitude + ((j + 1) * longitudeStep))
                                 }];
        }
    }
    
    return regions;
}

- (void)fetchSpotsInRegion:(DSMapViewRegion)region limit:(NSUInteger)limit completion:(void (^)(NSArray * regionSpots))completion {
    
    NSManagedObjectContext * context = [DSGlobalData sharedData].childContext;
    
    [context performBlock:^{
        
        NSFetchRequest * fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"Spot"];
        
        NSString * condition = [NSString stringWithFormat:@"latitude <= %f AND latitude > %f AND longitude >= %f AND longitude < %f", region.topLatitude, region.bottomLatitude, region.leftLongitude, region.rightLongitude];
        
        fetchRequest.predicate = [NSPredicate predicateWithFormat:condition];
        fetchRequest.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"modified" ascending:NO]];
        fetchRequest.fetchLimit = limit;
        
        NSError * error;
        NSArray * result = [context executeFetchRequest:fetchRequest error:&error];
        
        if (error) {
            NSLog(@"fetchSpotsInRegion:limit:completion: An Error occured: %@", error.localizedDescription);
        } else {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (completion) {
                    completion(result);
                }
            });
        }
    }];
}

- (void)sortSpots:(NSArray *)spots byDistanceToLocation:(CLLocation *)location block:(void (^)(NSArray * sortedSpots))block {
    
    [[DSGlobalData sharedData].childContext performBlock:^{
        NSMutableArray * array = [NSMutableArray array];
        for (Spot * spot in spots) {
            CLLocation * l = [[CLLocation alloc] initWithLatitude:spot.latitude longitude:spot.longitude];
            [array addObject:@{@"spot" : spot,
                               @"distance" : [NSNumber numberWithFloat:[l distanceFromLocation:location]]}];
        }
        NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"distance" ascending:YES];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            NSArray * sortedSpotDictionaries = [array sortedArrayUsingDescriptors:@[descriptor]];
            
            NSMutableArray * sortedSpots = [NSMutableArray array];
            for (NSDictionary * dict in sortedSpotDictionaries) {
                
                [sortedSpots addObject:dict[@"spot"]];
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if (block) {
                    block(sortedSpots);
                }
            });            
        });
    }];
}

#pragma mark - Table view delegate & dataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return (nearbySpots.count > 0) ? nearbySpots.count : 1;
        case 1:
            return 1;
        case 2:
            return 1 + ([DSUser currentUser].state == DSUserStateTemporary);
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell * cell;
    if (indexPath.section == 0) {
        if (nearbySpots.count == 0) {
            cell = [tableView dequeueReusableCellWithIdentifier:@"noSpotsCell" forIndexPath:indexPath];
        } else {
            cell = [tableView dequeueReusableCellWithIdentifier:@"spotCell" forIndexPath:indexPath];
        }
    } else if (indexPath.section <= 1) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"collectionCell" forIndexPath:indexPath];
    } else {
        cell = [tableView dequeueReusableCellWithIdentifier:@"bottomCell" forIndexPath:indexPath];
    }
    
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        if (nearbySpots.count > 0) {
            Spot * spot = nearbySpots[indexPath.row];
            return [SpotCell heightForCellWithSpot:spot style:(kDSTileViewStyleBig)?DSTileViewStyleBig:DSTileViewStyleSmall info:((kDSHideBeenThereDoneThat)?spot.address:@"") time:NO];
        } else {
            return 49;
        }
    } else if (indexPath.section <= 1) {
        return 340;
    } else {
        return 64;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return [self headerViewForSection:section];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section <= 1) {
        return 60;
    }
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView * view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 60)];
    view.backgroundColor = [UIColor whiteColor];
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0;
}

- (UIView *)headerViewForSection:(NSInteger)section {
    UIView * view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 60)];
    view.backgroundColor = [UIColor whiteColor];
    
    UILabel * label = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 300, 60)];
    label.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
    label.font = [UIFont fontWithName:@"HelveticaNeue-Thin" size:32];
    label.backgroundColor = [UIColor clearColor];
    
    switch (section) {
        case 0:
            label.text = translate(@"FEED_VIEW_LABEL_NEARBY_SPOTS");
            break;
        case 1:
            label.text = translate(@"FEED_VIEW_LABEL_MY_COLLECTIONS");
            break;
            
        default:
            label.text = @"";
            break;
    }

    [view addSubview:label];
    
    return view;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        if ([cell isKindOfClass:[SpotCell class]]) {
            Spot * spot = nearbySpots[indexPath.row];
            SpotCell * spotCell = (SpotCell *)cell;
            
            float distance = [spot.location distanceFromLocation:self.locationManager.location];

            DSTileView * view = spotCell.tileView;
            view.style = DSTileViewStyleSmall;
            [DataHelper imageWithFilePath:[DataHelper pathForCacheFile:spot.thumbnailFile] block:^(UIImage *image) {
                if (image) {
                    
                    view.image = image;
                } else {
                    
                    [DataHelper imageWithFilePath:[DataHelper pathForCacheFile:spot.photoFile] block:^(UIImage *image) {
                        if (image) {
                            
                            view.image = image;
                        } else {

                            view.image = nil;
                        }
                    }];
                }
            }];
            
            view.text = spot.text;
            view.tag = indexPath.row;
            [DSGlobal distancePropertiesFromDistance:distance properties:^(double value, NSString *measurement) {
                if ([measurement isEqualToString:[DSGlobal lowestMeasurement]]) {
                    view.distance = [NSString stringWithFormat:@"%.0f%@", value, measurement];
                } else {
                    view.distance = [NSString stringWithFormat:@"%.2f%@", value, measurement];
                }
            }];
            [view addTarget:self selector:@selector(tileViewPressed:)];
            view.infoStyle = DSTileViewInfoStyleNormal;
            view.info = @"";
            
            NSString * labelString = nil;
            
            if (spot.collections.count == 0) {
                labelString = spot.slug;
            } else {
                labelString = [NSString stringWithFormat:@"%@/%@", [(Collection *)spot.collections.anyObject slug], spot.slug];
            }
        } else if ([cell isKindOfClass:[NoNewsNearbyCell class]]) {
            NoNewsNearbyCell * _cell = (NoNewsNearbyCell *)cell;
            _cell.label.text = translate(@"FEED_VIEW_LABEL_NO_NEWS_NEARBY");
        }
    } else if (indexPath.section == 1) {
        if ([cell isKindOfClass:[CollectionCell class]]) {
            CollectionCell * collectionCell = (CollectionCell *)cell;
            collectionCell.collectionBrowser.collections = myCollections;
        }
    } else {
        if ([cell isKindOfClass:[MasterBottomCell class]]) {
            MasterBottomCell * bottomCell = (MasterBottomCell *)cell;

            if (indexPath.row == 0) {
                [bottomCell.settingsButton setTitle:translate(@"DETAIL_VIEW_CONTROL_BUTTON_TITLE_SETTINGS")
                                           forState:UIControlStateNormal];
                [bottomCell.settingsButton addTarget:self action:@selector(settingsButtonPressed:)
                                    forControlEvents:UIControlEventTouchUpInside];
            } else {
                [bottomCell.settingsButton setTitle:translate(@"DETAIL_VIEW_CONTROL_BUTTON_TITLE_SIGN_UP")
                                           forState:UIControlStateNormal];
                [bottomCell.settingsButton addTarget:self action:@selector(signUpButtonPressed:)
                                    forControlEvents:UIControlEventTouchUpInside];
            }
            [bottomCell setLayout];
        }
    }
}

#pragma mark - UIScrollView delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    float transitionHeight = 150.f;
    if (scrollView.contentOffset.y < -scrollView.contentInset.top) {
        if (scrollView.contentOffset.y > -transitionHeight - scrollView.contentInset.top) {
            scrollView.alpha = (transitionHeight - abs(scrollView.contentOffset.y + scrollView.contentInset.top) * 1.0) / transitionHeight;
        } else {
            scrollView.alpha = 0.0;
        }
    } else {
        scrollView.alpha = 1.0;
    }
    
    float height = self.view.frame.size.height + scrollView.contentOffset.y - scrollView.contentSize.height + 50;
    
    CGRect frame = mapCoveringView.frame;
    frame.size.height = height > 0 ? height : 0;
    frame.origin.y = self.view.frame.size.height - frame.size.height;
    [mapCoveringView setFrame:frame];
    
    if (scrollViewIsDragging) {
        float diff = scrollView.contentOffset.y - scrollViewDraggingStartY;
        
        [self setNavigationBarOffset:diff];
    }
}

- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView {
    if (scrollViewIsDisappearing) {
        [scrollView setContentOffset:scrollView.contentOffset animated:YES];
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    scrollViewIsDragging = YES;
    scrollViewDraggingStartY = scrollView.contentOffset.y;
    navBarDraggingStartOffset = self.navigationController.navigationBar.layer.frame.origin.y;
}

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset {
    scrollViewIsDragging = NO;
    
    if (velocity.y < 0) {
        [self setNavigationBarHidden:NO];
    } else if (velocity.y > 0) {
        [self setNavigationBarHidden:YES];
    }
    
    if (scrollView.contentOffset.y < -scrollView.contentInset.top) {
        if (scrollView.contentOffset.y < -scrollView.contentInset.top-70) {
            [self switchToMap];
        } else {
            if (velocity.y < -1) {
                [self switchToMap];
            }
        }
    }
}

- (BOOL)scrollViewShouldScrollToTop:(UIScrollView *)scrollView {
    [self setNavigationBarHidden:NO];

    return YES;
}

@end

/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  RootNavigationController.m
//  Dropspot3
//
//  Created by Lukas Würzburger on 1/10/14.
//  Copyright (c) 2014 Dropspot. All rights reserved.
//

#import "RootNavigationController.h"

#import "DSReaderView.h"

@interface RootNavigationController () <UINavigationBarDelegate>

@end

@implementation RootNavigationController

#pragma mark - Getter

- (DSReaderView *)readerView {
    
    if (!_readerView) {
        
        // Create reader view
        _readerView = [[DSReaderView alloc] initWithFrame:CGRectMake(120, 74, 200, 44)];
        [self.view addSubview:self.readerView];
    }
    return _readerView;
}

#pragma mark - Delegates

- (UIBarPosition)positionForBar:(id<UIBarPositioning>)bar {
    return UIBarPositionTopAttached;
}

- (BOOL)shouldAutorotate {
    return [(UIViewController *)[self.viewControllers lastObject] shouldAutorotate];
}

- (NSUInteger)supportedInterfaceOrientations {
    return [(UIViewController *)[self.viewControllers lastObject] supportedInterfaceOrientations];
}

@end

/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  SelectCollectionViewController.h
//  Dropspot3
//
//  Created by Lukas Würzburger on 11/16/13.
//  Copyright (c) 2013 Dropspot. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SelectCollectionViewController, Collection;

@protocol SelectCollectionViewControllerDelegate <NSObject>

- (void)selectCollectionViewControllerDidCancel:(SelectCollectionViewController *)selectCollectionViewController;
- (void)selectCollectionViewController:(SelectCollectionViewController *)selectCollectionViewController didSelectCollections:(NSArray *)collections;

@end

@interface SelectCollectionViewController : UITableViewController

@property (nonatomic, retain) NSMutableArray * collections;
@property (nonatomic) id <SelectCollectionViewControllerDelegate> delegate;

@end

/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  SpotLink+Dropspot.h
//  Dropspot3
//
//  Created by Lukas Würzburger on 5/14/14.
//  Copyright (c) 2014 Dropspot GmbH. All rights reserved.
//

#import "SpotLink.h"
#import "DSDropspot.h"

@interface SpotLink (Dropspot)

+ (SpotLink *)spotLinkWithAPIResponse:(DSAPISpotLinkResponse *)response
               inManagedObjectContext:(NSManagedObjectContext *)context;

@end

/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  UILabel+AutoHeight.m
//  Dropspot3
//
//  Created by Lukas Würzburger on 11/8/13.
//  Copyright (c) 2013 Dropspot. All rights reserved.
//

#import "UILabel+AutoHeight.h"

@implementation UILabel (AutoHeight)

- (CGRect)autoAdjustHeight {
    NSDictionary * stringAttributes = @{ NSFontAttributeName : self.font };
    CGRect frame = self.frame;
    CGRect rect = [self.text boundingRectWithSize:CGSizeMake(self.frame.size.width, CGFLOAT_MAX)
                                               options:(NSStringDrawingUsesFontLeading | NSStringDrawingUsesLineFragmentOrigin)
                                            attributes:stringAttributes
                                               context:nil];
    frame.size.height = rect.size.height;
    self.frame = frame;
    
    return self.frame;
}

@end

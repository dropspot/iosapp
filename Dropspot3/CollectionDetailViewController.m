/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  CollectionDetailViewController.m
//  Dropspot3
//
//  Created by Lukas Würzburger on 11/13/13.
//  Copyright (c) 2013 Dropspot. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import <LRResty/LRResty.h>
#import <CoreLocation/CoreLocation.h>
#import <MessageUI/MessageUI.h>

#import "CollectionDetailViewController.h"

#import "UILabel+AutoHeight.h"
#import "DSDropspot.h"
#import "DSGlobal.h"
#import "DSGlobalData.h"
#import "CollectionMapViewController.h"
#import "DSSyncController.h"
#import "SignInViewController.h"
#import "DSTileView.h"
#import "Spot+Location.h"
#import "UIImage+Tint.h"
#import "DSServices.h"
#import "DataHelper.h"

#import "CollectionTopCell.h"
#import "SpotCell.h"
#import "CollectionBottomCell.h"

#import "GAIHeader.h"

#import "Reachability.h"

#define kActionSheetTagDelete 1

@interface CollectionDetailViewController () <UIScrollViewDelegate, UITableViewDataSource, UITableViewDelegate, UIActionSheetDelegate, MFMessageComposeViewControllerDelegate, CollectionMapViewControllerDelegate, DSSyncControllerDelegate> {

    // Shared
    
    IBOutlet UIView * coverView;
    IBOutlet UIImageView * photoImageView;
    IBOutlet UIImageView * sourceImageView;
    IBOutlet UILabel * sourceLabel;
    IBOutlet UILabel * titleLabel;
    IBOutlet UIView * loadingOverlayView;
    
    NSArray * _sortedSpots;

    // Dropspot General
    
    // Single Collection
    
    SignInViewController * signInViewController;
    UIViewController * noLocationViewController;
    
    NSDate * _lastLocationUpdate;
    int _photosToLoad;    
}

@property (nonatomic, weak) IBOutlet UITableView * tableView;

@end

@implementation CollectionDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIBarButtonItem *newBackButton = [[UIBarButtonItem alloc] initWithTitle:translate(@"MAP_VIEW_BACK_BUTTON_TITLE")
                                                                      style:UIBarButtonItemStyleBordered
                                                                     target:nil
                                                                     action:nil];
    [[self navigationItem] setBackBarButtonItem:newBackButton];

    photoImageView.layer.masksToBounds = YES;
    sourceImageView.layer.masksToBounds = YES;
    
    sourceImageView.layer.cornerRadius = 3.f;

    if (kIsSingleCollectionApp) {
        if (!kDSHideMapButton) {
            UIBarButtonItem * mapButton = [[UIBarButtonItem alloc] initWithTitle:@"Map" style:UIBarButtonItemStyleBordered target:self action:@selector(mapButtonPressed:)];
            self.navigationItem.rightBarButtonItem = mapButton;
        }
        
        /*
        UIBarButtonItem * addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addButtonPressed:)];
        self.navigationItem.leftBarButtonItem = addButton;
        */
        [[DSGlobalData sharedData].childContext performBlock:^{
            if (!self.collection) {
                self.collection = [self fetchCollectionInManagedObjectContext:[DSGlobalData sharedData].childContext];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (!self.collection) {
                        [self refresh];
                    } else {
                        [self configureView];
                    }
                });
            } else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self configureView];
                });
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self checkSessionState];
            });
        }];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkSessionState) name:kDSDropspotSessionChangedNotificationName object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateData) name:kDSDropspotSpotDeletedNotificationName object:nil];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self configureView];
    
    [[DSGlobal sharedGlobals] analyticsView:kDSScreenNameCollectionDetailView];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateLocationNotification:) name:kDSLocationManagerDidUpdateLocationNotificationName object:nil];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kDSLocationManagerDidUpdateLocationNotificationName object:nil];
}

- (BOOL)shouldAutorotate {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (void)configureView {
    [self updateData];
    
    if (kIsSingleCollectionApp) {
        if (kDSForceAuthorName) {
            self.navigationItem.title = kDSForceAuthorName;
        } else {
            self.navigationItem.title = self.collection.user.displayName;
        }
    } else {
        self.navigationItem.title = @"Detail";
    }
    
    if (kDSForceAuthorName) {
        sourceLabel.text = kDSForceAuthorName;
    } else {
        sourceLabel.text = self.collection.user.displayName;
    }
    if (kDSForceAuthorImageName) {
        sourceImageView.image = [UIImage imageNamed:kDSForceAuthorImageName];
    } else {
        sourceImageView.image = [DataHelper imageWithFilePath:[DataHelper pathForCacheFile:self.collection.user.photoFile]];
    }
    if (kDSForceCollectionPhotoName) {
        photoImageView.image = [UIImage imageNamed:kDSForceCollectionPhotoName];
    } else {
        
        [DataHelper imageWithFilePath:[DataHelper pathForCacheFile:self.collection.photoFile] block:^(UIImage *image) {
            
            if (image) {
                
                photoImageView.image = image;
            } else {
                
                [DataHelper imageWithFilePath:[DataHelper pathForCacheFile:self.collection.thumbnailFile] block:^(UIImage *image) {
                    
                    if (image) {
                        
                        photoImageView.image = image;
                    }
                }];
            }
        }];
    }
    if (kDSForceCollectionTitle) {
        titleLabel.text = kDSForceCollectionTitle;
    } else {
        titleLabel.text = self.collection.title;
    }
    
    if (!kIsSingleCollectionApp) {
        if (self.collection.isOwnedByMe) {
            //UIBarButtonItem * editButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(editButtonPressed:)];
            //self.navigationItem.rightBarButtonItem = editButton;
        }
    }

    [self setLayout];
}

- (void)setLayout {
    CGRect frame;
    CGFloat positionY;
    
    frame = photoImageView.frame;
    frame.origin.y = 0;
    photoImageView.frame = frame;
    
    if (sourceImageView.image) {
        frame = sourceImageView.frame;
        frame.origin.y = 20;
        sourceImageView.frame = frame;
    }
    
    if (sourceLabel.text) {
        frame = sourceLabel.frame;
        if (sourceImageView.image) {
            frame.origin.x = sourceImageView.frame.origin.x + sourceImageView.frame.size.width + 10;
            frame.origin.y = (sourceImageView.frame.size.height / 2) - (frame.size.height / 2) + sourceImageView.frame.origin.y;
        } else {
            frame.origin.x = 20;
            frame.origin.y = 20;
        }
        sourceLabel.frame = frame;
    }
    
    if (titleLabel.text) {
        frame = [titleLabel autoAdjustHeight];
        frame.origin.y = photoImageView.frame.origin.y + photoImageView.frame.size.height - 20 - frame.size.height;
        titleLabel.frame = frame;
    }
    
    positionY = photoImageView.frame.origin.y + photoImageView.frame.size.height + 20;
}

#pragma mark - Setter & Getter

- (void)setCollection:(Collection *)collection {
    if (_collection != collection) {
        _collection = collection;
    }
    
    [self configureView];
}

#pragma mark - Helper

- (void)updateData {
    [self updateSortedSpots];
}

- (void)showSpot:(Spot *)spot {
    if (kIsSingleCollectionApp) {
        [self performSegueWithIdentifier:@"showCollectionMap" sender:spot];
    }
}

- (void)deleteCollection {
    if (self.collection.url && [NSURL URLWithString:self.collection.url]) {
        LRRestyClient * client = [LRResty client];
        [client setHandlesCookiesAutomatically:NO];
        
        [client attachRequestModifier:^(LRRestyRequest *request) {
            NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
            [request addHeader:@"Authorization" value:[NSString stringWithFormat:@"Token %@", [[defaults objectForKey:kDSDropspotSessionDomainKeyName] valueForKey:kDSDropspotSessionTokenKeyName]]];
        }];
        
        loadingOverlayView.hidden = NO;
        loadingOverlayView.alpha = 0.0;
        [self.view bringSubviewToFront:loadingOverlayView];
        
        [UIView animateWithDuration:0.25 animations:^{
            loadingOverlayView.alpha = 1.0;
        }];
        
        self.navigationItem.rightBarButtonItem = nil;
        
        [client delete:self.collection.url withBlock:^(LRRestyResponse *response) {
            if (response.status == 204 || response.status == 404) {
                [[DSGlobalData sharedData].childContext performBlock:^{
                    [[DSGlobalData sharedData].childContext deleteObject:self.collection];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [[NSNotificationCenter defaultCenter] postNotificationName:kDSDropspotCollectionDeletedNotificationName object:nil];
                        [self.navigationController popViewControllerAnimated:YES];
                    });
                }];
            } else {
                UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Oops..." message:@"There was a problem deleting this spot, please try again later." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
                
                NSLog(@"status code %lu", (unsigned long)response.status);
            }
        }];
    }
}

- (void)checkLocationStatus {
    if (kIsSingleCollectionApp) {
        if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorized || [CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined) {
            if (noLocationViewController && !noLocationViewController.isBeingDismissed) {
                [noLocationViewController dismissViewControllerAnimated:YES completion:^{
                    noLocationViewController = nil;
                }];
            }
        } else {
            if (!noLocationViewController) {
                noLocationViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"noLocation"];
            }
            if (self.presentedViewController == nil && !noLocationViewController.isBeingPresented) {
                [self presentViewController:noLocationViewController animated:YES completion:nil];
            }
        }
    }
}

- (void)updateSortedSpots {
    [[DSGlobalData sharedData].childContext performBlock:^{
        __block CLLocation * location = self.locationManager.location;
        if (!location) {
            [DSGlobal lastLocationUpdate:^(BOOL available, NSDate *date, CLLocation *_location) {
                location = _location;
            }];
        }
        _sortedSpots = [NSArray arrayWithArray:[self.collection spotsSortedByDistanceFromLocation:location
                                                                           inManagedObjectContext:[DSGlobalData sharedData].childContext]];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableView reloadData];
        });
    }];
}

- (void)updateLocationNotification:(NSNotification *)notification {
    [self.tableView reloadData];
}

- (void)updateLocation:(CLLocation *)location {
    if (kIsSingleCollectionApp) {
        // should update but not every 2 seconds
        
        NSDate * nowDate = [NSDate date];
        
        if ([nowDate timeIntervalSince1970] - [_lastLocationUpdate timeIntervalSince1970] > 5) {
            _lastLocationUpdate = nowDate;
            
            [self.tableView reloadData];
        }
    }
}

- (Collection *)fetchCollectionInManagedObjectContext:(NSManagedObjectContext *)context {
    NSFetchRequest * request = [[NSFetchRequest alloc] initWithEntityName:@"Collection"];
    if (kDSOwnCollectionApp) {
        request.predicate = [NSPredicate predicateWithFormat:@"slug = %@", kOwnCollectionSlug];
    } else {
        request.predicate = [NSPredicate predicateWithFormat:@"slug = %@", kCollectionSlug];
    }
    NSArray * matches = [context executeFetchRequest:request error:nil];
    if (matches && matches.count > 0) {
        return matches[0];
    }
    return nil;
}

- (void)setLoadingOverlayHidden:(BOOL)hidden {
    if (hidden) {
        self.navigationItem.rightBarButtonItem.enabled = YES;
        loadingOverlayView.userInteractionEnabled = NO;
        [UIView animateWithDuration:0.2f animations:^{
            loadingOverlayView.alpha = 0.0;
        } completion:^(BOOL finished) {
            loadingOverlayView.hidden = YES;
        }];
    } else {
        self.navigationItem.rightBarButtonItem.enabled = NO;
        [self.view bringSubviewToFront:loadingOverlayView];
        loadingOverlayView.alpha = 0.0;
        loadingOverlayView.hidden = NO;
        loadingOverlayView.userInteractionEnabled = YES;
        [UIView animateWithDuration:0.25f animations:^{
            loadingOverlayView.alpha = 1.0;
        } completion:nil];
    }
}

- (void)unsubscribeCollection {
    [[DSDropspot session] unsubscribeFromCollectionWithSlug:self.collection.slug block:^(NSDictionary *result, NSError *error) {
        if (!error) {
            NSLog(@"Error unsubscribing: %@", error.localizedDescription);
        }
    }];
    [[DSGlobalData sharedData].childContext performBlock:^{
        [[DSGlobalData sharedData].childContext deleteObject:self.collection];
        [[DSGlobalData sharedData].childContext save:nil];
    }];
}

#pragma mark - UITableView delegate & dataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1 + _sortedSpots.count;// + 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell * cell;
    if (indexPath.row == 0) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"descriptionCell" forIndexPath:indexPath];
    } else if (indexPath.row <= _sortedSpots.count) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"spotCell" forIndexPath:indexPath];
    } else {
        cell = [tableView dequeueReusableCellWithIdentifier:@"bottomCell" forIndexPath:indexPath];
    }
    
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        return [CollectionTopCell heightForCellWithDescription:self.collection.text subscribeButton:NO/*(!kIsSingleCollectionApp?!self.collection.subscribed:NO)*/ addSpotButton:(kDSOwnCollectionApp != nil) activateSwitch:(!kIsSingleCollectionApp)];
    } else if (indexPath.row <= _sortedSpots.count) {
        Spot * spot = _sortedSpots[indexPath.row - 1];
        return [SpotCell heightForCellWithSpot:spot style:(kDSTileViewStyleBig)?DSTileViewStyleBig:DSTileViewStyleSmall info:((kDSHideBeenThereDoneThat)?spot.address:((spot.seen)?@"Been there, done that!":@"")) time:NO];
    } else {
        return [CollectionBottomCell heightForCellUnsubscribeButton:(!kIsSingleCollectionApp) settingsButton:kIsSingleCollectionApp];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        if ([cell isKindOfClass:[CollectionTopCell class]]) {
            CollectionTopCell * topCell = (CollectionTopCell *)cell;
            topCell.descriptionLabel.text = self.collection.text;
            //if (kIsSingleCollectionApp) {
                topCell.subscribeButton.hidden = YES;
            //} else {
            //    topCell.subscribeButton.hidden = self.collection.subscribed;
            //}
            topCell.activateLabel.hidden = NO;
            topCell.activateSwitch.on = self.collection.active;
            [topCell.activateSwitch addTarget:self action:@selector(activateSwitchPressed:) forControlEvents:UIControlEventValueChanged];
            topCell.addSpotButton.hidden = (kDSOwnCollectionApp == nil);
            [topCell.subscribeButton addTarget:self action:@selector(subscribeButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
            [topCell.addSpotButton addTarget:self action:@selector(createSpotButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
            [topCell setLayout];
        }
    } else if (indexPath.row <= _sortedSpots.count) {
        if ([cell isKindOfClass:[SpotCell class]]) {
            Spot * spot = _sortedSpots[indexPath.row - 1];
            SpotCell * spotCell = (SpotCell *)cell;
            DSTileView * view = spotCell.tileView;
            if (kDSTileViewStyleBig) {
                view.style = DSTileViewStyleBig;
                
                [view.inviteButton addTarget:self action:@selector(inviteButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
            } else {
                view.style = DSTileViewStyleSmall;
            }

            view.image = nil;
            [DataHelper imageWithFilePath:[DataHelper pathForCacheFile:spot.thumbnailFile] block:^(UIImage *image) {
                if (image) {
                    
                    view.image = image;
                } else {
                    
                    [DataHelper imageWithFilePath:[DataHelper pathForCacheFile:spot.photoFile] block:^(UIImage *image) {
                        if (image) {
                            
                            view.image = image;
                        }
                    }];
                }
            }];
            
            view.text = spot.text;
            
            __block CLLocation * location = self.locationManager.location;
            if (!location) {
                [DSGlobal lastLocationUpdate:^(BOOL available, NSDate *date, CLLocation *_location) {
                    location = _location;
                }];
            }
            
            float distance = [spot.location distanceFromLocation:location];
            [DSGlobal distancePropertiesFromDistance:distance properties:^(double value, NSString *measurement) {
                if ([measurement isEqualToString:[DSGlobal lowestMeasurement]]) {
                    view.distance = [NSString stringWithFormat:@"%.0f%@", value, measurement];
                } else {
                    view.distance = [NSString stringWithFormat:@"%.2f%@", value, measurement];
                }
            }];
            [view addTarget:self selector:@selector(tileViewPressed:)];
            
            if (kDSHideBeenThereDoneThat) {
                view.infoStyle = DSTileViewInfoStyleNormal;
                view.info = spot.address;
            } else {
                if (spot.seen) {
                    view.infoStyle = DSTileViewInfoStyleGreenBordered;
                    //view.info = translate(@"TILE_VIEW_LABEL_BEEN_THERE");
                } else {
                    view.infoStyle = DSTileViewInfoStyleNormal;
                    view.info = @"";
                }
            }
            
            view.tag = indexPath.row - 1;
            
            NSString * labelString = nil;
            
            if (spot.collections.count == 0) {
                labelString = spot.slug;
            } else {
                labelString = [NSString stringWithFormat:@"%@/%@", [(Collection *)spot.collections.anyObject slug], spot.slug];
            }
        }
    } else {
        if ([cell isKindOfClass:[CollectionBottomCell class]]) {
            CollectionBottomCell * bottomCell = (CollectionBottomCell *)cell;
            //if (kIsSingleCollectionApp) {
                bottomCell.unsubscribeButton.hidden = YES;
                bottomCell.deleteButton.hidden = self.collection.isOwnedByMe;
            //} else {
            //    bottomCell.unsubscribeButton.hidden = !self.collection.subscribed;
            //}
            bottomCell.settingsButton.hidden = (!kIsSingleCollectionApp);
            [bottomCell.unsubscribeButton addTarget:self action:@selector(subscribeButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
            [bottomCell.settingsButton addTarget:self action:@selector(settingsButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
            [bottomCell.deleteButton addTarget:self action:@selector(deleteButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
            [bottomCell setLayout];
        }
    }
}

#pragma mark - Collection map view controller

- (void)collectionMapViewControllerDidCancelSpot:(CollectionMapViewController *)collectionMapViewController {
    [self updateSortedSpots];

    collectionMapViewController.spots = _sortedSpots;
    collectionMapViewController.selectedSpot = nil;
    if (_sortedSpots.count > 0) {
        collectionMapViewController.selectedSpot = _sortedSpots[0];
    }
}

- (Collection *)collectionForCreationInMapViewController:(CollectionMapViewController *)collectionMapViewController {
    return self.collection;
}

#pragma mark - Handle session

- (void)checkSessionState {
    if ([DSUser currentUser]) {
        [self refresh];
        if (signInViewController.presentingViewController != nil) {
            [signInViewController dismissViewControllerAnimated:YES completion:nil];
        }
    } else {
        [self showSignInView];
    }
}

- (void)logout {
    [[DSDropspot session] closeSession];
    [self checkSessionState];
}

- (void)showSignInView {
    if (!signInViewController) {
        signInViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"signIn"];
    }
    if (self.presentedViewController != signInViewController) {
        if (self.presentedViewController != nil) {
            [self.presentedViewController dismissViewControllerAnimated:YES completion:^{
                [self presentViewController:signInViewController animated:YES completion:nil];
            }];
        } else {
            [self presentViewController:signInViewController animated:YES completion:nil];
        }
    }
}

#pragma mark - Data

- (void)refresh {
    [self refreshManually:NO];
}

- (void)refreshManually:(BOOL)manually {
    if (!manually && [DSGlobal sharedGlobals].lastSyncDate) {
        if ([NSDate date].timeIntervalSince1970 - [DSGlobal sharedGlobals].lastSyncDate.timeIntervalSince1970 < 10800) {
            return;
        }
    }
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus != NotReachable) {
//        [DSSyncController sharedController].delegate = self;
//        [[DSSyncController sharedController] synchronize];
        
        [self setLoadingOverlayHidden:NO];
    } else {
        NSLog(@"You have no internet connection");
    }
}

- (void)syncController:(DSSyncController *)syncController didFinishWithErrors:(NSArray *)errors {
    if (errors.count > 0) {
        for (NSError * error in errors) {
            NSLog(@"err %@", error.localizedDescription);
            if (error.code == kDSDropspotAPIErrorCodeNotAuthorized) {
                [[DSDropspot session] closeSession];
                [[DSDropspot session] checkSession];
                break;
            }
        }
    } else {
        [[DSGlobalData sharedData].childContext performBlock:^{
            if (!self.collection) {
                self.collection = [self fetchCollectionInManagedObjectContext:[DSGlobalData sharedData].childContext];
                if (self.collection) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self setLoadingOverlayHidden:YES];
                        [self configureView];
                    });
                }
            } else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self setLoadingOverlayHidden:YES];
                    [self configureView];
                });
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                [self updateSortedSpots];
            });
        }];
    }
}

- (void)syncController:(DSSyncController *)syncController didFinishLoadingCollection:(Collection *)collection {
}

- (void)syncController:(DSSyncController *)syncController didFinishLoadingSpot:(Spot *)spot {
}

#pragma mark - Gesture recognizer

- (IBAction)settingsButtonPressed:(id)sender {
    [self performSegueWithIdentifier:@"showSettings" sender:nil];
}

- (void)tileViewPressed:(DSTileView *)sender {
    Spot * spot = _sortedSpots[sender.tag];
    [self performSegueWithIdentifier:@"showCollectionMap" sender:spot];
}

#pragma mark - IBActions

- (IBAction)subscribeButtonPressed:(id)sender {
    self.collection.subscribed = !self.collection.subscribed;
    
    if (self.collection.subscribed) {
        [[DSDropspot session] subscribeToCollectionWithSlug:self.collection.slug block:^(NSDictionary *result, NSError *error) {
            [[NSNotificationCenter defaultCenter] postNotificationName:kDSDropspotCollectionSubscriptionChanged object:nil userInfo:@{ @"collection" : self.collection }];
        }];
    } else {
        [[DSDropspot session] unsubscribeFromCollectionWithSlug:self.collection.slug block:^(NSDictionary *result, NSError *error) {
            [[NSNotificationCenter defaultCenter] postNotificationName:kDSDropspotCollectionSubscriptionChanged object:nil userInfo:@{ @"collection" : self.collection }];
        }];
    }
    [self configureView];
}

- (IBAction)activateSwitchPressed:(UISwitch *)sender {
    [[DSGlobalData sharedData].childContext performBlock:^{
        self.collection.active = sender.on;
        
        [[DSGlobalData sharedData].childContext save:nil];
    }];
}

- (void)inviteButtonPressed:(UIButton *)button {
    Spot * spot = _sortedSpots[button.superview.tag];
    
    MFMessageComposeViewController * viewController = [[MFMessageComposeViewController alloc] init];
    if ([MFMessageComposeViewController canSendText]) {
        NSArray * addressParts = [spot.address componentsSeparatedByString:@","];
        if (addressParts.count >= 2) {
            NSMutableString * shortAddressString = [[addressParts[0] stringByAppendingFormat:@", %@", [addressParts[1] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]] mutableCopy];
            viewController.body = [NSString stringWithFormat:@"Ich geh kickern! Wo? %@.\nBiste dabei?", shortAddressString];
        } else {
            viewController.body = [NSString stringWithFormat:@"Ich geh kickern! Wo? %@.\nBiste dabei?", spot.address];
        }
        viewController.messageComposeDelegate = self;
        [self presentViewController:viewController animated:YES completion:nil];
    }
}

- (void)mapButtonPressed:(id)sender {
    if (_sortedSpots.count > 0) {
        [self performSegueWithIdentifier:@"showCollectionMap" sender:_sortedSpots[0]];
    } else {
        [self performSegueWithIdentifier:@"showCollectionMap" sender:nil];
/*
        if (kDSNoQRCodeSpots) {
            [[[UIAlertView alloc] initWithTitle:@"There are no places where people use QR-Codes." message:nil delegate:nil cancelButtonTitle:@"I knew it..." otherButtonTitles:nil] show];
        }
*/
    }
}

- (IBAction)deleteButtonPressed:(id)sender {
    if (self.collection.subscribed) {
        UIActionSheet * sheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:translate(@"DETAIL_VIEW_CONTROL_BUTTON_TITLE_DELETE") otherButtonTitles:nil];
        sheet.tag = kActionSheetTagDelete;
        [sheet showInView:self.view];
    }
}

- (Spot *)createSpotAtLocationCoordinate:(CLLocationCoordinate2D)coordinate
                  inManagedObjectContext:(NSManagedObjectContext *)context {
    
    Spot * spot = [Spot spotWithLatitude:coordinate.latitude
                               longitude:coordinate.longitude
                  inManagedObjectContext:context];
    
    // Add the author and author image.
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    if ([defaults objectForKey:kDSDropspotSessionDomainKeyName]) {
        User * user = [NSEntityDescription insertNewObjectForEntityForName:@"User" inManagedObjectContext:context];
        user.displayName = [[defaults objectForKey:kDSDropspotSessionDomainKeyName] valueForKey:kDSDropspotSessionNameKeyName];
        
        NSData * data = [[defaults objectForKey:kDSDropspotSessionDomainKeyName] valueForKey:kDSDropspotSessionImageKeyName];
        if (data) {
            NSString * fileName = @"user-avatar.png";
            [data writeToFile:[DataHelper pathForCacheFile:fileName] atomically:NO];
            user.photoFile = fileName;
        }
        spot.user = user;
    }
    
    // Search asynchronously for the address
    [DSServices geocodeBySearch:[NSString stringWithFormat:@"%f,%f", spot.latitude, spot.longitude] completion:^(NSArray *array) {
        if (array && array.count > 0) {
            NSString * address = array[0][@"formatted_address"];
            spot.address = address;
        }
        spot.isDeterminingAddress = NO;
        [[NSNotificationCenter defaultCenter] postNotificationName:kDSSpotDidDetermineAddressNotificationName object:nil userInfo:@{ @"spot" : spot }];
    }];
    
    return spot;
}

- (IBAction)createSpotButtonPressed:(id)sender {
    [[DSGlobalData sharedData].childContext performBlock:^{
        __block CLLocation * location = self.locationManager.location;
        if (!location) {
            [DSGlobal lastLocationUpdate:^(BOOL available, NSDate *date, CLLocation *_location) {
                location = _location;
            }];
        }
        
        Spot * spot = [self createSpotAtLocationCoordinate:location.coordinate
                                    inManagedObjectContext:[DSGlobalData sharedData].childContext];
        spot.collections = [NSSet setWithObject:self.collection];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self updateSortedSpots];
            [self performSegueWithIdentifier:@"showCollectionMap" sender:spot];
        });
    }];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showCollectionMap"]) {
        CollectionMapViewController * mapViewController = segue.destinationViewController;
        mapViewController.delegate = self;
        mapViewController.collection = self.collection;
        mapViewController.spots = _sortedSpots;
        if (sender && [sender isKindOfClass:[Spot class]]) {
            mapViewController.selectedSpot = sender;
        }
    }
}

#pragma mark - UIActionSheet delegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (actionSheet.tag == kActionSheetTagDelete) {
        if (buttonIndex == 0) {
            [self unsubscribeCollection];
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}

#pragma mark - UIScrollView delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat offset = scrollView.contentOffset.y + scrollView.contentInset.top;
    if (offset < 0) {
        CGRect frame = photoImageView.frame;
        frame.origin.y = offset;
        frame.size.height = coverView.frame.size.height - offset;
        photoImageView.frame = frame;
        
        frame = sourceImageView.frame;
        frame.origin.y = 20 + offset;
        sourceImageView.frame = frame;
        
        frame = sourceLabel.frame;
        
        if (sourceImageView.image) {
            frame.origin.y = (sourceImageView.frame.size.height / 2) - (frame.size.height / 2) + sourceImageView.frame.origin.y;
        } else {
            frame.origin.y = 20 + offset;
        }
        
        sourceLabel.frame = frame;
    } else {
        CGRect frame = photoImageView.frame;
        frame.origin.y = 0;
        frame.size.height = coverView.frame.size.height;
        photoImageView.frame = frame;
        
        if (sourceImageView.image) {
            frame = sourceImageView.frame;
            frame.origin.y = 20;
            sourceImageView.frame = frame;
        }
        
        if (sourceLabel.text) {
            frame = sourceLabel.frame;
            if (sourceImageView.image) {
                frame.origin.y = (sourceImageView.frame.size.height / 2) - (frame.size.height / 2) + sourceImageView.frame.origin.y;
            } else {
                frame.origin.y = 20;
            }
            sourceLabel.frame = frame;
        }
    }
}

#pragma mark - Message Compose Delegate

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result {
    if (result == MessageComposeResultFailed) {
        [[[UIAlertView alloc] initWithTitle:@"Sorry" message:@"Something went wrong. Please try again." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    } else if (result == MessageComposeResultSent) {
        [[[UIAlertView alloc] initWithTitle:@"Success!" message:@"Message sent." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        [controller dismissViewControllerAnimated:YES completion:nil];
    } else {
        [controller dismissViewControllerAnimated:YES completion:nil];
    }
}
@end

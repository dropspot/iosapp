/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  Collection+Dropspot.h
//  Dropspot3
//
//  Created by Lukas Würzburger on 04.09.13.
//  Copyright (c) 2013 Dropspot. All rights reserved.
//

#import "Collection.h"
#import "DSDropspot.h"

@interface Collection (Dropspot)

+ (Collection *)collectionWithAPIResponse:(DSAPICollectionResponse *)response
                   inManagedObjectContext:(NSManagedObjectContext *)context;

- (void)updateWithAPIResponse:(DSAPICollectionResponse *)response
       inManagedObjectContext:(NSManagedObjectContext *)context;

- (void)fillWithResponse:(DSAPICollectionResponse *)response
  inManagedObjectContext:(NSManagedObjectContext *)context;

// Photos
- (void)resumeLoadingThumbnailInManagedObjectContext:(NSManagedObjectContext *)context completion:(void (^)(void))completion;
- (void)resumeLoadingPhotoInManagedObjectContext:(NSManagedObjectContext *)context completion:(void (^)(void))completion;

@end

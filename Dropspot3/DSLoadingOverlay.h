/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  DSLoadingOverlay.h
//  Dropspot3
//
//  Created by Lukas Würzburger on 4/3/14.
//  Copyright (c) 2014 Dropspot GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DSLoadingOverlay : UIView

@property (nonatomic, strong) NSString * loadingString;

- (instancetype)initWithLoadingString:(NSString *)loadingString userInteractionEnabled:(BOOL)userInteractionEnabled;
- (void)showInView:(UIView *)view;
- (void)hide;

@end

/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  DSAnnotationView.m
//  dropspotApp
//
//  Created by Fritz on 22.05.13.
//  Copyright (c) 2013 dropspot GmbH. All rights reserved.
//

#import "DSMapAnnotationView.h"

#import "DSMapViewAnnotation.h"
#import "Spot.h"

#define kCalloutImageViewOffset CGPointMake(-6.0f, -30.0f)

@interface DSMapAnnotationView () {
    UIImageView * _bouncingCalloutView;
    UIImageView * _calloutView;
    UIImageView * _originImageView;
}

@end

@implementation DSMapAnnotationView

#pragma mark - Initialization

- (id)initWithAnnotation:(id<MKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier];
    if (self) {
        DSMapViewAnnotation * ann = annotation;
        if (ann.selectable) {
            self.alpha = kDSMapAnnotationViewAlphaNormal;
            self.draggable = ann.spot.isOwnedByMe;
        } else {
            self.alpha = kDSMapAnnotationViewAlphaNotSelectable;
            self.draggable = NO;
        }
        self.canShowCallout = NO;
        
        [self update];
    }
    return self;
}

- (void)setAnnotation:(id<MKAnnotation>)annotation {
    super.annotation = annotation;
    
    [self update];
}

- (void)didAddSubview:(UIView *)subview {
    if ([[[subview class] description] isEqualToString:@"UICalloutView"]) {
        for (UIView *subsubView in subview.subviews) {
            if ([subsubView class] == [UIImageView class]) {
                UIImageView *imageView = ((UIImageView *)subsubView);
                [imageView removeFromSuperview];
            } else if ([subsubView class] == [UILabel class]) {
                UILabel *labelView = ((UILabel *)subsubView);
                [labelView removeFromSuperview];
            }
        }
    }
}

#pragma mark - Setter & Getter

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    if (selected) {
        self.calledOut = YES;
    }
}

- (void)setIsDragging:(BOOL)isDragging {
    _isDragging = isDragging;
    
    if (self.isDragging) {
        self.calledOut = YES;

        _originImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"dropMovingCross.png"]];
        _originImageView.frame = CGRectMake(1, -47, 11, 8);
        [self addSubview:_originImageView];
        
        CGRect frame = _calloutView.frame;
        frame.origin.y = kCalloutImageViewOffset.y - 67;
        _calloutView.frame = frame;
    } else {
        if (_originImageView) {
            [_originImageView removeFromSuperview];
            _originImageView = nil;
        }
        CGRect frame = _calloutView.frame;
        frame.origin.y = kCalloutImageViewOffset.y - 3;
        _calloutView.frame = frame;
    }
}

- (void)setCalledOut:(BOOL)calledOut {
    if (calledOut != _calledOut) {
        _calledOut = calledOut;
        
        DSMapViewAnnotation * ann = self.annotation;
        
        if (self.selected && ann.selectable) {
            if (_calloutView.superview == self) {
                [_calloutView removeFromSuperview];
                _calloutView = nil;
            }
            
            self.alpha = kDSMapAnnotationViewAlphaSelected;
            
            if (ann.spot) {
                if (ann.spot.active) {
                    _calloutView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"drop.png"]];
                    self.image = [UIImage imageWithCGImage:[UIImage imageNamed:@"dropButton.png"].CGImage scale:3 orientation:UIImageOrientationUp];
                } else {
                    if (ann.spot.seen) {
                        _calloutView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"dropCustom.png"]];
                        self.image = [UIImage imageWithCGImage:[UIImage imageNamed:@"dropButtonCustom.png"].CGImage scale:3 orientation:UIImageOrientationUp];
                    } else {
                        _calloutView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"dropInactive.png"]];
                        self.image = [UIImage imageWithCGImage:[UIImage imageNamed:@"dropButtonInactive.png"].CGImage scale:3 orientation:UIImageOrientationUp];
                    }
                }
            }
            
            [_calloutView setFrame:CGRectMake(kCalloutImageViewOffset.x, kCalloutImageViewOffset.y, 0.0f, 0.0f)];
            [_calloutView sizeToFit];
            
            [self addSubview:_calloutView];
            _calloutView.hidden = NO;
            [self setShowsCallout:YES];
        } else {
            [self setShowsCallout:NO];
        }
    }
}

#pragma mark - Helper

- (void)update {
    DSMapViewAnnotation * ann = self.annotation;
    if (ann.spot) {
        if (ann.spot.active) {
            _calloutView.image = [UIImage imageNamed:@"drop.png"];
            self.image = [UIImage imageWithCGImage:[UIImage imageNamed:@"dropButton.png"].CGImage scale:3 orientation:UIImageOrientationUp];
        } else {
            if (ann.spot.seen) {
                _calloutView.image = [UIImage imageNamed:@"dropCustom.png"];
                self.image = [UIImage imageWithCGImage:[UIImage imageNamed:@"dropButtonCustom.png"].CGImage scale:3 orientation:UIImageOrientationUp];
            } else {
                _calloutView.image = [UIImage imageNamed:@"dropInactive.png"];
                self.image = [UIImage imageWithCGImage:[UIImage imageNamed:@"dropButtonInactive.png"].CGImage scale:3 orientation:UIImageOrientationUp];
            }
        }
    }
}

- (void)setShowsCallout:(BOOL)showsCallout animated:(BOOL)animated {
    CGFloat scale = 0.001f;
    
    if (showsCallout) {
        _calloutView.transform = CGAffineTransformMake(scale, 0.0f, 0.0f, scale, -10.0f, 15.0f);
        
        [UIView animateWithDuration:(animated ? 0.15f : 0.0f) animations:^{
            CGFloat scale = 1.1f;
            _calloutView.transform = CGAffineTransformMake(scale, 0.0f, 0.0f, scale, -2.0f, -5.0f);
        } completion:^(BOOL finished) {
            if (finished) {
                [UIView animateWithDuration:(animated ? 0.10f : 0.0f) animations:^{
                    CGFloat scale = 1.0f;
                    _calloutView.transform = CGAffineTransformMake(scale, 0.0f, 0.0f, scale, -3.0f, -3.0f);
                }];
            }
        }];
    } else {
        [UIView animateWithDuration:(animated ? 0.15f : 0.0f) animations:^{
            CGFloat scale = 0.001f;
            
            _calloutView.transform = CGAffineTransformMake(scale, 0.0f, 0.0f, scale, -10.0f, 15.0f);
        } completion:^(BOOL finished) {
            if (finished) {
                [_calloutView removeFromSuperview];
                
                self.alpha = kDSMapAnnotationViewAlphaNormal;
            }
        }];
    }
}

- (void)setShowsCallout:(BOOL)showsCallout {
    [self setShowsCallout:showsCallout animated:YES];
}

@end

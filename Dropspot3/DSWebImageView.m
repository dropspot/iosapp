/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  DSWebImageView.m
//  Dropspot3
//
//  Created by Lukas Würzburger on 5/13/14.
//  Copyright (c) 2014 Dropspot GmbH. All rights reserved.
//

#import "DSWebImageView.h"

#import "DataHelper.h"
#import "DSGlobalData.h"

@implementation DSWebImageView

- (void)setImageWithURL:(NSURL *)url placeholder:(UIImage *)placeholder {
    [self setImageWithURL:url placeholder:placeholder completion:nil];
}

- (void)setImageWithURL:(NSURL *)url placeholder:(UIImage *)placeholder completion:(void(^)(void))completion {
    
    NSLog(@"urlFileString %@", url.absoluteString);
    
    // Convert url into file system readable string.
    NSString * urlFileString = DSURLToFileName([url absoluteString]);
    
    NSLog(@"urlFileString %@", urlFileString);
    
    NSString * filePath = [DataHelper pathForCacheFile:urlFileString];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {

        NSData * data = [NSData dataWithContentsOfFile:filePath];
        if (data) {
            
            UIImage * image = [UIImage imageWithData:data];
            if (image) {
                
                self.image = image;
                self.contentMode = UIViewContentModeScaleAspectFill;
            } else {
                
                self.image = placeholder;
                self.contentMode = UIViewContentModeCenter;
                [[NSFileManager defaultManager] removeItemAtPath:filePath error:nil];
            }
        } else {
            
            self.image = placeholder;
            self.contentMode = UIViewContentModeCenter;
            [[NSFileManager defaultManager] removeItemAtPath:filePath error:nil];
        }
        
        if (completion) {
            completion();
        }
    } else {
        
        self.image = placeholder;
        self.contentMode = UIViewContentModeCenter;
        
        __weak DSWebImageView * weakSelf = self;
        
        [[DSGlobalData sharedData] downloadStuffFromURL:url withCompletion:^(NSData *data, NSError *error) {
        
            if (error) {
            
                NSLog(@"downloadStuffFromURL:withCompletion: Error: %@", error.localizedDescription);
            } else {
                
                if (data) {
                    [data writeToFile:filePath atomically:NO];
                
                    UIImage * image = [UIImage imageWithData:data];
                    if (image) {
                    
                        weakSelf.image = image;
                        weakSelf.contentMode = UIViewContentModeScaleAspectFill;
                        
                        NSLog(@"download done %@", weakSelf);
                    }
                }
            }
            if (completion) {
                completion();
            }
        }];
    }
}

@end

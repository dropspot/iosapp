/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  DSLocationPermissionViewController.h
//  Dropspot3
//
//  Created by Lukas Würzburger on 4/15/14.
//  Copyright (c) 2014 Dropspot GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@class DSLocationPermissionViewController;

@protocol DSLocationPermissionViewControllerDelegate <NSObject>

- (void)locationPermissionViewControllerDidAllow:(DSLocationPermissionViewController *)locationPermissionViewController;
- (void)locationPermissionViewControllerDidDeny:(DSLocationPermissionViewController *)locationPermissionViewController;

@end

@interface DSLocationPermissionViewController : UIViewController

@property (nonatomic, assign) id <DSLocationPermissionViewControllerDelegate> delegate;

@end

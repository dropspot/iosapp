/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  AddressViewController.h
//  Dropspot3
//
//  Created by Lukas Würzburger on 1/20/14.
//  Copyright (c) 2014 Dropspot. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AddressViewController;

@protocol AddressViewControllerDelegate <NSObject>

- (void)addressViewController:(AddressViewController *)addressViewController didSaveWithAddress:(NSString *)addressString;

@end

@interface AddressViewController : UIViewController

@property (nonatomic, strong) id <AddressViewControllerDelegate> delegate;

@end

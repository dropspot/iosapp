/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  DSGlobal.h
//  Dropspot3
//
//  Created by Lukas Würzburger on 12.09.13.
//  Copyright (c) 2013 Dropspot. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "GAIHeader.h"

// Colors

#define kDSColorCoral [UIColor colorWithRed:1.0 green:118.0/255.0 blue:111.0/255.0 alpha:1.0]
#define kDSColorSpotSeenGreen [UIColor colorWithRed:0.0 green:0.4 blue:0.0 alpha:1.0]

// UI Actions

#define kCollectionPressedNotificationName @"kCollectionPressedNotificationName"
#define kSelectSpotNotificationName @"kSelectSpotNotificationName"
#define kDSFacebookLoginButtonPressedDateKey @"kDSFacebookLoginButtonPressedDateKey"

// Location manager

#define kDSLocationManagerDidUpdateLocationNotificationName @"kDSLocationManagerDidUpdateLocationNotificationName"
#define kDSLocationManagerDidUpdateLocationKey @"kDSLocationManagerDidUpdateLocationKey"

// Constants

#define kScreenHeight [[UIScreen mainScreen] applicationFrame].size.height

// UserDefaults keys

#define kDSNotificationFrequencyDefaultsKey @"kDSNotificationFrequencyDefaultsKey"
#define kDSNotificationMeasurementDefaultsKey @"kDSNotificationMeasurementDefaultsKey"
#define kDSReadaloudDefaultsKey @"kDSReadaloudDefaultsKey"

#define kDSNotificationFrequencyDefaultValue 2
#define kDSNotificationMeasurementDefaultValue 0

// Bundle macros

#define kBaseURLWithPath(path)([NSString stringWithFormat:@"%@%@", [[NSBundle mainBundle] objectForInfoDictionaryKey:@"DSBaseURL"], path])
#define kCollectionsURLWithAuthor(author)([NSString stringWithFormat:@"%@users/%@/collections%@", [[NSBundle mainBundle] objectForInfoDictionaryKey:@"DSBaseURL"], author, kDefaultExtension])
#define kCollectionSlug [[NSBundle mainBundle] objectForInfoDictionaryKey:@"DSCollectionSlug"]
#define kDefaultExtension [[NSBundle mainBundle] objectForInfoDictionaryKey:@"DSDefaultURLExtension"]
#define kCollectionURL [NSString stringWithFormat:@"%@collections/%@%@", [[NSBundle mainBundle] objectForInfoDictionaryKey:@"DSBaseURL"], kCollectionSlug, kDefaultExtension]
#define kCollectionURLWithSlug(slug) ([NSString stringWithFormat:@"%@collections/%@%@", [[NSBundle mainBundle] objectForInfoDictionaryKey:@"DSBaseURL"], slug, kDefaultExtension])
#define kOwnCollectionSlug [[[NSUserDefaults standardUserDefaults] objectForKey:kDSDropspotSessionDomainKeyName] objectForKey:kDSDropspotSessionCollectionSlugNameKeyName]

typedef NS_ENUM(NSUInteger, DSNotificationFrequency) {
    DSNotificationFrequencyOff = 0,
    DSNotificationFrequencyFew = 1,
    DSNotificationFrequencyMany = 2
};

typedef NS_ENUM(NSUInteger, DSMeasurement) {
    DSMeasurementAuto = 0,
    DSMeasurementMiles = 1,
    DSMeasurementMeters = 2
};

@interface DSGlobal : NSObject

@property (nonatomic, readonly) BOOL isLoading;
@property (nonatomic) DSNotificationFrequency notificationFrequency;
@property (nonatomic) DSMeasurement measurement;
@property (nonatomic, assign) BOOL readaloud;
@property (nonatomic, strong) NSDate * lastSyncDate;

@property (nonatomic, strong) NSMutableArray * receivedNotificationSpots;

+ (DSGlobal *)sharedGlobals;

- (void)addLoadingStackObject:(NSString *)key;
- (void)removeFromLoadingStack:(NSString *)key;

+ (NSString *)lowestMeasurement;
+ (void)distancePropertiesFromDistance:(float)distance properties:(void (^)(double value, NSString * measurement))properties;
+ (void)lastLocationUpdate:(void (^)(BOOL available, NSDate * date, CLLocation * location))locationUpdate;

- (void)analyticsView:(NSString *)viewString;
- (void)analyticsEvent:(NSString *)eventString category:(NSString *)categoryString;
- (void)analyticsEvent:(NSString *)eventString category:(NSString *)categoryString info:(NSString *)info;
- (void)analyticsEvent:(NSString *)eventString category:(NSString *)categoryString info:(NSString *)info number:(NSNumber *)number;

@end

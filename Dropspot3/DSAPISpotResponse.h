/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  DSAPISpotResponse.h
//  Dropspot3
//
//  Created by Lukas Würzburger on 10/30/13.
//  Copyright (c) 2013 Dropspot. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "DSAPIUserResponse.h"

@interface DSAPISpotResponse : NSObject

@property (nonatomic, retain) NSString * url;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * slug;
@property (nonatomic, retain) NSString * photoURL;
@property (nonatomic, retain) NSString * thumbnailURL;
@property (nonatomic, retain) NSString * address;
@property (nonatomic) double latitude;
@property (nonatomic) double longitude;
@property (nonatomic, retain) NSString * created;
@property (nonatomic, retain) NSString * modified;
@property (nonatomic, retain) NSString * expiresAt;
@property (nonatomic, retain) NSString * collectionSlugs;
@property (nonatomic, assign) BOOL isOwnedByMe;
@property (nonatomic, assign) BOOL subscribed;
@property (nonatomic, retain) DSAPIUserResponse * user;
@property (nonatomic, retain) NSArray * links;
@property (nonatomic, retain) NSString * linksURL;

@end

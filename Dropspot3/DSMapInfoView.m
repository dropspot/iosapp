/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  DSMapInfoView.m
//  Dropspot3
//
//  Created by Lukas Würzburger on 07.10.13.
//  Copyright (c) 2013 Dropspot. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

#import "DSMapInfoView.h"

#import "DSMapInfoItemViewController.h"

#import "DSDropspot.h"

@interface DSMapInfoView () <UIPageViewControllerDataSource, UIPageViewControllerDelegate> {
    UIPageViewController * thePageViewController;
}

@end

@implementation DSMapInfoView

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.hidden = YES;
        self.backgroundColor = [UIColor clearColor];
        self.layer.masksToBounds = NO;
        
        thePageViewController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
        thePageViewController.view.frame = CGRectMake(0, 0, frame.size.width, frame.size.height);
        thePageViewController.dataSource = self;
        thePageViewController.delegate = self;
        [self addSubview:thePageViewController.view];
    }
    return self;
}

- (void)reloadData {
    
    // Give me the last selected view controller.
    id vc = [self lastSelectedPageViewController];
    if (vc) {
        [thePageViewController setViewControllers:@[vc]
                                        direction:UIPageViewControllerNavigationDirectionForward
                                         animated:NO
                                       completion:nil];
        [self adjustHeight];
        
        [thePageViewController.view setHidden:NO];
    } else {
        
        // If the last selected view controller is nil, give me the view controller at index 0.
        vc = [self pageViewControllerForIndex:0];
        if (vc) {
            [thePageViewController setViewControllers:@[vc]
                                            direction:UIPageViewControllerNavigationDirectionForward
                                             animated:NO
                                           completion:nil];
            [self adjustHeight];
            
            [thePageViewController.view setHidden:NO];
        } else {
            
            // If there is no data hide the map info view.
            [thePageViewController.view setHidden:YES];
        }
    }
}

- (void)selectSpot:(Spot *)spot atIndex:(NSUInteger)index {

    [self selectSpot:spot atIndex:index animated:YES];
}

- (void)selectSpot:(Spot *)spot atIndex:(NSUInteger)index animated:(BOOL)animated {
    int lastIndex = -1;
    
    DSMapInfoItemViewController * vc = (DSMapInfoItemViewController *)thePageViewController.viewControllers.lastObject;
    if (vc) {
        lastIndex = vc.index;
        if (index == lastIndex) {
            return;
        }
    }
    
    UIPageViewControllerNavigationDirection direction;
    
    if (lastIndex == -1 || index == lastIndex) {
        direction = UIPageViewControllerNavigationDirectionForward;
        animated = NO;
    } else if (index < lastIndex) {
        direction = UIPageViewControllerNavigationDirectionReverse;
    } else {
        direction = UIPageViewControllerNavigationDirectionForward;
    }
    
    vc = [self pageViewControllerForIndex:index];
    if (vc) {
        
        __typeof__(self) __weak wself = self;
        
        [thePageViewController setViewControllers:@[vc]
                                        direction:direction
                                         animated:NO
                                       completion:^(BOOL finished) {
                                           [wself adjustHeight];
                                       }];
    }
}

#pragma mark - PageViewController delegate & dataSource

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(DSMapInfoItemViewController *)viewController {
    return [self pageViewControllerForIndex:viewController.index + 1];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(DSMapInfoItemViewController *)viewController {
    return [self pageViewControllerForIndex:viewController.index - 1];
}

- (void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray *)previousViewControllers transitionCompleted:(BOOL)completed {
    if (!completed) {
        return;
    }

    if ([self.delegate respondsToSelector:@selector(mapViewInfo:didSelectItemWithSpot:)]) {
        [self.delegate mapViewInfo:self didSelectItemWithSpot:[(DSMapInfoItemViewController *)pageViewController.viewControllers.lastObject spot]];
    }
    
    [self adjustHeight];
}

- (DSMapInfoItemViewController *)pageViewControllerForIndex:(NSInteger)index {
    if (index < 0 || index >= [self.dataSource numberOfItemsInMapInfoView:self]) {
        return nil;
    }
    return [self.dataSource mapInfoItemViewControllerForIndex:index];
}

- (DSMapInfoItemViewController *)lastSelectedPageViewController {
    DSMapInfoItemViewController * vc = (DSMapInfoItemViewController *)thePageViewController.viewControllers.lastObject;
    if (vc) {
        if (vc.spot) {
            return [self.dataSource mapInfoItemViewControllerForSpot:vc.spot];
        }
    }
    return nil;
}

- (void)adjustHeight {
    float newHeight = [thePageViewController.viewControllers.lastObject viewHeight];
    
    [UIView animateWithDuration:0.1 animations:^{
        CGRect frame = self.frame;
        frame.origin.y -= newHeight - frame.size.height;
        frame.size.height = newHeight;
        self.frame = frame;
    }];
}

@end

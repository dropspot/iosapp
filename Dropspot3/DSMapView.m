/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  DSMapView.m
//  Dropspot3
//
//  Created by Lukas Würzburger on 1/23/14.
//  Copyright (c) 2014 Dropspot. All rights reserved.
//

// Frameworks
#import <MapKit/MapKit.h>

// Header
#import "DSMapView.h"

// UI elements
#import "DSMapAnnotationView.h"
#import "DSMapInfoView.h"
#import "DSMapAuthorView.h"

// Helper
#import "Spot+Location.h"
#import "DSServices.h"
#import "DSDropspot.h"
#import "DSGlobal.h"
#import "DataHelper.h"

@interface DSMapView () <MKMapViewDelegate, UISearchBarDelegate, UITableViewDataSource, UITableViewDelegate, DSMapInfoViewDelegate, DSMapInfoViewDataSource, DSMapInfoItemViewControllerDelegate> {
    // User interface
    MKMapView * _mapView;
    UITableView * _searchTableView;
    __strong DSMapInfoView * _mapInfoView;
    DSMapAuthorView * _authorView;
    
    // Check
    NSUInteger selectedSpotIndex;
    
    // Search
    NSArray * _addressSearchResult;
    
    // Gestures
    UILongPressGestureRecognizer * _longPressGesture;
    UISwipeGestureRecognizer * _swipeUp;
}

@property (nonatomic, strong) dispatch_queue_t handleSpotsQueue;

@end

@implementation DSMapView

#pragma mark - Initialization

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // Map view
        _mapView = [[MKMapView alloc] initWithFrame:CGRectMake(0.f, 0.f, frame.size.width, frame.size.height)];
        _mapView.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
        _mapView.delegate = self;
        _mapView.rotateEnabled = NO;
        _mapView.pitchEnabled = NO;
        [self addSubview:_mapView];

        // Adjust map view's region
        MKCoordinateRegion adjustedRegion = _mapView.region;
        adjustedRegion.span.longitudeDelta = 0.01f;
        adjustedRegion.span.latitudeDelta = 0.01f;
        [_mapView setRegion:adjustedRegion animated:NO];
        
        // Search bar
        _searchBar = [[DSSearchBar alloc] initWithFrame:CGRectMake(0.f, 0.f, self.frame.size.width, 0.f)];
        _searchBar.delegate = self;
        _searchBar.alpha = 0;
        [self addSubview:_searchBar];
        
        // Author view
        _authorView = [[DSMapAuthorView alloc] initWithFrame:CGRectMake(5., _searchBar.frame.origin.y + _searchBar.frame.size.height + 5., 310., 50.)];
        _authorView.hidden = YES;
        [self addSubview:_authorView];
        
        // Search table view
        _searchTableView = [[UITableView alloc] initWithFrame:CGRectMake(0.f, _searchBar.frame.origin.y + _searchBar.frame.size.height, frame.size.width, frame.size.height - _searchBar.frame.origin.y - _searchBar.frame.size.height) style:UITableViewStylePlain];
        _searchTableView.delegate = self;
        _searchTableView.dataSource = self;
        _searchTableView.hidden = YES;
        [self addSubview:_searchTableView];
        
        // Map info view
        _mapInfoView = [[DSMapInfoView alloc] initWithFrame:CGRectMake(0.f, frame.size.height - 150.f, frame.size.width, 150.f)];
        _mapInfoView.delegate = self;
        _mapInfoView.dataSource = self;
        [self addSubview:_mapInfoView];
        
        // Default settings
        _followsUserLocation = YES;
        _state = DSMapViewStateBackground;
        
        // Gesture recognizers
        _longPressGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPressGesture:)];
        _longPressGesture.minimumPressDuration = 0.3f;
        [_mapView addGestureRecognizer:_longPressGesture];
        
        _swipeUp = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeGesture:)];
        _swipeUp.direction = UISwipeGestureRecognizerDirectionUp;
        [_mapInfoView addGestureRecognizer:_swipeUp];
    }
    return self;
}

#pragma mark - Setter & Getter

- (void)setShowsUserLocation:(BOOL)showsUserLocation {
    _showsUserLocation = showsUserLocation;
    
    // Forward property to the map.
    _mapView.showsUserLocation = self.showsUserLocation;
}

- (void)setShowsSearchBar:(BOOL)showsSearchBar {
    _showsSearchBar = showsSearchBar;
    
    _searchBar.alpha = (float)_showsSearchBar;
    
    if (_showsSearchBar) {
        
        // Show results table if the result has items.
        if (_addressSearchResult.count > 0) {
            
            _searchTableView.hidden = NO;
        }
        
        // Show keyboard
        [_searchBar becomeFirstResponder];
    } else {
        
        // Hide results table if it is visible.
        if (!_searchTableView.hidden) {
            
            _searchTableView.hidden = YES;
        }
        
        // Hide keyboard
        [_searchBar resignFirstResponder];
    }
    
    [self setLayout];
}

- (void)setShowsAuthorView:(BOOL)showsAuthorView {
    _showsAuthorView = showsAuthorView;
    
    [self setLayout];
}

- (void)setSpots:(NSArray *)spots {
    _spots = spots;
    
    NSInteger indexOfSelectedSpot = NSNotFound;
    
    // Check if the selected spot is still in the new array of spots.
    if (self.selectedSpot) {
        
        indexOfSelectedSpot = [self.dataSource mapView:self indexForSpot:self.selectedSpot];
        if (indexOfSelectedSpot == NSNotFound) {
        
            // Add selected spot to spots array.
            NSMutableArray * spots = [_spots mutableCopy];
            [spots addObject:self.selectedSpot];
            _spots = spots;
            
            // Update index
            indexOfSelectedSpot = [self.dataSource mapView:self indexForSpot:self.selectedSpot];
        }
    }
    
    // Update spots on the map
    [self exchangeSpotsForRegion:_mapView.region];

    // Update map info
    [_mapInfoView reloadData];
    
    if (selectedSpotIndex != indexOfSelectedSpot) {
        selectedSpotIndex = indexOfSelectedSpot;

        if (selectedSpotIndex != NSNotFound) {
            [_mapInfoView selectSpot:self.selectedSpot atIndex:selectedSpotIndex];
        }
    }
}

- (void)setSelectedSpot:(Spot *)selectedSpot {
    
    BOOL animated = (_selectedSpot != nil);
    
    _selectedSpot = selectedSpot;
    
    if (self.selectedSpot) {
        
        selectedSpotIndex = [self.dataSource mapView:self indexForSpot:self.selectedSpot];
        
        // Check if the new selected spot is also in the spots array.
        // When the user creates a new spot it might not be in it.
        if (selectedSpotIndex == NSNotFound) {
            
            // If the selected spot wasn't found in the spots array. Add it.
            NSMutableArray * spots = [_spots mutableCopy];
            [spots addObject:self.selectedSpot];
            self.spots = spots;
        }
        
        // Stop centering the map on user's location.
        _followsUserLocation = NO;
        
        // Show the info for the selected spot.
        _mapInfoView.hidden = NO;
        _authorView.hidden = NO;
        
        // Change the author view.
        [self showAuthorViewWithSpot:self.selectedSpot];
        
        // Get the selected annotation on the map
        DSMapViewAnnotation * annotation = [self annotationForSpot:self.selectedSpot];
        if (!annotation) {
            
            // Create new annotation if it's nil.
            annotation = [[DSMapViewAnnotation alloc] initWithSpot:self.selectedSpot];
        }
        
        // Check if the annotation is added to the map view. If not add it.
        if ([_mapView.annotations indexOfObject:annotation] == NSNotFound) {
            [_mapView addAnnotation:annotation];
        }
        _mapView.selectedAnnotations = @[annotation];
        
        // Center the spot on the map
        [self setCenterCoordinate:annotation.coordinate animated:animated];
        
        // Set the selected item for map info view
        if (selectedSpotIndex != NSNotFound) {

            [_mapInfoView selectSpot:self.selectedSpot atIndex:selectedSpotIndex];
        } else {
            
            NSLog(@"setSelectedSpot: Index out of bounds.");
        }
    } else {
        
        // Hide the info if there is no spot selected.
        _mapInfoView.hidden = YES;
        _authorView.hidden = YES;
    }
}

- (void)setFollowsUserLocation:(BOOL)followsUserLocation {
    if (followsUserLocation && _mapView.userLocation.location) {
        _followsUserLocation = followsUserLocation;
        
        // Center user location.
        [self setCenterCoordinate:_mapView.userLocation.location.coordinate];
    } else if (!followsUserLocation) {
        _followsUserLocation = followsUserLocation;
    } else {
        _followsUserLocation = NO;
    }
}

- (void)setState:(DSMapViewState)state {
    if (_state != state) {
        _state = state;
        
        if (_state == DSMapViewStateNormal) {
            
            // Fade in elements.
            if (_showsAuthorView) {

                _authorView.alpha = 0.f;
                _authorView.hidden = NO;
            }
            [UIView animateWithDuration:0.25f animations:^{

                if (_showsAuthorView) {
                
                    _authorView.alpha = 1.f;
                }
            }];
        } else if (_state == DSMapViewStateBackground) {
            
            // Set selected spot nil to hide the map info view.
            self.selectedSpot = nil;

            // Fade out elements.
            [UIView animateWithDuration:0.25f animations:^{

                self.showsSearchBar = NO;
                if (_showsAuthorView) {
                    _authorView.alpha = 0.f;
                }
            } completion:^(BOOL finished) {

                if (_showsAuthorView) {
                    _authorView.hidden = YES;
                }
            }];
        }
    }
}

- (void)setContentInset:(UIEdgeInsets)contentInset {
    _contentInset = contentInset;
    
    [self setLayout];
}

- (dispatch_queue_t)handleSpotsQueue {
    if (!_handleSpotsQueue) {
        _handleSpotsQueue = dispatch_queue_create("handle-spots", DISPATCH_QUEUE_SERIAL);
    }
    return _handleSpotsQueue;
}

#pragma mark - IBActions

- (void)locateMeButtonPressed:(id)sender {
    
    // Center user location.
    [self setCenterCoordinate:_mapView.userLocation.location.coordinate];
    
    // Automatic follow the user location.
    self.followsUserLocation = YES;
}

#pragma mark - Helper

- (void)update {
    [_mapInfoView reloadData];
    
    if (self.selectedSpot && !self.selectedSpot.isDeleted) {
        [[self annotationForSpot:self.selectedSpot] setCoordinate:self.selectedSpot.location.coordinate];
        [self setCenterCoordinate:self.selectedSpot.location.coordinate];
    } else {
        if (_spots.count > 0) {
            if (!self.selectedSpot) {
                self.selectedSpot = _spots[0];
            }
        }
    }
}

- (void)setLayout {
    
    // Adjust search bar
    CGRect frame;
    frame = _searchBar.frame;
    frame.origin.y = _contentInset.top;
    _searchBar.frame = frame;
    
    if (_showsAuthorView) {
        _authorView.hidden = NO;

        // Adjust author view
        frame = _authorView.frame;
        if (!_showsSearchBar) {
            frame.origin.y = _searchBar.frame.origin.y + 5.f;
        } else {
            frame.origin.y = _searchBar.frame.origin.y + _searchBar.frame.size.height + 5.;
        }
        _authorView.frame = frame;
    } else {
        _authorView.hidden = YES;
    }
    // Adjust table view
    frame = _searchTableView.frame;
    frame.origin.y = _searchBar.frame.origin.y + _searchBar.frame.size.height;
    frame.size.height = self.frame.size.height - frame.origin.y - 214.f;
    _searchTableView.frame = frame;
}

/**
 * Annotation view's outside of the region should be removed to keep performance efficient.
 */
- (void)exchangeSpotsForRegion:(MKCoordinateRegion)region {
    
    // Calculate edge insets.
    double leftLongitude = region.center.longitude - (region.span.longitudeDelta / 2);
    double rightLongitude = region.center.longitude + (region.span.longitudeDelta / 2);
    double topLatitude = region.center.latitude + (region.span.latitudeDelta / 2);
    double bottomLatitude = region.center.latitude - (region.span.latitudeDelta / 2);
    
    // Search for spots to remove and spots to add async to keep performance efficient.
    dispatch_async(self.handleSpotsQueue, ^{
        
        NSMutableArray * annotationsToDelete = [NSMutableArray array];
        
        Spot * selectedSpot = nil;
        
        // Find annotations outside the viewport to remove.
        for (id annotation in _mapView.annotations) {
            if ([annotation isKindOfClass:[DSMapViewAnnotation class]]) {
                DSMapViewAnnotation * ann = (DSMapViewAnnotation *)annotation;
                if (ann.spot && !ann.spot.isDeleted) {
                    CLLocationCoordinate2D coordinate = ann.coordinate;
                    if (coordinate.longitude > leftLongitude &&
                        coordinate.longitude < rightLongitude &&
                        coordinate.latitude < topLatitude &&
                        coordinate.latitude > bottomLatitude) {
                    } else {
                        [annotationsToDelete addObject:ann];
                        if (ann.spot == _selectedSpot) {
                            selectedSpot = ann.spot;
                        }
                    }
                } else {
                    [annotationsToDelete addObject:ann];
                    if (ann.spot == _selectedSpot) {
                        selectedSpot = ann.spot;
                    }
                }
            }
        }
        
        if (annotationsToDelete.count > 0) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [_mapView removeAnnotations:annotationsToDelete];
                [_mapInfoView reloadData];
            });
        }
        
        // Find spots that can be added to the map view.
        for (Spot * spot in self.spots) {
            CLLocationCoordinate2D coordinate = spot.location.coordinate;
            
            if (coordinate.longitude >= leftLongitude &&
                coordinate.longitude <= rightLongitude &&
                coordinate.latitude <= topLatitude &&
                coordinate.latitude >= bottomLatitude) {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    DSMapViewAnnotation * annotation = [self annotationForSpot:spot];
                    if (!annotation) {
                        annotation = [[DSMapViewAnnotation alloc] initWithSpot:spot];
                        [_mapView addAnnotation:annotation];
                    }
                });
            }
        }
    });
}

/**
 * Returns an array of annotations for given spots
 */
- (NSArray *)annotationsForSpots:(NSArray *)spots {
    NSMutableArray * array = [NSMutableArray array];
    for (Spot * spot in spots) {
        DSMapViewAnnotation * annotation = [[DSMapViewAnnotation alloc] initWithSpot:spot];
        if (annotation) {
            [array addObject:annotation];
        }
    }
    return array;
}

/**
 * Returns an array of the map view's annotations of given class.
 */
- (NSArray *)annotationsOfClass:(Class)class {
    NSMutableArray * array = [NSMutableArray array];
    for (id annotation in _mapView.annotations) {
        if ([annotation isKindOfClass:class]) {
            [array addObject:annotation];
        }
    }
    return array;
}

/**
 * Returns the annotation for the given spot if it exists, otherwise nil.
 */
- (DSMapViewAnnotation *)annotationForSpot:(Spot *)spot {
    for (id annotation in _mapView.annotations) {
        if ([annotation isKindOfClass:[DSMapViewAnnotation class]]) {
            if ([(DSMapViewAnnotation *)annotation spot] == spot) {
                return annotation;
            }
        }
    }
    return nil;
}

- (void)setCenterCoordinate:(CLLocationCoordinate2D)coordinate {
    [self setCenterCoordinate:coordinate animated:YES];
}

- (void)setCenterCoordinate:(CLLocationCoordinate2D)coordinate animated:(BOOL)animated {
    [self setCenterCoordinate:coordinate inRect:CGRectMake(0.f, 0.f, self.frame.size.width, self.frame.size.height - _mapInfoView.frame.size.height) animated:animated];
}

/**
 * Centers the map at the given coordinate in given rect.
 */
- (void)setCenterCoordinate:(CLLocationCoordinate2D)coordinate inRect:(CGRect)rect animated:(BOOL)animated {
    
    // XY coordinates of where the uncentered spot is on the map.
    CGPoint coordPoint = [_mapView convertCoordinate:coordinate toPointToView:self];
    
    // The offset to subtract from the Y-position, e.g. because of overlays on the map.
    float offset = self.frame.size.height / 2 - (rect.size.height / 2) - 10;
    
    // New XY center for the spot with offset added.
    CGPoint fakeCenter = CGPointMake(coordPoint.x, coordPoint.y + offset);
    
    // Converte XY to map coordinates.
    CLLocationCoordinate2D newCoordinate = [_mapView convertPoint:fakeCenter toCoordinateFromView:self];
    
    // Check if the new coordinate is valid
    if (CLLocationCoordinate2DIsValid(newCoordinate)) {
        
        // Tell the map the actual coordinates to center.
        [_mapView setCenterCoordinate:newCoordinate animated:animated];
    } else {

        // Tell the map the old coordinates to center.
        [_mapView setCenterCoordinate:coordinate animated:animated];
    }
}

/**
 * Creates a spot and drops an annotation at given location coordinate.
 */
- (void)dropSpotAtLocationCoordinate:(CLLocationCoordinate2D)coordinate {
    
    // Check if the delegate responds to the required method.
    if ([self.delegate respondsToSelector:@selector(mapView:didRecognizeLongPressAtLocationCoordinate:)]) {
        [self.delegate mapView:self didRecognizeLongPressAtLocationCoordinate:coordinate];
    } else {
        @throw [NSException exceptionWithName:@"Implementation incomplete" reason:@"Delegate (DSMapViewDelegate) needs method didRecognizeLongPressAtLocationCoordinate to be implemented." userInfo:nil];
    }
}

/**
 * Adjust the author view.
 */
- (void)showAuthorViewWithSpot:(Spot *)spot {
    if (kDSForceAuthorImageName) {
        [_authorView setImage:[UIImage imageNamed:kDSForceAuthorImageName] animated:YES];
    } else {
        [_authorView setImage:[DataHelper imageWithFilePath:[DataHelper pathForCacheFile:spot.user.photoFile]] animated:YES];
    }
    if (kDSForceAuthorName) {
        [_authorView setName:kDSForceAuthorName animated:YES];
    } else {
        [_authorView setName:spot.user.displayName animated:YES];
    }
}

- (void)centerUserLocationAnimated:(BOOL)animated {
    
    // Center the map at the user's location
    [self setCenterCoordinate:_mapView.userLocation.location.coordinate
                     animated:animated];
}

#pragma mark - Gestures

/**
 * Handles the long press gesture.
 */
- (void)handleLongPressGesture:(UIGestureRecognizer *)gestureRecognizer {
    if (gestureRecognizer.state == UIGestureRecognizerStateRecognized) {
        CLLocationCoordinate2D coordinate = [_mapView convertPoint:[gestureRecognizer locationInView:gestureRecognizer.view] toCoordinateFromView:_mapView];
        
        // Create spot at touch location.
        [self dropSpotAtLocationCoordinate:coordinate];
    }
}

/**
 *
 */
- (void)handleSwipeGesture:(UISwipeGestureRecognizer *)gestureRecognizer {
    if (gestureRecognizer.state == UIGestureRecognizerStateRecognized) {
        if ([self.delegate respondsToSelector:@selector(mapViewDidRecognizeSwipeUp:)]) {
            [self.delegate mapViewDidRecognizeSwipeUp:self];
        }
    }
}

#pragma mark - Touches

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {

    // Stop center the user location on the map if the user touches the view.
    _followsUserLocation = NO;
}

#pragma mark - MKMapView delegate

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation {
    if ([annotation isKindOfClass:[DSMapViewAnnotation class]]) {
        DSMapViewAnnotation * mapViewAnnotation = (DSMapViewAnnotation *)annotation;
        NSString * AnnotationViewID = @"dsAnnotation";
        DSMapAnnotationView * annotationView = (DSMapAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:AnnotationViewID];
        if (annotationView == nil) {
            annotationView = [[DSMapAnnotationView alloc] initWithAnnotation:mapViewAnnotation reuseIdentifier:AnnotationViewID];
        } else {
            annotationView.annotation = mapViewAnnotation;
        }

        // If the view is reused it takes the old state. To prevent wrong display set calledOut state.
        annotationView.calledOut = NO;
        
        // Prepare for fade in animation in mapView:didAddAnnotationViews:
        annotationView.alpha = 0.0;
        
        return annotationView;
    }
    return nil;
}

- (void)mapView:(MKMapView *)mapView didAddAnnotationViews:(NSArray *)views {
    for (MKAnnotationView * view in views) {
        if ([view.annotation isKindOfClass:[DSMapViewAnnotation class]]) {
            [UIView animateWithDuration:0.25 animations:^{
                view.alpha = kDSMapAnnotationViewAlphaNormal;
            } completion:^(BOOL finished) {
                DSMapViewAnnotation * annotation = (DSMapViewAnnotation *)view.annotation;
                if (annotation.spot == self.selectedSpot) {
                    [_mapView selectAnnotation:annotation animated:YES];
                }
            }];
        }
    }
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view {
    if ([view.annotation isKindOfClass:[DSMapViewAnnotation class]]) {
        DSMapViewAnnotation * annotation = (DSMapViewAnnotation *)view.annotation;
        if (annotation.selectable) {
            self.selectedSpot = annotation.spot;
            if ([self.delegate respondsToSelector:@selector(mapView:didSelectSpot:)]) {
                [self.delegate mapView:self didSelectSpot:annotation.spot];
            }
            for (DSMapViewAnnotation * ann in mapView.annotations) {
                if (ann != annotation) {
                    DSMapAnnotationView * anView = (DSMapAnnotationView *)[mapView viewForAnnotation: ann];
                    if ([anView isKindOfClass:[DSMapAnnotationView class]]) {
                        anView.calledOut = NO;
                    }
                }
            }
        }
    }
}

- (void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view {
    if ([view.annotation isKindOfClass:[DSMapViewAnnotation class]]) {
        if ([self.delegate respondsToSelector:@selector(mapView:didDeselectSpot:)]) {
            [self.delegate mapView:self didDeselectSpot:self.selectedSpot];
        }
    }
}

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation {
    if (_followsUserLocation) {
        [self setCenterCoordinate:userLocation.coordinate];
    }
}

- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated {
    
    // Create region
    DSMapViewRegion region;
    region.topLatitude = mapView.region.center.latitude + (mapView.region.span.latitudeDelta / 2);
    region.bottomLatitude = mapView.region.center.latitude - (mapView.region.span.latitudeDelta / 2);
    region.leftLongitude = mapView.region.center.longitude - (mapView.region.span.longitudeDelta / 2);
    region.rightLongitude = mapView.region.center.longitude + (mapView.region.span.longitudeDelta / 2);

    _region = region;
    
    // The delegate has to know that the region did change
    // to provide new spots for the new region.
    if ([self.delegate respondsToSelector:@selector(mapView:didChangeRegion:)]) {

        [self.delegate mapView:self didChangeRegion:region];
    }
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view didChangeDragState:(MKAnnotationViewDragState)newState fromOldState:(MKAnnotationViewDragState)oldState {
    if ([view isKindOfClass:[DSMapAnnotationView class]]) {
        DSMapAnnotationView * annotationView = (DSMapAnnotationView *)view;

        if (newState == MKAnnotationViewDragStateStarting) {
            annotationView.isDragging = YES;
        } else if (newState == MKAnnotationViewDragStateEnding || newState == MKAnnotationViewDragStateCanceling) {
            annotationView.isDragging = NO;

            __block DSMapViewAnnotation * annotation = (DSMapViewAnnotation *)annotationView.annotation;

            // When moving a spot on the map it will show the drop point (Cross) a few pixels above
            // the actual coordinate, so that the drop is visible and not hidden by the user's thumb.
            // To make it right we have to add/subtract pixels.
            // Offset point constant: kDSMapAnnotationViewDragOffset
            CGPoint oldPoint = [mapView convertCoordinate:annotation.coordinate toPointToView:mapView];
            CGPoint newPoint = CGPointMake(oldPoint.x, oldPoint.y + kDSMapAnnotationViewDragOffset.y - 3);
            CLLocationCoordinate2D newCoords = [mapView convertPoint:newPoint toCoordinateFromView:mapView];
            
            // Bring the new coordinates to the annotation.
            annotation.coordinate = newCoords;
            
            // Give the spot the new coordinates.
            annotation.spot.latitude = annotation.coordinate.latitude;
            annotation.spot.longitude = annotation.coordinate.longitude;

            // Fix the annotation view on the map.
            [annotationView setDragState:MKAnnotationViewDragStateNone animated:YES];
            
            // Loading stack key
            NSString * stackKey = @"DSMapView:searchAddress:dragSpot";
            [[DSGlobal sharedGlobals] addLoadingStackObject:stackKey];
            
            annotation.spot.isDeterminingAddress = YES;
            
            // Make sure this is still the selected spot.
            self.selectedSpot = annotation.spot;
            
            // Search for address by given coordinates.
            [DSServices geocodeBySearch:[NSString stringWithFormat:@"%f,%f", annotation.spot.latitude, annotation.spot.longitude] completion:^(NSArray *array) {
                if (array && array.count > 0) {
                    NSString * address = array[0][@"formatted_address"];
                    annotation.spot.address = address;
                    
                    _searchBar.inputText = address;
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:kDSSpotDidDetermineAddressNotificationName object:nil userInfo:@{ @"spot" : annotation.spot }];
                }

                annotation.spot.isDeterminingAddress = NO;
                
                if (annotation.spot.isSynced) {
                    
                    // Update spot online.
                    [[DSDropspot session] editSpot:annotation.spot imageStatus:DSPhotoStatusNoChange block:^(DSAPISpotResponse *collection, NSError *error) {

                        // Work is done, remove key from loading stack.
                        [[DSGlobal sharedGlobals] removeFromLoadingStack:stackKey];
                    }];
                } else {
                    // Work is done, remove key from loading stack.
                    [[DSGlobal sharedGlobals] removeFromLoadingStack:stackKey];
                }
            }];
        }
    }
}

#pragma mark - DSMapInfoView dataSource

- (NSUInteger)numberOfItemsInMapInfoView:(DSMapInfoView *)mapInfoView {
    return [self.dataSource mapView:self numberOfItemsForMapInfoView:mapInfoView];
}

- (DSMapInfoItemViewController *)mapInfoItemViewControllerForIndex:(NSInteger)index {

    DSMapInfoItemViewController * vc = [self.dataSource mapView:self viewControllerAtIndex:index];
    vc.delegate = self;
    vc.index = index;
    
    return vc;
}

- (DSMapInfoItemViewController *)mapInfoItemViewControllerForSpot:(Spot *)spot {
    
    DSMapInfoItemViewController * vc = [self.dataSource mapView:self viewControllerWithSpot:spot];
    vc.delegate = self;
    vc.index = [self.dataSource mapView:self indexForSpot:spot];

    return vc;
}

#pragma mark - DSMapInfoView delegate

- (void)mapViewInfo:(DSMapInfoView *)mapViewInfo didSelectItemWithSpot:(Spot *)spot {
    self.selectedSpot = spot;
    
    // Tell the delegate that the selected item has changed.
    if ([self.delegate respondsToSelector:@selector(mapView:didSelectSpot:)]) {
        [self.delegate mapView:self didSelectSpot:self.selectedSpot];
    }
}

#pragma mark - DSMapInfoViewController delegate

- (void)mapInfoItemDidPressSpot:(DSMapInfoItemViewController *)mapInfoItemViewController {
    
    // Tell the delegate that the selected item were tapped.
    if ([self.delegate respondsToSelector:@selector(mapView:didPressSpot:)]) {
        [self.delegate mapView:self didPressSpot:mapInfoItemViewController.spot];
    }
}

- (void)mapInfoItemDidPressCreateSpot:(Spot *)spot {
    
    // Tell the delegate that the create button were pressed.
    if ([self.delegate respondsToSelector:@selector(mapView:didPressCreateSpot:)]) {
        [self.delegate mapView:self didPressCreateSpot:spot];
    }
}

- (void)mapInfoItemDidPressCancelSpot:(Spot *)spot {
    
    // Remove the annotation from the map.
    DSMapViewAnnotation * annotation = [self annotationForSpot:spot];
    if (annotation) {
        [_mapView removeAnnotation:annotation];
    }
    
    // Remove the spot from spots array.
    NSMutableArray * spots = [self.spots mutableCopy];
    [spots removeObject:spot];
    self.spots = spots;
    
    // Jump to the most nearest spot.
    if (self.spots.count > 0) {
        self.selectedSpot = self.spots[0];
    }
    
    // Tell the delegate that the cancel button was pressed.
    if ([self.delegate respondsToSelector:@selector(mapView:didPressCancelSpot:)]) {
        [self.delegate mapView:self didPressCancelSpot:spot];
    }
}

#pragma mark - UISearchBar delegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    NSString * key = [NSString stringWithFormat:@"DSMapView:searchAddressCompletionFor:%@", searchText];
    
    [[DSGlobal sharedGlobals] addLoadingStackObject:key];
    
    NSLog(@"searching for %@", searchText);
    [DSServices addressCompletionBySearch:searchText completion:^(NSArray * array) {
        
        _addressSearchResult = array;
        [_searchTableView reloadData];
        
        [[DSGlobal sharedGlobals] removeFromLoadingStack:key];
    }];
    
    _searchTableView.hidden = NO;
}

#pragma mark - UITableView delegate & dataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _addressSearchResult.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString * cellIdentifier = @"cell";
    
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        cell.textLabel.font = [UIFont systemFontOfSize:14.f];
    }
    
    cell.textLabel.text = _addressSearchResult[indexPath.row][@"description"];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    // Reset the table views state and hide it.
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    tableView.hidden = YES;
    
    // Hide keyboard
    [_searchBar resignFirstResponder];
    
    // Identifier for global loading stack.
    NSString * loadingKey = @"DSMapView:searchAddress";
    [[DSGlobal sharedGlobals] addLoadingStackObject:loadingKey];
    
    if (_addressSearchResult.count > indexPath.row && _addressSearchResult[indexPath.row][@"description"]) {

        // Get coordinates by searching input string with google places api.
        [DSServices geocodeBySearch:_addressSearchResult[indexPath.row][@"description"] completion:^(NSArray *array) {
            if (array.count > 0) {
                
                // The map shouldn't follow the user location anymore.
                self.followsUserLocation = NO;
                
                // Get coordinates from result.
                CLLocationCoordinate2D coords = CLLocationCoordinate2DMake([array[0][@"geometry"][@"location"][@"lat"] doubleValue],
                                                                           [array[0][@"geometry"][@"location"][@"lng"] doubleValue]);

                // Jump to new coordinates
                [self setCenterCoordinate:coords];
                
                // If i've just dropped a spot and didn't save it yet, move it too.
                if (self.selectedSpot && !self.selectedSpot.isSynced && self.selectedSpot.isOwnedByMe) {
                    self.selectedSpot.latitude = coords.latitude;
                    self.selectedSpot.longitude = coords.longitude;
                    self.selectedSpot.address = _addressSearchResult[indexPath.row][@"description"];
                    
                    [[self annotationForSpot:self.selectedSpot] setCoordinate:coords];
                }
                
                // Clear search result.
                _addressSearchResult = @[];
                [_searchTableView reloadData];
            }
            
            // Work is done, so remove key from loading stack.
            [[DSGlobal sharedGlobals] removeFromLoadingStack:loadingKey];
        }];
    }
}

@end







/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  DSAnnotation.h
//  Dropspot
//
//  Created by Lukas Würzburger on 20.06.13.
//  Copyright (c) 2013 Lukas Würzburger. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@class Spot;

@interface DSMapViewAnnotation : NSObject <MKAnnotation>

@property (nonatomic, assign) CLLocationCoordinate2D coordinate;

@property (nonatomic, weak) Spot * spot;

@property (nonatomic, assign) BOOL selectable;

/**
 * Returns an instance of DSMapViewAnnotation if spot coordinates are valid, otherwise nil.
 */
- (id)initWithSpot:(Spot *)spot;

@end

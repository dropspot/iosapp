/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  User+Session.m
//  Dropspot3
//
//  Created by Lukas Würzburger on 4/16/14.
//  Copyright (c) 2014 Dropspot GmbH. All rights reserved.
//

#import "DSUser.h"

#import "DSDropspot.h"

@implementation DSUser

+ (DSUser *)currentUser {
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary * dictionary = [defaults objectForKey:kDSDropspotSessionDomainKeyName];

    // Return user if a session is open and a token exists.
    if (dictionary && [dictionary objectForKey:kDSDropspotSessionTokenKeyName]) {
        static DSUser * instance;
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            instance = [[DSUser alloc] init];
        });
        return instance;
    } else {
        return nil;
    }
}

+ (DSUser *)newUserWithToken:(NSString *)token {
    if ([DSUser currentUser]) {
        [DSUser killCurrentUser];
    }

    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:@{ kDSDropspotSessionTokenKeyName : token } forKey:kDSDropspotSessionDomainKeyName];
    [defaults synchronize];
    
    return [DSUser currentUser];
}

+ (void)killCurrentUser {
    if ([DSUser currentUser]) {
        NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
        [defaults removeObjectForKey:kDSDropspotSessionDomainKeyName];
        [defaults synchronize];
    }
}

- (void)setProperty:(id)property forKey:(NSString *)key {
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary * sessionDict = [[defaults objectForKey:kDSDropspotSessionDomainKeyName] mutableCopy];
    
    if (property == nil) {
        [sessionDict removeObjectForKey:key];
    } else {
        [sessionDict setObject:property forKey:key];
    }
    [defaults setObject:sessionDict forKey:kDSDropspotSessionDomainKeyName];
    [defaults synchronize];
}

- (id)objectForKey:(NSString *)key {
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary * sessionDict = [[defaults objectForKey:kDSDropspotSessionDomainKeyName] mutableCopy];
    return [sessionDict objectForKey:key];
}

- (void)setUsername:(NSString *)username {
    [self setProperty:username forKey:kDSDropspotSessionUsernameKeyName];
}

- (NSString *)username {
    return [self objectForKey:kDSDropspotSessionUsernameKeyName];
}

- (void)setDisplayName:(NSString *)displayName {
    [self setProperty:displayName forKey:kDSDropspotSessionNameKeyName];
}

- (NSString *)displayName {
    return [self objectForKey:kDSDropspotSessionNameKeyName];
}

- (void)setAvatar:(UIImage *)avatar {
    [self setProperty:UIImageJPEGRepresentation(avatar, 1.0) forKey:kDSDropspotSessionImageKeyName];
}

- (UIImage *)avatar {
    return [UIImage imageWithData:[self objectForKey:kDSDropspotSessionImageKeyName]];
}

- (void)setToken:(NSString *)token {
    [self setProperty:token forKey:kDSDropspotSessionTokenKeyName];
}

- (NSString *)token {
    return [self objectForKey:kDSDropspotSessionTokenKeyName];
}

- (void)setJoined:(NSDate *)joined {
    [self setProperty:joined forKey:kDSDropspotSessionDateJoinedKeyName];
}

- (NSDate *)joined {
    return [self objectForKey:kDSDropspotSessionDateJoinedKeyName];
}

- (DSUserState)state {
    BOOL tmpUser = ([self objectForKey:kDSDropspotSessionIsTmpUserKeyName] != nil);
    BOOL realUser = ([self objectForKey:kDSDropspotSessionIsRealUserKeyName] != nil);
    
    if (!tmpUser) {
        return DSUserStateReal;
    } else if (!realUser) {
        return DSUserStateTemporary;
    } else {
        return DSUserStateTransitionToReal;
    }
}

- (void)setState:(DSUserState)state {
    if (state == DSUserStateReal) {
        [self setProperty:@1 forKey:kDSDropspotSessionIsRealUserKeyName];
        [self setProperty:nil forKey:kDSDropspotSessionIsTmpUserKeyName];
    } else if (state == DSUserStateTemporary) {
        [self setProperty:nil forKey:kDSDropspotSessionIsRealUserKeyName];
        [self setProperty:@1 forKey:kDSDropspotSessionIsTmpUserKeyName];
    } else if (state == DSUserStateTransitionToReal) {
        [self setProperty:@1 forKey:kDSDropspotSessionIsRealUserKeyName];
        [self setProperty:@1 forKey:kDSDropspotSessionIsTmpUserKeyName];
    }
}

- (DSUserLoginType)loginType {
    if ([self objectForKey:kDSDropspotSessionLoginTypeKeyName]) {
        NSNumber * number = [self objectForKey:kDSDropspotSessionLoginTypeKeyName];
        if ([number intValue] == 1) {
            return DSUserLoginTypeNative;
        } else if ([number intValue] == 2) {
            return DSUserLoginTypeFacebook;
        } else {
            return DSUserLoginTypeNone;
        }
    } else {
        return DSUserLoginTypeNone;
    }
}

- (void)setLoginType:(DSUserLoginType)loginType {
    if (loginType == DSUserLoginTypeNative) {
        [self setProperty:@1 forKey:kDSDropspotSessionLoginTypeKeyName];
    } else if (loginType == DSUserLoginTypeFacebook) {
        [self setProperty:@2 forKey:kDSDropspotSessionLoginTypeKeyName];
    } else {
        [self setProperty:@0 forKey:kDSDropspotSessionLoginTypeKeyName];
    }
}

@end

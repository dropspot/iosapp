/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  CollectionMapViewController.h
//  Dropspot3
//
//  Created by Lukas Würzburger on 1/30/14.
//  Copyright (c) 2014 Dropspot. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@class CollectionMapViewController, Collection, Spot;

@protocol CollectionMapViewControllerDelegate <NSObject>

@optional
- (void)collectionMapViewControllerDidCancelSpot:(CollectionMapViewController *)collectionMapViewController;
- (Collection *)collectionForCreationInMapViewController:(CollectionMapViewController *)collectionMapViewController;

@end

@interface CollectionMapViewController : UIViewController

/**
 * The Array which provides the spots.
 */
@property (nonatomic, strong) NSArray * spots;

/**
 * The collection which provides the spots.
 */
@property (nonatomic, strong) Collection * collection;

/**
 * The selected spot which will be shown first.
 */
@property (nonatomic, strong) Spot * selectedSpot;

/**
 * The object that receives delegation calls.
 */
@property (nonatomic, assign) id <CollectionMapViewControllerDelegate> delegate;

@end

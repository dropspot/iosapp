/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  DSMapView.h
//  Dropspot3
//
//  Created by Lukas Würzburger on 1/23/14.
//  Copyright (c) 2014 Dropspot. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DSSearchBar.h"
#import "DSMapInfoView.h"
#import "DSMapViewAnnotation.h"

typedef enum {
    DSMapViewStateNormal,
    DSMapViewStateBackground
} DSMapViewState;

typedef struct  {
    CLLocationDegrees topLatitude;
    CLLocationDegrees bottomLatitude;
    CLLocationDegrees leftLongitude;
    CLLocationDegrees rightLongitude;
} DSMapViewRegion;

static DSMapViewRegion DSMapViewRegionMake(double top, double bottom, double left, double right) {
    DSMapViewRegion p; p.topLatitude = top; p.bottomLatitude = bottom; p.leftLongitude = left; p.rightLongitude = right; return p;
}

typedef struct {
    NSUInteger columns;
    NSUInteger rows;
} DSMapViewRegionGrid;

static DSMapViewRegionGrid DSMapViewRegionGridMake(NSUInteger columns, NSUInteger rows) {
    DSMapViewRegionGrid p; p.rows = rows; p.columns = columns; return p;
}

@class DSMapView, Spot;

@protocol DSMapViewDelegate <NSObject>

@optional
/**
 * Called after a long press gesture was recognized on the map view.
 */
- (void)mapView:(DSMapView *)mapView didRecognizeLongPressAtLocationCoordinate:(CLLocationCoordinate2D)coordinate;

/**
 * Called after a long press gesture was recognized on the map view.
 */
- (void)mapViewDidRecognizeSwipeUp:(DSMapView *)mapView;

/**
 * Called after a spot description was pressed.
 */
- (void)mapView:(DSMapView *)mapView didPressSpot:(Spot *)spot;

/**
 * Called when the map info view create button was pressed.
 */
- (void)mapView:(DSMapView *)mapView didPressCreateSpot:(Spot *)spot;

/**
 * Called when the map info view cancel button was pressed.
 */
- (void)mapView:(DSMapView *)mapView didPressCancelSpot:(Spot *)spot;

/**
 * Called after a spot was selected on the map.
 */
- (void)mapView:(DSMapView *)mapView didSelectSpot:(Spot *)spot;

/**
 * Called after a spot was deselected.
 */
- (void)mapView:(DSMapView *)mapView didDeselectSpot:(Spot *)spot;

/**
 * Called when the region has changed.
 */
- (void)mapView:(DSMapView *)mapView didChangeRegion:(DSMapViewRegion)region;

@end

@protocol DSMapViewDataSource <NSObject>

/**
 * Determines the number of spot descriptions in map info view.
 */
- (NSUInteger)mapView:(DSMapView *)mapView numberOfItemsForMapInfoView:(DSMapInfoView *)mapInfoView;

/**
 * Determines a new view controller for the map info view.
 */
- (DSMapInfoItemViewController *)mapView:(DSMapView *)mapView viewControllerAtIndex:(NSUInteger)index;

/**
 * Determines a new view controller for the map info view.
 */
- (NSUInteger)mapView:(DSMapView *)mapView indexForSpot:(Spot *)spot;

/**
 * Determines a new view controller for the map info view.
 */
- (DSMapInfoItemViewController *)mapView:(DSMapView *)mapView viewControllerWithSpot:(Spot *)spot;

@end

@interface DSMapView : UIView

/**
 * The search bar to find addresses on the map.
 */
@property (nonatomic, strong, readonly) DSSearchBar * searchBar;

/**
 * Show or hide the search bar.
 */
@property (nonatomic, assign) BOOL showsSearchBar;

/**
 * Show or hide the author view on the map.
 */
@property (nonatomic, assign) BOOL showsAuthorView;

/**
 * The object that acts as the delgate of the receiving map view.
 */
@property (nonatomic, assign) id <DSMapViewDelegate> delegate;

/**
 * The object that acts as the dataSource of the receiving map view.
 */
@property (nonatomic, assign) id <DSMapViewDataSource> dataSource;

/**
 * Array with elements of class Spot.
 */
@property (nonatomic, strong) NSArray * spots;

/**
 * The selected spot or nil.
 */
@property (nonatomic, weak) Spot * selectedSpot;

/**
 * Shows/Hides user location.
 */
@property (nonatomic, assign) BOOL showsUserLocation;

/**
 * Changing the state also changes the UI.
 */
@property (nonatomic, assign) DSMapViewState state;

/**
 * If true, the map will center on the user location. This will ignore if any spot is selected or not.
 */
@property (nonatomic, assign) BOOL followsUserLocation;

/**
 * The content insets for where to place elements above the map.
 */
@property (nonatomic) UIEdgeInsets contentInset;

/**
 * Represents the current region of the map.
 */
@property (nonatomic, readonly) DSMapViewRegion region;

/**
 * Updates the map and info view.
 */
- (void)update;

/**
 * Centeres the map at the user's current location.
 */
- (void)centerUserLocationAnimated:(BOOL)animated;

@end

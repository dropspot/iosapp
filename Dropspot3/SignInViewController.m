/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  NativeSignInViewController.m
//  SingleCollection
//
//  Created by Lukas Würzburger on 2/13/14.
//  Copyright (c) 2014 Lukas Würzburger. All rights reserved.
//

#import "SignInViewController.h"

#import "DSGlobal.h"
#import "DSDropspot.h"
#import "DSUser.h"

#import "DSSignInTextField.h"

#import "GAIHeader.h"

#define kAlertViewTagLoginFailed        1
#define kAlertViewTagNoConnection       2
#define kAlertViewTagPleaseEnterLogin   3

@interface SignInViewController () {
    IBOutlet UIImageView * backgroundImageView;
    
    IBOutlet UIScrollView * scrollView;
    
    IBOutlet DSSignInTextField * usernameTextField;
    IBOutlet DSSignInTextField * passwordTextField;
    IBOutlet UIButton * cancelButton;
    IBOutlet UIButton * signInButton;
    IBOutlet UILabel * noAccountLabel;
    IBOutlet UIButton * signUpButton;

    IBOutlet UIView * progressOverlayView;
    IBOutlet UILabel * progressOverlayLabel;
}

@end

@implementation SignInViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    usernameTextField.placeholder = translate(@"SIGNIN_NATIVE_LOGIN_USERNAME_PLACEHOLDER");

    passwordTextField.placeholder = translate(@"SIGNIN_NATIVE_LOGIN_PASSWORD_PLACEHOLDER");

    progressOverlayLabel.text = translate(@"SIGNIN_OVERLAY_LABEL");
    
    [cancelButton setTitle:translate(@"SIGNIN_NATIVE_LOGIN_BUTTON_CANCEL") forState:UIControlStateNormal];
    
    [signInButton setTitle:translate(@"SIGNIN_NATIVE_LOGIN_BUTTON_LOGIN") forState:UIControlStateNormal];
    
    noAccountLabel.text = translate(@"SIGNIN_NATIVE_LABEL_NO_ACCOUNT");
    
    [signUpButton setTitle:translate(@"SIGNIN_NATIVE_LOGIN_BUTTON_SIGNUP") forState:UIControlStateNormal];
    
    backgroundImageView.image = self.backgroundImage;
    
    [usernameTextField becomeFirstResponder];
    
    scrollView.contentSize = CGSizeMake(scrollView.frame.size.width, signUpButton.frame.origin.y + signUpButton.frame.size.height + 20);
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[DSGlobal sharedGlobals] analyticsView:kDSScreenNameNativeSignInView];
}

- (BOOL)shouldAutorotate {
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

#pragma mark - IBActions

- (IBAction)cancelButtonPressed:(id)sender {
    if ([self.delegate respondsToSelector:@selector(signInViewControllerDidCancel:)]) {
        [self.delegate signInViewControllerDidCancel:self];
    }
}

- (IBAction)registerButtonPressed:(id)sender {
    if ([self.delegate respondsToSelector:@selector(signInViewControllerDidPressRegisterButton:)]) {
        [self.delegate signInViewControllerDidPressRegisterButton:self];
    }
}

- (IBAction)loginButtonPressed:(id)sender {
    [self nativeLogin];
}

#pragma mark - Helper

- (void)nativeLogin {
    if (usernameTextField.text && passwordTextField.text && ![usernameTextField.text isEqualToString:@""] && ![passwordTextField.text isEqualToString:@""]) {
        [self setProgressOverlayViewHidden:NO];
        [usernameTextField resignFirstResponder];

        [[DSDropspot session] openSessionWithUsername:usernameTextField.text password:passwordTextField.text block:^(DSUser *user, NSError *error) {
            [self setProgressOverlayViewHidden:YES];
            if (!error) {
                if ([self.delegate respondsToSelector:@selector(signInViewController:didLoginWithUser:)]) {
                    [self.delegate signInViewController:self didLoginWithUser:user];
                }
            } else {
                //[[DSGlobal sharedGlobals] analyticsEvent:kDSEventNameNativeLoginFailure category:kDSCategoryNameOnboarding];

                [usernameTextField becomeFirstResponder];
                if (error.code == kDSDropspotAPIErrorCodeNotAuthorized) {
                    UIAlertView * alert = [[UIAlertView alloc]
                                           initWithTitle:translate(@"SIGNIN_NATIVE_LOGIN_FAILED_ALERT_TITLE")
                                           message:error.localizedDescription
                                           delegate:nil
                                           cancelButtonTitle:translate(@"SIGNIN_NATIVE_LOGIN_FAILED_ALERT_DISMISS_BUTTON")
                                           otherButtonTitles:nil];
                    [alert show];
                } else if (error.code == kDSDropspotAPIErrorCodeNoResponse) {
                    UIAlertView * alert = [[UIAlertView alloc]
                                           initWithTitle:translate(@"SIGNIN_NATIVE_LOGIN_FAILED_NO_CONNECTION_TITLE")
                                           message:translate(@"SIGNIN_NATIVE_LOGIN_FAILED_NO_CONNECTION_MESSAGE")
                                           delegate:self
                                           cancelButtonTitle:translate(@"SIGNIN_NATIVE_LOGIN_FAILED_NO_CONNECTION_DISMISS_BUTTON_LABEL")
                                           otherButtonTitles:nil];
                    alert.tag = kAlertViewTagNoConnection;
                    [alert show];
                } else {
                    NSLog(@"error %@", error.localizedDescription);
                }
            }
        }];
    } else {
        //[[DSGlobal sharedGlobals] analyticsEvent:kDSEventNameNativeLoginFailure category:kDSCategoryNameOnboarding];

        UIAlertView * alert = [[UIAlertView alloc]
                               initWithTitle:translate(@"SIGNIN_NATIVE_LOGIN_FAILED_NO_NAME_PASS_TITLE")
                               message:translate(@"SIGNIN_NATIVE_LOGIN_FAILED_NO_NAME_PASS_MESSAGE")
                               delegate:self
                               cancelButtonTitle:translate(@"SIGNIN_NATIVE_LOGIN_FAILED_NO_NAME_PASS_DISMISS_BUTTON_LABEL")
                               otherButtonTitles:nil];
        alert.tag = kAlertViewTagPleaseEnterLogin;
        [alert show];
    }
}

- (void)setProgressOverlayViewHidden:(BOOL)hidden {
    if (hidden) {
        [UIView animateWithDuration:0.25f animations:^{
            progressOverlayView.alpha = 0.f;
        } completion:^(BOOL finished) {
            progressOverlayView.hidden = YES;
        }];
    } else {
        progressOverlayView.alpha = 0.f;
        progressOverlayView.hidden = NO;
        [UIView animateWithDuration:0.25f animations:^{
            progressOverlayView.alpha = 0.6f;
        }];
    }
}

#pragma mark - UIAlertView delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == kAlertViewTagPleaseEnterLogin) {
        [usernameTextField becomeFirstResponder];
    } else if (alertView.tag == kAlertViewTagLoginFailed) {
        [usernameTextField becomeFirstResponder];
    }
}

#pragma mark - UITextField delegate

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    [self setViewOffsetForKeyboard:YES];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == usernameTextField) {
        [passwordTextField becomeFirstResponder];
    } else if (textField == passwordTextField) {
        [passwordTextField resignFirstResponder];
        [self nativeLogin];
        
        [self setViewOffsetForKeyboard:NO];
    }
    
    return YES;
}

- (void)setViewOffsetForKeyboard:(BOOL)keyboardVisible {
    float speed = 0.25;
    
    CGRect frame = scrollView.frame;
    if (keyboardVisible) {
        if (frame.size.height == self.view.frame.size.height) {
            frame.size.height = self.view.frame.size.height - 216;
            [UIView animateWithDuration:speed animations:^{
                scrollView.frame = frame;
            }];
        }
    } else {
        if (frame.size.height < self.view.frame.size.height) {
            frame.size.height = self.view.frame.size.height;
            [UIView animateWithDuration:speed animations:^{
                scrollView.frame = frame;
            }];
        }
    }
}

@end

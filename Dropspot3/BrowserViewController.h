/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  BrowserViewController.h
//  Dropspot3
//
//  Created by Lukas Würzburger on 12/20/13.
//  Copyright (c) 2013 Dropspot. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BrowserViewController : UIViewController

@property (nonatomic, strong) NSURL * url;

@end

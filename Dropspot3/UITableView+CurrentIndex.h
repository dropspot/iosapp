/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  UITableView+CurrentIndex.h
//  Dropspot3
//
//  Created by Lukas Würzburger on 01.10.13.
//  Copyright (c) 2013 Dropspot. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITableView (CurrentIndex)

@property (nonatomic, readonly, getter=currentIndex) NSInteger currentIndex;

- (NSInteger)currentIndex;

@end

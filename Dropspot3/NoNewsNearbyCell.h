/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  NoNewsNearbyCell.h
//  Dropspot3
//
//  Created by Lukas Würzburger on 4/11/14.
//  Copyright (c) 2014 Dropspot GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NoNewsNearbyCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel * label;

@end

/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  DSDropspot.h
//  Dropspot3
//
//  Created by Lukas Würzburger on 11.09.13.
//  Copyright (c) 2013 Dropspot. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "Collection.h"

#import "DSAPICollectionResponse.h"
#import "DSAPISpotResponse.h"
#import "DSAPIProfileResponse.h"
#import "DSAPISpotLinkResponse.h"
#import "DSUser.h"

/*
 * NSNotifications
 */
#define kDSDropspotSessionChangedNotificationName @"kDSDropspotSessionChangedNotificationName"
#define kDSDropspotSessionUserChangedNotificationName @"kDSDropspotSessionUserChangedNotificationName"
#define kDSDropspotSessionUserImageChangedNotificationName @"kDSDropspotSessionUserImageChangedNotificationName"
#define kDSDropspotSpotCreatedRemoteNotificationName @"kDSDropspotSpotCreatedRemoteNotificationName"
#define kDSDropspotSpotCreatedLocalNotificationName @"kDSDropspotSpotCreatedLocalNotificationName"
#define kDSDropspotSpotEditedNotificationName @"kDSDropspotSpotEditedNotificationName"
#define kDSDropspotCollectionEditedNotificationName @"kDSDropspotCollectionEditedNotificationName"
#define kDSDropspotCollectionCreatedRemoteNotificationName @"kDSDropspotCollectionCreatedRemoteNotificationName"
#define kDSDropspotCollectionCreatedLocalNotificationName @"kDSDropspotCollectionCreatedLocalNotificationName"
#define kDSDropspotCollectionSubscriptionChanged @"kDSDropspotCollectionSubscriptionChanged"
#define kDSDropspotSpotDeletedNotificationName @"kDSDropspotSpotDeletedNotificationName"
#define kDSDropspotCollectionDeletedNotificationName @"kDSDropspotCollectionDeletedNotificationName"
#define kDSDropspotShowLoginNotificationName @"kDSDropspotShowLoginNotificationName"
#define kDSDropspotSyncingFinished @"kDSDropspotSyncingFinished"

/*
 * Keys for NSUserDefaults
 */
#define kDSDropspotSessionDomainKeyName @"com.dropspot.dropspot:DSSession"
#define kDSDropspotSessionTokenKeyName @"kDSDropspotTokenKeyName"
#define kDSDropspotSessionDateJoinedKeyName @"kDSDropspotSessionDateJoinedKeyName"
#define kDSDropspotSessionUsernameKeyName @"kDSDropspotSessionUsernameKeyName"
#define kDSDropspotSessionPrimaryAvatarURLKeyName @"kDSDropspotSessionPrimaryAvatarURLKeyName"
#define kDSDropspotSessionCollectionsURLKeyName @"kDSDropspotSessionCollectionsURLKeyName"
#define kDSDropspotSessionSpotsURLKeyName @"kDSDropspotSessionSpotsURLKeyName"
#define kDSDropspotSessionSubscriptionsURLKeyName @"kDSDropspotSessionSubscriptionsURLKeyName"
#define kDSDropspotSessionCollectionSlugNameKeyName @"kDSDropspotSessionCollectionSlugNameKeyName"
#define kDSDropspotSessionPasswordKeyName @"kDSDropspotSessionPasswordKeyName"
#define kDSDropspotSessionNameKeyName @"kDSDropspotSessionNameKeyName"
#define kDSDropspotSessionImageKeyName @"kDSDropspotSessionImageKeyName"
#define kDSDropspotSessionIsTmpUserKeyName @"kDSDropspotSessionIsTmpUserKeyName"
#define kDSDropspotSessionIsRealUserKeyName @"kDSDropspotSessionIsRealUserKeyName"
#define kDSDropspotSessionLastSyncKeyName @"kDSDropspotSessionLastSyncKeyName"
#define kDSDropspotSessionLoginTypeKeyName @"kDSDropspotSessionLoginTypeKeyName"

#define kDSDropspotAPIErrorDomainName @"com.dropspot.error:APIError"

/*
 * API Error codes
 */
#define kDSDropspotAPIErrorCodeWrongResponse 1
#define kDSDropspotAPIErrorDescriptionWrongResponse @"No valid response code"

#define kDSDropspotAPIErrorCodeNoSessionToken 2
#define kDSDropspotAPIErrorDescriptionNoSessionToken @"No valid session token"

#define kDSDropspotAPIErrorCodeNoValidResult 3
#define kDSDropspotAPIErrorDescriptionNoValidResult @"No valid result"

#define kDSDropspotAPIErrorCodeNotAuthorized 4
#define kDSDropspotAPIErrorDescriptionNotAuthorized @"Not authorized"

#define kDSDropspotAPIErrorCodeSignout 4
#define kDSDropspotAPIErrorDescriptionSignout @"Manually signed out"

#define kDSDropspotAPIErrorCodeRegistrationFailed 5
#define kDSDropspotAPIErrorDescriptionRegistrationFailed @"Registration failed due to validation."

#define kDSDropspotAPIErrorCodeNoResponse 6
#define kDSDropspotAPIErrorDescriptionNoResponse @"You may not have internet connection."

/*
 * Session states
 */
typedef enum {
    DSDropspotSessionStateOpen,
    DSDropspotSessionStateClose,
    DSDropspotSessionStateFailed,
    DSDropspotSessionStateLoginFailed
} DSDropspotSessionState;

/*
 * Response mapping
 */

#define DRPSPT_COLLECTION_URL @"url"
#define DRPSPT_COLLECTION_SLUG @"slug"
#define DRPSPT_COLLECTION_TITLE @"name"
#define DRPSPT_COLLECTION_DESCRIPTION @"description"
#define DRPSPT_COLLECTION_PHOTO_URL @"photo"
#define DRPSPT_COLLECTION_PHOTO_URL_KEYPATH @"photo.small"
#define DRPSPT_COLLECTION_PHOTO_IPHONE_URL_KEYPATH @"photo.iphone"
#define DRPSPT_COLLECTION_SUBSCRIBE_URL @"subscribe_url"
#define DRPSPT_COLLECTION_SPOTS @"spots"
#define DRPSPT_COLLECTION_MODIFIED @"modified"
#define DRPSPT_COLLECTION_MODIFIED_SPOTS @"modified_spots"
#define DRPSPT_COLLECTION_CREATED @"created"
#define DRPSPT_COLLECTION_PUBLISHED @"published"
#define DRPSPT_COLLECTION_CREATOR_NAME_KEYPATH @"created_by.name"
#define DRPSPT_COLLECTION_CREATOR_USERNAME_KEYPATH @"created_by.username"
#define DRPSPT_COLLECTION_CREATOR_URL_KEYPATH @"created_by.url"
#define DRPSPT_COLLECTION_CREATOR_IMAGE_KEYPATH @"created_by.primary_avatar"

#define DRPSPT_SUBSCRIPTIONS @"subscriptions"
#define DRPSPT_SUBSCRIPTION_MODIFIED @"modified"
#define DRPSPT_SUBSCRIPTION_CREATED @"created"
#define DRPSPT_SUBSCRIPTION_CONTENT_MODIFIED_KEYPATH @"content_object.modified"
#define DRPSPT_SUBSCRIPTION_CONTENT_CREATED_KEYPATH @"content_object.created"
#define DRPSPT_SUBSCRIPTION_CONTENT_SLUG_KEYPATH @"content_object.slug"
#define DRPSPT_SUBSCRIPTION_CONTENT_URL_KEYPATH @"content_object.url"
#define DRPSPT_SUBSCRIPTION_CONTENT_TITLE_KEYPATH @"content_object.name"
#define DRPSPT_SUBSCRIPTION_CONTENT_CREATOR_NAME_KEYPATH @"content_object.created_by.name"

#define DRPSPT_SPOT_URL @"url"
#define DRPSPT_SPOT_LINKS_URL_KEYPATH @"content_links.url"
#define DRPSPT_SPOT_LINKS_KEYPATH @"content_links.links"
#define DRPSPT_SPOT_SLUG @"slug"
#define DRPSPT_SPOT_TEXT @"name"
#define DRPSPT_SPOT_ADDRESS @"address"
#define DRPSPT_SPOT_PHOTO_URL @"photo"
#define DRPSPT_SPOT_PHOTO_SMALL_URL_KEYPATH @"photo.small"
#define DRPSPT_SPOT_PHOTO_IPHONE_URL_KEYPATH @"photo.iphone"
#define DRPSPT_SPOT_PHOTO_TINY_URL_KEYPATH @"photo.tiny"
#define DRPSPT_SPOT_RADIUS @"radius"
#define DRPSPT_SPOT_LATITUDE @"latitude"
#define DRPSPT_SPOT_LONGITUDE @"longitude"
#define DRPSPT_SPOT_MODIFIED @"modified"
#define DRPSPT_SPOT_EXPIRES_AT @"expiration_date"
#define DRPSPT_SPOT_CREATED @"created"
#define DRPSPT_SPOT_COLLECTION_SLUGS @"collection_slugs"
#define DRPSPT_SPOT_CREATOR_IMAGE_KEYPATH @"created_by.primary_avatar"
#define DRPSPT_SPOT_CREATOR_NAME_KEYPATH @"created_by.name"
#define DRPSPT_SPOT_CREATOR_USERNAME_KEYPATH @"created_by.username"
#define DRPSPT_SPOT_CREATOR_URL_KEYPATH @"created_by.url"

#define DRPSPT_SPOTLINK_ID @"id"
#define DRPSPT_SPOTLINK_URL @"url"
#define DRPSPT_SPOTLINK_LINK @"link"
#define DRPSPT_SPOTLINK_TITLE @"title"
#define DRPSPT_SPOTLINK_IMAGE_URL @"picture_link"
#define DRPSPT_SPOTLINK_ICON_URL @"favicon_url"
#define DRPSPT_SPOTLINK_MODIFIED @"modified"

#define DRPSPT_USER_URL @"url"
#define DRPSPT_USER_NAME @"name"
#define DRPSPT_USER_USERNAME @"username"
#define DRPSPT_USER_JOINED @"date_joined"
#define DRPSPT_USER_COLLECTIONS_ARRAY @"collections"
#define DRPSPT_USER_SPOTS_ARRAY @"spots"
#define DRPSPT_USER_SUBSCRIPTIONS_ARRAY @"subscriptions"

#define DRPSPT_USER_CREATE_EMAIL @"email"
#define DRPSPT_USER_CREATE_USERNAME @"username"
#define DRPSPT_USER_CREATE_PASSWORD @"password"

#define DRPSPT_DATE_FORMAT @"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"

/*
 * Creation Mapping
 */
#define DRPSPT_CREATE_COLLECTION_SLUG @"slug"
#define DRPSPT_CREATE_COLLECTION_TITLE @"name"
#define DRPSPT_CREATE_COLLECTION_DESCRIPTION @"description"
#define DRPSPT_CREATE_COLLECTION_PHOTO @"image"
#define DRPSPT_CREATE_COLLECTION_SPOTS @"spots"
#define DRPSPT_CREATE_COLLECTION_MODIFIED @"modified"
#define DRPSPT_CREATE_COLLECTION_CREATED @"created"

#define DRPSPT_CREATE_SPOT_SLUG @"slug"
#define DRPSPT_CREATE_SPOT_TEXT @"text"
#define DRPSPT_CREATE_SPOT_LINK @"link"
#define DRPSPT_CREATE_SPOT_ADDRESS @"address"
#define DRPSPT_CREATE_SPOT_PHOTO @"image"
#define DRPSPT_CREATE_SPOT_LATITUDE @"latitude"
#define DRPSPT_CREATE_SPOT_LONGITUDE @"longitude"
#define DRPSPT_CREATE_SPOT_MODIFIED @"modified"
#define DRPSPT_CREATE_SPOT_CREATED @"created"
#define DRPSPT_CREATE_SPOT_COLLECTION_SLUGS @"collection_slugs"

/**
 * Formatting static functions
 */
static NSDate * DSDateFromString(NSString * string) {
    NSDateFormatter * dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:DRPSPT_DATE_FORMAT];
    return [dateFormatter dateFromString:string];
}

static NSString * DSShortHostFromURL(NSURL * url) {
    if (url) {
        return [[url.host uppercaseString] stringByReplacingOccurrencesOfString:@"WWW." withString:@""];
    } else {
        return nil;
    }
}

/*
 * Blocks
 */
typedef void (^DSDictionaryResultBlock)(NSDictionary * result, NSError * error);
typedef void (^DSUserResultBlock)(DSUser * user, NSError * error);
typedef void (^DSCollectionResultBlock)(DSAPICollectionResponse * collectionResponse);
typedef void (^DSSpotResultBlock)(DSAPISpotResponse * spotResponse);
typedef void (^DSCollectionResultAndErrorBlock)(DSAPICollectionResponse * collection, NSError * error);
typedef void (^DSSpotResultAndErrorBlock)(DSAPISpotResponse * spot, NSError * error);
typedef void (^DSProfileResultBlock)(DSAPIProfileResponse * profile, NSError * error);
typedef void (^DSLinksResultBlock)(DSAPISpotLinkResponse * linkResponse);

typedef enum {
    DSPhotoStatusNoChange,
    DSPhotoStatusNewImage,
    DSPhotoStatusRemoveImage
} DSPhotoStatus;

@class DSDropspot;

@protocol DSDropspotDelegate <NSObject>

@required
- (void)dropspotSessionChangedState:(DSDropspot *)dropspot state:(DSDropspotSessionState)state error:(NSError *)error;

@end

@interface DSDropspot : NSObject

@property (nonatomic, assign) id<DSDropspotDelegate> delegate;
@property (nonatomic, assign) DSDropspotSessionState state;
@property (nonatomic, strong) NSDictionary * user;

+ (DSDropspot *)session;

// Session
- (void)checkSession;
- (void)openSessionWithUsername:(NSString *)username password:(NSString *)password block:(DSUserResultBlock)block;
- (void)openSessionWithFacebookToken:(NSString *)token block:(DSUserResultBlock)block;
- (void)closeSession;

// SignUp
- (void)registerNewUserWithEmail:(NSString *)email username:(NSString *)username password:(NSString *)password block:(DSDictionaryResultBlock)block;

// Get data
- (void)profileWithBlock:(DSProfileResultBlock)block;

- (void)collectionsWithURL:(NSString *)urlString paginatedWithBlock:(DSCollectionResultBlock)block completion:(void (^)(NSError * error))completion;
- (void)collectionWithURL:(NSString *)urlString success:(DSCollectionResultBlock)block completion:(void (^)(NSError * error))completion;
- (void)collectionWithSlug:(NSString *)slug block:(DSDictionaryResultBlock)block;
- (void)collectionsBySearch:(NSString *)search block:(DSDictionaryResultBlock)block;
- (void)allCollectionsPaginatedWithBlock:(DSCollectionResultBlock)block completion:(void (^)(NSError * error))completion;

- (void)subscriptionsWithURL:(NSString *)urlString paginatedWithBlock:(DSCollectionResultBlock)block completion:(void (^)(NSError * error))completion;

- (void)spotsWithURL:(NSString *)urlString paginatedWithBlock:(DSSpotResultBlock)block completion:(void (^)(NSError * error))completion;

- (void)linksWithURL:(NSString *)urlString paginatedWithBlock:(DSLinksResultBlock)block completion:(void (^)(NSError * error))completion;

// Subscription
- (void)subscribeToCollectionWithSlug:(NSString *)slug block:(DSDictionaryResultBlock)block;
- (void)unsubscribeFromCollectionWithSlug:(NSString *)slug block:(DSDictionaryResultBlock)block;

// Create content
- (void)createCollection:(Collection *)collection block:(DSCollectionResultAndErrorBlock)block;
- (void)createSpot:(Spot *)spot block:(DSSpotResultAndErrorBlock)block;
- (void)editSpot:(Spot *)spot imageStatus:(DSPhotoStatus)status block:(DSSpotResultAndErrorBlock)block;
- (void)editCollection:(Collection *)collection imageStatus:(DSPhotoStatus)status block:(DSCollectionResultAndErrorBlock)block;

@end




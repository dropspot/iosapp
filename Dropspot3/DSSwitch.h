/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  DSSwitch.h
//  Dropspot
//
//  Created by Lukas Würzburger on 19.07.13.
//  Copyright (c) 2013 Lukas Würzburger. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DSSwitch : UIView

@property (strong, nonatomic) UIColor * tintColor;
@property (assign, nonatomic) BOOL on;

- (void)addTarget:(id)target selector:(SEL)selector;
- (void)setOn:(BOOL)on animated:(BOOL)animated;

@end

/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  DSLog.h
//  Dropspot3
//
//  Created by Lukas Würzburger on 4/2/14.
//  Copyright (c) 2014 Dropspot GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <Parse/Parse.h>

@interface DSLog : NSObject

+ (instancetype)sharedLog;

- (void)logWithUser:(NSString *)username event:(NSString *)event;
- (void)logWithUser:(NSString *)username event:(NSString *)event spot:(NSString *)spot;
- (void)logWithUser:(NSString *)username event:(NSString *)event comment:(NSString *)comment;
- (void)logWithUser:(NSString *)username event:(NSString *)event spot:(NSString *)spot comment:(NSString *)comment;

@end

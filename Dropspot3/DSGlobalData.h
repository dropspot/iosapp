/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/iosapp/src/master/LICENSE)
 */

//
//  DSGlobalData.h
//  Dropspot3
//
//  Created by Lukas Würzburger on 10/28/13.
//  Copyright (c) 2013 Dropspot. All rights reserved.
//

#import <Foundation/Foundation.h>

static const int DSDownloadErrorKeyFailed = 1;
static const int DSDownloadErrorKeyItemAlreadyInStack = 2;

typedef void(^SyncingCompletion)(BOOL success, NSError * error);

@interface DSGlobalData : NSObject

@property (readonly, strong, nonatomic) NSManagedObjectContext * parentContext;
@property (readonly, strong, nonatomic) NSManagedObjectContext * childContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel * managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator * persistentStoreCoordinator;

+ (DSGlobalData *)sharedData;

- (void)downloadStuffFromURL:(NSURL *)url withCompletion:(void (^)(NSData *data, NSError *error))completion;

- (void)loadFileFromURL:(NSURL *)url
     filePathForStorage:(NSString *)filePath
            oldFilePath:(NSString *)oldFilePath
             completion:(void (^)(BOOL success))completion;

- (BOOL)isDownloadingObjectWithURL:(NSString *)urlString;

- (void)deleteFile:(NSString *)filePath;

- (void)saveContext;

- (void)synchronizeWithCompletion:(SyncingCompletion)completion;

@end

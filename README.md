# README #

### Description ###

The Dropspot App delivers stories right where they happened. In combination with [Dropspot API](https://https://bitbucket.org/dropspot/api/), [Dropspot Dashboard](https://https://bitbucket.org/dropspot/dropspot-project/), [Dropspot Widget](https://https://bitbucket.org/dropspot/widget/) and [Dropspot Widget (mobile)](https://https://bitbucket.org/dropspot/mobile-widget/) it provides publisher with a powerful platform for delivering location-based news.

### Features at glance ###

The app

* loads geo-tagged content (points of interest) from [Dropspot API](https://https://bitbucket.org/dropspot/api/) 
* uses geofencing for notifying users about proximity (~300m) to certain points of interest
* displays content associated with the points of interest
* updates its points of interest in background (even when terminated)

### System requirements ###

* iOS 7.0 or later
* Running [Dropspot API](https://https://bitbucket.org/dropspot/api/)

### Set up ###

* in `Dropspot/Dropspot3/<build>-Info.plist` set `DSBaseURL` to point to Dropspot API (search for the DSBaseURL key)
* in `Dropspot/Dropspot3/<build>-Info.plist` set `FacebookAppID` to id of the Facebook App that will be used for authenticating users. It must be the same Facebook App the Dropspot API is using.
* If you want to show only collections created by a certain user, in `Dropspot/Dropspot3/<build>-Info.plist` set the `DSPreselectedAuthor` to the username of the author.

### Contribution guidelines ###

Feel free to create a new Pull request if you want to propose a new feature. 

### Who do I talk to? ###

* If you need development support contact us at admin@drpspt.com


### License ###

MIT License. Copyright 2014 Dropspot GmbH. https://bitbucket.org/dropspot/iosapp](https://bitbucket.org/dropspot/iosapp)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
